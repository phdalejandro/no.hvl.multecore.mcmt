# MultEcore MCMT

![MultEcore](https://raw.githubusercontent.com/femaciasg/multecore-resources/master/images/logos/multecore-horizontal.png)

MultEcore is a metamodelling tool which brings together the best of
the worlds of fixed-level and multilevel metamodelling approaches.

The tool consists of two parts, created as plugins for the Eclipse
Modelling Framework (EMF). The main set of plugins provide an editor
based in Sirius. It combines the tool support, modelling ecosystem and
low learning curve of EMF with the unlimited number of abstraction
levels, potencies and flexible typing of multilevel metamodelling. A
second part of the tool allows for the specification of Multilevel
Coupled Model Transformations that exploit the multilevel capabilities
of the framework for model manipulation.

MultEcore is developed at the [ICT Engineering
Department](http://ict.hvl.no/) of the [Western Norway University of
Applied Sciences](https://www.hvl.no/en/). It has also been used in a
[joint work with University of
Lübeck](http://www.isp.uni-luebeck.de/rv+mm) and a [joint work with
Universidad Autónoma de
Madrid](http://ict.hvl.no/mlm-rearchitect-results/), which led to the
creation of a tool for MLM rearchitecting and MLM tool exchange, available
[on
Github](https://github.com/MLM-Rearchitect-Exchange/mlm-rearchitecter-tool).


# Installation and usage

## Main tool

Resources and tutorials for the main part of the tool are
available in the [MultEcore
website](https://ict.hvl.no/multecore-tutorials/) and in the [MultEcore repository](https://bitbucket.org/phd-fernando/no.hvl.multecore).


## MCMT tool

### Installation

**DSL PART, UNDER CONSTRUCTION**

Textual DSL for the specification of MCMTs, part of MultEcore.

Installation steps:

1) Install Eclipse Modelling Neon or newer
2) Install the first two packages under the "Xtext" group, using the
   update site and the instructions that they provide at:
   https://eclipse.org/Xtext/download.html
3) Clone the repository, and move the project called "no.hvl.multecore.mcmt.dsl" to
   a separate directory.
4) After restarting Eclipse, select File > New > Project... > Xtext >
   Xtext Project > Next.
5) Fill the following values in the project configuration:
   Project name: no.hvl.multecore.mcmt.dsl
   Language > Name: no.hvl.multecore.mcmt.dsl.MultEcoreDSL
   Language > Extensions: mcmt
6) Click Finish. Five new projects should appear. Delete the contents
   of the src folder (not the folder itself) from the first project
   (no.hvl.multecore.mcmt.dsl), and move back the project "no.hvl.multecore.mcmt.dsl" from
   the directory chosen in step 3. Replace all the content. 
7) Add the dependencies to the projects "org.eclipse.core.resources",
   "no.hvl.multecore.common", "no.hvl.multecore.core", 
   "no.hvl.multecore.mcmt.proliferation" and
   "no.hvl.multecore.mcmt.maude" in META-INF > MANIFEST.MF >
   Dependencies tab > Required Plug-ins > Add... > Select the
   projects > OK > Save the file. Alternatively, they can be added in
   the text under the "Require-Bundle" section.
7) Refresh the project. Then, go to src > no.hvl.multecore.mcmt.dsl >
   MultEcoreDSL.xtext > Right click > Run As > Generate Xtext
   Artifacts
8) Run the project as an Eclipse Application. In case there is some
   error with Java, go to run configurations and then click "Run".
9) Create a Java project and a file inside with ".mcmt" extension,
   and accept the change of nature of the project to Xtext.


### Working with MCMTs

**UNDER CONSTRUCTION**


### Importing an existing MLM project

1. Import it normally using the Eclipse wizard.
2. Activate MLM.

See [Resources](#resources) for example projects with MCMTs.


### Demo and tutorial

**UNDER CONSTRUCTION**


### Troubleshooting/FAQs

**UNDER CONSTRUCTION**


# Source

The source code of the MCMT part of the tool can be
found, together with some examples, [on
Bitbucket](https://bitbucket.org/account/user/phdalejandro/projects/MCMT).

The plugins which conform the MCMT part of the tool are three:

- **dsl**: This package contains the grammar specification of the textual DSL for the specification of MCMTs.
- **proliferation**: It contains the classes that translate the rules specified with the textual editor to objects.
                      Such objects are used to later create a complete Maude specification taking into account both the multilevel hierarchies and the MCMTs.
                     Originally, this part of the plugin was aimed to the proliferation process to generate two-level rules that could be used in traditional
                     engines for model transformation.
- **maude**: This package is aimed to encapsulate the logic for the transformation of MultEcore to Maude 
            (which is given by a ".maude" file containing the complete specification that can be directly executed.
            It also contains the files that make possible the transformation from the XML file provided by Maude as result of a new state of the multilevel hierarchy
            to MultEcore, that it is automatically parsed and integrated in the visual editor of MultEcore.

The stable versions of the plugins are also available as [JAR
files](https://github.com/femaciasg/multecore-resources/blob/master/plugins/no.hvl.multecore.mcmt.zip?raw=true)
which can be directly added to EMF.


# Publications

## PhD Theses

- Fernando Macias. **Multilevel Modelling and Domain-Specific
  Languages**. PhD thesis submitted to the Faculty of
  Mathematics and Natural Sciences. No. 2141. ISSN 1501-7710,
  University of Oslo,
  June 2019. [[arXiv]](https://arxiv.org/abs/1910.03313)

## Journals

- Fernando Macias, Uwe Wolter, Adrian Rutle, Francisco Duran, and
  Roberto Rodriguez-Echeverria. **Multilevel Coupled Model
  Transformations for Precise and Reusable Definition of Model
  Behaviour**. *J.LAMP*, 106:167– 195,
  August 2019. [[arXiv]](https://arxiv.org/abs/1901.05754)
- Alejandro Rodríguez, Francisco Durán, Adrian Rutle and Lars Michael Kristensen,
  **Executing Multilevel Domain-Specific Models in Maude**, Journal of Object Technology,
  Volume 18, no. 2 (July 2019), pp. 4:1-21, doi:10.5381/jot.2019.18.2.a4 [[JOT]](http://www.jot.fm/contents/issue_2019_02/article4.html)
- Fernando Macias, Adrian Rutle, Volker Stolz, Roberto
  Rodriguez-Echeverria, and Uwe Wolter. **An Approach to Flexible
  Multilevel Modelling**. Enterprise Modelling and Information Systems
  Architectures, 13:10:1– 10:35, 2018. [[EMISAJ]](https://emisa-journal.org/emisa/article/view/145)

## Workshops

- Alejandro Rodriguez and Fernando Macias. **Multilevel Modelling with
  MultEcore: A contribution to the MULTI Process challenge**. In *6th
  International Workshop on Multi-Level Modelling (MULTI2019)*, pages
  152–163, 2019. [[IEEE Xplore)]](https://ieeexplore.ieee.org/document/8904589)
- Alejandro Rodriguez, Adrian Rutle, Lars Michael Kristensen and Francisco Duran. **A Foundation for the Composition of Multilevel Domain-Specific Languages**.
  In *6th International Workshop on Multi-Level Modelling (MULTI2019)*, pages
  88-97, 2019. [[IEEE Xplore]](https://ieeexplore.ieee.org/document/8904851)  
- Fernando Macias, Volker Stolz, Torben Scheffel, Malte Schmitz, and Adrian Rutle. **Empowering Multilevel DSMLs with Integrated Runtime Verification**. In *3rd International Workshop on Verification of Objects at Runtime Execution (VORTEX 2019)*, *To appear on EPTCS*, 2019. [[EPTCS (to appear)]](http://published.eptcs.org/)
- Alejandro Rodriguez, Adrian Rutle, Francisco Duran, Lars Michael
  Kristensen, and Fernando Macias. **Multilevel modelling of coloured
  Petri nets**. In *5th International Workshop on Multi-Level
  Modelling (MULTI2018)*, volume 2245 of CEUR Workshop Proceedings,
  pages
  663–672, 2018. [[CEUR-WS]](http://ceur-ws.org/Vol-2245/multi_paper_4.pdf)
- Fernando Macias, Adrian Rutle, and Volker Stolz. **Multilevel
  Modelling with MultEcore: A Contribution to the MULTI 2017
  Challenge**. In *4th International Workshop on Multi-Level Modelling
  (MULTI2017)*, volume 2019 of *CEUR Workshop
  Proceedings*, 2017. [[CEUR-WS]](http://ceur-ws.org/Vol-2019/multi_9.pdf)
- Fernando Macias, Adrian Rutle, and Volker Stolz. **MultEcore:
  Combining the best of fixed-level and multilevel metamodelling**. In
  *3rd International Workshop on Multi-Level Modelling (MULTI2016)*,
  volume 1722 of *CEUR Workshop
  Proceedings*, 2016. [[CEUR-WS]](http://ceur-ws.org/Vol-1722/p6.pdf)
- Fernando Macias, Torben Scheffel, Malte Schmitz, and Rui
  Wang. **Integration of Runtime Verification into Metamodeling for
  Simulation and Code Generation (Position Paper)**. In *16th
  International Conference in Runtime Verification (RV 2016)*, pages
  454–461, 2016. [[Springer]](https://link.springer.com/chapter/10.1007/978-3-319-46982-9_29)
  
## Technical reports

- Uwe Wolter, Fernando Macias, and Adrian Rutle. **On the Category of
  Graph Chains and Graph Chain Morphisms**. Technical Report 2017-416,
  University of Bergen, Department of Informatics, January 2018. [[UiB]](http://www.ii.uib.no/publikasjoner/texrap/pdf/2018-416.pdf)


# Resources

- The MultEcore website. [[Website]](https://ict.hvl.no/multecore/)
- The plugin files for the tool. [[Main]](https://github.com/femaciasg/multecore-resources/blob/master/plugins/no.hvl.multecore.zip?raw=true) [[MCMT]](https://github.com/femaciasg/multecore-resources/blob/master/plugins/no.hvl.multecore.mcmt.zip?raw=true)
- The repositories with the source code. [[Main]](https://bitbucket.org/phd-fernando/no.hvl.multecore) [[Main -
  stable]](https://github.com/femaciasg/multecore) [[MCMT]](https://bitbucket.org/phdalejandro/no.hvl.multecore.mcmt)
- The video tutorial that demonstrates the common way of using the main
  part of the
  tool. [[Youtube]](https://www.youtube.com/watch?v=J9onDlPQDbI)
- The project with the multilevel hierarchy created in the video
  demo. [[ZIP]](https://github.com/femaciasg/multecore-resources/blob/master/examples/no.hvl.multecore.examples.tutorial.zip?raw=true)
- The project with the multilevel hierarchy for the PLS
  example. [[ZIP]](https://github.com/femaciasg/multecore-resources/blob/master/examples/no.hvl.multecore.examples.pls.zip?raw=true)
- Repositories with more example projects with multilevel hierarchies. [[Bitbucket]](https://bitbucket.org/account/user/phd-fernando/projects/MUL)
- SVG files containing the source of the big commutative diagrams in
the PhD thesis "Multilevel Modelling and Domain-Specific Languages"
(see [Publications](#publications)),
with layers that can be disabled to ease readability. [[ZIP]](https://github.com/femaciasg/multecore-resources/blob/master/images/commutative-diagrams-detail.zip?raw=true)


# License

MultEcore is offered under the GNU General Public License, version 3.


# Contact

Fernando Macias – <http://fernandomacias.es>

Alejandro Rodriguez - <arte@hvl.no>

Adrian Rutle – <adrian.rutle@hvl.no>

Volker Stolz – <volker.stolz@hvl.no>
