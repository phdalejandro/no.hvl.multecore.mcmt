package no.hvl.multecore.mcmt.dsl.generator

import java.util.HashMap
import java.util.HashSet
import java.util.Set
import no.hvl.multecore.common.MultEcoreManager
import no.hvl.multecore.common.exceptions.MultEcoreException
import no.hvl.multecore.common.hierarchy.Edge
import no.hvl.multecore.common.hierarchy.IEdge
import no.hvl.multecore.common.hierarchy.IModel
import no.hvl.multecore.common.hierarchy.INode
import no.hvl.multecore.common.hierarchy.MultilevelHierarchy
import no.hvl.multecore.common.hierarchy.Node
import no.hvl.multecore.core.Utils
import no.hvl.multecore.mcmt.amalgamation.AmalgamationManager
import no.hvl.multecore.mcmt.amalgamation.McmtModuleHierarchyRules
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.AssignmentExp
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.Block
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.Box
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.ConditionalBlock
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.InhExpr
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.InstantiatedAttributeExp
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.MetaAssignmentExp
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.MetaConstantEdge
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.MetaConstantEdgeDec
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.MetaConstantNode
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.MetaConstantNodeDec
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.MetaDeclaredAttributeDec
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.MetaElementDec
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.MetaElementNode
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.MetaVariableEdge
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.MetaVariableEdgeDec
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.MetaVariableNode
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.MetaVariableNodeDec
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.Module
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.Rule
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.StringExpression
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.Variable
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.VariableExp
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.VariableMultiplicity
import no.hvl.multecore.mcmt.maude.MaudeManager
import no.hvl.multecore.mcmt.proliferation.Constants
import no.hvl.multecore.mcmt.proliferation.Flattener
import no.hvl.multecore.mcmt.proliferation.Matcher
import no.hvl.multecore.mcmt.proliferation.rule.NodeConstant
import no.hvl.multecore.mcmt.proliferation.rule.RuleHierarchy
import no.hvl.multecore.mcmt.proliferation.rule.RuleModel
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import no.hvl.multecore.mcmt.dsl.multEcoreDSL.MetaBlock

class MultEcoreDSLGenerator extends AbstractGenerator {
	
	var HashMap<String, RuleHierarchy> parsedRules
	var HashMap<MetaVariableNode, MetaVariableNodeDec> metaVariableNodes
	var HashMap<MetaVariableEdge, MetaVariableEdgeDec> metaVariableEdges
	var HashMap<MetaConstantNode, MetaConstantNodeDec> constantNodes
	var HashMap<MetaConstantEdge, MetaConstantEdgeDec> constantEdges
	var HashMap<String, Set<String>> inheritanceRelations
	var HashMap<Variable, VariableExp> variables
	var HashMap<String, MultilevelHierarchy> hierarchyAliases
	var	String applicationHierarchyAlias

	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		parsedRules = new HashMap<String, RuleHierarchy>
		metaVariableNodes = new HashMap<MetaVariableNode, MetaVariableNodeDec>
		metaVariableEdges = new HashMap<MetaVariableEdge, MetaVariableEdgeDec>
		constantNodes = new HashMap<MetaConstantNode, MetaConstantNodeDec>
		constantEdges = new HashMap<MetaConstantEdge, MetaConstantEdgeDec>
		inheritanceRelations = new HashMap<String, Set<String>>
		variables = new HashMap<Variable, VariableExp>
		hierarchyAliases = new HashMap<String, MultilevelHierarchy>
		var module = resource.allContents.head as Module
		
		// Check if target hierarchy exists
		val multilevelHierarchyName = module?.hierarchy.name
		val multilevelHierarchy = MultEcoreManager.instance.getMultilevelHierarchy(multilevelHierarchyName)
		if (null===multilevelHierarchy) {
			no.hvl.multecore.common.Utils.showPopup('Target hierarchy not found',
					'Could not find the multilevel hierarchy \"' + multilevelHierarchyName +
					'\". Make sure MultEcore is enabled in the corresponding project.')
			return;
		}
		applicationHierarchyAlias = module?.hierarchy.aliasHierarchy.name
		hierarchyAliases.put(applicationHierarchyAlias, multilevelHierarchy)
		
		
		val suppHierarchiesNames = newHashSet
		//Check if supplementary hierarchies exist  and are part of the application hierarchy
		for (hierarchy : module?.suppHierarchy) {
			val suppHierarchy = multilevelHierarchy.getSupplementaryHierarchy(hierarchy.name)
			if (suppHierarchy === null) {
				no.hvl.multecore.common.Utils.showPopup('Target  supplementary hierarchy not found',
					'Could not find the supplementary multilevel hierarchy \"' + hierarchy.name +
						'\". Make sure MultEcore is enabled in the corresponding project and is added as supplementary hierarchy in' + multilevelHierarchyName + '.' )
			}
			else{
				suppHierarchiesNames.add(hierarchy.aliasHierarchy.name)
				hierarchyAliases.put(hierarchy.aliasHierarchy.name, suppHierarchy)
			}
			
		}
		
		
		// Check if target model exists
		val modelName = module?.model.name
		val model = multilevelHierarchy.getModelByName(modelName)
		
		if (null===model) {
			no.hvl.multecore.common.Utils.showPopup('Target model not found', 
					'Could not find the model \"' + modelName +
					'\" in hierarchy \"' + multilevelHierarchyName + '\".')			
			return;
		}
		
		val locationURI = resource.URI.trimSegments(1)
		
		// Generate flattened (2-level) hierarchy
		Flattener.flatten(model, locationURI);
		
		// Parse rules
		val moduleName = module?.name
		for (rule : module?.rules) {
            rule.parse(moduleName, multilevelHierarchy, suppHierarchiesNames)
        }
        MaudeManager.instance().setRules(parsedRules)
        
        val mcmt = new McmtModuleHierarchyRules ();
        mcmt.moduleName = moduleName
        mcmt.multilevelHierarchy = multilevelHierarchy
        mcmt.setHierarchyRules(parsedRules)
        
        //TODO Workaround until I find why contains does not invoke the equals method of McmtModuleHierarchyRules 
        var found = false
        for (mcmtModuleHierachyRules : AmalgamationManager.instance().mcmtModuleHierarchyRules) {
        	if (mcmtModuleHierachyRules.moduleName.equals(mcmt.moduleName) && 
        		mcmtModuleHierachyRules.multilevelHierarchy.name.equals(mcmt.multilevelHierarchy.name)){
        			mcmtModuleHierachyRules.hierarchyRules.clear
        			mcmtModuleHierachyRules.hierarchyRules.addAll(mcmt.hierarchyRules)
        			found = true
        		}
        }
        if (!found){
        	AmalgamationManager.instance().setMcmtModuleHierarchyRules(mcmt)
        }
        
        
//        if (!AmalgamationManager.instance().mcmtModuleHierarchyRules.contains(mcmt)){
//        	AmalgamationManager.instance().setMcmtModuleHierarchyRules(mcmt)
//        }

        Utils.addtoMultEcoreConsole(parsedRules.values().join('\n\n'))
	}
	
	
	protected def parse(Rule r, String moduleName, MultilevelHierarchy multilevelHierarchy, HashSet<String> suppHierarchiesAliases) {
		val ruleName = r.name
		var ruleHierarchy = new RuleHierarchy(ruleName)
		parsedRules.put(moduleName + Constants.MODULE_NAME_SEPARATOR + ruleName, ruleHierarchy)
		for (suppHierarchyNameAlias : suppHierarchiesAliases) {
			var suppRuleHierarchy = new RuleHierarchy(suppHierarchyNameAlias)
			ruleHierarchy.addSupplementaryHierarchy(suppRuleHierarchy)
			//TODO Run over the elements and properly distribute into the correct meta_x metamodel
		}
		val metaElements = r.meta.elements
		
		// Parse META
		// Parse node constants in META
		for (mcn : metaElements.filter(MetaConstantNodeDec)) {
			try {
				mcn.parse(getRuleHierarchy(mcn, ruleHierarchy, moduleName))
			} catch (MultEcoreException e) {
				e.createNotificationDialog
			}
		}
		// Parse node variables in META
		for (mvn : metaElements.filter(MetaVariableNodeDec)) {
			try {
				mvn.parse(getRuleHierarchy(mvn, ruleHierarchy, moduleName))
			} catch (MultEcoreException e) {
				e.createNotificationDialog
			}
		}
		
		
		//Parse inheritances
		for (inh : r.meta.inheritances){
			try{
				inh.parse(r.meta.elements.filter(MetaConstantNodeDec),ruleHierarchy)
			} catch (MultEcoreException e){
				e.createNotificationDialog
			}
		}
		
		//Parse declared attributes
		for (mvn : metaElements.filter(MetaDeclaredAttributeDec)) {
			try {
				mvn.parse(getRuleHierarchy(mvn, ruleHierarchy, moduleName))
			} catch (MultEcoreException e) {
				e.createNotificationDialog
			}
		}
		
		// Parse edge constants in META
		for (mce : metaElements.filter(MetaConstantEdgeDec)) {
			try {
				mce.parse(getRuleHierarchy(mce, ruleHierarchy, moduleName))
			} catch (MultEcoreException e) {
				e.createNotificationDialog
			}
		}
		// Parse edge variables in META
		for (mve : metaElements.filter(MetaVariableEdgeDec)) {
			try {
				mve.parse(r.meta.exprs.filter(MetaAssignmentExp), getRuleHierarchy(mve, ruleHierarchy, moduleName))
			} catch (MultEcoreException e) {
				e.createNotificationDialog
			}
		}
		
		
		val bottomMetaModel = ruleHierarchy.getMetaModelInLevel(ruleHierarchy.maxLevel)
		
		// Parse FROM
		val fromModel = ruleHierarchy.createModel(Constants.FROM_BLOCK_ID, bottomMetaModel.name)
		
		for (suppRuleHierarchy : ruleHierarchy.supplementaryHierarchies) {
			val bottomMostSuppMetamodel = (suppRuleHierarchy as RuleHierarchy).getMetaModelInLevel(suppRuleHierarchy.maxLevel)
			ruleHierarchy.addSupplementaryModel(fromModel, (suppRuleHierarchy as RuleHierarchy).name, bottomMostSuppMetamodel.name)
		}
		
		for (ve : r.from.varExprs) {
			try {
				ve.parse(r.from.exprs.filter(AssignmentExp), fromModel, ruleHierarchy)
			} catch (MultEcoreException e) {
				e.createNotificationDialog
			}
		}
		
		//Parse instantiated attributes
		for (ve : r.from.attExprs) {
			try {
				ve.parse(fromModel, ruleHierarchy)
			} catch (MultEcoreException e) {
				e.createNotificationDialog
			}
		}		
		
		// Parse Boxes in From
		for (bo : r.from.boxes) {
			try {
				bo.parse(fromModel, ruleHierarchy)
			} catch (MultEcoreException e) {
				e.createNotificationDialog
			}
		}
		
		// Parse conditions
		try {
			r.from.conditionBlock.parse(fromModel, ruleHierarchy)
		} catch (MultEcoreException e) {
			e.createNotificationDialog
		}
		
		// Parse TO
		val toModel = ruleHierarchy.createModel(Constants.TO_BLOCK_ID, bottomMetaModel.name)
		
		for (suppRuleHierarchy : ruleHierarchy.supplementaryHierarchies) {
			
			val bottomMostSuppMetamodel = (suppRuleHierarchy as RuleHierarchy).getMetaModelInLevel(suppRuleHierarchy.maxLevel)
			ruleHierarchy.addSupplementaryModel(toModel, (suppRuleHierarchy as RuleHierarchy).name, bottomMostSuppMetamodel.name)
			ruleHierarchy.addSupplementaryHierarchy(suppRuleHierarchy)
		}
		for (ve : r.to.varExprs) {
			try {
				ve.parse(r.to.exprs.filter(AssignmentExp), toModel, ruleHierarchy)
			} catch (MultEcoreException e) {
				e.createNotificationDialog
			}
		}
		
		// Parse instantiated attributes
		for (ve : r.to.attExprs) {
			try {
				ve.parse(toModel, ruleHierarchy)
			} catch (MultEcoreException e) {
				e.createNotificationDialog
			}
		}
		
		// Parse Boxes in To
		for (bo : r.to.boxes) {
			try {
				bo.parse(toModel, ruleHierarchy)
			} catch (MultEcoreException e) {
				e.createNotificationDialog
			}
		}	
		// Proliferate and generate 2-level rules
		// proliferate(multilevelHierarchy, ruleHierarchy)
	}

	protected def RuleHierarchy getRuleHierarchy(MetaElementDec declaration, RuleHierarchy mainRuleHierarchy, String moduleName) {
//		val ruleHierarchyAlias = switch (declaration.eClass) {
//			MetaConstantNodeDec: {val mcn = declaration as MetaConstantNodeDec; mcn.alias.name}
//			MetaVariableNodeDec: {val mvn = declaration as MetaVariableNodeDec; mvn.alias.name}
//			MetaConstantEdgeDec: {val mce = declaration as MetaConstantEdgeDec; mce.alias.name}
//			MetaVariableEdgeDec: {val mve = declaration as MetaVariableEdgeDec; mve.alias.name}
//		}
		var ruleHierarchyAlias = ""
		if (declaration instanceof MetaConstantNodeDec){
			ruleHierarchyAlias = (declaration as MetaConstantNodeDec).alias.name 
		}
		else if (declaration instanceof MetaVariableNodeDec){
			ruleHierarchyAlias = (declaration as MetaVariableNodeDec).alias.name 
		}
		else if (declaration instanceof MetaConstantEdgeDec){
			ruleHierarchyAlias = (declaration as MetaConstantEdgeDec).alias.name 
		}		
		else if (declaration instanceof MetaVariableEdgeDec){
			ruleHierarchyAlias = (declaration as MetaVariableEdgeDec).alias.name 
		}	
		else if (declaration instanceof MetaDeclaredAttributeDec){
			val parentNode = (declaration as MetaDeclaredAttributeDec).parentNode
			val parentNodeDeclaration = if (parentNode instanceof MetaConstantNode){
				constantNodes.get(parentNode as MetaConstantNode)
			}
			else {
				metaVariableNodes.get(parentNode as MetaVariableNode)
			}
			
			return getRuleHierarchy(parentNodeDeclaration, mainRuleHierarchy, moduleName)
		}
		
		if (ruleHierarchyAlias.equals(applicationHierarchyAlias)){
			return mainRuleHierarchy
		}
		return mainRuleHierarchy.getSupplementaryHierarchy(ruleHierarchyAlias) as RuleHierarchy
	}
	
	
	protected def parse(MetaConstantNodeDec mcn, RuleHierarchy ruleHierarchy) {
		//Check if the node belongs to the hierarchy with a given alias
		ruleHierarchy.addNodeConstant(mcn.constant.name, new HashSet<String>, getModel(ruleHierarchy, mcn.level))
		constantNodes.put(mcn.constant, mcn)
		var set = new HashSet<String>
		set.add(mcn.constant.name)
		inheritanceRelations.put(mcn.constant.name, set)
	}
	
	protected def parse(MetaVariableNodeDec mvn, RuleHierarchy ruleHierarchy) {
		for (mvnv : mvn.vars) {
			var typeName = no.hvl.multecore.common.Constants.ECLASS_ID
			var typeReversePotency = no.hvl.multecore.common.Constants.TYPE_REVERSE_POTENCY_DEFAULT_VALUE
			if (null !== mvn.type) {
				typeName = mvn.type.name
				val typeLevel = switch (mvn.type) {
					MetaConstantNode: {
						val typeNode = mvn.type as MetaConstantNode;
						constantNodes.get(typeNode).level
					}
					MetaVariableNode: {
						val typeNode = mvn.type as MetaVariableNode;
						metaVariableNodes.get(typeNode).level
					}
				}
				typeReversePotency = mvn.level - typeLevel
			}
			ruleHierarchy.addNodeVariable(mvnv.name, typeName, typeReversePotency, new HashSet<String>, null, getModel(ruleHierarchy, mvn.level))
			metaVariableNodes.put(mvnv, mvn)
			var set = new HashSet<String>
			set.add(mvnv.name)
			inheritanceRelations.put(mvnv.name, set)
		}
	}

	protected def parse(MetaDeclaredAttributeDec mvn, RuleHierarchy ruleHierarchy) {
		val parentNodeLevel = switch (mvn.parentNode) {
			MetaConstantNode: {val parentNode = mvn.parentNode as MetaConstantNode; constantNodes.get(parentNode).level}
			MetaVariableNode: {val parentNode = mvn.parentNode as MetaVariableNode;metaVariableNodes.get(parentNode).level}
		}		
		ruleHierarchy.addMetaDeclaredAttribute(mvn.parentNode.name, mvn.attributeName.name, mvn.attributeType.name(), getModel(ruleHierarchy, parentNodeLevel));
	}	
	
	protected def parse(MetaConstantEdgeDec mce, RuleHierarchy ruleHierarchy) {
		var String multiplicityVariable = null
		if (mce.constant.multvar !== null){
			multiplicityVariable = mce.constant.multvar.mult.name
		} 
		ruleHierarchy.addEdgeConstant(mce.type, mce.typeSource, multiplicityVariable, getModel(ruleHierarchy, mce.level))
		constantEdges.put(mce.constant, mce)
	}
	
	
	protected def parse(MetaVariableEdgeDec mve, Iterable<MetaAssignmentExp> exprs, RuleHierarchy ruleHierarchy) {
		//TODO This code is terrible
		var typeReversePotency = 1
		for (mvev : mve.vars) {
			var String sourceType = null
			val iterator = exprs.iterator
			var found = false
			var MetaAssignmentExp expr = null
			while (iterator.hasNext && !found) {
				val next = iterator.next
//				sourceType = switch (next.source) {
//					MetaConstantNode: constantNodes.get(next.source).type
//					MetaVariableNode: metaVariableNodes.get(next.source).type.name
//				}
				var sourceTypeNode = next.source
				var stop = false
				while ((next.edge.name == mvev.name) && !found && !stop){
					if (sourceTypeNode instanceof MetaConstantNode){
						sourceType = constantNodes.get(sourceTypeNode).type
						stop = true
					}
					else if (sourceTypeNode instanceof MetaVariableNode){
						sourceTypeNode = metaVariableNodes.get(sourceTypeNode).type
						sourceType = sourceTypeNode.name
					}
					if (inheritanceRelations.get(sourceType).contains(mve.typeSource) || (null === mve.type)) {
						found = true
						expr = next
					}
				}
			}
			var typeName = no.hvl.multecore.common.Constants.EREFERENCE_ID
			var sourceTypeName = no.hvl.multecore.common.Constants.ECLASS_ID
			var targetTypeName = no.hvl.multecore.common.Constants.ECLASS_ID
			if (null !== mve.type) {
				sourceTypeName = mve.typeSource
				typeName = mve.type.name
				switch (mve.type) {
					MetaConstantEdge: {val typeDec = constantEdges.get(mve.type); typeReversePotency = mve.level - typeDec.level}
					MetaVariableEdge: {val typeDec = metaVariableEdges.get(mve.type); typeReversePotency = mve.level - typeDec.level}
				}
				targetTypeName = switch (expr.target) {
					MetaConstantNode: {val typeDec = constantNodes.get(expr.target); typeDec.type}
					MetaVariableNode: {val typeDec = metaVariableNodes.get(expr.target); typeDec.type.name}
				}
			} else {
				typeReversePotency = mve.level
			}
			var String multiplicityVariable = null
			if (mvev.multvar !== null){
				multiplicityVariable = mvev.multvar.mult.name
			}
			ruleHierarchy.addEdgeVariable(mvev.name, typeName, expr.source.name, sourceTypeName, expr.target.name, targetTypeName, typeReversePotency, multiplicityVariable, null, getModel(ruleHierarchy, mve.level))
			metaVariableEdges.put(mvev, mve)
		}
	}
	
	
	protected def parse(VariableExp ve, Iterable<AssignmentExp> exprs, IModel fromModel, RuleHierarchy ruleHierarchy) {
		var isNode = true
		var typeLevel = 0

		//Now we can have multiple types. NOTE: The first one will always be the application one
		//Also note  that we cannot specify supplementary types on edges, not supported for the moment	
		
		//Checking the type of the variable (application hierarchy type)	
		val applicationTypeName = switch (ve.type.get(0)) {
			MetaConstantNode: {val mcn = ve.type.get(0) as MetaConstantNode; val typeDec = constantNodes.get(mcn); typeLevel = typeDec.level; typeDec.type}
			MetaVariableNode: {val mvn = ve.type.get(0) as MetaVariableNode; typeLevel = metaVariableNodes.get(mvn).level; mvn.name}
			MetaConstantEdge: {val mce = ve.type.get(0) as MetaConstantEdge; val typeDec = constantEdges.get(mce); typeLevel = typeDec.level; isNode = false; typeDec.type}
			MetaVariableEdge: {val mve = ve.type.get(0) as MetaVariableEdge; typeLevel = metaVariableEdges.get(mve).level; isNode = false; mve.name}
		}
		val metamodel = fromModel.getMetamodelInLevel(typeLevel)
		for (v : ve.vars) {
			val name = v.name
			if (isNode) {
				val nodeType = metamodel.getNode(applicationTypeName)
				val setSuppTypes = newHashSet;
				if (ve.type.size > 1) {
					for (var i = 1; i < ve.type.size; i++) {
						val suppType = switch (ve.type.get(i)) {
							MetaConstantNode: {
								val mcn = ve.type.get(i) as MetaConstantNode;
								val typeDec = constantNodes.get(mcn)
								typeLevel = typeDec.level
								val suppRuleHierarchyAlias = typeDec.alias.name
								val suppRuleHierarchy = ruleHierarchy.getSupplementaryHierarchy(suppRuleHierarchyAlias) as RuleHierarchy
								suppRuleHierarchy.getMetaModelInLevel(typeLevel).getNode(typeDec.type)
							}
							MetaVariableNode: {
								val mvn = ve.type.get(i) as MetaVariableNode
								typeLevel = metaVariableNodes.get(mvn).level
								val suppRuleHierarchyAlias = metaVariableNodes.get(mvn).alias.name
								val suppRuleHierarchy = ruleHierarchy.getSupplementaryHierarchy(suppRuleHierarchyAlias) as RuleHierarchy
								suppRuleHierarchy.getMetaModelInLevel(typeLevel).getNode(mvn.name)								
							}
						}
						setSuppTypes.add(suppType);
					}
				}				
				ruleHierarchy.addNodeVariable(name, nodeType, new HashSet<String>, setSuppTypes, fromModel)
			}
			else {
				val setSuppTypes = newHashSet;
				if (ve.type.size > 1) {
					for (var i = 1; i < ve.type.size; i++) {
						var String suppTypeSourceName
						var String suppTypeTargetName
						val suppType = switch (ve.type.get(i)) {
							MetaConstantEdge: {
								val mce = ve.type.get(i) as MetaConstantEdge;
								val typeDec = constantEdges.get(mce);
								val suppRuleHierarchyAlias = typeDec.alias.name;
								val suppRuleHierarchy = ruleHierarchy.getSupplementaryHierarchy(suppRuleHierarchyAlias) as RuleHierarchy
								suppTypeSourceName = typeDec.typeSource
								typeLevel = typeDec.level;
								
								var typeModel = suppRuleHierarchy.getMetaModelInLevel(typeLevel)
								var sourceNodeType = typeModel.getNode(suppTypeSourceName)
								var edgeType = typeModel.getOutEdges(sourceNodeType).findFirst[e | e.name == typeDec.type]
								 
								suppTypeTargetName = edgeType.target.name
								
								suppRuleHierarchy.getMetaModelInLevel(typeLevel).getEdge(typeDec.type, suppTypeSourceName, suppTypeTargetName)		
							}
							
							MetaVariableEdge: {
								val mve = ve.type.get(i) as MetaVariableEdge;
								//TODO Check if we need +1 in typeLevel
								typeLevel = metaVariableEdges.get(mve).level;
								val typeDec = metaVariableEdges.get(mve);
								val suppRuleHierarchyAlias =  metaVariableEdges.get(mve).alias.name
								val suppRuleHierarchy = ruleHierarchy.getSupplementaryHierarchy(suppRuleHierarchyAlias) as RuleHierarchy
								suppTypeSourceName = typeDec.typeSource
								var typeModel = suppRuleHierarchy.getMetaModelInLevel(typeLevel)
								var sourceNodeType = typeModel.getNode(suppTypeSourceName)
								var edgeType = typeModel.getOutEdges(sourceNodeType).findFirst[e | e.name == mve.name]
							
								suppTypeTargetName = edgeType.target.name
								suppRuleHierarchy.getMetaModelInLevel(typeLevel).getEdge(mve.name, suppTypeSourceName, suppTypeTargetName)		
							}
						}
						setSuppTypes.add(suppType);
					}			
				}
				//TODO Include source name to verify it is the right relation (we might have same names)
				var expr = exprs.findFirst[e | e.edge == v]
				var source = fromModel.getNode(expr.source.name)
				var target = fromModel.getNode(expr.target.name)
				var sourceType = ruleHierarchy.getTypeInModel(source, metamodel)
				var targetType = ruleHierarchy.getTypeInModel(target, metamodel)
				var IEdge edgeType = null
				if (null === sourceType) {
					edgeType = metamodel.edges.findFirst[e | e.name == applicationTypeName]
					val finalTypeLevel = typeLevel
					var sourceTransitiveTypes = source.transitiveTypes.toList.sortInplace(e1, e2 | e1.model.compareTo(e2.model))
					var sourceIntermediateType = sourceTransitiveTypes.findFirst[n | n.model.level > finalTypeLevel]
					if (sourceIntermediateType instanceof NodeConstant) {
						sourceIntermediateType.type = edgeType.source
						if (null === targetType) {
							targetType = edgeType.target
						} else {
							
						}
					} else {
						System.err.println("Could not fix typing of variable " + sourceIntermediateType.name) //TODO Turn into exception?
					}
				} else {
					edgeType = metamodel.getEdgePlusInherited(applicationTypeName, sourceType.name, targetType.name)
					if (null === edgeType){
						edgeType  = (metamodel as RuleModel).getConstantEdgePlusInherited(applicationTypeName, sourceType.name);
					}
				}
				ruleHierarchy.addEdgeVariable(name, edgeType, source, target, null, setSuppTypes, fromModel)
			}
			variables.put(v, ve)
		}
	}

	protected def parse(InstantiatedAttributeExp instAttribute, IModel fromModel, RuleHierarchy ruleHierarchy) {
		if (instAttribute.attributeExpression instanceof StringExpression){
			ruleHierarchy.addVariableInstantiatedAttribute(instAttribute.variableAttributeParent.name,instAttribute.attributeName.name,instAttribute.attributeExpression.exp,fromModel);
		}
	}
	
	protected def  parse(Box b, IModel model, RuleHierarchy ruleHierarchy) {	
		var boxExpression = ""
		if (b.boxExp.multvar instanceof VariableMultiplicity){
			boxExpression = (b.boxExp.multvar as VariableMultiplicity).name
		}
		else{
			boxExpression = (b.boxExp.multvar as StringExpression).exp
			boxExpression = boxExpression.replaceAll("#", "");
		}
		val box = ruleHierarchy.addBox(boxExpression, model)
		for (element : b.boxExp.variables) {
			val variableExp = variables.get(element)
			if (variableExp.type.get(0) instanceof MetaVariableNode ||
				variableExp.type.get(0) instanceof MetaConstantNode) {
				val nodeToAdd = model.getNode(element.name) as Node
				box.addNode(nodeToAdd)
			} else if (variableExp.type.get(0) instanceof MetaVariableEdge ||
				variableExp.type.get(0) instanceof MetaConstantEdge) {
				val block = element.eContainer.eContainer as Block;
				val edge = block.exprs.filter(AssignmentExp).findFirst[it.edge == element].edge;
				val source = block.exprs.filter(AssignmentExp).findFirst[it.edge == element].source;
				val target = block.exprs.filter(AssignmentExp).findFirst[it.edge == element].target;
				val edgeToAdd = model.getEdge(edge.name, source.name, target.name) as Edge;
				box.addEdge(edgeToAdd);
			}
		}
		for (element : b.boxExp.metaNodes) {
			val metaVariableNodeDec = metaVariableNodes.get(element)
			val metaVariableNodeLevel = metaVariableNodeDec.level
			val nodeToAdd = ruleHierarchy.getMetaModelInLevel(metaVariableNodeLevel).getNode(element.name) as Node
			box.addNode(nodeToAdd)
		}
		for (element : b.boxExp.metaEdges) {
			val metaVariableEdgeDec = metaVariableEdges.get(element)
			val metaVariableEdgeLevel = metaVariableEdgeDec.level
			val block = element.eContainer.eContainer as MetaBlock;
			val edge = block.exprs.filter(MetaAssignmentExp).findFirst[it.edge == element].edge;
			val source = block.exprs.filter(MetaAssignmentExp).findFirst[it.edge == element].source;
			val target = block.exprs.filter(MetaAssignmentExp).findFirst[it.edge == element].target;			
			val edgeToAdd = ruleHierarchy.getMetaModelInLevel(metaVariableEdgeLevel).getEdge(edge.name, source.name, target.name) as Edge
			box.addEdge(edgeToAdd)
		}
		for (Box inBox : b.boxExp.inBoxes) {
			processInnerBoxes(box, inBox, model, ruleHierarchy);
		}
	}
	
	//Recursive version, for n-layers
	private def processInnerBoxes(no.hvl.multecore.mcmt.proliferation.rule.Box box, Box inBox,
		IModel model, RuleHierarchy ruleHierarchy) {
		var newBoxExpression = ""
		if (inBox.boxExp.multvar instanceof VariableMultiplicity){
			newBoxExpression = (inBox.boxExp.multvar as VariableMultiplicity).name
		}
		else{
			newBoxExpression = (inBox.boxExp.multvar as StringExpression).exp
			newBoxExpression = newBoxExpression.replaceAll("#", "");
		}		
		val newBox = new no.hvl.multecore.mcmt.proliferation.rule.Box(newBoxExpression);
		for (element : inBox.boxExp.variables) {
			val variableExp = variables.get(element)
			if (variableExp.type.get(0) instanceof MetaVariableNode ||
				variableExp.type.get(0) instanceof MetaConstantNode) {
				val nodeToAdd = model.getNode(element.name) as Node
				newBox.addNode(nodeToAdd)
			} else if (variableExp.type.get(0) instanceof MetaVariableEdge ||
				variableExp.type.get(0) instanceof MetaConstantEdge) {
				val block = element.eContainer.eContainer as Block;
				val edge = block.exprs.filter(AssignmentExp).findFirst[it.edge == element].edge;
				val source = block.exprs.filter(AssignmentExp).findFirst[it.edge == element].source;
				val target = block.exprs.filter(AssignmentExp).findFirst[it.edge == element].target;
				val edgeToAdd = model.getEdge(edge.name, source.name, target.name) as Edge;
				newBox.addEdge(edgeToAdd);
			}
		}
		for (element : inBox.boxExp.metaNodes) {
			val metaVariableNodeDec = metaVariableNodes.get(element)
			val metaVariableNodeLevel = metaVariableNodeDec.level
			val nodeToAdd = ruleHierarchy.getMetaModelInLevel(metaVariableNodeLevel).getNode(element.name) as Node
			box.addNode(nodeToAdd)
		}
		for (element : inBox.boxExp.metaEdges) {
			val metaVariableEdgeDec = metaVariableEdges.get(element)
			val metaVariableEdgeLevel = metaVariableEdgeDec.level
			val block = element.eContainer.eContainer as MetaBlock;
			val edge = block.exprs.filter(MetaAssignmentExp).findFirst[it.edge == element].edge;
			val source = block.exprs.filter(MetaAssignmentExp).findFirst[it.edge == element].source;
			val target = block.exprs.filter(MetaAssignmentExp).findFirst[it.edge == element].target;			
			val edgeToAdd = ruleHierarchy.getMetaModelInLevel(metaVariableEdgeLevel).getEdge(edge.name, source.name, target.name) as Edge
			box.addEdge(edgeToAdd)
		}		
		box.addInnerBox(newBox);
		for (Box innerBox : inBox.boxExp.inBoxes) {
			processInnerBoxes(newBox, innerBox, model, ruleHierarchy);
		}
		return;
	}
	
	protected def parse(ConditionalBlock condBlock, IModel model, RuleHierarchy ruleHierarchy){
		if (null !== condBlock){
			for (expression : condBlock.expressions) {
				val condExpression = expression as StringExpression
				val conditionalExpression = condExpression.exp.substring(1, condExpression.exp.length()-1);
			 	if (conditionalExpression  !== ""){
			 		ruleHierarchy.addCondition(conditionalExpression, model)
			 	}
			}
		}
	}
	

	protected def parse(InhExpr inh, Iterable<MetaConstantNodeDec> metaNodes , RuleHierarchy ruleHierarchy) {
		val child = inh.inheritanceExpr.child.name;
		val parent = inh.inheritanceExpr.parent.name;
		val level = metaNodes.findFirst[n | n.constant.name == child].level
		val metamodel = getModel(ruleHierarchy, level)
		val childNode = metamodel.getNode(child)
		val parentNode = metamodel.getNode(parent)
		ruleHierarchy.addParentToNode(childNode,parentNode,metamodel)
		
		inheritanceRelations.get(child).addAll(inheritanceRelations.get(parent))
		for (set : inheritanceRelations.values) {
			if (set.contains(child)){
				set.add(parent)
			}
		}	
	}
	
	//TODO Improve this later with potency, null checks, etc.
	protected def IModel getModel(RuleHierarchy ruleHierarchy, int level) {
		var iModel = ruleHierarchy.getMetaModelInLevel(level)
		if (null === iModel) {
			val modelName = Constants.META_MODEL_NAME_PREFIX + level
			iModel = ruleHierarchy.createModel(modelName, getModel(ruleHierarchy, level - 1).name)
		}
		return iModel
	}
	
	protected def INode getSupplementaryNodeFromHierarchy (RuleHierarchy suppRuleHierarchy, String nodeName){
		var node = null as INode
		for (suppModel : suppRuleHierarchy.allModels) {
			node = suppModel.getNode(nodeName)
			if (node  !== null){
				 return node
			}
		}
		return node
	}
	
	protected def IEdge getSupplementaryEdgeFromHierarchy (RuleHierarchy suppRuleHierarchy, String edgeName, String sourceEdgeName, String targetEdgeName){
		var edge = null as IEdge
		for (suppModel : suppRuleHierarchy.allModels) {
			edge = suppModel.getEdge(edgeName, sourceEdgeName, targetEdgeName)
			if (edge  !== null){
				 return edge
			}
		}
		return edge
	}	
	
	
	def proliferate(MultilevelHierarchy multilevelHierarchy, RuleHierarchy ruleHierarchy) {
		val matcher = new Matcher
		val matches = matcher.findMatches(multilevelHierarchy, ruleHierarchy)
		if(null === matches)
			println("NO MATCHES FOUND!")
		else
			println("NUMBER OF MATCHES: " + matches.size)
	}

}
