package no.hvl.multecore.mcmt.maude;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.core.internal.resources.File;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManager;
import org.eclipse.sirius.viewpoint.DView;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import no.hvl.multecore.common.Debugger;
import no.hvl.multecore.common.MultEcoreManager;
import no.hvl.multecore.common.hierarchy.DeclaredAttribute;
import no.hvl.multecore.common.hierarchy.DeclaredAttribute.NativeType;
import no.hvl.multecore.common.hierarchy.Edge;
import no.hvl.multecore.common.hierarchy.IEdge;
import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.common.hierarchy.InstantiatedAttribute;
import no.hvl.multecore.common.hierarchy.Model;
import no.hvl.multecore.common.hierarchy.MultilevelHierarchy;
import no.hvl.multecore.common.hierarchy.Node;
import no.hvl.multecore.common.registry.ModelRegistry;
import no.hvl.multecore.common.registry.ModelRegistryEntry;
import no.hvl.multecore.core.actions.AbstractTransformerAction;
import no.hvl.multecore.core.actions.MefFromHierarchyTransformerAction;
import no.hvl.multecore.core.actions.MetamodelFromHierarchyTransformerAction;
import no.hvl.multecore.core.actions.ModelFromHierarchyTransformerAction;
import no.hvl.multecore.core.events.Utils;
import no.hvl.multecore.mcmt.maude.actions.OpenedProjectFilter;
import no.hvl.multecore.mcmt.maude.auxiliarhierarchy.AuxiliarAttribute;
import no.hvl.multecore.mcmt.maude.auxiliarhierarchy.AuxiliarEdge;
import no.hvl.multecore.mcmt.maude.auxiliarhierarchy.AuxiliarHierarchy;
import no.hvl.multecore.mcmt.maude.auxiliarhierarchy.AuxiliarModel;
import no.hvl.multecore.mcmt.maude.auxiliarhierarchy.AuxiliarNode;
import no.hvl.multecore.mcmt.maude.wrapper.MaudeProcess;

public class Maude2MultEcore implements IObjectActionDelegate {

	Shell shell = new Shell();

	@SuppressWarnings("restriction")
	@Override
	public void run(IAction action) {
		// Open dialog to select the XML file
		ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
                Display.getDefault().getActiveShell(),
                new WorkbenchLabelProvider(),
                new BaseWorkbenchContentProvider());
		dialog.addFilter(new OpenedProjectFilter("xml"));
		dialog.setTitle("Select the maude file containing the MCMTs");
		dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
		dialog.open();
		
		MultilevelHierarchy mh = MultEcoreManager.instance().getCurrentHierarchy();
		AuxiliarHierarchy ah = new AuxiliarHierarchy();

		File selectedXmlFile = (File) dialog.getFirstResult();
		IPath selectedXmlFileAbsolutePath = selectedXmlFile.getLocation();
		String absoluteDirectoryPath = no.hvl.multecore.mcmt.maude.Utils.getRelativeDirectoryFromFile (selectedXmlFileAbsolutePath.toString());
		java.io.File file = new java.io.File(absoluteDirectoryPath + selectedXmlFile.getName());
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(file);
			doc.getDocumentElement().normalize();
			
			//Get model elements of the XML 
			NodeList modelList = doc.getElementsByTagName("model");
			String initialStateModelName = MaudeProcess.instance().getModelNameAssignedToProcess();
			// Remove extension
			String newStateModelName = file.getName().replace(".xml", "");
			// Running over the models
			for (int i = 0; i < modelList.getLength(); i++) {
				org.w3c.dom.Node modelNode = modelList.item(i);
				// Name of the model
				String nameModel = modelNode.getAttributes().getNamedItem("name").getNodeValue();
				// Level of the model
				int levelModel = Integer.parseInt(modelNode.getAttributes().getNamedItem("level").getNodeValue());
				// Om of the model
				String omModel = modelNode.getAttributes().getNamedItem("om").getNodeValue();
				AuxiliarModel auxModel = new AuxiliarModel(nameModel, levelModel, omModel);

				Model model = null;
				if (!auxModel.getName().equals(no.hvl.multecore.common.Constants.ECORE_ID)) {
					model = (Model) mh.getModelByName(nameModel);					
				}
				auxModel.setName(nameModel);

				//Create a new model as null (this will be the updated model in the last level;
				Model newVersionModel = null;
				// Running over children (it contains both nodes and relations
				for (int j = 0; j < modelNode.getChildNodes().getLength(); j++) {
					org.w3c.dom.Node potentialSet = modelNode.getChildNodes().item(j);
					// Running over nodes (elts)
					if (potentialSet.getNodeName().equals("elts")) {
						// Processing each node
						newVersionModel = processNodes(initialStateModelName, newStateModelName, potentialSet, auxModel,  ah, model, newVersionModel,  mh);
					}
					// Running over children, edges (rels)
					if (potentialSet.getNodeName().equals("rels")) {
						newVersionModel = processEdges(initialStateModelName, potentialSet, auxModel, ah, model, newVersionModel, mh);
					}
				}
				if (newVersionModel != null) {
					
					ModelRegistry mr = MultEcoreManager.instance().getModelRegistry();
					URI initialStateModelBaseUri = mr.getEntry(model).getBaseUri();
					URI newStateModelUri = initialStateModelBaseUri.appendSegment(newStateModelName);
					
					//We first create the MEF to create the Entry
					Resource newStateModelMefResource = mr.createResource(newStateModelUri.appendFileExtension(no.hvl.multecore.core.Constants.FILE_EXTENSION_MEF));
					IProject mhIProject = MultEcoreManager.instance().getProject(mh);
//						URI initialStateModelProjectRelativeUri = mr.getEntry(model).getProjectRelativeUri().trimSegments(1).appendSegment(newStateModelName);
					IResource newStateModelMefIResource = mhIProject.getFile(mr.getEntry(model).getProjectRelativeUri().trimSegments(1).appendSegment(newStateModelName).toFileString());
							
//								mhIProject.getFile(newStateModelMefResource.getURI()
//								.deresolve(initialStateModelProjectRelativeUri)
//								.toFileString());
					ModelRegistryEntry mre = mr.createEntry(newStateModelMefIResource, mhIProject);
					
					//Fill the information in the Entry
					mre.setMefResource(newStateModelMefResource);
					mre.setMetamodelResource(mr.createResource(newStateModelUri.appendFileExtension(no.hvl.multecore.core.Constants.FILE_EXTENSION_METAMODEL)));
					mre.setModelResource(mr.createResource(newStateModelUri.appendFileExtension(no.hvl.multecore.core.Constants.FILE_EXTENSION_MODEL)));
					
					//Get the three files mef,ecore,xmi
					//Working with the resources
					List<AbstractTransformerAction> transformerActions = new ArrayList<AbstractTransformerAction>();		
					transformerActions.add(new MefFromHierarchyTransformerAction(mre.getMefResource().getURI(), mh));
					transformerActions.add(new ModelFromHierarchyTransformerAction(mre.getModelResource().getURI(), mh));
					transformerActions.add(new MetamodelFromHierarchyTransformerAction(mre.getMetamodelResource().getURI(), mh));
					
					//For future if we play with several instances take this one into account		
//						transformerActions.add(new GenerateRepresentationAction(newStateModelUri.appendFileExtension(no.hvl.multecore.core.Constants.FILE_EXTENSION_METAMODEL)));
					
					for (AbstractTransformerAction ita : transformerActions) {
						Utils.scheduleJob(ita);
						try {
							ita.join();
						} catch (InterruptedException e) {
							Debugger.logError("Waiting for job did not work");
						}
					}
//					IProgressMonitor monitor = new NullProgressMonitor();
//					Session session = SessionManager.INSTANCE.getSession(mre.getRepresentationResource().getURI(), monitor);
//					TransactionalEditingDomain ted = session.getTransactionalEditingDomain();
//					session.getSemanticResources().iterator().next(). 
//					Collection <DView> views = session.getSelectedViews();
				}
				ah.addModel(auxModel);
				
				
			}
			no.hvl.multecore.core.Utils.addtoMultEcoreLog("New model state created in file: " + newStateModelName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	@Override
	public void selectionChanged(IAction action, ISelection selection) {

	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {

	}

	
	private Model processNodes(String initialStateModelName, String newStateModelName, org.w3c.dom.Node potentialSet, AuxiliarModel auxModel, AuxiliarHierarchy ah, Model model, Model newVersionModel, MultilevelHierarchy mh) {
		for (int k = 0; k < potentialSet.getChildNodes().getLength(); k++) {
			if (potentialSet.getChildNodes().item(k).getNodeName().equals("node")) {

				org.w3c.dom.Node nodeElement = potentialSet.getChildNodes().item(k);

				String nodeNameExpression = nodeElement.getAttributes().getNamedItem("name").getNodeValue();
				String nodeTypeExpression = nodeElement.getAttributes().getNamedItem("type").getNodeValue();
				AuxiliarNode auxNode = new AuxiliarNode(nodeNameExpression, nodeTypeExpression, auxModel);
				
				auxModel.addAuxiliarNode(auxNode);
				// We just take into account the instance level as the model to update
				if (auxModel.getName().equals(initialStateModelName)) {
					// Check if the node already exists in the hierarchy
//					Node node = (Node) model.getNode(nodeName);
					if (newVersionModel == null) {
						newVersionModel = (Model) mh.createModel(newStateModelName, model.getMainMetamodel().getName());
					}
					// If not it is a new element
//					if (node == null) {
						// Getting the name and the level of the type
						String typeNodeName = auxNode.getType().getName();
						int typeNodeLevel = auxNode.getType().getLevel();
						if (typeNodeLevel != -1 && typeNodeName != null) {
							String modelOfType = ah.getmodelByLevel(typeNodeLevel).getName();
							if (modelOfType != null) {
								String nodeName ="";
								if ( auxNode.isNumericName()) {
									nodeName = auxNode.getType().getName().toLowerCase() + auxNode.getName().getName();
								}
								else {
									nodeName = auxNode.getName().getName();
								}
								Node node = new Node(nodeName, mh.getModelByName(modelOfType).getNode(typeNodeName), newVersionModel);
								newVersionModel.addOrReplaceNode(node);
								//Process attributes of the node. First stage we consider only attributes (instantiated attributes) are modified at bottom level
								processAttributes (nodeElement, auxNode, node);
								no.hvl.multecore.core.Utils.addtoMultEcoreLog("Added new node: " + node.getName() + " to " + newVersionModel.getName());
							}
						}
//					}
				}
			}
		}
		return newVersionModel;
	}
	
	
	private void processAttributes(org.w3c.dom.Node nodeElement, AuxiliarNode auxNode, Node node) {
		for (int i = 0; i < nodeElement.getChildNodes().getLength(); i++) {
			if (nodeElement.getChildNodes().item(i).getNodeName().equals("attributes")) {
			org.w3c.dom.Node attributes = nodeElement.getChildNodes().item(i);
				for (int j = 0; j < attributes.getChildNodes().getLength(); j++) {
					if (attributes.getChildNodes().item(j).getNodeName().equals("attribute")) {
						org.w3c.dom.Node attributeElement = attributes.getChildNodes().item(j);
						String attributeNameOrValue = attributeElement.getAttributes().getNamedItem("nameOrValue")
								.getNodeValue();
						String attributeType = attributeElement.getAttributes().getNamedItem("type").getNodeValue();

						//First of all we need to check if the nameOrValue can be corresponded to the type in the real node of the hierarchy, based on the type.
						AuxiliarAttribute auxAttribute = new AuxiliarAttribute(attributeNameOrValue, attributeType);
						Set<DeclaredAttribute> declaredAttributes = node.getDeclaredAttributes(true);
						for (DeclaredAttribute declaredAttribute : declaredAttributes) {
							//We find the declared attribute, now we need to check that the name can be casted to that type
							if (declaredAttribute.getNameOrValue().equals(auxAttribute.getType().getName())) {
								if (checkTypeCorrespondence(auxAttribute.getNameOrValue().getName(),
										auxAttribute.getType().getName(), (NativeType) declaredAttribute.getType())) {
									auxNode.addNewAttribute(auxAttribute);
									//Now the type corresponds, we create the instantiated attribute with the value updated 
									InstantiatedAttribute instantiatedAttribute = new InstantiatedAttribute(auxAttribute.getNameOrValue().getName(), declaredAttribute, node);
									node.addAttribute(instantiatedAttribute);
								}
							}
						}
						
					}
				}
			}
		}
	}



	private Model processEdges(String fileName, org.w3c.dom.Node potentialSet, AuxiliarModel auxModel,
			AuxiliarHierarchy ah, Model model, Model newVersionModel, MultilevelHierarchy mh) {
		for (int k = 0; k < potentialSet.getChildNodes().getLength(); k++) {
			if (potentialSet.getChildNodes().item(k).getNodeName().equals("relation")) {
				org.w3c.dom.Node edgeElement = potentialSet.getChildNodes().item(k);
				String edgeNameExpression = edgeElement.getAttributes().getNamedItem("name").getNodeValue();
				String edgeTypeExpression = edgeElement.getAttributes().getNamedItem("type").getNodeValue();
				String edgeSourceExpression = edgeElement.getAttributes().getNamedItem("source").getNodeValue();
				String edgeTargetExpression = edgeElement.getAttributes().getNamedItem("target").getNodeValue();
				AuxiliarEdge auxEdge = new AuxiliarEdge(edgeNameExpression, edgeTypeExpression, auxModel, edgeSourceExpression, edgeTargetExpression);
				auxModel.addAuxiliarEdge(auxEdge);
				if (auxModel.getName().equals(fileName)) {
					// Check if the edge already exists in the hierarchy
//					Edge edge = (Edge) model.getEdge(edgeName, edgeSourceName, edgeTargetName);
					// If not it is a new element
//					if (edge == null) {
						IModel typeModel = mh.getModelByName(ah.getmodelByLevel(auxEdge.getType().getLevel()).getName());
						//Maude Source and Target objects 
						AuxiliarNode sourceAuxNode = auxEdge.getSource();
						AuxiliarNode targetAuxNode = auxEdge.getTarget();
						
						//Name of the model where the source and the target are allocated
						String modelSourceTypeName = ah.getmodelByLevel(sourceAuxNode.getType().getLevel()).getName();
						String modelTargetTypeName = ah.getmodelByLevel(targetAuxNode.getType().getLevel()).getName();
						String edgeTypeName = auxEdge.getType().getName();
						//Searching the type edge (from Multilevel hierarchy) with the name of the type, the source type, and the target type 
						IEdge edgeType = typeModel.getEdge(edgeTypeName, 
								mh.getTypeInModel(mh.getModelByName(modelSourceTypeName).getNode(sourceAuxNode.getType().getName()), typeModel).getName(), 
								mh.getTypeInModel(mh.getModelByName(modelTargetTypeName).getNode(targetAuxNode.getType().getName()), typeModel).getName());
						
						
						String edgeName = "";						
						if (auxEdge.isNumericName()) {
							edgeName = auxEdge.getType().getName().toLowerCase() + auxEdge.getName().getName();
						}
						else {
							edgeName = auxEdge.getName().getName();
						}
						
						//Source node (from Multilevel hierarchy)
						String sourceName = "";
						if (sourceAuxNode.isNumericName()) {
							sourceName = sourceAuxNode.getType().getName().toLowerCase() + sourceAuxNode.getName().getName();
						}
						else {
							sourceName = sourceAuxNode.getName().getName();
						}
						INode sourceNode = newVersionModel.getNode(sourceName);
						
						//Target node (from Multilevel hierarchy)	
						String targetName = "";
						if (targetAuxNode.isNumericName()) {
							targetName = targetAuxNode.getType().getName().toLowerCase() + targetAuxNode.getName().getName();
						}
						else {
							targetName = targetAuxNode.getName().getName();
						}										
						INode targetNode = newVersionModel.getNode(targetName);
						Edge edge = new Edge(edgeName,edgeType , sourceNode, targetNode, newVersionModel);
						newVersionModel.addOrReplaceEdge(edge);
						no.hvl.multecore.core.Utils.addtoMultEcoreLog("Added new edge: " + edge.getName() + " to " + newVersionModel.getName());
					}
				}
//			}
		}	
		return newVersionModel;
	}
	
	private boolean checkTypeCorrespondence(String nameOrValue, String attributeType, NativeType type) {
		Boolean corresponds = true;
		switch (type.getNameOrValue()) {
		case ("Integer"):
			try {
				Integer.parseInt(nameOrValue);
			} catch (NumberFormatException | NullPointerException nfe) {
				corresponds = false;
				System.out.println("Error with attribute " + attributeType + " and value " + nameOrValue
						+ " cannot be casted to " + type.getNameOrValue());
			}
			break;
		case ("Real"):
			try {
				Float.parseFloat(nameOrValue);
			} catch (NumberFormatException | NullPointerException nfe) {
				corresponds = false;
				System.out.println("Error with attribute " + attributeType + " and value " + nameOrValue
						+ " cannot be casted to " + type.getNameOrValue());
			}
			break;
		case ("Boolean"):
			if (!nameOrValue.equals("true") && !nameOrValue.equals("false")) {
				corresponds = false;
				System.out.println("Error with attribute " + attributeType + " and value " + nameOrValue
						+ " cannot be casted to " + type.getNameOrValue());
			}
			break;
		}
		return corresponds;
	}
}
