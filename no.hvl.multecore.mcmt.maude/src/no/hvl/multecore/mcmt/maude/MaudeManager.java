package no.hvl.multecore.mcmt.maude;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import no.hvl.multecore.common.hierarchy.MultilevelHierarchy;
import no.hvl.multecore.mcmt.proliferation.rule.RuleHierarchy;

public class MaudeManager {
	private static MaudeManager MAUDE_INSTANCE = null;
	//TODO Change this to store multiple rules related to the hierarchy
	//It might be: private HashMap<MultilevelHierarchy, Set<RuleHierarchy>> rules;
	private Map<String, RuleHierarchy> rules;
	
	//Key: the multilevel hierarchy. Value: A set of the selected rules
	private Map<MultilevelHierarchy, Set<RuleHierarchy>> selectedRulesForHierarchy;
	
	//Key: the multilevel hierarchy. Value: The name of the model being executed (initial state)
	private Map<MultilevelHierarchy, String> modelBeingExecuted;
	
	private MaudeManager () {
		rules = new HashMap<String, RuleHierarchy>();
		selectedRulesForHierarchy = new HashMap<MultilevelHierarchy, Set<RuleHierarchy>>();
		modelBeingExecuted = new HashMap<MultilevelHierarchy, String>();
	}
	
	public void setRules (Map<String, RuleHierarchy> _rules) {
		this.rules.putAll( _rules);
	}
	
	public static MaudeManager instance() {
		if (null == MAUDE_INSTANCE)
			MAUDE_INSTANCE = new MaudeManager();
		return MAUDE_INSTANCE;
	}
	
	public Map<String, RuleHierarchy> getRules (){
		return this.rules;
	}
	
	public void addSelectedRulesToMultilevelHierarchy (MultilevelHierarchy mh, Set<RuleHierarchy> selectedRules) {
		this.selectedRulesForHierarchy.put(mh, selectedRules);
	}
	
	public Set<RuleHierarchy> getRulesFromSelectedMultilevelHierarchy (MultilevelHierarchy mh){
		return this.selectedRulesForHierarchy.get(mh);
	}
	
	public void addModelBeingExecutedFromHierarchy (MultilevelHierarchy mh, String modelName) {
		this.modelBeingExecuted.put(mh, modelName);
	}
	
	public String getModelNameFromHierarchy (MultilevelHierarchy mh){
		return this.modelBeingExecuted.get(mh);
	}	

}
