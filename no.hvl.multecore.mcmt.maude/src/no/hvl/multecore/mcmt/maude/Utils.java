package no.hvl.multecore.mcmt.maude;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.w3c.dom.NodeList;

public class Utils {
	
	
	public static String getModelNameFromMaudeLevel (int level, NodeList modelsMaude) {
		String modelName = null;
		for (int i = 0; i < modelsMaude.getLength(); i++) {
			org.w3c.dom.Node modelNode = modelsMaude.item(i);
			int levelInMaude = Integer.valueOf(modelNode.getAttributes().getNamedItem("level").getNodeValue());
			if (levelInMaude == level) {
				modelName = modelNode.getAttributes().getNamedItem("name").getNodeValue();
			}
		}
		return modelName;
	}
	
	public static String getNewElementName (String newElementName) {
        if (isNumericalName(newElementName)) {
        	String removedPotentialWhiteSpaces = newElementName.replaceAll(" ", "");
        	newElementName = removedPotentialWhiteSpaces;
        }
        return newElementName;
	}
	
	public static Boolean isNumericalName (String newElementName) {
		boolean isNumeric = true;
		String removedPotentialWhiteSpaces = newElementName.replaceAll(" ", "");
        try {
            Integer num = Integer.parseInt(removedPotentialWhiteSpaces);
        } catch (NumberFormatException e) {
        	isNumeric = false;
        }
        return isNumeric;
		
	}
	

    public static <E> Set<E> toSet(List<E> l) {
        return new HashSet<E>(l);
    }

    public static <E> List<E> toList(Set<E> s) {
        return new ArrayList<E>(s);
    }
    
    public static String getRelativeDirectoryFromFile (String path) {
    	String fileName = path.substring(path.lastIndexOf("/") + 1);
    	return  path.replace(fileName, "");
    }

	public static String[] splitFile(String coreMaudeFile, String string) {
		
		String[] splittedString;
		
		splittedString = coreMaudeFile.split(string);
		return splittedString;
	}
	
	  public static String getBetweenStrings(
			    String text,
			    String textFrom,
			    String textTo) {

			    String result = "";

			    // Cut the beginning of the text to not occasionally meet a      
			    // 'textTo' value in it:
			    result =
			      text.substring(
			        text.indexOf(textFrom) + textFrom.length(),
			        text.length());

			    // Cut the excessive ending of the text:
			    result =
			      result.substring(
			        0,
			        result.indexOf(textTo));

			    return result;
			  }
}
