package no.hvl.multecore.mcmt.maude.auxiliarhierarchy;

import no.hvl.multecore.mcmt.maude.Utils;

public class AuxiliarEdge extends AuxiliarElement{
	
	protected AuxiliarNode source;
	protected AuxiliarNode target;
	
	public AuxiliarEdge() {

	}
	
	public AuxiliarEdge(String name, String type, AuxiliarModel model, String sourceExpression, String targetExpression) {
		super(name, type, model);
		
		MaudeName sourceName = new MaudeName();
		sourceName.createNameFromMaudeExpression(sourceExpression);
		sourceName.setName(Utils.getNewElementName(sourceName.getName()));
		AuxiliarNode source = null;
		if (Utils.isNumericalName(sourceName.getName())) {
			source = this.model.getNumericalAuxiliarNodeByName(sourceName);
		}
		else {
			source = this.model.getAuxiliarNodeByName(sourceName);

		}
		if (source != null) {
			this.source = source;
		}
		MaudeName targetName = new MaudeName();
		targetName.createNameFromMaudeExpression(targetExpression);
		targetName.setName(Utils.getNewElementName(targetName.getName()));
		AuxiliarNode target = null;
		if (Utils.isNumericalName(targetName.getName())) {
			target = this.model.getNumericalAuxiliarNodeByName(targetName);
		}
		else {
			target = this.model.getAuxiliarNodeByName(targetName);
		}
		if (target != null) {
			this.target = target;
		}		
	}

	public AuxiliarNode getSource() {
		return source;
	}


	public AuxiliarNode getTarget() {
		return target;
	}


	
	
}
