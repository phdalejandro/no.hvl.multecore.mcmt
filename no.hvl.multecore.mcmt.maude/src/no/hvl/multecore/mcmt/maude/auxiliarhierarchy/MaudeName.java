package no.hvl.multecore.mcmt.maude.auxiliarhierarchy;

public class MaudeName {
	
	int level;
	String name;
	
	public MaudeName(int level, String name) {
		this.level = level;
		this.name = name;
	}
	
	public MaudeName() {
	}

	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public void createNameFromMaudeExpression (String expression) {
		if (expression.startsWith("id(")) {
			//Remove id(
			expression = expression.substring(3);
			//Remove the last ")
			expression = expression.substring(0, expression.length()-1);
			String[] split = expression.split(",");
			level = Integer.valueOf(split[0]); 
			name = split[1];
		}
		else {
			System.out.println("This is not a valid id()");
		}
	}
	
	public void createAttributeFromMaudeExpression (String expression) {
		if (expression.startsWith("id(")) {
			//Remove id(
			expression = expression.substring(3);
			//Remove the last ")
			expression = expression.substring(0, expression.length()-1);
			String[] split = expression.split(",");
			level = Integer.valueOf(split[0]); 
			name = split[1];
		}
		else {
			name = expression;
			level = 0;
		}
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + level;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MaudeName other = (MaudeName) obj;
		if (level != other.level)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	
}
