package no.hvl.multecore.mcmt.maude.auxiliarhierarchy;

public class AuxiliarElement {
	
	protected String id;
	protected MaudeName name;
	protected MaudeName type;
	protected AuxiliarModel model;
	public AuxiliarElement() {
		
	}
	

	public AuxiliarElement(String name, String type, AuxiliarModel model) {
		this.name = new MaudeName();
		this.name.createNameFromMaudeExpression(name);
		this.type = new MaudeName();
		this.type.createNameFromMaudeExpression(type);
		this.model = model;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public MaudeName getName() {
		return name;
	}

	public MaudeName getType() {
		return type;
	}
	
	public boolean isNumericName () {
		boolean isNumeric = true;
		String removedPotentialWhiteSpaces = this.name.getName().replaceAll(" ", "");
        try {
            Integer num = Integer.parseInt(removedPotentialWhiteSpaces);
        } catch (NumberFormatException e) {
        	isNumeric = false;
        }
        this.name.setName(removedPotentialWhiteSpaces);
        return isNumeric;
	}
}
