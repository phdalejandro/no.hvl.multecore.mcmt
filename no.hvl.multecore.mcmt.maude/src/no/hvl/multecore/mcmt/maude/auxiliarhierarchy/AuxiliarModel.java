package no.hvl.multecore.mcmt.maude.auxiliarhierarchy;

import java.util.ArrayList;
import java.util.List;

public class AuxiliarModel {
	
	protected String name;
	protected int level;
	protected String om;
	
	protected List<AuxiliarNode> nodes;
	protected List<AuxiliarEdge> edges;
	
	public AuxiliarModel() {
		nodes = new ArrayList<AuxiliarNode>();
		edges = new ArrayList<AuxiliarEdge>();
	}

	public AuxiliarModel(String name, int level, String om) {
		this.name = name;
		this.level = level;
		this.om = om;
		nodes = new ArrayList<AuxiliarNode>();
		edges = new ArrayList<AuxiliarEdge>();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getOm() {
		return om;
	}
	public void setOm(String om) {
		this.om = om;
	}
	
	public void addAuxiliarNode (AuxiliarNode node) {
		nodes.add(node);
	}

	public void addAuxiliarEdge (AuxiliarEdge edge) {
		edges.add(edge);
	}	
	
	public AuxiliarNode getAuxiliarNodeByName (MaudeName name) {
		boolean found = false;
		AuxiliarNode node = null;
		for (int i =0; i < this.nodes.size() && !found; i ++) {
			if (nodes.get(i).getName().equals(name)){
				found = true;
				node = this.nodes.get(i);
			}
		}
		return node;
	}
	
	public AuxiliarNode getNumericalAuxiliarNodeByName (MaudeName name) {
		boolean found = false;
		AuxiliarNode node = null;
		for (int i =0; i < this.nodes.size() && !found; i ++) {
			if (nodes.get(i).getName().equals(name) || nodes.get(i).getName().getName().contains(name.getName())){
				found = true;
				node = this.nodes.get(i);
			}
		}
		return node;
	}
}
