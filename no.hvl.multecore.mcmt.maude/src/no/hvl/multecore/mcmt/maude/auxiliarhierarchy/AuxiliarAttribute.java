package no.hvl.multecore.mcmt.maude.auxiliarhierarchy;

public class AuxiliarAttribute {

	
	private MaudeName nameOrValue;
	private MaudeName type;
	
	
	
	
	public AuxiliarAttribute() {
		
	}
	
	
	public AuxiliarAttribute(String nameOrValue, String type) {
		this.nameOrValue = new MaudeName();
		this.nameOrValue.createAttributeFromMaudeExpression(nameOrValue);
		this.type = new MaudeName();
		this.type.createAttributeFromMaudeExpression(type);
	}
	
	public MaudeName getNameOrValue() {
		return nameOrValue;
	}
	
	public MaudeName getType() {
		return type;
	}
	
	
}
