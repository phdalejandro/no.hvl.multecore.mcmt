package no.hvl.multecore.mcmt.maude.auxiliarhierarchy;

import java.util.ArrayList;
import java.util.List;

public class AuxiliarNode extends AuxiliarElement {

	List<AuxiliarAttribute> attributes;
	
	public AuxiliarNode() {
		super();
		new ArrayList<AuxiliarAttribute>();
	}

	
	public AuxiliarNode(String name, String type, AuxiliarModel model) {
		super(name, type, model);
		attributes = new ArrayList<AuxiliarAttribute>();

	}
	
	public void addNewAttribute (AuxiliarAttribute attribute) {
		this.attributes.add(attribute);
	}
	
	
}
