package no.hvl.multecore.mcmt.maude.auxiliarhierarchy;

import java.util.ArrayList;
import java.util.List;

public class AuxiliarHierarchy {
	
	protected List <AuxiliarModel> models;
	
	public AuxiliarHierarchy() {
		models = new ArrayList<AuxiliarModel>();	
	}
	
	public void addModel (AuxiliarModel model) {
		models.add(model);
	}
	
	public AuxiliarModel getmodelByLevel(int level) {
		boolean found = false;
		AuxiliarModel model = null;
		for (int i = 0; i < models.size() && !found; i ++) {
			if (models.get(i).getLevel() == level){
				found = true;
				model = models.get(i);
			}
		}
		return model;
	}
	
}
