package no.hvl.multecore.mcmt.maude.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

import no.hvl.multecore.mcmt.proliferation.rule.RuleHierarchy;



public class SelectSequenceOfRulesPage extends WizardPage implements Listener{

    private Composite container;
	private org.eclipse.swt.widgets.List availableRulesList;
	private org.eclipse.swt.widgets.List selectedRulesList;
	private List<String> auxiliarSelectedRulesList;
	private Button addButton;
	private Button removeButton;	
	private Button upButton;
	private Button downButton;		
	
	protected List<RuleHierarchy> sequenceOfRules;

	protected Set<RuleHierarchy> mcmtRulesAvailable;
	
    public SelectSequenceOfRulesPage(Set<RuleHierarchy> mcmtRulesAvailable, List<RuleHierarchy> sequenceOfRules) {
        super("First Page");
        setTitle("Select the sequence of rules to be applied to the instance model");
        setDescription("");
		this.mcmtRulesAvailable = mcmtRulesAvailable;
		this.sequenceOfRules = sequenceOfRules;
		auxiliarSelectedRulesList = new ArrayList<String>();
    }

	@Override
	public void createControl(Composite parent) {
        container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        layout.numColumns = 4;
        container.setLayout(layout);		
        
        //First label for available rules
        Label label1 = new Label(container, SWT.NONE);
        label1.setText("Available MCMT rules:");
        
        // Empty column
        Label emptyLabel = new Label(container, SWT.LEAD);
        
        //Second label for MCMTs
        Label label2 = new Label(container, SWT.NONE);
        label2.setText("Selected MCMT rules:");
        
        // Empty column
        Label emptyLabel2 = new Label(container, SWT.LEAD);     
        
        // Empty column
        Label emptyLabel3 = new Label(container, SWT.LEAD);   
        
        addButton = new Button(container, SWT.PUSH);
        addButton.setText("Add Rule");
        addButton.setAlignment(SWT.CENTER);
        addButton.setEnabled(false);
        addButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        addButton.addListener(SWT.Selection, this);             
        
        Label emptyLabel4 = new Label(container, SWT.LEAD);       
        
        upButton = new Button(container, SWT.PUSH);
        upButton.setText("Move up");
        upButton.setAlignment(SWT.CENTER);
        upButton.setEnabled(false);
        upButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        upButton.addListener(SWT.Selection, this);              
        
        // Available rules and its grid specification
        availableRulesList = new org.eclipse.swt.widgets.List(container, SWT.BORDER | SWT.SINGLE);
        GridData availableRulesGrid = new GridData();
        availableRulesGrid.horizontalAlignment = GridData.BEGINNING;
        availableRulesGrid.heightHint = 400;
        availableRulesGrid.widthHint = 300;
        availableRulesGrid.grabExcessVerticalSpace = true;
        availableRulesGrid.grabExcessHorizontalSpace = true;
        availableRulesList.setLayoutData(availableRulesGrid);
        availableRulesList.addListener(SWT.Selection, this);
        
        for (RuleHierarchy ruleHierarchy : mcmtRulesAvailable) {
			availableRulesList.add(ruleHierarchy.getName());
		}

        removeButton = new Button(container, SWT.PUSH);
        removeButton.setText("Remove Rule");
        removeButton.setAlignment(SWT.CENTER);
        removeButton.setEnabled(false);
        removeButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        removeButton.addListener(SWT.Selection, this);     
        
        //Selected rules and its grid specification
        selectedRulesList = new org.eclipse.swt.widgets.List(container, SWT.BORDER | SWT.SINGLE);
        GridData selectedRulesGrid = new GridData();
        selectedRulesGrid.horizontalAlignment = GridData.BEGINNING;
        selectedRulesGrid.heightHint = 400;
        selectedRulesGrid.widthHint = 300;
        selectedRulesGrid.grabExcessVerticalSpace = true;
        selectedRulesGrid.grabExcessHorizontalSpace = true;
        selectedRulesList.setLayoutData(selectedRulesGrid);
        selectedRulesList.addListener(SWT.Selection, this); 
        
        downButton = new Button(container, SWT.PUSH);
        downButton.setText("Move down");
        downButton.setAlignment(SWT.CENTER);
        downButton.setEnabled(false);
        downButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        downButton.addListener(SWT.Selection, this);
        
        setControl(container);
        setPageComplete(false);
	}

	@Override
	public void handleEvent(Event event) {
		if (event.widget == availableRulesList) {
			if (availableRulesList.getSelection().length == 1) {
				addButton.setEnabled(true);
			}
		}	
		if (event.widget == addButton) {
			selectedRulesList.add(availableRulesList.getSelection()[0]);
			auxiliarSelectedRulesList.add(availableRulesList.getSelection()[0]);
			for (RuleHierarchy ruleHierarchy : mcmtRulesAvailable) {
				if(ruleHierarchy.getName().equals(availableRulesList.getSelection()[0])) {
					sequenceOfRules.add(ruleHierarchy);
				}
			}
			selectedRulesList.redraw();
		}
		
		if (event.widget == selectedRulesList) {
			if (availableRulesList.getSelection().length == 1) {
				removeButton.setEnabled(true);
				if (selectedRulesList.getItemCount() > 1) {
					if(selectedRulesList.getSelectionIndex() < selectedRulesList.getItemCount()-1) {
						downButton.setEnabled(true);
					}
					else {
						downButton.setEnabled(false);
					}
					if(selectedRulesList.getSelectionIndex() > 0) {
						upButton.setEnabled(true);
					}
					else {
						upButton.setEnabled(false);
					}
				}
			}
			if (selectedRulesList.getItemCount() >= 1) {
		        setPageComplete(true);
			}
			else {
				setPageComplete(false);
			}
		}		
		
		if (event.widget == removeButton) {
			sequenceOfRules.remove(selectedRulesList.getSelectionIndex());
			auxiliarSelectedRulesList.remove(selectedRulesList.getSelectionIndex());
			selectedRulesList.remove(selectedRulesList.getSelectionIndex());
			selectedRulesList.redraw();
			if (selectedRulesList.getItemCount() <= 1) {
				upButton.setEnabled(false);
				downButton.setEnabled(false);
			}
		}
		
		if (event.widget == downButton) {
			Collections.swap(auxiliarSelectedRulesList, selectedRulesList.getSelectionIndex(), selectedRulesList.getSelectionIndex()+1);
			selectedRulesList.removeAll();
			for (int i = 0; i < auxiliarSelectedRulesList.size(); i++) {
				selectedRulesList.add(auxiliarSelectedRulesList.get(i));
			}
			selectedRulesList.redraw();
		}	
		if (event.widget == upButton) {
			Collections.swap(auxiliarSelectedRulesList, selectedRulesList.getSelectionIndex(), selectedRulesList.getSelectionIndex()-1);
			selectedRulesList.removeAll();
			for (int i = 0; i < auxiliarSelectedRulesList.size(); i++) {
				selectedRulesList.add(auxiliarSelectedRulesList.get(i));
			}
			selectedRulesList.redraw();
		}
	}

}
