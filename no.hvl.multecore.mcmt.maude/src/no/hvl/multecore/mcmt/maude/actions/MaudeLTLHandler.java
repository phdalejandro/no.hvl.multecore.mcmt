package no.hvl.multecore.mcmt.maude.actions;



import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;


import no.hvl.multecore.common.MultEcoreManager;
import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.MultilevelHierarchy;
import no.hvl.multecore.mcmt.maude.Constants;
import no.hvl.multecore.mcmt.maude.MaudeManager;
import no.hvl.multecore.mcmt.maude.wrapper.MaudeProcess;
import no.hvl.multecore.mcmt.proliferation.rule.RuleHierarchy;

public class MaudeLTLHandler extends AbstractHandler {

	private static MaudeProcess maudeProcess = MaudeProcess.instance();
	Shell shell;
	protected List<String> ltlFormulaeList;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		if (!maudeProcess.isRunning()) {
			no.hvl.multecore.common.Utils.showPopup("An error occurred", "The Maude Process is not yet running.");
		}
		
		else {
			MaudeManager maudeManager = MaudeManager.instance();
			MultEcoreManager multEcoreManager = MultEcoreManager.instance();
			ltlFormulaeList = new ArrayList<String> ();

			//TODO
			//Not sure if this is the right way.
			//Could this be wrong if we are editing some model but another project is selected in the Project Explorer?
			IProject multilevelHierarchyProject = multEcoreManager.getCurrentlySelectedProject();
			MultilevelHierarchy multilevelHierarchy = multEcoreManager.getMultilevelHierarchy(multilevelHierarchyProject);
			Set<RuleHierarchy> mcmtRulesAvailable = maudeManager.getRulesFromSelectedMultilevelHierarchy(multilevelHierarchy);
			if (null != mcmtRulesAvailable) {
				
				WizardDialog dialog = new WizardDialog(shell, new MaudeLTLWizard(ltlFormulaeList));
				dialog.open();
				String command = "";
				System.out.println(ltlFormulaeList);
			    List<String> newLtlXmlFileNames = new ArrayList<String> ();
				//Temporal solution, just create the XML's, but not create any MultEcore model
				for (int i = 0; i < ltlFormulaeList.size(); i++) {
				    JFrame frame2 = new JFrame("Name of new model state");
				    String numberOfProperty = String.valueOf(i+1);
				    String fileName = JOptionPane.showInputDialog(frame2, "Introduce the name of the new model state corresponding to property number " + numberOfProperty);							
					String newLtlXmlFileName = fileName + ".xml";
					command += "erew run(\"" + maudeProcess.getMaudeOutputsPath() + newLtlXmlFileName + "\", ";
					command += "xml("
							+ "downTerm("
								+ "getTerm"
									+ "(metaReduce([" + "'" +  getMetaModelInLevel(multilevelHierarchy.getAllModels(),1).getName().toUpperCase() + "],"
										+ "upTerm(modelCheck(MLM,"
											+ ltlFormulaeList.get(i) + ")))),"
								+ "false)))" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
					newLtlXmlFileNames.add(newLtlXmlFileName);
					
				}
				

			    maudeProcess.createMaudeJob(command, Constants.MAUDE_CURRENT_PROCESS_LTL, newLtlXmlFileNames, ltlFormulaeList.size());
			    maudeProcess.sendToMaude(maudeProcess.getMaudeJob().getCommandToBeExecuted());
				//TODO The nat number should be also parameterisable!
//				String dateTime = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").format(new Date());
//				String newStateXmlFileName = maudeProcess.getModelNameAssignedToProcess() + "_" + dateTime + ".xml";
//				String command = "erew run(\"" + maudeProcess.getMaudeOutputsPath() + newStateXmlFileName + "\", ";
//				command+= "xml(downTerm(getTerm(metaSrewrite(upModule(" + "'" + 
//			    		getMetaModelInLevel(multilevelHierarchy.getAllModels(),1).getName().toUpperCase() + "," + Constants.BREAK_LINE;
//				 command+= "false), 'MLM.System," + Constants.BREAK_LINE;
//				if (sequenceOfRules.size() >= 1) {
//					for (int i = 0; i < sequenceOfRules.size(); i++) {
//						command += "top('" + sequenceOfRules.get(i).getName() + "[none]{empty})";
//						command += " ;" + Constants.BREAK_LINE;
//					}
//					command = command.substring(0, command.lastIndexOf(";"));
//					command += "," + Constants.BREAK_LINE;
//					//TODO Consider make the fourth parameter parameterisable (breadthFirst)
//					command += "breadthFirst," + Constants.BREAK_LINE;
//					command += "0))," + Constants.BREAK_LINE;
//					command += "{none})))" + Constants.MAUDE_END_LINE;
//					
//				    maudeProcess.createMaudeJob(command, Constants.MAUDE_CURRENT_PROCESS_REW, newStateXmlFileName);
//				    maudeProcess.sendToMaude(maudeProcess.getMaudeJob().getCommandToBeExecuted());
//				    
//				}
//				else {
//					no.hvl.multecore.common.Utils.showPopup("An error occurred", "You must have selected at least 1 rule to perform this operation");
//				}
			}
			else {
				no.hvl.multecore.common.Utils.showPopup("An error occurred", "The selected project does not contain MCMTs rules or these have not been loaded");
			}
		}
		
		return null;
	}


	//TODO Used also in the rew other operation, refactor up
	private IModel getMetaModelInLevel(Set<IModel> models,  int level) {
		for (IModel iModel : models) {
			if (iModel.getLevel() == level)
				return iModel;
		}
		return null;
	}

	
}
