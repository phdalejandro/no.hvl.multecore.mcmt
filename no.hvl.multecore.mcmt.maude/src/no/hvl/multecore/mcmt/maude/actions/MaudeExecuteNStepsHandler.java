package no.hvl.multecore.mcmt.maude.actions;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;

import no.hvl.multecore.common.MultEcoreManager;
import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.MultilevelHierarchy;
import no.hvl.multecore.mcmt.maude.Constants;
import no.hvl.multecore.mcmt.maude.wrapper.MaudeProcess;

public class MaudeExecuteNStepsHandler extends AbstractHandler {

	private static MaudeProcess maudeProcess = MaudeProcess.instance();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!maudeProcess.isRunning()) {
			no.hvl.multecore.common.Utils.showPopup("An error occurred", "The Maude Process is not yet running.");
		}
		else {
			MultEcoreManager multEcoreManager = MultEcoreManager.instance();
		    JFrame frame = new JFrame("Number of steps");
		    String numberOfSteps= JOptionPane.showInputDialog(frame, "Introduce the number of steps to be performed");

		    
		    JFrame frame2 = new JFrame("Name of new model state");
		    String fileName = JOptionPane.showInputDialog(frame2, "Introduce the name of the new model state");		    
			//TODO
			//Not sure if this is the right way.
			//Could this be wrong if we are editing some model but another project is selected in the Project Explorer?
			IProject multilevelHierarchyProject = multEcoreManager.getCurrentlySelectedProject();
			MultilevelHierarchy multilevelHierarchy = multEcoreManager.getMultilevelHierarchy(multilevelHierarchyProject);
		    
//			String dateTime = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").format(new Date());
//		    String newStateXmlFileName = maudeProcess.getModelNameAssignedToProcess() + "_" + dateTime + ".xml";
			String newStateXmlFileName = fileName + ".xml";
		    String command = "erew run(\"" + maudeProcess.getMaudeOutputsPath() + newStateXmlFileName + "\", ";
		    command+= "xml(downTerm(getTerm(metaRewrite(upModule(" + "'" + 
		    		getMetaModelInLevel(multilevelHierarchy.getAllModels(),1).getName().toUpperCase() + ", ";
		    command+= "false), 'MLM.System,  ";
		    command += numberOfSteps + ")), {none}))) .";
		    
		    List<String> newStateXmlFileNames = new ArrayList<String> ();
		    newStateXmlFileNames.add(newStateXmlFileName);
		    maudeProcess.createMaudeJob(command, Constants.MAUDE_CURRENT_PROCESS_REW, newStateXmlFileNames);
		    maudeProcess.sendToMaude(maudeProcess.getMaudeJob().getCommandToBeExecuted());
		}
		return null;
	}
	
	
	//TODO Used also in the other srew operation, refactor up
	private IModel getMetaModelInLevel(Set<IModel> models,  int level) {
		for (IModel iModel : models) {
			if (iModel.getLevel() == level)
				return iModel;
		}
		return null;
	}

}
