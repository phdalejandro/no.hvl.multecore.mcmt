package no.hvl.multecore.mcmt.maude.actions;



import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;


import no.hvl.multecore.common.MultEcoreManager;
import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.MultilevelHierarchy;
import no.hvl.multecore.mcmt.maude.Constants;
import no.hvl.multecore.mcmt.maude.MaudeManager;
import no.hvl.multecore.mcmt.maude.wrapper.MaudeProcess;
import no.hvl.multecore.mcmt.proliferation.rule.RuleHierarchy;

public class MaudeSecuentialExecutionHandler extends AbstractHandler {

	private static MaudeProcess maudeProcess = MaudeProcess.instance();
	Shell shell;
	protected List<RuleHierarchy> sequenceOfRules;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		if (!maudeProcess.isRunning()) {
			no.hvl.multecore.common.Utils.showPopup("An error occurred", "The Maude Process is not yet running.");
		}
		else {
			MaudeManager maudeManager = MaudeManager.instance();
			MultEcoreManager multEcoreManager = MultEcoreManager.instance();
	        sequenceOfRules = new ArrayList<RuleHierarchy> ();

	        
		    JFrame frame2 = new JFrame("Name of new model state");
		    String fileName = JOptionPane.showInputDialog(frame2, "Introduce the name of the new model state");		    	        
			//TODO
			//Not sure if this is the right way.
			//Could this be wrong if we are editing some model but another project is selected in the Project Explorer?
			IProject multilevelHierarchyProject = multEcoreManager.getCurrentlySelectedProject();
			MultilevelHierarchy multilevelHierarchy = multEcoreManager.getMultilevelHierarchy(multilevelHierarchyProject);
			Set<RuleHierarchy> mcmtRulesAvailable = maudeManager.getRulesFromSelectedMultilevelHierarchy(multilevelHierarchy);
			if (null != mcmtRulesAvailable) {
				
				WizardDialog dialog = new WizardDialog(shell, new MaudeSecuentialExecutionWizard(mcmtRulesAvailable, sequenceOfRules));
				dialog.open();
				//TODO The nat number should be also parameterisable!
				String newStateXmlFileName =  fileName + ".xml";
				String command = "erew run(\"" + maudeProcess.getMaudeOutputsPath() + newStateXmlFileName + "\", ";
				command+= "xml(downTerm(getTerm(metaSrewrite(upModule(" + "'" + 
			    		getMetaModelInLevel(multilevelHierarchy.getAllModels(),1).getName().toUpperCase() + "," + Constants.BREAK_LINE;
				 command+= "false), 'MLM.System," + Constants.BREAK_LINE;
				if (sequenceOfRules.size() >= 1) {
					for (int i = 0; i < sequenceOfRules.size(); i++) {
						command += "top('" + sequenceOfRules.get(i).getName() + "[none]{empty})";
						command += " ;" + Constants.BREAK_LINE;
					}
					command = command.substring(0, command.lastIndexOf(";"));
					command += "," + Constants.BREAK_LINE;
					//TODO Consider make the fourth parameter parameterisable (breadthFirst)
					command += "breadthFirst," + Constants.BREAK_LINE;
					command += "0))," + Constants.BREAK_LINE;
					command += "{none})))" + Constants.MAUDE_END_LINE;
					
				    List<String> newStateXmlFileNames = new ArrayList<String> ();
				    newStateXmlFileNames.add(newStateXmlFileName);
				    maudeProcess.createMaudeJob(command, Constants.MAUDE_CURRENT_PROCESS_REW, newStateXmlFileNames);
				    maudeProcess.sendToMaude(maudeProcess.getMaudeJob().getCommandToBeExecuted());
				    
				}
				else {
					no.hvl.multecore.common.Utils.showPopup("An error occurred", "You must have selected at least 1 rule to perform this operation");
				}
			}
			else {
				no.hvl.multecore.common.Utils.showPopup("An error occurred", "The selected project does not contain MCMTs rules or these have not been loaded");
			}
		}
		
		return null;
	}


	//TODO Used also in the rew other operation, refactor up
	private IModel getMetaModelInLevel(Set<IModel> models,  int level) {
		for (IModel iModel : models) {
			if (iModel.getLevel() == level)
				return iModel;
		}
		return null;
	}

	
}
