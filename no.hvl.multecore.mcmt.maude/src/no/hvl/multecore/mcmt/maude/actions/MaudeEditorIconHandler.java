package no.hvl.multecore.mcmt.maude.actions;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.internal.resources.File;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

import no.hvl.multecore.common.MultEcoreManager;
import no.hvl.multecore.common.hierarchy.MultilevelHierarchy;
import no.hvl.multecore.mcmt.maude.Constants;
import no.hvl.multecore.mcmt.maude.MaudeManager;
import no.hvl.multecore.mcmt.maude.Utils;
import no.hvl.multecore.mcmt.maude.wrapper.MaudeProcess;

public class MaudeEditorIconHandler extends AbstractHandler {

	private static MaudeProcess maudeProcess = MaudeProcess.instance();

	@SuppressWarnings("restriction")
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		MultEcoreManager multEcoreManager = MultEcoreManager.instance();
		MaudeManager maudeManager = MaudeManager.instance();

		
		ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
                Display.getDefault().getActiveShell(),
                new WorkbenchLabelProvider(),
                new BaseWorkbenchContentProvider());
		dialog.addFilter(new OpenedProjectFilter("maude"));
		dialog.setTitle("Select the maude file containing the MCMTs");
		dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
		dialog.open();
		File selectedMCMTsMaudeResourceFile = (File) dialog.getFirstResult();
		IPath selectedMCMTsMaudeFileAbsolutePath = selectedMCMTsMaudeResourceFile.getLocation();
		
		String absoluteDirectoryPath = Utils.getRelativeDirectoryFromFile (selectedMCMTsMaudeFileAbsolutePath.toString());
		
		dialog.setTitle("Select the maude file containing the hierarchy");
		dialog.open();
		File selectedHierarchyMaudeResourceFile = (File) dialog.getFirstResult();
	
		java.io.File coreMaudeFile =  new java.io.File(absoluteDirectoryPath + Constants.MAUDE_FILE_CORE);
		java.io.File twoTupleMaudeFile =  new java.io.File(absoluteDirectoryPath + Constants.MAUDE_FILE_2_TUPLE);
		java.io.File threeTupleMaudeFile = new java.io.File(absoluteDirectoryPath + Constants.MAUDE_FILE_3_TUPLE);
		java.io.File mlmOclMaudeFile = new java.io.File(absoluteDirectoryPath + Constants.MAUDE_FILE_MLM_OCL);
		java.io.File xmlMaudeFile = new java.io.File(absoluteDirectoryPath + Constants.MAUDE_FILE_XML);
		
		java.io.File selectedMCMTsMaudeFile = new java.io.File(selectedMCMTsMaudeResourceFile.getLocation().toString());
		java.io.File selectedHierarchyMaudeFile = new java.io.File(selectedHierarchyMaudeResourceFile.getLocation().toString());
		
		String representation = "";
		
		if (!maudeProcess.isRunning()) {
			try {
				maudeProcess.execMaude();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		List <java.io.File> filesToBeProcessed = new ArrayList<>();
		filesToBeProcessed.add(twoTupleMaudeFile);
		filesToBeProcessed.add(threeTupleMaudeFile);
//		filesToBeProcessed.add(coreMaudeFile);
		filesToBeProcessed.add(mlmOclMaudeFile);
		filesToBeProcessed.add(selectedMCMTsMaudeFile);
		filesToBeProcessed.add(selectedHierarchyMaudeFile);
		filesToBeProcessed.add(xmlMaudeFile);
		

		//Until we have a definitive version, since mlm-ocl is being loaded in the middle of mlm-core file
		//I have to split mlm-core into two, and before load mlm-ocl is loaded, I have to load the actual file
		String parsedCoreMaudeFile = maudeProcess.processMaudeFile(coreMaudeFile);
		
		
		String[] splittedCoreMaude = Utils.splitFile(parsedCoreMaudeFile, "load mlm-ocl.maude");
		
		for (int i = 0; i < filesToBeProcessed.size(); i++) {
			if (filesToBeProcessed.get(i).equals(mlmOclMaudeFile)) {
				representation += splittedCoreMaude [0];
				representation += maudeProcess.processMaudeFile(filesToBeProcessed.get(i));
				representation += "load mlm-ocl.maude" + Constants.BREAK_LINE;
				representation += splittedCoreMaude [1];
			}
			else {
				representation += maudeProcess.processMaudeFile(filesToBeProcessed.get(i));
			}
		}
		maudeProcess.sendToMaude(representation);
		
		//TODO
		//Not sure if this is the right way.
		//Could this be wrong if we are editing some model but another project is selected in the Project Explorer?
		IProject multilevelHierarchyProject = multEcoreManager.getCurrentlySelectedProject();
		MultilevelHierarchy multilevelHierarchy = multEcoreManager.getMultilevelHierarchy(multilevelHierarchyProject);
		//Removing the initial "file:/"
		String projectPath = multilevelHierarchyProject.getLocationURI().toString().substring(6,multilevelHierarchyProject.getLocationURI().toString().length());
		projectPath += Constants.MAUDE_OUTPUTS_DIRECTORY;
		maudeProcess.setMaudeOutputsPath(projectPath);
		String modelName = maudeManager.getModelNameFromHierarchy(multilevelHierarchy);
		maudeProcess.setModelNameAssignedToProcess(modelName);
		
		no.hvl.multecore.common.Utils.showPopup("Maude Process", "Maude Process is now running and the configuration files have been properly loaded");
//		String result = maudeProcess.getResultFromMaude();
//		maudeProcess.printMaudeResultInMultEcoreConsole(result);
		return null;
	}

}
