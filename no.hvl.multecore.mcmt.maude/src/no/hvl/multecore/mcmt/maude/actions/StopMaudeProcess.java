package no.hvl.multecore.mcmt.maude.actions;


import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import no.hvl.multecore.mcmt.maude.wrapper.MaudeProcess;

public class StopMaudeProcess extends AbstractHandler {

	private static MaudeProcess maudeProcess = MaudeProcess.instance();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (null == maudeProcess || !maudeProcess.isRunning()) {
			no.hvl.multecore.common.Utils.showPopup("An error occurred", "The Maude Process is not yet running.");
		}
		else {
			maudeProcess.quitMaude();
			no.hvl.multecore.common.Utils.showPopup("Maude process stopped", "The current Maude process has been stopped. To initiate it again, right click on the project and select MultEcore > MultEcore to Maude");
		}
		return null;
	}

}
