package no.hvl.multecore.mcmt.maude.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

public class OpenedProjectFilter extends ViewerFilter {
	
	String fileExtension;
	
	public OpenedProjectFilter (String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public boolean select(Viewer viewer, Object parent, Object element) {

		if (element instanceof IFile) {
			return ((IFile) element).getName().toLowerCase().endsWith("." + fileExtension);
		}
		if (element instanceof IProject && !((IProject) element).isOpen()) {
			return false;
		}
		return true;
	}
}
