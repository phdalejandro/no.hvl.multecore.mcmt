package no.hvl.multecore.mcmt.maude.actions;

import java.util.List;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;


public class LtlPropertiesPage  extends WizardPage implements Listener{


	private Composite container;
    
    private List<String> ltlFormulaeList;
    
    private org.eclipse.swt.widgets.List selectedFormulaeList;
    
	private Button addButton;
	
	private Button removeButton;	
	
	private Text ltlProperty;

	protected LtlPropertiesPage(List<String> ltlFormulaeList) {
        super("First Page");
        setTitle("Write down the LTL properties to be checked");
        setDescription("");
        this.ltlFormulaeList = ltlFormulaeList;
	}

	@Override
	public void createControl(Composite parent) {
        container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        container.setLayout(layout);
        
        //First label for available rules
        Label label1 = new Label(container, SWT.NONE);
        label1.setText("Write the property to be checked");

        
        // Empty column
        Label emptyLabel = new Label(container, SWT.LEAD);
        
        ltlProperty = new Text(container, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
        GridData gridData = new GridData();
        gridData.horizontalAlignment = GridData.BEGINNING;
        gridData.grabExcessVerticalSpace = true;
        gridData.grabExcessHorizontalSpace = true;
        gridData.heightHint = 400;
        gridData.widthHint = 600;
        ltlProperty.setLayoutData(gridData);
        ltlProperty.addListener(SWT.Selection, this); 
        
        addButton = new Button(container, SWT.PUSH);
        addButton.setText("Add Property");
        addButton.setAlignment(SWT.CENTER);
        addButton.setEnabled(true);
        addButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        addButton.addListener(SWT.Selection, this);
        
        //First label for available rules
        Label label2 = new Label(container, SWT.NONE);
        label2.setText("Saved LTL properties that will be checked");
        
        // Empty column
        Label emptyLabel2 = new Label(container, SWT.LEAD);
        
        //Selected rules and its grid specification
        selectedFormulaeList = new org.eclipse.swt.widgets.List(container, SWT.BORDER | SWT.SINGLE);
        GridData selectedFormulaeGrid = new GridData();
        selectedFormulaeGrid.horizontalAlignment = GridData.BEGINNING;
        selectedFormulaeGrid.heightHint = 400;
        selectedFormulaeGrid.widthHint = 600;
        selectedFormulaeGrid.grabExcessVerticalSpace = true;
        selectedFormulaeGrid.grabExcessHorizontalSpace = true;
        selectedFormulaeList.setLayoutData(selectedFormulaeGrid);
        selectedFormulaeList.addListener(SWT.Selection, this); 
        
        removeButton = new Button(container, SWT.PUSH);
        removeButton.setText("Remove Property");
        removeButton.setAlignment(SWT.CENTER);
        removeButton.setEnabled(false);
        removeButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        removeButton.addListener(SWT.Selection, this);        
        
        setControl(container);
        setPageComplete(false);
	}

	@Override
	public void handleEvent(Event event) {
		if (event.widget == addButton) {
			if (!ltlFormulaeList.contains(ltlProperty.getText())) {
				selectedFormulaeList.add(ltlProperty.getText());
				ltlFormulaeList.add(ltlProperty.getText());
				ltlProperty.setText("");
				ltlProperty.redraw();
			}
			if (selectedFormulaeList.getItemCount()>0) {
				setPageComplete(true);
			}
		}
		
		if (event.widget == selectedFormulaeList) {
			if (selectedFormulaeList.getItemCount() > 0) {
				removeButton.setEnabled(true);
			}
		}
		
		if (event.widget == removeButton) {
			ltlFormulaeList.remove(selectedFormulaeList.getSelectionIndex());
			selectedFormulaeList.remove(selectedFormulaeList.getSelectionIndex());
			selectedFormulaeList.redraw();
			removeButton.setEnabled(false);
			if (selectedFormulaeList.getItemCount() == 0) {
				setPageComplete(false);
			}
		}
	}
}
