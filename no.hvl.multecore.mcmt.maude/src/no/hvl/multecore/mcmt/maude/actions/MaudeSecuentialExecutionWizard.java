package no.hvl.multecore.mcmt.maude.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.IWizardContainer;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import no.hvl.multecore.mcmt.amalgamation.McmtModuleHierarchyRules;
import no.hvl.multecore.mcmt.amalgamation.events.SelectionHierarchiesPage;
import no.hvl.multecore.mcmt.proliferation.rule.RuleHierarchy;

public class MaudeSecuentialExecutionWizard extends Wizard {
	
	
	protected SelectSequenceOfRulesPage selectSequenceOfRulesPage;

	protected List<RuleHierarchy> sequenceOfRules;
	
	protected Set<RuleHierarchy> mcmtRulesAvailable;
	
	
    public MaudeSecuentialExecutionWizard() {
        super();
        setNeedsProgressMonitor(true);
    }

	public MaudeSecuentialExecutionWizard(Set<RuleHierarchy> mcmtRulesAvailable,  List<RuleHierarchy> sequenceOfRules) {
        super();
        setNeedsProgressMonitor(true);
		this.mcmtRulesAvailable = mcmtRulesAvailable;
		this.sequenceOfRules = sequenceOfRules;
		}

	@Override
	public void addPages() {
		selectSequenceOfRulesPage = new SelectSequenceOfRulesPage(mcmtRulesAvailable, sequenceOfRules);
		addPage(this.selectSequenceOfRulesPage);
	}

//	@Override
//	public boolean canFinish() {
//		return false;
//	}
//
//	@Override
//	public void createPageControls(Composite pageContainer) {
//
//	}
//
//	@Override
//	public void dispose() {
//
//	}
//
//	@Override
//	public IWizardContainer getContainer() {
//		return null;
//	}
//
//	@Override
//	public Image getDefaultPageImage() {
//		return null;
//	}
//
//	@Override
//	public IDialogSettings getDialogSettings() {
//		return null;
//	}
//
//	@Override
//	public IWizardPage getNextPage(IWizardPage page) {
//		return null;
//	}
//
//	@Override
//	public IWizardPage getPage(String pageName) {
//		return null;
//	}
//
//	@Override
//	public int getPageCount() {
//		return 0;
//	}
//
//	@Override
//	public IWizardPage[] getPages() {
//		return null;
//	}
//
//	@Override
//	public IWizardPage getPreviousPage(IWizardPage page) {
//		return null;
//	}
//
//	@Override
//	public IWizardPage getStartingPage() {
//		return null;
//	}
//
//	@Override
//	public RGB getTitleBarColor() {
//		return null;
//	}

	@Override
	public String getWindowTitle() {
		return "Sequence of MCMT Rules";
	}
//
//	@Override
//	public boolean isHelpAvailable() {
//		return false;
//	}
//
//	@Override
//	public boolean needsPreviousAndNextButtons() {
//		return false;
//	}
//
//	@Override
//	public boolean needsProgressMonitor() {
//		return false;
//	}
//
//	@Override
//	public boolean performCancel() {
//		return false;
//	}

	@Override
	public boolean performFinish() {
		return true;
	}

//	@Override
//	public void setContainer(IWizardContainer wizardContainer) {
//
//	}

}
