package no.hvl.multecore.mcmt.maude;

import org.eclipse.core.internal.resources.File;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.ListDialog;
import org.eclipse.ui.dialogs.ListSelectionDialog;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

import no.hvl.multecore.common.Constants;
import no.hvl.multecore.common.MultEcoreManager;
import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.MultilevelHierarchy;
import no.hvl.multecore.mcmt.maude.actions.OpenedProjectFilter;
import no.hvl.multecore.mcmt.maude.wrapper.MaudeProcess;
import no.hvl.multecore.mcmt.proliferation.rule.RuleHierarchy;

public class MultEcore2Maude implements IObjectActionDelegate {
	//	Check clone
	Shell shell;
	
	private static MaudeProcess maudeProcess = MaudeProcess.instance();


	@SuppressWarnings("restriction")
	@Override
	public void run(IAction action) {
		
		if (null == maudeProcess) {
			maudeProcess = new MaudeProcess();
		}		
		
		// Step 1: Get all the models of the current hierarchy
		// Showing only the instance models, as they are the only candidates to be executed
		MultEcoreManager multEcoreManager = MultEcoreManager.instance();
		MultilevelHierarchy mh = multEcoreManager.getCurrentHierarchy();
		List<IModel> models = new ArrayList<IModel>();
		for (IModel model : mh.getAllModels()) {
			if (mh.getChildrenModels(model).isEmpty()) {
				models.add(model);
			}
		}
		// Step 2: Add all those models to a dialog
		ListDialog ld = new ListDialog(shell);
		ld.setAddCancelButton(true);
		ld.setContentProvider(new ArrayContentProvider());
		ld.setLabelProvider(new LabelProvider());
		ld.setInput(models.toArray());
		ld.setInitialSelections(models.toArray());
		ld.setTitle("Select the instance model to transform to Maude");
		ld.open();

		// Step 3: The selected model will be the bottom of the sub-hierarchy selected.
		// We also remove Ecore.
		// E.g.: If we select hammer_config of the PLS example, the selectedHierarchy
		// will look -> ["generic_plant", "hammer_plant", "hammer_config"]

		Object[] selection = ld.getResult();
		if (null == selection)
			return;
		List<IModel> selectedHierarchy = processSelection(selection);
		//The initial state is the selected model in the wizard
		maudeProcess.setModelNameAssignedToProcess(selectedHierarchy.get(selectedHierarchy.size()-1).getName());
		
		// Step 4: Create the maude directory
		// ResourceSetImpl resourceSet = new ResourceSetImpl();
		// EPackage.Registry packageRegistry = resourceSet.getPackageRegistry();
		
		String projectLocation = multEcoreManager.getProject(mh).getLocation().toString() + "//maude-outputs";
		java.io.File maudeDirectory = new java.io.File(projectLocation);
	    if (maudeDirectory.mkdir()) {
	    	System.out.println("Directory created succesfully");
	    }
	    else {
	    	System.out.println("Directory not created");
	    }
	    
	    String hierarchyUpperModelName = selectedHierarchy.get(selectedHierarchy.size()-1).getName();
		String hierarchyFileName = hierarchyUpperModelName + ".maude";
		hierarchyFileName = hierarchyFileName.toLowerCase();
		String mcmtsFileName = "generic-" + selectedHierarchy.get(1) + ".maude";
		mcmtsFileName = mcmtsFileName.toLowerCase();
		
		//Adding Ecore again to build the maude hierarchy
		MultEcore2MaudeOperations maudeOperations = new MultEcore2MaudeOperations();
		java.io.File twoTupleFile = maudeOperations.createFile(projectLocation, "2-tuple" + ".maude");
		java.io.File threeTupleFile = maudeOperations.createFile(projectLocation, "3-tuple" + ".maude");
		
		java.io.File mlmOclFile = maudeOperations.createFile(projectLocation, "mlm-ocl" + ".maude");
		java.io.File oclFile = maudeOperations.createFile(projectLocation, "ocl" + ".maude");
		java.io.File coreMlmFile = maudeOperations.createFile(projectLocation, "mlm-core" + ".maude");
		java.io.File unboxingFile = maudeOperations.createFile(projectLocation, "unboxing" + ".maude");
		
		java.io.File mcmtsFile = maudeOperations.createFile(projectLocation, mcmtsFileName);		
		java.io.File hierarchyFile = maudeOperations.createFile(projectLocation, hierarchyFileName);
		java.io.File runXmlFile = maudeOperations.createFile(projectLocation, "run-xml" + ".maude");
		
		java.io.File xmlFile = maudeOperations.createFile(projectLocation, "xml" + ".maude");
		java.io.File xmlOclFile = maudeOperations.createFile(projectLocation, "xml-ocl" + ".maude");
		java.io.File xmlSmlFile = maudeOperations.createFile(projectLocation, "xml-sml" + ".maude");
		
//		File mlmPredsFile = maudeOperations.createFile(projectLocation, "mlm-preds" + ".maude");
		
		java.io.File mlmSmlFile = maudeOperations.createFile(projectLocation, "mlm-sml" + ".maude");
		java.io.File smlFile = maudeOperations.createFile(projectLocation, "sml" + ".maude");
		
		java.io.File maudePreludeFile = maudeOperations.createFile(projectLocation, "maude-prelude" + ".maude");
		java.io.File mlmPreludeFile = maudeOperations.createFile(projectLocation, "mlm-prelude" + ".maude");
		java.io.File oclPreludeFile = maudeOperations.createFile(projectLocation, "ocl-prelude" + ".maude");
		java.io.File smlPreludeFile = maudeOperations.createFile(projectLocation, "sml-prelude" + ".maude");
		java.io.File languageDeclarations = null;
		
		twoTupleFile = maudeOperations.addTwoTupleBlock(twoTupleFile);
		threeTupleFile = maudeOperations.addThreeTupleBlock(threeTupleFile);
		mlmPreludeFile = maudeOperations.addMlmPreludeBlock(mlmPreludeFile);
		coreMlmFile = maudeOperations.addCoreBlock(coreMlmFile);
		maudePreludeFile = maudeOperations.addMaudePreludeBlock(maudePreludeFile);
		oclPreludeFile = maudeOperations.addOclPreludeBlock(oclPreludeFile);
		smlPreludeFile = maudeOperations.addSmlPreludeBlock(smlPreludeFile);		
		oclFile = maudeOperations.addOclBlock(oclFile);
		smlFile = maudeOperations.addSmlBlock(smlFile);
		unboxingFile = maudeOperations.addUnboxingBlock(unboxingFile);
		mlmOclFile = maudeOperations.addMlmOclBlock(mlmOclFile);
		mlmSmlFile = maudeOperations.addMlmSmlBlock(mlmSmlFile);		
//		mlmPredsFile = maudeOperations.addMlmPredsBlock(mlmPredsFile);
		xmlFile = maudeOperations.addXml(xmlFile);
		xmlOclFile = maudeOperations.addXmlOcl(xmlOclFile);
		xmlSmlFile = maudeOperations.addXmlSml(xmlSmlFile);
		runXmlFile = maudeOperations.addRunXml(hierarchyFileName, runXmlFile, selectedHierarchy);
		
		hierarchyFile = maudeOperations.addHierarchyBlock(selectedHierarchy, hierarchyFile);
		
		HashMap<String, RuleHierarchy> rulesLoaded = new HashMap<String, RuleHierarchy>();
		
		List<String> ruleNames = new ArrayList<String>();
		MaudeManager maudeManager = MaudeManager.instance();
		for (RuleHierarchy rule : maudeManager.getRules().values()) {
			ruleNames.add(rule.getName());
		} 
		
		// Ask which rules to be transformed
		ListSelectionDialog selectRules = new ListSelectionDialog(shell, ruleNames, ArrayContentProvider.getInstance(), new LabelProvider(), "MCMTs selection");
		selectRules.setTitle("Select the rules to be generated in Maude");
		selectRules.open();
		
		Object[] rulesSelected = selectRules.getResult();
		if (null == rulesSelected)
			return;
		
		String[] stringArray = Arrays.copyOf(rulesSelected, rulesSelected.length, String[].class);
		List<String> selectedRuleNames = new ArrayList<String>(Arrays.asList(stringArray));
		Set <RuleHierarchy> selectedRulesSet = new HashSet<RuleHierarchy>();
		maudeManager.getRules().forEach((k,v)->{
			if(selectedRuleNames.contains(v.getName())){
				rulesLoaded.put(k, v);
				selectedRulesSet.add(v);
			}
		});
		
		
		//Dialog to select the available languages. OCL is coming by default, and SML is the only one extra supported
		List<String> languagesAvailable = new ArrayList<String>();
//		languagesAvailable.add(no.hvl.multecore.mcmt.maude.Constants.OCL);
		languagesAvailable.add(no.hvl.multecore.mcmt.maude.Constants.SML);
		ListDialog selectLanguage = new ListDialog(shell);
		selectLanguage.setAddCancelButton(true);
		selectLanguage.setContentProvider(new ArrayContentProvider());
		selectLanguage.setLabelProvider(new LabelProvider());
		selectLanguage.setInput(languagesAvailable.toArray());
		selectLanguage.setTitle("Select whether you will use an additional programming language");
		selectLanguage.open();
		Object[] languagesSelected = selectLanguage.getResult();
		if (null == languagesSelected)
			return;		
		
		//If SML is selected, we have to request the declarations file
		String[] languagesArray = Arrays.copyOf(languagesSelected, languagesSelected.length, String[].class);
		List<String> selectLanguageNames = new ArrayList<String>(Arrays.asList(languagesArray));
		if (selectLanguageNames.size()==1) {
			maudeOperations.setAdditionalLanguage(selectLanguageNames.get(0).toLowerCase());
			maudeOperations.createFile(projectLocation, maudeOperations.getAdditionalLanguage() + "-declarations" + ".maude");
			ElementTreeSelectionDialog languageDeclarationsSelection = new ElementTreeSelectionDialog(
	                Display.getDefault().getActiveShell(),
	                new WorkbenchLabelProvider(),
	                new BaseWorkbenchContentProvider());
			languageDeclarationsSelection.addFilter(new OpenedProjectFilter("txt"));
			languageDeclarationsSelection.setTitle("Select the declarations txt file");
			languageDeclarationsSelection.setInput(ResourcesPlugin.getWorkspace().getRoot());
			languageDeclarationsSelection.open();
			File selectedSmlDeclarationsResourceFile = (File) languageDeclarationsSelection.getFirstResult();
			java.io.File selectedHierarchyMaudeFile = new java.io.File(selectedSmlDeclarationsResourceFile.getLocation().toString());
			languageDeclarations = maudeOperations.createFile(projectLocation, "sml-declarations" + ".maude");
			languageDeclarations = maudeOperations.processDeclarationsFile (selectedHierarchyMaudeFile, languageDeclarations);
		}
		else {
			no.hvl.multecore.common.Utils.showPopup("Languages selection", "You can only select 1 language");	
		}
		
		
		//Add the selected rules and the hierarchy they belong to to the manager (this will help when interacting with Maude from MultEcore.
		maudeManager.addSelectedRulesToMultilevelHierarchy(mh, selectedRulesSet);
		maudeManager.addModelBeingExecutedFromHierarchy(mh, hierarchyUpperModelName);
		//rulesLoaded.putAll(MaudeManager.instance().getRules());
		mcmtsFile = maudeOperations.addRulesBlock(rulesLoaded, mcmtsFile, selectedHierarchy);
		
		//System.out.println(hierarchyFile);


		//Here we now start the Maude Process
		String representation = "";
		if (!maudeProcess.isRunning()) {
			try {
				maudeProcess.execMaude();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else {
			maudeProcess.quitMaude();
			try {
				maudeProcess.execMaude();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		
		List <java.io.File> filesToBeProcessed = new ArrayList<>();
		filesToBeProcessed.add(twoTupleFile);
		filesToBeProcessed.add(threeTupleFile);
		filesToBeProcessed.add(mlmPreludeFile);
		filesToBeProcessed.add(coreMlmFile);
		filesToBeProcessed.add(maudePreludeFile);
		filesToBeProcessed.add(oclPreludeFile);
		filesToBeProcessed.add(smlPreludeFile);
		if (null != languageDeclarations) {
			filesToBeProcessed.add(languageDeclarations);
		}
		filesToBeProcessed.add(oclFile);
		filesToBeProcessed.add(smlFile);
		filesToBeProcessed.add(unboxingFile);
		filesToBeProcessed.add(mlmOclFile);
		filesToBeProcessed.add(mlmSmlFile);
//		filesToBeProcessed.add(mlmPredsFile);
		filesToBeProcessed.add(mcmtsFile);	
		filesToBeProcessed.add(hierarchyFile);
		filesToBeProcessed.add(xmlFile);
		filesToBeProcessed.add(xmlOclFile);
		filesToBeProcessed.add(xmlSmlFile);
		filesToBeProcessed.add(runXmlFile);

		
		
		for (int i = 0; i < filesToBeProcessed.size(); i++) {
			representation += maudeProcess.processMaudeFile(filesToBeProcessed.get(i));
		}
//		maudeProcess.sendToMaude(representation);
		
		
		//TODO
		//Not sure if this is the right way.
		//Could this be wrong if we are editing some model but another project is selected in the Project Explorer?
		IProject multilevelHierarchyProject = multEcoreManager.getCurrentlySelectedProject();
		//Removing the initial "file:/"
		String projectPath = multilevelHierarchyProject.getLocationURI().toString().substring(6,multilevelHierarchyProject.getLocationURI().toString().length());
		projectPath += no.hvl.multecore.mcmt.maude.Constants.MAUDE_OUTPUTS_DIRECTORY;
		maudeProcess.setMaudeOutputsPath(projectPath);

		
		no.hvl.multecore.common.Utils.showPopup("Maude Process", "Maude Process is now running and the configuration files have been properly loaded");
//		String result = maudeProcess.getResultFromMaude();
//		maudeProcess.printMaudeResultInMultEcoreConsole(result);		

	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {

	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {

	}
	
	

	@SuppressWarnings("unchecked")
	private List<IModel> processSelection(Object[] selection) {
		List<IModel> selectedHierarchy = new ArrayList<IModel>();

		Set<IModel> selectedModels = new HashSet<IModel>((Collection<? extends IModel>) Arrays.asList(selection));
		selectedHierarchy.add(selectedModels.iterator().next());
		Set<IModel> parents = selectedModels.iterator().next().getAllMetamodels();
		selectedHierarchy.addAll(parents);
		Collections.sort(selectedHierarchy);
		return selectedHierarchy;

	}
}
