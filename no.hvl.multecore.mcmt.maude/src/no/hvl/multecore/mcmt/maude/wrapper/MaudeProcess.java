package no.hvl.multecore.mcmt.maude.wrapper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;


//import es.upv.dsic.issi.moment.maudedaemon.ui.Messages;
//import es.upv.dsic.issi.moment.maudedaemon.ui.preferences.PreferenceConstants;
import no.hvl.multecore.core.events.LogView;
import no.hvl.multecore.mcmt.maude.Constants;

//import es.upv.dsic.issi.moment.maudedaemon.ui.console.ConsoleOutputView;

public class MaudeProcess {
	
	private static MaudeProcess MAUDE_PROCESS_INSTANCE = null;

	
	protected Runtime runTime;

	protected BufferedWriter  buffStdin;
	
//	protected BufferedReader buffStderr;
	
	protected BufferedReader buffStdout;
	
	protected boolean running;
	
	protected LogView logView;

	protected String maudeOutputsPath; 
	
	protected String modelNameAssignedToProcess;
	
	private ThreadGetOutput thGet;

	private Process maudeProcess = null;
	
	private MaudeJob maudeJob;
//	private String pathMaudeBin = ""; // Path of Maude's binary
//	private String pathFMaude = ""; // Path of Full Maude file
//	private String logFile = ""; // Path of the log file
	
	
	

//	private static final Logger log = Logger.getLogger(MaudeProcess.class.getName());
	
	
	
	// Console
//	private static ConsoleOutputView consoleOut;
	
	public MaudeProcess() {
		/* sets up command to exec */
		runTime = Runtime.getRuntime();
		logView = (LogView) no.hvl.multecore.common.Utils.getWindow().getActivePage().findView(LogView.ID);
		thGet = new ThreadGetOutput();
		thGet.start();
	}
	
	public static MaudeProcess instance() {
		if (null == MAUDE_PROCESS_INSTANCE)
			MAUDE_PROCESS_INSTANCE = new MaudeProcess();
		return MAUDE_PROCESS_INSTANCE;
	}
	
	public void setModelNameAssignedToProcess (String name) {
		this.modelNameAssignedToProcess = name;
	}
	
	public String getModelNameAssignedToProcess () {
		return this.modelNameAssignedToProcess;
	}
	
	public void setMaudeOutputsPath (String path) {
		this.maudeOutputsPath = path;
	}
	
	public String getMaudeOutputsPath () {
		return this.maudeOutputsPath;
	}
	
	public boolean isRunning() {
		return running;
	}
	
	public BufferedWriter getIn() {
		return buffStdin;
	}
	
//	public BufferedReader getErr() {
//		return buffStderr;
//	}
	
	public BufferedReader getOut() {
		return buffStdout;
	}
	
	public void createMaudeJob (String command, String type, List<String> fileNames) {
		this.maudeJob = new MaudeJob(command, type, fileNames);
	}
	
	public void createMaudeJob(String command, String maudeCurrentProcess, List<String> newLtlXmlFileNames, int size) {
		this.maudeJob = new MaudeJob(command, maudeCurrentProcess, newLtlXmlFileNames, size);
	}
	
	public void resetMaudeJob () {
		this.maudeJob = null;
	}
	
	public MaudeJob getMaudeJob () {
		return this.maudeJob;
	}
	

	public String processMaudeFile (File maudeFile) {
		String representation = "";
		
		Pattern pat = Pattern.compile(".*\\R|.+\\z");
		
		String line;
		try (Scanner in = new Scanner(Paths.get(maudeFile.toURI()), "UTF-8")) {
			while ((line = in.findWithinHorizon(pat, 0)) != null) {
				representation += line;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		representation += "\n\n";
		return representation;
	}

	public boolean execMaude() throws IOException {
		
		maudeProcess = runTime.exec("cmd.exe /c maude -trust");
		
		buffStdin = new BufferedWriter(new OutputStreamWriter(maudeProcess
				.getOutputStream()));

		buffStdout = new BufferedReader(new InputStreamReader(maudeProcess
				.getInputStream()));
		
//		buffStderr = new BufferedReader(new InputStreamReader(maudeProcess
//				.getErrorStream()));		
		
		if (null == thGet) {
			thGet = new ThreadGetOutput();
			thGet.start();
		}
		
		running = true;
		
		return running;

	}

	public void sendToMaude(String txt){
		try {
			buffStdin.write(txt+"\n");
			buffStdin.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getResultFromMaude() {
		String result = "";
		String line = "";
		try {
			while ((line = buffStdout.readLine()) != null) {
				result += line + "\n";
				//TODO This is a very bad and temporal solution.
				//I need to find a way to detect when the process has finished the job
				if (line.endsWith("Maude> ")) break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
		
	public void printMaudeResultInMultEcoreConsole (String result) {
		if (null != logView) {
			logView = (LogView) no.hvl.multecore.common.Utils.getWindow().getActivePage().findView(LogView.ID);
		}
		no.hvl.multecore.core.Utils.addtoMultEcoreConsole(result);
	}
	
	public void quitMaude () {
		try {
			sendToMaude("q");
			buffStdin.close();
			buffStdout.close();
//			buffStderr.close();
			maudeProcess.destroy();
			thGet = null;
			running = false;

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Thread to get the output of Maude and save it in the Job Object
	private class ThreadGetOutput extends Thread {
		
		public ThreadGetOutput() {
			super("Maude (get output)");
		}
		/**
		 * Run method Actions to do while the thread is active
		 */
		public synchronized void run() {
//			String line = "";
//			String result = "";
			char c;
			StringBuffer sbOut = new StringBuffer();
			try {
				while (true) {
					do {
						while (isRunning() && buffStdout.ready()) {
							c = (char)buffStdout.read();
							sbOut.append(c);
						}
					} while (!sbOut.toString().endsWith("\r\n"));
						
//					while ((line = buffStdout.readLine()) != null) {
//						result += line + "\n";
//						//TODO This is a very bad and temporal solution.
//						//I need to find a way to detect when the process has finished the job
//						if (line.endsWith("Maude> ")) break;
//					}
					if(!interrupted()) {
						printMaudeResultInMultEcoreConsole(sbOut.toString());
						sbOut.delete(0, sbOut.length()-1);
						if (null != maudeJob) {
							if (maudeJob.getCurrentProcessType().equals(Constants.MAUDE_CURRENT_PROCESS_REW)
									|| maudeJob.getCurrentProcessType().equals(Constants.MAUDE_CURRENT_PROCESS_SREW)) {
								maudeJob.xmlToMultEcoreRew();
							}
							else if (maudeJob.getCurrentProcessType().equals(Constants.MAUDE_CURRENT_PROCESS_LTL)) {
								if (maudeJob.getRemainingLtlProperties() > 0)
								maudeJob.xmlLtl();
							}							
						}
					} else {
						return;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				return;
			} 
		}
	}
}
