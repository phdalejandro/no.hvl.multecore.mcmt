package no.hvl.multecore.mcmt.maude.rules;

import java.util.HashSet;
import java.util.Set;

public class RuleBox {
	
	private int id;

	private String multiplicityVariable;

	// TODO We should have an abstract class that represent both types of edges for rules

	private Set<Entity> entities;
	
	private Set<Relation> relations;
	
	private Set <RuleBox> innerBoxes;
	
	private String boxesRepresentation;

	public RuleBox(int id, String multiplicityVariable) {
		this.id = id;
		this.multiplicityVariable = multiplicityVariable;
		this.entities = new HashSet<Entity>();
		this.relations = new HashSet<Relation>();
		this.innerBoxes = new HashSet<RuleBox>();
	}

	public String getMultiplicityVariable() {
		return multiplicityVariable;
	}

	public Set<Entity> getEntities() {
		return entities;
	}

	public Set<Relation> getRelations() {
		return relations;
	}

	public void addEntity(Entity entity) {
		this.entities.add(entity);
	}

	public void addRelation(Relation relation) {
		this.relations.add(relation);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Set<RuleBox> getInnerBoxes() {
		return innerBoxes;
	}

	public void addInnerBox(RuleBox box) {
		this.innerBoxes.add(box);
	}

	public Set <Element> getElementsExceptExcluded(Block from) {
		Set <Element> elementsExceptExcluded = new HashSet<Element>();
		elementsExceptExcluded.addAll(from.getElts());
		elementsExceptExcluded.addAll(from.getRels());
		elementsExceptExcluded.removeAll(this.getEntities());
		elementsExceptExcluded.removeAll(this.getRelations());
		return elementsExceptExcluded;
	}

	public String getBoxesRepresentation() {
		return boxesRepresentation;
	}

	public void setBoxesRepresentation(String boxesRepresentation) {
		this.boxesRepresentation = boxesRepresentation;
	}
	
	

}
