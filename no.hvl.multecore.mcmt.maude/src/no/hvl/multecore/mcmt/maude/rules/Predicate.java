package no.hvl.multecore.mcmt.maude.rules;

public class Predicate {


	private String actualType;
	private String startingLevel;
	private String topType;

	public Predicate() {
		
	}

	public Predicate(String actualType, String startingLevel, String topType) {
		this.actualType = actualType;
		this.startingLevel = startingLevel;
		this.topType = topType;
	}

	public String getActualType() {
		return actualType;
	}

	public void setActualType(String actualType) {
		this.actualType = actualType;
	}

	public String getStartingLevel() {
		return startingLevel;
	}

	public void setStartingLevel(String startingLevel) {
		this.startingLevel = startingLevel;
	}

	public String getTopType() {
		return topType;
	}

	public void setTopType(String topType) {
		this.topType = topType;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Predicate other = (Predicate) obj;
		if (actualType == null) {
			if (other.actualType != null)
				return false;
		} else if (!actualType.equals(other.actualType))
			return false;
		if (startingLevel == null) {
			if (other.startingLevel != null)
				return false;
		} else if (!startingLevel.equals(other.startingLevel))
			return false;
		if (topType == null) {
			if (other.topType != null)
				return false;
		} else if (!topType.equals(other.topType))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((actualType == null) ? 0 : actualType.hashCode());
		result = prime * result + ((startingLevel == null) ? 0 : startingLevel.hashCode());
		result = prime * result + ((topType == null) ? 0 : topType.hashCode());
		return result;
	}


}
