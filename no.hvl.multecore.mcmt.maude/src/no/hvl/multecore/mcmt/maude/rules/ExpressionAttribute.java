package no.hvl.multecore.mcmt.maude.rules;
import java.util.HashMap;

import no.hvl.multecore.mcmt.maude.Constants;
import no.hvl.multecore.mcmt.proliferation.rule.AttributesParser;
import no.hvl.multecore.mcmt.proliferation.rule.RuleInstantiatedAttribute;

public class ExpressionAttribute extends Attribute{
	

	public ExpressionAttribute() {
		super();
		variables = new HashMap<>();
	}
	
	
	
	public void parseExpressionAttribute (RuleInstantiatedAttribute ruleInstantiatedAttribute) {
		this.id = Constants.MAUDE_ID_OID + String.format("%02d", this.getNode().getBlock().getRule().getVariableCounter());
		this.attributeId = Constants.MAUDE_ID_ATTRIBUTE_SET + String.format("%02d", this.getNode().getBlock().getRule().getVariableCounter());
		this.getNode().getBlock().getRule().increaseVariableCounter();
		this.nameOrValue = AttributesParser.parseVariableExpression(ruleInstantiatedAttribute).getParsedExpression();
		ruleInstantiatedAttribute.getVariables().forEach((key, value) ->this.variables.put(key, value));
		ruleInstantiatedAttribute.getVariables()
        .forEach(
        		(key, value) -> {
                    if (!this.node.block.getIntVariables().contains(key) && !key.startsWith("eval")) {
                    	this.node.block.addIntVariable(key);
                    }
                }
        );		

		this.type = Constants.MAUDE_ID_ID_NEW + "(L" + ruleInstantiatedAttribute.getType().getContainingNode().getModel().getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE + "\""+ruleInstantiatedAttribute.getType().getNameOrValue()+"\")";
	}
	
	public void parseExistingExpressionAttribute (RuleInstantiatedAttribute ruleInstantiatedAttribute, ExpressionAttribute expressionAttribute) {
		this.id = expressionAttribute.getId();
		this.attributeId = expressionAttribute.getAttributeId();
		this.nameOrValue = AttributesParser.parseVariableExpression(ruleInstantiatedAttribute).getParsedExpression();
		ruleInstantiatedAttribute.getVariables().forEach((key, value) ->this.variables.put(key, value));
		ruleInstantiatedAttribute.getVariables()
        .forEach(
        		(key, value) -> {
                    if (!this.node.block.getIntVariables().contains(key) && !key.startsWith("eval")) {
                    	this.node.block.addIntVariable(key);
                    }
                }
        );
		this.type = Constants.MAUDE_ID_ID_NEW + "(L" + ruleInstantiatedAttribute.getType().getContainingNode().getModel().getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE + "\""+ruleInstantiatedAttribute.getType().getNameOrValue()+"\")";
	}
	
	public String toString() {
		String attString = "";
		attString = attString + Constants.TABULATION3 + Constants.TABULATION3 + Constants.MAUDE_OPEN_CONFIGURATION
				+ this.id + Constants.MAUDE_TYPE_SEPARATOR + Constants.MAUDE_ATTRI + Constants.MAUDE_VERTICAL_SEPARATOR;
		attString = attString + "nameOrValue" + Constants.MAUDE_TYPE_SEPARATOR + this.nameOrValue
				+ Constants.MAUDE_SEPARATE_ATTRIBUTE;
		attString = attString + "type" + Constants.MAUDE_TYPE_SEPARATOR + this.type
				+ Constants.MAUDE_SEPARATE_ATTRIBUTE;
		attString = attString + this.attributeId + Constants.MAUDE_CLOSE_CONFIGURATION + Constants.BREAK_LINE;
		return attString;
	}

	
}
