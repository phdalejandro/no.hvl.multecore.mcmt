package no.hvl.multecore.mcmt.maude.rules;

import java.util.HashMap;

public abstract class Attribute {
	
	protected String id;
	//Entire expression
	protected String nameOrValue;
	//Map with pairs of variable-type
	protected HashMap <String,String> variables;
	protected String type;
	protected String attributeId;
	protected Entity node;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNameOrValue() {
		return nameOrValue;
	}
	public void setNameOrValue(String nameOrValue) {
		this.nameOrValue = nameOrValue;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}
	public Entity getNode() {
		return node;
	}
	public void setNode(Entity node) {
		this.node = node;
	}
	public HashMap<String, String> getVariables() {
		return variables;
	}
	
}