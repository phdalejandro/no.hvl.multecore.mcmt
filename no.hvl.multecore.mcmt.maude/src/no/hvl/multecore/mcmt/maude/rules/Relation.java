package no.hvl.multecore.mcmt.maude.rules;

import no.hvl.multecore.common.hierarchy.EReferenceEdge;
import no.hvl.multecore.common.hierarchy.IEdge;
import no.hvl.multecore.mcmt.maude.Constants;
import no.hvl.multecore.mcmt.proliferation.rule.EdgeConstant;
import no.hvl.multecore.mcmt.proliferation.rule.EdgeVariable;

public class Relation extends Element {

	private Entity source;
	private Entity target;
	private String multiplicityVariable;
	
	public Relation() {
		super();
		multiplicityVariable = null;
	}

	public Entity getSource() {
		return source;
	}

	public void setSource(Entity source) {
		this.source = source;
	}

	public Entity getTarget() {
		return target;
	}

	public void setTarget(Entity target) {
		this.target = target;
	}

	public String getMultiplicityVariable() {
		return multiplicityVariable;
	}

	public void setMultiplicityVariable(String multiplicityVariable) {
		this.multiplicityVariable = multiplicityVariable;
	}

	public void parseRelation(IEdge edge) {
		this.id = Constants.MAUDE_ID_OID + String.format("%02d", this.getBlock().getRule().getVariableCounter());
		this.elementId = Constants.MAUDE_ID_ATTRIBUTE_SET
				+ String.format("%02d", this.getBlock().getRule().getVariableCounter());
		this.getBlock().getRule().increaseVariableCounter();

		if (edge instanceof EdgeConstant) {
			this.isConstant = true;
			this.name = Constants.MAUDE_ID_ID_NEW + "(L" + edge.getModel().getLevel()
					+ Constants.MAUDE_SEPARATE_ATTRIBUTE + "\"" + edge.getName() + "\")";
			// Using the level as a constant, actually this is not shown in the rule, since
			// it is a constant.
			this.type = Constants.MAUDE_ID_ID_NEW + "(" + edge.getType().getModel().getLevel()
					+ Constants.MAUDE_SEPARATE_ATTRIBUTE + "\"" + edge.getType().getName() + "\")";
		} else {
			this.isConstant = false;
			//Control the number of appearances of a relation.
			this.name = edge.getName();
			this.nameAppearance = this.getBlock().getRelationAppareance(edge);
			this.getBlock().incrementRelationAppareance(edge);
			
			//If not flexible we do not add the type as *
			if (!this.getBlock().getRule().getIsFlexible()) {
				if (edge.getType() instanceof EReferenceEdge) {
					this.type = Constants.MAUDE_ID_ID_NEW + "(" + edge.getType().getModel().getLevel()
							+ Constants.MAUDE_SEPARATE_ATTRIBUTE + "\"" + edge.getType().getName() + "\")";
				}
				else {
					if (edge.getType() instanceof EdgeVariable) {
						this.type = edge.getType().getName();
						this.typeAppearance = this.getBlock().getRelationAppareance(edge.getType());
					}
					else {
						this.type = Constants.MAUDE_ID_ID_NEW + "(L" + edge.getType().getModel().getLevel()
								+ Constants.MAUDE_SEPARATE_ATTRIBUTE + "\"" + edge.getType().getName() + "\")";
					}
				}
			}
			else {
				this.type = Constants.MAUDE_STAR_SYMBOL + edge.getType().getName();
				this.typeAppearance = this.getBlock().getRelationAppareance(edge.getType());
			}

			//this.getBlock().incrementRelationAppareance(edge.getType());
			
			// Fill the predicate for the * operator.
			if (this.getBlock().getRule().getIsFlexible()) {
				this.predicate = new Predicate();
				this.predicate.setActualType(this.type + "_" + this.getTypeAppearance());
				this.predicate
				.setStartingLevel("sd(L" + edge.getModel().getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE + "1)");
				if (edge.getType() instanceof EdgeConstant) {
					this.predicate.setTopType(Constants.MAUDE_ID_ID_NEW + "(L" + edge.getType().getModel().getLevel()
							+ Constants.MAUDE_SEPARATE_ATTRIBUTE + "\"" + edge.getType().getName() + "\")");
				}
				// Exceptional case if EReference is used (example has1 has2 in assemble)
				else if (edge.getType() instanceof EReferenceEdge) {
					this.predicate.setTopType(Constants.MAUDE_ID_ID_NEW + "(" + edge.getType().getModel().getLevel()
							+ Constants.MAUDE_SEPARATE_ATTRIBUTE + "\"" + edge.getType().getName() + "\")");
				} else {
					this.predicate.setTopType(edge.getType().getName() + "_" + this.getTypeAppearance());
				}
			}
		}

		// Checking if source/target are constants or variables
			this.source =  this.getBlock().getRule().getEntityFromNodeEntity(edge.getSource());

			this.target = this.getBlock().getRule().getEntityFromNodeEntity(edge.getTarget());
	}

	public void parseNewRelation(IEdge edge) {
		this.id = Constants.MAUDE_ID_OID_NEW + "(" + "L" + edge.getModel().getLevel()
				+ Constants.MAUDE_SEPARATE_ATTRIBUTE + RulesBlock.getValueCounter() + "N)";
		this.isConstant = false;
		this.name = Constants.MAUDE_ID_ID_NEW + "(L" + this.getBlock().getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE
				+ RulesBlock.getValueCounter() + "N)";
		RulesBlock.increaseValueCounter();
		
		if (edge.getType() instanceof EdgeConstant || edge.getType() instanceof EReferenceEdge) {
			this.type = Constants.MAUDE_ID_ID_NEW + "(L"
					+ this.getBlock().getRule().getRelationFromEdgeRelation(edge.getType()).getBlock().getLevel()
					+ Constants.MAUDE_SEPARATE_ATTRIBUTE + "\"" + edge.getType().getName() + "\")";
		} else {
			this.type = edge.getType().getName();
			this.typeAppearance = this.getBlock().getRelationAppareance(edge.getType());
		}
		this.source = this.getBlock().getRule().getEntityFromNodeEntity(edge.getSource());
		this.target = this.getBlock().getRule().getEntityFromNodeEntity(edge.getTarget());
	}

	@Override
	public String toString() {
		String relString = "";
		relString = relString + Constants.TABULATION3 + Constants.MAUDE_OPEN_CONFIGURATION + this.id
				+ Constants.MAUDE_TYPE_SEPARATOR + "Relation" + Constants.MAUDE_VERTICAL_SEPARATOR + "name"
				+ Constants.MAUDE_TYPE_SEPARATOR;
	
		if (this.isConstant) {
			relString = relString + this.name + Constants.MAUDE_SEPARATE_ATTRIBUTE; 
			relString = relString + "type" + Constants.MAUDE_TYPE_SEPARATOR + this.type + Constants.MAUDE_SEPARATE_ATTRIBUTE;
			relString = relString + "source" + Constants.MAUDE_TYPE_SEPARATOR + this.source.getName();
			if (this.multiplicityVariable != null) {
				relString = relString + Constants.MAUDE_SEPARATE_ATTRIBUTE;
				relString = relString + "min-mult" +  Constants.MAUDE_TYPE_SEPARATOR + Constants.MAUDE_ID_MULTIPLICITY + this.multiplicityVariable + Constants.MAUDE_SEPARATE_ATTRIBUTE;
				relString = relString + "max-mult" +  Constants.MAUDE_TYPE_SEPARATOR + Constants.MAUDE_ID_MULTIPLICITY + this.multiplicityVariable;
			}
			if (this.elementId != null) {
				relString = relString + Constants.MAUDE_SEPARATE_ATTRIBUTE + this.elementId;
			}
		}
		else {
			if (this.name.startsWith(Constants.MAUDE_ID_ID_NEW)) {
				relString = relString + this.name + Constants.MAUDE_SEPARATE_ATTRIBUTE;
			}
			else {
				relString = relString + this.name+ "_" + this.nameAppearance + Constants.MAUDE_SEPARATE_ATTRIBUTE;
			}
			if (this.type.startsWith(Constants.MAUDE_ID_ID_NEW)) {
				relString = relString + "type" + Constants.MAUDE_TYPE_SEPARATOR + this.type + Constants.MAUDE_SEPARATE_ATTRIBUTE;
			}
			else {
				relString = relString + "type" + Constants.MAUDE_TYPE_SEPARATOR + this.type + "_" + this.typeAppearance + Constants.MAUDE_SEPARATE_ATTRIBUTE;
			}
			// Second part of the condition if it is a new element
			if (this.source.isConstant || this.source.getName().startsWith(Constants.MAUDE_ID_ID_NEW)) {
				relString = relString + "source" + Constants.MAUDE_TYPE_SEPARATOR + this.source.getName()
						+ Constants.MAUDE_SEPARATE_ATTRIBUTE;
			} else {
				relString = relString + "source" + Constants.MAUDE_TYPE_SEPARATOR + this.source.getName() + "_"
						+ this.source.getNameAppearance() + Constants.MAUDE_SEPARATE_ATTRIBUTE;
			}
			if (this.target.isConstant || this.target.getName().startsWith(Constants.MAUDE_ID_ID_NEW)) {
				relString = relString + "target" + Constants.MAUDE_TYPE_SEPARATOR + this.target.getName();
			} else {
				relString = relString + "target" + Constants.MAUDE_TYPE_SEPARATOR + this.target.getName() + "_"
						+ this.target.getNameAppearance();
			}
				if (this.multiplicityVariable != null) {
					relString = relString + Constants.MAUDE_SEPARATE_ATTRIBUTE;
					relString = relString + "min-mult" +  Constants.MAUDE_TYPE_SEPARATOR + Constants.MAUDE_ID_MULTIPLICITY + this.multiplicityVariable + Constants.MAUDE_SEPARATE_ATTRIBUTE;
					relString = relString + "max-mult" +  Constants.MAUDE_TYPE_SEPARATOR + Constants.MAUDE_ID_MULTIPLICITY + this.multiplicityVariable;
				}
				//We still need to show multiplicities for new elements, otherwise Maude does not find an operation to parse this to XML.
				else if (this.name.startsWith(Constants.MAUDE_ID_ID_NEW)){
					relString = relString + Constants.MAUDE_SEPARATE_ATTRIBUTE;
					relString = relString + "min-mult" +  Constants.MAUDE_TYPE_SEPARATOR + "0" + Constants.MAUDE_SEPARATE_ATTRIBUTE;
					relString = relString + "max-mult" +  Constants.MAUDE_TYPE_SEPARATOR + "*";
				}
			if (this.elementId != null) {
				relString = relString + Constants.MAUDE_SEPARATE_ATTRIBUTE + this.elementId;
			}
		}
		relString = relString + Constants.MAUDE_CLOSE_CONFIGURATION;
		return relString;
	}
	@Override
	public boolean equals(Relation obj) {
		Relation relation = (Relation) obj;
		if (this.id.equals(relation.getId()) && this.name.equals(relation.getName())) {
			return true;
		} else {
			return false;
		}
	}

}
