package no.hvl.multecore.mcmt.maude.rules;

public abstract class Element {

	protected String id;
	protected String name;
	protected String type;
	protected Boolean isConstant;
	protected String elementId;
	protected Block block;
	protected Predicate predicate;
	protected Integer nameAppearance;
	protected Integer typeAppearance;

	public Element() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getIsConstant() {
		return isConstant;
	}

	public void setIsConstant(Boolean isConstant) {
		this.isConstant = isConstant;
	}

	public String getElementId() {
		return elementId;
	}

	public void setElementId(String entityId) {
		this.elementId = entityId;
	}

	public Block getBlock() {
		return block;
	}

	public void setBlock(Block block) {
		this.block = block;
	}

	public Predicate getPredicate() {
		return predicate;
	}

	public void setPredicate(Predicate predicate) {
		this.predicate = predicate;
	}

	public Integer getNameAppearance() {
		return nameAppearance;
	}

	public void setNameAppearance(Integer nameAppearance) {
		this.nameAppearance = nameAppearance;
	}

	public Integer getTypeAppearance() {
		return typeAppearance;
	}

	public void setTypeAppearance(Integer typeAppearance) {
		this.typeAppearance = typeAppearance;
	}

	public boolean equals(Relation obj) {
		return false;
	}
}
