package no.hvl.multecore.mcmt.maude.rules;

import java.util.HashSet;
import java.util.Set;

import no.hvl.multecore.common.hierarchy.EClassNode;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.common.hierarchy.Node;
import no.hvl.multecore.mcmt.maude.Constants;
import no.hvl.multecore.mcmt.proliferation.rule.NodeConstant;
import no.hvl.multecore.mcmt.proliferation.rule.NodeVariable;

public class Entity extends Element {
	
	private Set<MetaAttribute> metaAttributes;
	private Set<ExpressionAttribute> expressionAttributes;
	private String attributeId;
	

	
	public Entity() {
		super();
		this.metaAttributes = new HashSet<MetaAttribute>();
		this.expressionAttributes = new HashSet<ExpressionAttribute>();
		this.attributeId = null;
	}

	public Entity(String id, String name, String type, Boolean isConstant) {
		this.id = id;
		this.name = name;
		this.type = type;
		this.isConstant = isConstant;
		this.metaAttributes = new HashSet<MetaAttribute>();
		this.expressionAttributes = new HashSet<ExpressionAttribute>();
		this.attributeId = null;

	}
	

	public Set<MetaAttribute> getMetaAttributes() {
		return metaAttributes;
	}

	public void setMetaAttributes(Set<MetaAttribute> metaAttributes) {
		this.metaAttributes = metaAttributes;
	}
	
	public String getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}

	public Set<ExpressionAttribute> getExpressionAttributes() {
		return expressionAttributes;
	}

	public void setExpressionAttributes(Set<ExpressionAttribute> expressionAttributes) {
		this.expressionAttributes = expressionAttributes;
	}

	public void parseEntity(INode iNode) {
		this.id = Constants.MAUDE_ID_OID + String.format("%02d", this.getBlock().getRule().getVariableCounter());
		this.elementId = Constants.MAUDE_ID_ATTRIBUTE_SET + String.format("%02d", this.getBlock().getRule().getVariableCounter());
		this.getBlock().getRule().increaseVariableCounter();
		if (iNode instanceof NodeConstant || iNode.getType() instanceof EClassNode) {
			this.isConstant = true;
			this.name = Constants.MAUDE_ID_ID_NEW + "(L" + iNode.getModel().getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE + "\""+iNode.getName()+"\")";
			this.type = Constants.MAUDE_ID_ID_NEW  + "(" + iNode.getType().getModel().getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE + "\"" +iNode.getType().getName()+"\")";
		}
		else {
			this.isConstant = false;
			//Control the number of appearances of an entity.
			this.name = iNode.getName();
			this.nameAppearance = this.getBlock().getEntityAppareance(iNode);
			this.getBlock().incrementEntityAppareance(iNode);
			//If not flexible we do not add the type as *
			if (!this.getBlock().getRule().getIsFlexible()) {
				if (iNode.getType() instanceof NodeVariable) {
					this.type = iNode.getType().getName();
					this.typeAppearance = this.getBlock().getEntityAppareance(iNode.getType());
				}
				else {
					this.type = Constants.MAUDE_ID_ID_NEW  + "(L" + iNode.getType().getModel().getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE + "\"" +iNode.getType().getName()+"\")";
				}
			}
			else {
				this.type = Constants.MAUDE_STAR_SYMBOL + iNode.getType().getName();
				this.typeAppearance = this.getBlock().getEntityAppareance(iNode.getType());
			}
//			this.getBlock().incrementEntityAppareance(iNode.getType());
			
			
			//Fill the predicate for the * operator if is flexible.
			if (this.getBlock().getRule().getIsFlexible()) {
				this.predicate = new Predicate();
				this.predicate.setActualType(this.type + "_" + this.getTypeAppearance());
				this.predicate.setStartingLevel("sd(L"+iNode.getModel().getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE + "1)");
				if (iNode.getType() instanceof NodeConstant) {
					this.predicate.setTopType(Constants.MAUDE_ID_ID_NEW + "(L" + iNode.getType().getModel().getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE + "\"" +iNode.getType().getName()+"\")");
				}
				else {
					this.predicate.setTopType(iNode.getType().getName() + "_" + this.getTypeAppearance());
				}
			}
		}
	}

	public void replicateEntityforToBlock(Entity entity) {
		this.id = entity.getId();
		this.attributeId = entity.getAttributeId();
		this.elementId = entity.getElementId();
		this.isConstant = false;
		this.name = entity.getName();
		this.type = entity.getType();
		this.typeAppearance = entity.getTypeAppearance();
		this.nameAppearance = entity.getNameAppearance();
		this.predicate = entity.getPredicate();
		this.expressionAttributes.addAll(entity.getExpressionAttributes());
	}	
	
	public void parseNewEntity(INode iNode) {
		this.id = Constants.MAUDE_ID_OID_NEW + "(" + "L" + iNode.getModel().getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE + RulesBlock.getValueCounter() + "N)";
		this.getBlock().getRule().increaseVariableCounter();
		this.isConstant = false;
		this.name = Constants.MAUDE_ID_ID_NEW + "(" + "L" + iNode.getModel().getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE + RulesBlock.getValueCounter() + "N)";
		RulesBlock.increaseValueCounter();
		
		if (existsTypeWithStar(iNode.getType()) && this.getBlock().getRule().getIsFlexible()) {
			this.type = Constants.MAUDE_STAR_SYMBOL + iNode.getType().getName();			
			this.typeAppearance = this.getBlock().getEntityAppareance(iNode.getType());
		}
		else {
			if (iNode.getType() instanceof NodeVariable) {
				this.type= iNode.getType().getName();
				this.typeAppearance = this.getBlock().getEntityAppareance(iNode.getType());
			}
			else {
				this.type = Constants.MAUDE_ID_ID_NEW  + "(L" + iNode.getType().getModel().getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE + "\"" +iNode.getType().getName()+"\")";			
			}
		}
	}	
	
	//If the type has been established in the "from" and it has the *, we can use it in the "to".
	public boolean existsTypeWithStar (INode type) {
		//We check if a node with * and the name of the type exists
		Boolean foundTypeWithStar = false;
		if (this.block.findEntityByName(Constants.MAUDE_STAR_SYMBOL + type.getName())) {
			foundTypeWithStar = true;
		}
		
		return foundTypeWithStar;
	}
	
	@Override
	public String toString() {
		String elString = "";
		elString = elString + Constants.TABULATION3 + Constants.MAUDE_OPEN_CONFIGURATION + this.id
				+ Constants.MAUDE_TYPE_SEPARATOR + Constants.MAUDE_NODE + Constants.MAUDE_VERTICAL_SEPARATOR + "name"
				+ Constants.MAUDE_TYPE_SEPARATOR;

		if (this.isConstant) {
			elString = elString + this.name + Constants.MAUDE_SEPARATE_ATTRIBUTE;
			elString = elString + "type" + Constants.MAUDE_TYPE_SEPARATOR + this.type;
			if (!this.metaAttributes.isEmpty()) {
				elString = elString + Constants.MAUDE_SEPARATE_ATTRIBUTE + "attributes" + Constants.MAUDE_TYPE_SEPARATOR + "(" + Constants.BREAK_LINE;
				for (MetaAttribute metaAttribute : metaAttributes) {
					elString = elString + metaAttribute.toString();
				}
				if (this.attributeId != null) {
					elString = elString + this.attributeId; 
				}
				elString = elString + ")";
			}
			if (!this.expressionAttributes.isEmpty()) {
				if (this.metaAttributes.isEmpty()) {
					elString = elString + Constants.MAUDE_SEPARATE_ATTRIBUTE + "attributes" + Constants.MAUDE_TYPE_SEPARATOR + "(" + Constants.BREAK_LINE;
				}
				for (ExpressionAttribute expressionAttribute: expressionAttributes) {
					elString = elString + expressionAttribute.toString();
				}
				if (this.attributeId != null) {
					elString = elString + this.attributeId; 
				}
				elString = elString + ")";
			}			
			if (this.elementId != null) {
				elString = elString + Constants.MAUDE_SEPARATE_ATTRIBUTE + this.elementId;
			}	
		}
		else {
			if (this.name.startsWith(Constants.MAUDE_ID_ID_NEW)) {
				elString = elString + this.name + Constants.MAUDE_SEPARATE_ATTRIBUTE;
			}
			else {
				elString = elString + this.name+ "_" + this.nameAppearance + Constants.MAUDE_SEPARATE_ATTRIBUTE;
			}
			if (this.typeAppearance != null) {
				elString = elString + "type" + Constants.MAUDE_TYPE_SEPARATOR + this.type + "_" + this.typeAppearance;
			}
			else {
				elString = elString + "type" + Constants.MAUDE_TYPE_SEPARATOR + this.type;
			}
	
			if (!this.metaAttributes.isEmpty()) {
				elString = elString + Constants.MAUDE_SEPARATE_ATTRIBUTE + "attributes" + Constants.MAUDE_TYPE_SEPARATOR + "(" + Constants.BREAK_LINE;
				for (MetaAttribute metaAttribute : metaAttributes) {
					elString = elString + metaAttribute.toString();
				}
				if (this.attributeId != null) {
					elString = elString + this.attributeId; 
				}
				elString = elString + ")";
			}
			if (!this.expressionAttributes.isEmpty()) {
				if (this.metaAttributes.isEmpty()) {
					// If its lhs we process only the attributes in the lhs in the from
					//Over here
						elString = elString + Constants.MAUDE_SEPARATE_ATTRIBUTE + "attributes"
								+ Constants.MAUDE_TYPE_SEPARATOR + "(" + Constants.BREAK_LINE;
						for (ExpressionAttribute expressionAttribute : expressionAttributes) {
								elString = elString + expressionAttribute.toString();
						}
						if (this.attributeId != null) {
							elString = elString + this.attributeId; 
						}
						elString = elString + ")";
				}
			}
			if (this.elementId != null) {
				elString = elString + Constants.MAUDE_SEPARATE_ATTRIBUTE + this.elementId;
			}
			//We need to explicitly say that there are not attributes (for new entities) when thats the case, for the Maude to XML to work
			else {
				elString = elString + Constants.MAUDE_SEPARATE_ATTRIBUTE;
				elString = elString + "attributes" +  Constants.MAUDE_TYPE_SEPARATOR + "none";
			}
		}
		elString = elString + Constants.MAUDE_CLOSE_CONFIGURATION;
		return elString;
	}

	@Override
	public boolean equals (Object obj) {
		Entity entity = (Entity) obj;
		if (this.id.equals(entity.getId()) && this.name.equals(entity.getName())) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public void addMetaAttribute (MetaAttribute metaAttribute) {
		this.metaAttributes.add(metaAttribute);
	}
	
	public void addExpressionAttribute (ExpressionAttribute expressionAttribute) {
		this.expressionAttributes.add(expressionAttribute);
	}
	
	public void removeExpressionAttribute (ExpressionAttribute expressionAttribute) {
		this.expressionAttributes.remove(expressionAttribute);
	}
	
	public ExpressionAttribute findModifiedAttributeInToBlock(String type) {
		ExpressionAttribute foundExpression= null;
		for (ExpressionAttribute expressionAttribute : expressionAttributes) {
			if (expressionAttribute.getType().contains(type)) {
				foundExpression = expressionAttribute;
			}
		}
		return foundExpression;
	}
}