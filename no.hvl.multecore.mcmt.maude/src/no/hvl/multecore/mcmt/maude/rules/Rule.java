package no.hvl.multecore.mcmt.maude.rules;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import no.hvl.multecore.common.hierarchy.IEdge;
import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.mcmt.maude.Constants;
import no.hvl.multecore.mcmt.maude.Utils;
import no.hvl.multecore.mcmt.proliferation.rule.RuleHierarchy;

public class Rule {
	private String name;
	private ArrayList<Block> models;
	private ArrayList<Block> lhs;
	private ArrayList<Block> rhs;
	private Boolean isConditional;
	private int variableCounter; 
	private Map<INode, Entity> nodeEntity;
	private Map<IEdge, Relation> edgeRelation;
	//Added to make it less flexible but to fasten the execution
	private Boolean isFlexible;
	private String nameModule;
	
	public Rule() {
		models = new ArrayList<Block>();
		lhs = new ArrayList<Block>();
		rhs = new ArrayList<Block>();
		variableCounter = 1;
		this.nodeEntity = new HashMap<INode, Entity>();
		this.edgeRelation = new HashMap<IEdge, Relation>();
	}

	public Rule(String name, ArrayList<Block> lhs, ArrayList<Block> rhs, Boolean isConditional) {
		this.name = name;
		this.lhs = lhs;
		this.rhs = rhs;
		this.isConditional = isConditional;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Block> getModels() {
		return models;
	}

	public void setModels(ArrayList<Block> models) {
		this.models = models;
	}

	public List<Block> getLhs() {
		return lhs;
	}

	public void setLhs(ArrayList<Block> lhs) {
		this.lhs = lhs;
	}

	public List<Block> getRhs() {
		return rhs;
	}

	public void setRhs(ArrayList<Block> rhs) {
		this.rhs = rhs;
	}

	public Boolean getIsConditional() {
		return isConditional;
	}

	public void setIsConditional(Boolean isConditional) {
		this.isConditional = isConditional;
	}
	

	public int getVariableCounter() {
		return variableCounter;
	}

	public void setVariableCounter(int variableCounter) {
		this.variableCounter = variableCounter;
	}

	public void increaseVariableCounter() {
		this.variableCounter += 1;
	}


	public Map<INode, Entity> getNodeEntity() {
		return nodeEntity;
	}

	public void setNodeRntity(Map<INode, Entity> nodeEntity) {
		this.nodeEntity = nodeEntity;
	}

	public Map<IEdge, Relation> getEdgeRelation() {
		return edgeRelation;
	}

	public void setEdgeRelation(Map<IEdge, Relation> edgeRelation) {
		this.edgeRelation = edgeRelation;
	}

	public void insertNodeEntity(INode iNode, Entity entity) {
		this.nodeEntity.put(iNode, entity);
	}
	
	public void insertEdgeRelation(IEdge iEdge, Relation relation) {
		this.edgeRelation.put(iEdge, relation);
	}
	

	public Boolean getIsFlexible() {
		return isFlexible;
	}

	public void setIsFlexible(Boolean isFlexible) {
		this.isFlexible = isFlexible;
	}
	
	public void setNameModule(String string) {
		this.nameModule = string;
	}		
	
	public String getNameModule() {
	return this.nameModule;	
	}	

	public Entity getEntityFromNodeEntity (INode iNode) {
		Entity entity = null;
	    boolean found = false;
		if (this.nodeEntity.get(iNode)!= null) {
			entity = this.nodeEntity.get(iNode);
		}
		else {
		    Iterator<Entry<INode, Entity>> it = this.nodeEntity.entrySet().iterator();
			while (it.hasNext() && !found) {
				Entry<INode, Entity> pair = it.next();
		        if (pair.getKey().getName().equals(iNode.getName()) && pair.getKey().getType().getName().equals(iNode.getType().getName())) {
		        	entity = pair.getValue();
		        	found = true;
		        }
		    }
		}		
		return entity;
	}
	
	public Relation getRelationFromEdgeRelation (IEdge iEdge) {		
		Relation relation = null;
	    boolean found = false;
		if (this.nodeEntity.get(iEdge)!= null) {
			relation = this.edgeRelation.get(iEdge);
		}
		else {
		    Iterator<Entry<IEdge, Relation>> it = this.edgeRelation.entrySet().iterator();
			while (it.hasNext() && !found) {
				Entry<IEdge, Relation> pair = it.next();
		        if (pair.getKey().getName().equals(iEdge.getName()) && pair.getKey().getType().getName().equals(iEdge.getType().getName())) {
		        	relation = pair.getValue();
		        	found = true;
		        }
		    }
		}		
		return relation;
	}
	
	public void parseRule(RuleHierarchy ruleHierarchy) {

		List<IModel> sortedModels = Utils.toList(ruleHierarchy.getAllModels());
		
		Collections.sort(sortedModels);

		List<IModel> metas = new ArrayList<IModel>();
		metas.addAll(sortedModels);

		metas.remove(ruleHierarchy.getFromModel());
		metas.remove(ruleHierarchy.getToModel());

		for (IModel model : metas) {
			// Reference to From block to check in the To block a new entity is created.
			if (!model.getName().equals(no.hvl.multecore.common.Constants.ECORE_ID)) {
				// If it is in the meta part
				Block meta = new Block();
				meta.setRule(this);
				meta.parseBlock(model);
				this.rhs.add(meta);
				this.lhs.add(meta);
				this.models.add(meta);
			}
		}
		Block from = new Block();
		from.setRule(this);
		from.parseBlock(ruleHierarchy.getFromModel());
		this.lhs.add(from);
		this.models.add(from);

		Block to = new Block();
		to.setRule(this);
		to.parseBlock(ruleHierarchy.getFromModel(), ruleHierarchy.getToModel());
		this.rhs.add(to);
		this.models.add(to);
		
		//Order all the elements by Oid once the rule is completely processed:
		for (int i = 0; i < this.models.size(); i++) {
			//First order entities
			for (int j = 0; j < this.models.get(i).getElts().size(); j ++) {
				List <Entity> entities = this.models.get(i).getElts();
				entities = entities.stream()
				  			.sorted(Comparator.comparing(Entity::getId))
				  			.collect(Collectors.toList());
			}
			for (int j = 0; j < this.models.get(i).getRels().size(); j ++) {
				List <Relation> relations = this.models.get(i).getRels();
				relations = relations.stream()
				  			.sorted(Comparator.comparing(Relation::getId))
				  			.collect(Collectors.toList());
			}			
		}
	}
	

	public String createPredicatesPart () {
		String conditionalBlock = "";
		conditionalBlock += conditionalBlock + Constants.MAUDE_START_CONDITION_BLOCK;
		
		//Conf' := LHS
		conditionalBlock +=  Constants.BREAK_LINE + Constants.MAUDE_CONF_PRIME_VARIABLE + Constants.MAUDE_MATCHING_EQUATION + Constants.BREAK_LINE;
		
		//Processing left-hand side
		for (int i = 0; i < this.lhs.size(); i++) {
			conditionalBlock += this.lhs.get(i);
		}
		conditionalBlock +=  Constants.MAUDE_COUNTER_LHS + Constants.BREAK_LINE;
		
		conditionalBlock += Constants.MAUDE_BINARY_CONJUNCTION_CONNECTIVE;
		
		//IF L1 < L2 /\ ...
		for (int i = 0; i < this.lhs.size()-1; i++) {
			conditionalBlock += "L" + this.lhs.get(i).getLevel() + Constants.MAUDE_BINARY_LESS_THAN + "L" + this.lhs.get(i+1).getLevel();
			conditionalBlock += Constants.MAUDE_BINARY_CONJUNCTION_CONNECTIVE;
		}
		
		//To check whether there are no conditions at all
		boolean addedCondition = false;
		//Rule conditions
		for (int i = 0; i < this.lhs.size(); i++) {
			if (!this.lhs.get(i).getRuleConditions().isEmpty()) {
				for (int j = 0; j < this.lhs.get(i).getRuleConditions().size(); j ++) {
					conditionalBlock += " " + this.lhs.get(i).getRuleConditions().get(j).getExpression() + " ";
					conditionalBlock += Constants.BREAK_LINE;
					conditionalBlock += Constants.MAUDE_BINARY_CONJUNCTION_CONNECTIVE;
					addedCondition = true;
				}
			}
		}
		if (!addedCondition) {
			conditionalBlock = conditionalBlock.substring(0, conditionalBlock.length()-3);
		}
		
		List <Predicate> usedVariables = new ArrayList<Predicate>();
		//Running over node variables (not constants) 
		if (this.isFlexible) {
			for (int i = 0; i < this.lhs.size(); i ++) {
				for (int j = 0; j < this.lhs.get(i).getElts().size(); j ++) {
					Entity entity = this.lhs.get(i).getElts().get(j);
					if (entity.getPredicate()!= null && !usedVariables.contains(entity.getPredicate())) {
						conditionalBlock += Constants.MAUDE_BINARY_CONJUNCTION_CONNECTIVE; 
						//Parameter 1. Type
						conditionalBlock += Constants.MAUDE_STAR_SYMBOL + "(" +  entity.getPredicate().getActualType() + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 2. starting level
						conditionalBlock += "level(" + entity.getPredicate().getStartingLevel() + ")" + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 3. top level to match
						conditionalBlock += entity.getPredicate().getTopType() + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 4. Conf'
						conditionalBlock += Constants.MAUDE_CONF_PRIME_VARIABLE + ")" + Constants.BREAK_LINE;
						usedVariables.add(entity.getPredicate());
					}
				}
			}
			//Running over edge variables (not constants) 
			for (int i = 0; i < this.lhs.size(); i ++) {
				for (int j = 0; j < this.lhs.get(i).getRels().size(); j ++) {
					Relation relation = this.lhs.get(i).getRels().get(j);
					if (relation.getPredicate()!= null && !usedVariables.contains(relation.getPredicate())) {
						conditionalBlock += Constants.MAUDE_BINARY_CONJUNCTION_CONNECTIVE; 
						//Parameter 1. Type
						conditionalBlock += Constants.MAUDE_STAR_SYMBOL + "(" +  relation.getPredicate().getActualType() + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 2. starting level
						conditionalBlock += "level(" + relation.getPredicate().getStartingLevel() + ")" + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 3. top level to match
						conditionalBlock += relation.getPredicate().getTopType() + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 4. Conf'
						conditionalBlock += Constants.MAUDE_CONF_PRIME_VARIABLE + ")" + Constants.BREAK_LINE;
						usedVariables.add(relation.getPredicate());
	
					}
				}
			}
		}
		return conditionalBlock;
	}
	
	
	//This produces the rule if there are not any boxes
	@Override
	public String toString() {
		String rule = "";
		if (this.isConditional) {
			rule = rule + Constants.MAUDE_CONDITIONAL_RULE;
		} else {
			rule = rule + Constants.MAUDE_UNCONDITIONAL_RULE;
		}
		rule = rule + "[" + this.name + "]" + Constants.MAUDE_TYPE_SEPARATOR + Constants.BREAK_LINE;
		rule = rule + Constants.MAUDE_OPEN_RULE + Constants.BREAK_LINE;
		for (int i = 0; i < this.lhs.size(); i++) {
			rule = rule + this.lhs.get(i).toString();
			// TODO Add properly the counter part
		}
		rule = rule + Constants.MAUDE_COUNTER_LHS + Constants.MAUDE_CLOSE_RULE + Constants.BREAK_LINE;
		rule = rule + Constants.MAUDE_REWRITE_RULE + Constants.BREAK_LINE;
		rule = rule + Constants.MAUDE_OPEN_RULE + Constants.BREAK_LINE;
		for (int i = 0; i < this.rhs.size(); i++) {
			rule = rule + this.rhs.get(i).toString();
		}
		
		rule = rule + getCalculatedCounterRHS()+ Constants.MAUDE_CLOSE_RULE + Constants.BREAK_LINE;
		rule = rule + createPredicatesPart();
		rule = rule + Constants.MAUDE_END_LINE;
		return rule;
	}

	
	private String getCalculatedCounterRHS () {
		String calculatedCounter = "";
		calculatedCounter += "< counter : Counter | value : " + RulesBlock.getValueCounter() + "N > " + Constants.MAUDE_CONF_VARIABLE;
		return calculatedCounter;
	}

	public String printWithBoxesVersion() {
		String rule = "";
		if (this.isConditional) {
			rule = rule + Constants.MAUDE_CONDITIONAL_RULE;
		} else {
			rule = rule + Constants.MAUDE_UNCONDITIONAL_RULE;
		}
		rule = rule + "[" + this.name + "Boxes" + "]" + Constants.MAUDE_TYPE_SEPARATOR + Constants.BREAK_LINE;
		rule = rule + Constants.MAUDE_OPEN_RULE + Constants.BREAK_LINE;
		for (int i = 0; i < this.lhs.size(); i++) {
			rule = rule + this.lhs.get(i).toString();
		}
		if (this.lhs.get(this.lhs.size()-1).getRuleBoxes().size() > 0) {
			rule = rule + "boxes((";
			for (int i = 0; i < this.lhs.get(this.lhs.size()-1).getRuleBoxes().size(); i++) {
				rule = rule + this.lhs.get(this.lhs.size()-1).getRuleBoxes().get(i).getBoxesRepresentation() + "," + Constants.BREAK_LINE;
			}
			//Remove last comma and \n
			rule =  rule.substring(0, rule.length()-2);
			rule = rule + "))" + Constants.BREAK_LINE;
		}
		
		rule = rule + Constants.MAUDE_COUNTER_LHS + Constants.MAUDE_CLOSE_RULE + Constants.BREAK_LINE;
		rule = rule + Constants.MAUDE_REWRITE_RULE + Constants.BREAK_LINE;
		rule = rule + Constants.MAUDE_OPEN_RULE + Constants.BREAK_LINE;
		for (int i = 0; i < this.rhs.size(); i++) {
			rule = rule + this.rhs.get(i).toString();
		}
		if (this.rhs.get(this.rhs.size()-1).getRuleBoxes().size() > 0) {
			rule = rule + "boxes((";
			for (int i = 0; i < this.rhs.get(this.rhs.size()-1).getRuleBoxes().size(); i++) {
				rule = rule + this.rhs.get(this.rhs.size()-1).getRuleBoxes().get(i).getBoxesRepresentation() + "," + Constants.BREAK_LINE;
			}		
			rule = rule.substring(0, rule.length()-2);
			rule = rule + "))" + Constants.BREAK_LINE;		
		}

		rule = rule + getCalculatedCounterRHS()+ Constants.MAUDE_CLOSE_RULE + Constants.BREAK_LINE;
		rule = rule + createPredicatesPartRuleWithBoxes();
		rule = rule + Constants.BREAK_LINE +Constants.MAUDE_END_LINE;

		return rule;
	}	

	public String createPredicatesPartRuleWithBoxes () {
		String conditionalBlock = "";
		conditionalBlock += conditionalBlock + Constants.MAUDE_START_CONDITION_BLOCK;
		
		//Conf' := LHS
		conditionalBlock +=  Constants.BREAK_LINE + Constants.MAUDE_CONF_PRIME_VARIABLE + Constants.MAUDE_MATCHING_EQUATION + Constants.BREAK_LINE;
		//Processing left-hand side
		for (int i = 0; i < this.lhs.size(); i++) {
			conditionalBlock += this.lhs.get(i);
		}
		conditionalBlock +=  Constants.MAUDE_COUNTER_LHS + Constants.BREAK_LINE;
		
		conditionalBlock += Constants.MAUDE_BINARY_CONJUNCTION_CONNECTIVE;

		
		//IF L1 < L2 /\ ...
		for (int i = 0; i < this.lhs.size()-1; i++) {
			conditionalBlock += "L" + this.lhs.get(i).getLevel() + Constants.MAUDE_BINARY_LESS_THAN + "L" + this.lhs.get(i+1).getLevel();
			conditionalBlock += Constants.BREAK_LINE;
			conditionalBlock += Constants.MAUDE_BINARY_CONJUNCTION_CONNECTIVE;
		}
		
		conditionalBlock= conditionalBlock.substring(0, conditionalBlock.lastIndexOf(Constants.MAUDE_BINARY_CONJUNCTION_CONNECTIVE));
		
		List <Predicate> usedVariables = new ArrayList<Predicate>();
		//Running over node variables (not constants) 
		if (this.isFlexible) {
			for (int i = 0; i < this.lhs.size(); i ++) {
				for (int j = 0; j < this.lhs.get(i).getElts().size(); j ++) {
					Entity entity = this.lhs.get(i).getElts().get(j);
					if (entity.getPredicate()!= null && !usedVariables.contains(entity.getPredicate())) {
						conditionalBlock += Constants.MAUDE_BINARY_CONJUNCTION_CONNECTIVE; 
						//Parameter 1. Type
						conditionalBlock += Constants.MAUDE_STAR_SYMBOL + "(" +  entity.getPredicate().getActualType() + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 2. starting level
						conditionalBlock += "level(" + entity.getPredicate().getStartingLevel() + ")" + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 3. top level to match
						conditionalBlock += entity.getPredicate().getTopType() + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 4. Conf'
						conditionalBlock += Constants.MAUDE_CONF_PRIME_VARIABLE + ")" + Constants.BREAK_LINE;
						usedVariables.add(entity.getPredicate());
					}
				}
			}
			//Running over edge variables (not constants) 
			for (int i = 0; i < this.lhs.size(); i ++) {
				for (int j = 0; j < this.lhs.get(i).getRels().size(); j ++) {
					Relation relation = this.lhs.get(i).getRels().get(j);
					if (relation.getPredicate()!= null && !usedVariables.contains(relation.getPredicate())) {
						conditionalBlock += Constants.MAUDE_BINARY_CONJUNCTION_CONNECTIVE; 
						//Parameter 1. Type
						conditionalBlock += Constants.MAUDE_STAR_SYMBOL + "(" +  relation.getPredicate().getActualType() + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 2. starting level
						conditionalBlock += "level(" + relation.getPredicate().getStartingLevel() + ")" + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 3. top level to match
						conditionalBlock += relation.getPredicate().getTopType() + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 4. Conf'
						conditionalBlock += Constants.MAUDE_CONF_PRIME_VARIABLE + ")" + Constants.BREAK_LINE;
						usedVariables.add(relation.getPredicate());
	
					}
				}
			}
		}
		conditionalBlock += "[nonexec]";
		return conditionalBlock;
	}		
	
	public String printWithoutBoxesVersion() {
		String rule = "";
		if (this.isConditional) {
			rule = rule + Constants.MAUDE_CONDITIONAL_RULE;
		} else {
			rule = rule + Constants.MAUDE_UNCONDITIONAL_RULE;
		}
		rule = rule + "[" + this.name + "]" + Constants.MAUDE_TYPE_SEPARATOR + Constants.BREAK_LINE;
		rule = rule + Constants.MAUDE_OPEN_RULE + Constants.BREAK_LINE;
		for (int i = 0; i < this.lhs.size(); i++) {
			rule = rule + this.lhs.get(i).toString();
			// TODO Add properly the counter part
		}
		rule = rule + Constants.MAUDE_COUNTER_LHS + Constants.MAUDE_CLOSE_RULE + Constants.BREAK_LINE;
		rule = rule + Constants.MAUDE_REWRITE_RULE;
		
		rule = rule + " downTerm(T9:Term, {none})" + Constants.BREAK_LINE;
		
		
		rule = rule + createPredicatesPartRuleWithoutBoxes();
		return rule;
	}

	public String createPredicatesPartRuleWithoutBoxes () {
		String conditionalBlock = "";
		conditionalBlock += conditionalBlock + Constants.MAUDE_START_CONDITION_BLOCK;
		
		//Conf' := LHS
		conditionalBlock +=  Constants.BREAK_LINE + Constants.MAUDE_CONF_PRIME_VARIABLE + Constants.MAUDE_MATCHING_EQUATION + Constants.BREAK_LINE;
		
		//Processing left-hand side
		for (int i = 0; i < this.lhs.size(); i++) {
			conditionalBlock += this.lhs.get(i);
		}
		conditionalBlock +=  Constants.MAUDE_COUNTER_LHS + Constants.BREAK_LINE;
		
		conditionalBlock +=  Constants.MAUDE_BINARY_CONJUNCTION_CONNECTIVE;
		
		//IF L1 < L2 /\ ...
		for (int i = 0; i < this.lhs.size()-1; i++) {
			conditionalBlock += "L" + this.lhs.get(i).getLevel() + Constants.MAUDE_BINARY_LESS_THAN + "L" + this.lhs.get(i+1).getLevel();
			conditionalBlock +=  Constants.BREAK_LINE;
			conditionalBlock += Constants.MAUDE_BINARY_CONJUNCTION_CONNECTIVE;
		}
		

		
		//Rule conditions
		for (int i = 0; i < this.lhs.size(); i++) {
			if (!this.lhs.get(i).getRuleConditions().isEmpty()) {
				for (int j = 0; j < this.lhs.get(i).getRuleConditions().size(); j ++) {
					conditionalBlock += " " + this.lhs.get(i).getRuleConditions().get(j).getExpression() + " ";
					conditionalBlock += Constants.MAUDE_BINARY_CONJUNCTION_CONNECTIVE;
				}
			}
		}
		
		//Running over node variables (not constants) 
		if (this.isFlexible) {
			List <Predicate> usedVariables = new ArrayList<Predicate>();
			for (int i = 0; i < this.lhs.size(); i ++) {
				for (int j = 0; j < this.lhs.get(i).getElts().size(); j ++) {
					Entity entity = this.lhs.get(i).getElts().get(j);
					if (entity.getPredicate()!= null && !usedVariables.contains(entity.getPredicate())) {
						conditionalBlock += Constants.MAUDE_BINARY_CONJUNCTION_CONNECTIVE; 
						//Parameter 1. Type
						conditionalBlock += Constants.MAUDE_STAR_SYMBOL + "(" +  entity.getPredicate().getActualType() + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 2. starting level
						conditionalBlock += "level(" + entity.getPredicate().getStartingLevel() + ")" + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 3. top level to match
						conditionalBlock += entity.getPredicate().getTopType() + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 4. Conf'
						conditionalBlock += Constants.MAUDE_CONF_PRIME_VARIABLE + ")" + Constants.BREAK_LINE;
						usedVariables.add(entity.getPredicate());
					}
				}
			}
			//Running over edge variables (not constants) 
			for (int i = 0; i < this.lhs.size(); i ++) {
				for (int j = 0; j < this.lhs.get(i).getRels().size(); j ++) {
					Relation relation = this.lhs.get(i).getRels().get(j);
					if (relation.getPredicate()!= null && !usedVariables.contains(relation.getPredicate())) {
						conditionalBlock += Constants.MAUDE_BINARY_CONJUNCTION_CONNECTIVE; 
						//Parameter 1. Type
						conditionalBlock += Constants.MAUDE_STAR_SYMBOL + "(" +  relation.getPredicate().getActualType() + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 2. starting level
						conditionalBlock += "level(" + relation.getPredicate().getStartingLevel() + ")" + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 3. top level to match
						conditionalBlock += relation.getPredicate().getTopType() + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						//Parameter 4. Conf'
						conditionalBlock += Constants.MAUDE_CONF_PRIME_VARIABLE + ")" + Constants.BREAK_LINE;
						usedVariables.add(relation.getPredicate());
	
					}
				}
			}
		}
		conditionalBlock += Constants.BREAK_LINE + "Subst" + Constants.MAUDE_MATCHING_EQUATION + "(" + Constants.TABULATION3;
		//We need to run for each of the elements declared: nodes, attributes and edges
		for (int i = 0; i < this.lhs.size(); i++) {
			for(int j = 0; j < this.lhs.get(i).getElts().size(); j ++) {
				Entity entity = this.lhs.get(i).getElts().get(j);
				conditionalBlock += "'" + entity.getId() + ":Oid <- upTerm(" + entity.getId() + ") ;" + Constants.BREAK_LINE + Constants.TABULATION3 + Constants.TABULATION3;
				if (!entity.getMetaAttributes().isEmpty()) {
					//Get a List from the Set
					List<MetaAttribute> listMetaAttributes = Utils.toList(entity.getMetaAttributes());
					//Order the List by oid
				     listMetaAttributes = listMetaAttributes.stream()
				  			.sorted(Comparator.comparing(MetaAttribute::getId))
				  			.collect(Collectors.toList());
				     for (int k = 0; k < listMetaAttributes.size(); k ++ ) {
							conditionalBlock += "'" + listMetaAttributes.get(k).getId() + ":Oid <- upTerm(" + listMetaAttributes.get(k).getId() + ") ;" + Constants.BREAK_LINE + Constants.TABULATION3 + Constants.TABULATION3;
				     }
				}
				if (!entity.getExpressionAttributes().isEmpty()) {
					//Get a List from the Set
					List<ExpressionAttribute> listExpressionAttributes = Utils.toList(entity.getExpressionAttributes());
					//Order the List by oid
				     listExpressionAttributes = listExpressionAttributes.stream()
				  			.sorted(Comparator.comparing(ExpressionAttribute::getId))
				  			.collect(Collectors.toList());
				     for (int k = 0; k < listExpressionAttributes.size(); k ++ ) {
							conditionalBlock += "'" + listExpressionAttributes.get(k).getId() + ":Oid <- upTerm(" + listExpressionAttributes.get(k).getId() + ") ;" + Constants.BREAK_LINE + Constants.TABULATION3 + Constants.TABULATION3;
				     }
				}
			}
			for (int j = 0; j < this.lhs.get(i).getRels().size(); j++) {
				Relation relation = this.lhs.get(i).getRels().get(j);
				conditionalBlock += "'" + relation.getId() + ":Oid <- upTerm(" + relation.getId() + ") ;" + Constants.BREAK_LINE + Constants.TABULATION3 + Constants.TABULATION3;
			}
		}

		for (int i = 0; i < this.lhs.size(); i++) {
			for (int j = 0; j < this.lhs.get(i).getIntVariables().size(); j++) {
				String var = this.lhs.get(i).getIntVariables().get(j);
				//TODO Check this in further versions, for now simply using OCL INT by default
				conditionalBlock += "'" + var + ":Int <- upTerm(" + var + ") ;" + Constants.BREAK_LINE + Constants.TABULATION3 + Constants.TABULATION3;
			}
			for (int j = 0; j < this.lhs.get(i).getNameVariables().size(); j++) {
				String var = this.lhs.get(i).getNameVariables().get(j);
				//TODO Check this in further versions, for now simply using INT by default
				conditionalBlock += "'" + var + ":Name <- upTerm(" + var + ") ;" + Constants.BREAK_LINE + Constants.TABULATION3 + Constants.TABULATION3;
			}	
			for (int j = 0; j < this.lhs.get(i).getNatVariables().size(); j++) {
				String var = this.lhs.get(i).getNatVariables().get(j);
				//TODO Check this in further versions, for now simply using INT by default
				conditionalBlock += "'" + var + ":Nat <- upTerm(" + var + ") ;" + Constants.BREAK_LINE + Constants.TABULATION3 + Constants.TABULATION3;
			}				
		}
		
		
	
		conditionalBlock += "'" + "Conf':Configuration <- upTerm(Conf') ";
		
		conditionalBlock += ")" + Constants.BREAK_LINE;
		conditionalBlock += Constants.MAUDE_BINARY_CONJUNCTION_CONNECTIVE + "{T9:Term, Ty9:Type, Subst9:Substitution}" + Constants.BREAK_LINE;
		conditionalBlock += Constants.TABULATION + Constants.MAUDE_MATCHING_EQUATION + "metaApply(" + Constants.BREAK_LINE;
		conditionalBlock += Constants.TABULATION2 +  "makeModule(" + Constants.BREAK_LINE;
		conditionalBlock += Constants.TABULATION3 +  "upModule('" + this.nameModule +",false)," + Constants.BREAK_LINE;
		conditionalBlock += Constants.TABULATION3 +  "upTerm({Conf'})," + Constants.BREAK_LINE;
		conditionalBlock += Constants.TABULATION3 +  "'" + this.name + "Boxes," + Constants.BREAK_LINE;
		conditionalBlock += Constants.TABULATION3 +  "Subst)," + Constants.BREAK_LINE;
		conditionalBlock += Constants.TABULATION2 +  "upTerm({Conf'})," + Constants.BREAK_LINE;
		conditionalBlock += Constants.TABULATION2 +  "'" + this.name + "Boxes," + Constants.BREAK_LINE;
		conditionalBlock += Constants.TABULATION2 +  "Subst," + Constants.BREAK_LINE;
		conditionalBlock += Constants.TABULATION2 +  "0)" + Constants.BREAK_LINE;
		conditionalBlock += Constants.MAUDE_END_LINE;
		return conditionalBlock;
	}

}
