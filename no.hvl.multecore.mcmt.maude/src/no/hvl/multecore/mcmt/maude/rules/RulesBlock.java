package no.hvl.multecore.mcmt.maude.rules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import no.hvl.multecore.mcmt.maude.Constants;

public class RulesBlock {

	private Map<String, ArrayList<String>> variables;
	private List<Rule> rules;
	
	// Counter for the declaration of variables of type AttributeSet
	public static int GLOBAL_COUNTER = 1;

	public static int BOXES_GLOBAL_COUNTER = 1;
	
	
	public RulesBlock() {
		this.variables = new HashMap<String, ArrayList<String>>();
		this.rules = new ArrayList<Rule>();
	}

	public RulesBlock(Map<String, ArrayList<String>> variables, List<Rule> rules) {
		super();
		this.variables = variables;
		this.rules = rules;
	}

	public Map<String, ArrayList<String>> getVariables() {
		return variables;
	}

	public void setVariables(Map<String, ArrayList<String>> variables) {
		this.variables = variables;
	}

	public List<Rule> getRules() {
		return rules;
	}

	public void setRules(List<Rule> rules) {
		this.rules = rules;
	}

	public void addRules(Rule rule) {
		this.rules.add(rule);
	}

	// Calculate and generate all the variables needed explicitly in Maude. It also
	// updates the information in the rules.
	public void calculateVariables() {
		ArrayList<String> nameVars = new ArrayList<String>();
		ArrayList<String> oidVars = new ArrayList<String>();
		ArrayList<String> attributeSetVars = new ArrayList<String>();
		ArrayList<String> configurationVars = new ArrayList<String>();
		ArrayList<String> natVars = new ArrayList<String>();
		ArrayList<String> setOidVars = new ArrayList<String>();
//		ArrayList<String> listOidVars = new ArrayList<String>();
		ArrayList<String> objectVars = new ArrayList<String>();
		ArrayList<String> intVars = new ArrayList<String>();
		ArrayList<String> stringVars = new ArrayList<String>();
		ArrayList<String> boolVars = new ArrayList<String>();
		ArrayList<String> floatVars = new ArrayList<String>();
		ArrayList<String> substitutionVars = new ArrayList<String>();
		
		//TODO Once SML is ready here we have to put it depending on the Modeller choice
		String decidedLanguage = "OCL";
		ArrayList<String> languageInt = new ArrayList<String>();
		ArrayList<String> languageBool = new ArrayList<String>();
		ArrayList<String> languageString = new ArrayList<String>();
		ArrayList<String> languageReal = new ArrayList<String>();
		
		for (int i = 0; i < rules.size(); i++) {
			for (int j = 0; j < rules.get(i).getModels().size(); j++) {
				for (int k = 0; k < rules.get(i).getModels().get(j).getElts().size(); k++) {

					Entity entity = rules.get(i).getModels().get(j).getElts().get(k);
					if (!entity.getIsConstant() && !entity.getName().startsWith(Constants.MAUDE_ID_ID_NEW)
							&& !nameVars.contains(entity.getName() + "_" + entity.getNameAppearance())) {
						nameVars.add(entity.getName() + "_" + entity.getNameAppearance());
					}

					if (!entity.getType().startsWith(Constants.MAUDE_ID_ID_NEW) && !entity.getName().startsWith(Constants.MAUDE_ID_ID_NEW)
							&& !nameVars.contains(entity.getType() + "_" + entity.getTypeAppearance())) {
						nameVars.add(entity.getType() + "_" + entity.getTypeAppearance());
					}

					if (!oidVars.contains(entity.getId()) && !entity.getId().startsWith(Constants.MAUDE_ID_OID_NEW)) {
						oidVars.add(entity.getId());
					}
					if (!attributeSetVars.contains(entity.getElementId()) && entity.getElementId() != null && !entity.getId().startsWith(Constants.MAUDE_ID_OID_NEW)) {
						attributeSetVars.add(entity.getElementId());
					}
					for (MetaAttribute metaAttribute : entity.getMetaAttributes()) {
						if (!oidVars.contains(metaAttribute.getId())) {
							oidVars.add(metaAttribute.getId());
						}
						if (!attributeSetVars.contains(metaAttribute.getAttributeId())) {
							attributeSetVars.add(metaAttribute.getAttributeId());
						}
					}
					for (ExpressionAttribute expressionAttribute : entity.getExpressionAttributes()) {
						if (!oidVars.contains(expressionAttribute.getId())) {
							oidVars.add(expressionAttribute.getId());
						}
						if (!attributeSetVars.contains(expressionAttribute.getAttributeId())) {
							attributeSetVars.add(expressionAttribute.getAttributeId());
						}
						
						expressionAttribute.getVariables().forEach((key,value)->{
							switch (value) {
							case ("Integer"):
								if (!languageInt.contains(key))
									languageInt.add(key);
								break;
							case ("String"):
								if (!languageString.contains(key))
									languageString.add(key);
								break;
							case ("Boolean"):
								if (!languageBool.contains(key))
									languageBool.add(key);
								break;
							case ("Real"):
								if (!languageReal.contains(key))
									languageReal.add(key);							
								break;
							default:
								break;
							}
						});
					}
					
					if (entity.getAttributeId() != null && !configurationVars.contains(entity.getAttributeId())) {
						configurationVars.add(entity.getAttributeId());
					}
				}
				for (int k = 0; k < rules.get(i).getModels().get(j).getRels().size(); k++) {
					Relation relation = rules.get(i).getModels().get(j).getRels().get(k);
					if (!relation.getIsConstant() && !relation.getName().startsWith(Constants.MAUDE_ID_ID_NEW)
							&& !nameVars.contains(relation.getName() + "_" + relation.getNameAppearance())) {
						nameVars.add(relation.getName() + "_" + relation.getNameAppearance());
					}

					if (!relation.getName().startsWith(Constants.MAUDE_ID_ID_NEW) 
							&& !relation.getType().startsWith(Constants.MAUDE_ID_ID_NEW)
							&& !nameVars.contains(relation.getType() + "_" + relation.getTypeAppearance())) {
						nameVars.add(relation.getType() + "_" + relation.getTypeAppearance());
					}

					if (!oidVars.contains(relation.getId())
							&& !relation.getId().startsWith(Constants.MAUDE_ID_OID_NEW)) {
						oidVars.add(relation.getId());
					}
					if (!attributeSetVars.contains(relation.getElementId()) && relation.getElementId() != null) {
						attributeSetVars.add(relation.getElementId());
					}
				}

				Block block = rules.get(i).getModels().get(j);
				// Elts, Elts' ...
				if (!configurationVars.contains(block.getEltsId())) {
					configurationVars.add(block.getEltsId());
				}
				// Rels, Rels' ...
				if (!configurationVars.contains(block.getRelsId())) {
					configurationVars.add(block.getRelsId());
				}
				// Atts, Atts', ...
				if (!attributeSetVars.contains(block.getId())) {
					attributeSetVars.add(block.getId());
				}
				// M, M' ...
				if (!stringVars.contains(block.getName())) {
					stringVars.add(block.getName());
				}

				if (!natVars.contains("L" + block.getLevel())) {
					natVars.add("L" + block.getLevel());
				}
			}
		}
		// Variable N for using the counter
		natVars.add(Constants.MAUDE_NAT_VARIABLE);

		//Variable Oid for the definition of the removeNodes/removeRels ops.
		oidVars.add("Oid");
		
		//Var for substitution used in rules with boxes
		substitutionVars.add("Subst");
		
		// Variables Conf and Conf'
		configurationVars.add(Constants.MAUDE_CONF_VARIABLE);
		configurationVars.add(Constants.MAUDE_CONF_PRIME_VARIABLE);
		
		//Default variables for Set{Oid}
		setOidVars.add("Oids");
		setOidVars.add(Constants.MAUDE_ID_BOX_NODE);
		setOidVars.add(Constants.MAUDE_ID_BOX_RELS);
		
		//Default variable for List{Oid}
//		listOidVars.add(Constants.MAUDE_ID_BOX_MATCH);
		
		//Default variable for Object
		objectVars.add(Constants.MAUDE_ID_BOX_OBJECT);
		this.variables.put(Constants.MAUDE_SORT_NAME, nameVars);
		this.variables.put(Constants.MAUDE_SORT_OID, oidVars);
		this.variables.put(Constants.MAUDE_SORT_ATTRIBUTE_SET, attributeSetVars);
		this.variables.put(Constants.MAUDE_SORT_CONFIGURATION, configurationVars);
		this.variables.put(Constants.MAUDE_SORT_NAT, natVars);
		this.variables.put(Constants.MAUDE_SORT_SET_OID, setOidVars);
//		this.variables.put(Constants.MAUDE_SORT_LIST_OID, listOidVars);
		this.variables.put(Constants.MAUDE_SORT_OBJECT, objectVars);
		this.variables.put(Constants.MAUDE_SORT_INT, intVars);
		this.variables.put(Constants.MAUDE_SORT_STRING, stringVars);
		this.variables.put(Constants.MAUDE_SORT_BOOL, boolVars);
		this.variables.put(Constants.MAUDE_SORT_FLOAT, floatVars);
		this.variables.put(Constants.MAUDE_SORT_SUBSTITUTION, substitutionVars);
		
		switch (decidedLanguage) {
		case ("OCL"):
			this.variables.put(Constants.MAUDE_SORT_OCL_INT, languageInt);
			this.variables.put(Constants.MAUDE_SORT_OCL_STRING, languageString);
			this.variables.put(Constants.MAUDE_SORT_OCL_BOOL, languageBool);
			this.variables.put(Constants.MAUDE_SORT_OCL_REAL, languageReal);
			break;
		case ("SML"):
			this.variables.put(Constants.MAUDE_SORT_SML_INT, languageInt);
			this.variables.put(Constants.MAUDE_SORT_SML_STRING, languageString);
			this.variables.put(Constants.MAUDE_SORT_SML_BOOL, languageBool);
			this.variables.put(Constants.MAUDE_SORT_SML_REAL, languageReal);			
			break;
		}
	}

	public String showVariables() {
		String variablesLine = "";
		Iterator<Entry<String, ArrayList<String>>> iterator = this.variables.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, ArrayList<String>> variables = iterator.next();
			if (variables.getValue().size() > 0) {
				if (variables.getValue().size() > 1) {
					variablesLine = variablesLine + "vars ";
				} else {
					variablesLine = variablesLine + "var  ";
				}
				for (int i = 0; i < variables.getValue().size(); i++) {
					variablesLine = variablesLine + variables.getValue().get(i) + " ";
				}
				variablesLine = variablesLine + Constants.MAUDE_TYPE_SEPARATOR;
				variablesLine = variablesLine + variables.getKey();
				variablesLine = variablesLine + Constants.MAUDE_END_LINE;
				iterator.remove();
			}
		}
		return variablesLine;
	}

	public String showRules() {
		String rules = "";
		for (int i = 0; i < this.getRules().size(); i++) {
			Block lowerLhs = this.getRules().get(i).getLhs().get(this.getRules().get(i).getLhs().size()-1);
			Block lowerRhs = this.getRules().get(i).getRhs().get(this.getRules().get(i).getRhs().size()-1);
			if ((lowerLhs.getRuleBoxes() != null && !lowerLhs.getRuleBoxes().isEmpty())
					|| (lowerRhs.getRuleBoxes() != null && !lowerRhs.getRuleBoxes().isEmpty())) {
				//Handle rule with boxes
				rules += this.getRules().get(i).printWithBoxesVersion();
				rules += this.getRules().get(i).printWithoutBoxesVersion();
			}
			else {
				//Handle rule without boxes
				rules += this.getRules().get(i).toString();
			}
			rules += Constants.BREAK_LINE + Constants.BREAK_LINE;
		}
		return rules;
	}
	
	public static final String getValueCounter() {
		String counterTimes = "";
		for (int i = 0; i < GLOBAL_COUNTER; i++) {
			counterTimes = counterTimes + "s ";
		}
		return counterTimes;
	}

	public static final void increaseValueCounter() {
		GLOBAL_COUNTER = GLOBAL_COUNTER + 1;
	}
	
	public static final void increaseBoxeGlobalCounter() {
		BOXES_GLOBAL_COUNTER = BOXES_GLOBAL_COUNTER + 1;
	}
	
	public static final int getBoxesGlobalCounter () {
		return BOXES_GLOBAL_COUNTER;
	}
	
	public Set<Element> calculateAffectedElements (Set<Element> elementsInBox, RuleBox box){
		
		for (Entity entity : box.getEntities()) {
			if (!elementsInBox.contains(entity)) {
				elementsInBox.add(entity);
			}
		}
		for (Relation relation : box.getRelations()) {
			if (!elementsInBox.contains(relation)) {
				elementsInBox.add(relation);
			}
			if (!elementsInBox.contains(relation.getSource())){
				elementsInBox.add(relation.getSource());
			}
			if (!elementsInBox.contains(relation.getTarget())){
				elementsInBox.add(relation.getTarget());
			}			
		}
		return elementsInBox;
		
	}

}
