package no.hvl.multecore.mcmt.maude.rules;

import java.util.HashMap;

public class RuleCondition {

	private String expression;
	
	//TODO Not used now, maybe in the future if we put more logic into this
	private HashMap <String,String> variables;
	
	public RuleCondition(String expression) {
		this.expression = expression;
		variables = new HashMap<>();
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}
	
	
}
