package no.hvl.multecore.mcmt.maude.rules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import no.hvl.multecore.common.hierarchy.DeclaredAttribute;
import no.hvl.multecore.common.hierarchy.Edge;
import no.hvl.multecore.common.hierarchy.IEdge;
import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.common.hierarchy.InstantiatedAttribute;
import no.hvl.multecore.common.hierarchy.Node;
import no.hvl.multecore.mcmt.maude.Constants;
import no.hvl.multecore.mcmt.maude.Utils;
import no.hvl.multecore.mcmt.proliferation.rule.Box;
import no.hvl.multecore.mcmt.proliferation.rule.Condition;
import no.hvl.multecore.mcmt.proliferation.rule.EdgeConstant;
import no.hvl.multecore.mcmt.proliferation.rule.EdgeVariable;
import no.hvl.multecore.mcmt.proliferation.rule.RuleDeclaredAttribute;
import no.hvl.multecore.mcmt.proliferation.rule.RuleInstantiatedAttribute;
import no.hvl.multecore.mcmt.proliferation.rule.RuleModel;

/**
 * 
 * Each level in the model
 * 
 * @author Alex
 *
 */
public class Block implements Comparable<Block> {

	private int level;
	private String id;
	private String name;
	private String om;
	private List<Entity> elts;
	private String eltsId;
	private List<Relation> rels;
	private String relsId;
	private Rule rule;
	private List<RuleBox> boxes;
	//All the INT variables used in the FROM
	private List<String> intVariables;
	//All the NAME variables used in the FROM
	private List<String> nameVariables;
	//All the NAT variables used in the FROM
	private List<String> natVariables;	
	//All the conditions used in the FROM
	private List<RuleCondition> conditions;
	
	private Map <INode,Integer> typeEntityRegistry;
	private Map <IEdge,Integer> typeRelationRegistry;
	
	public Block() {
		om = "";
		this.elts = new ArrayList<Entity>();
		this.rels = new ArrayList<Relation>();
		this.boxes = new ArrayList<RuleBox>();
		this.intVariables = new ArrayList<String>();
		this.nameVariables = new ArrayList<String>();
		this.natVariables = new ArrayList<String>();
		this.conditions = new ArrayList<RuleCondition>();
	}

	public Block(int level, String id, String name, String om, List<Entity> elts, List<Relation> rels) {
		this.level = level;
		this.id = id;
		this.name = name;
		this.om = om;
		this.elts = elts;
		this.rels = rels;
		this.boxes = new ArrayList<RuleBox>();
		this.intVariables = new ArrayList<String>();
		this.nameVariables = new ArrayList<String>();
		this.natVariables = new ArrayList<String>();		
		this.conditions = new ArrayList<RuleCondition>();
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOm() {
		return om;
	}

	public void setOm(String om) {
		this.om = om;
	}

	public List<Entity> getElts() {
		return elts;
	}

	public void setElts(List<Entity> elts) {
		this.elts = elts;
	}

	public List<Relation> getRels() {
		return rels;
	}

	public void setRels(List<Relation> rels) {
		this.rels = rels;
	}

	public String getEltsId() {
		return eltsId;
	}

	public void setEltsId(String eltsId) {
		this.eltsId = eltsId;
	}

	public String getRelsId() {
		return relsId;
	}

	public void setRelsId(String relsId) {
		this.relsId = relsId;
	}

	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	public Map<INode, Integer> getTypeEntityRegistry() {
		return typeEntityRegistry;
	}

	public void setTypeEntityRegistry(Map<INode, Integer> typeEntityRegistry) {
		this.typeEntityRegistry = typeEntityRegistry;
	}

	public Map<IEdge, Integer> getTypeRelationRegistry() {
		return typeRelationRegistry;
	}

	public void setTypeRelationRegistry(Map<IEdge, Integer> typeRelationRegistry) {
		this.typeRelationRegistry = typeRelationRegistry;
	}

	public Integer getEntityAppareance (INode node) {
		if (this.typeEntityRegistry == null) {
			this.typeEntityRegistry = new HashMap <INode, Integer>();
		}
		if (this.typeEntityRegistry.containsKey(node)) {
			return this.typeEntityRegistry.get(node);
		}
		else {
			this.typeEntityRegistry.put(node, 1);
			return this.typeEntityRegistry.get(node);
		}
	}
	
	public Integer getRelationAppareance (IEdge edge) {
		if (this.typeRelationRegistry == null) {
			this.typeRelationRegistry = new HashMap <IEdge, Integer>();
		}
		if (this.typeRelationRegistry.containsKey(edge)) {
			return this.typeRelationRegistry.get(edge);
		}
		else {
			this.typeRelationRegistry.put(edge, 1);
			return this.typeRelationRegistry.get(edge);
		}
	}
	
	public void incrementEntityAppareance (INode node) {
		if (this.typeEntityRegistry.containsKey(node)) {
			this.typeEntityRegistry.replace(node, this.typeEntityRegistry.get(node)+1);
		}
		else {
			this.typeEntityRegistry.put(node, 1);
		}
	}
	
	public void incrementRelationAppareance (IEdge edge) {
		if (this.typeRelationRegistry.containsKey(edge)) {
			this.typeRelationRegistry.replace(edge, this.typeRelationRegistry.get(edge)+1);
		}
		else {
			this.typeRelationRegistry.put(edge, 1);
		}
	}
	
	public List<RuleBox> getRuleBoxes (){
		return this.boxes;
	}
	
	public void addRuleBox (RuleBox box) {
		this.boxes.add(box);
	}
	
	public void addIntVariable (String var) {
		this.intVariables.add(var);
	}	
	
	public List<String> getIntVariables () {
		return this.intVariables;
	}
	
	public void addNameVariable (String var) {
		this.nameVariables.add(var);
	}	
	
	public List<String> getNameVariables () {
		return this.nameVariables;
	}
	
	public void addNatVariable (String var) {
		this.natVariables.add(var);
	}	
	
	public List<String> getNatVariables () {
		return this.natVariables;
	}		
	
	public List<RuleCondition> getRuleConditions (){
		return this.conditions;
	}
	
	public void addRuleCondition (RuleCondition condition) {
		this.conditions.add(condition);
	}	
	
	public void parseBlock(IModel model) {
		this.level = model.getLevel();
		// TODO add the id or remove it
		this.name = calculateName();
		this.id = calculateBlockId();
		this.eltsId = calculateEltsId();
		this.relsId = calculateRelsId();
		
		//Parse nodes
		for (INode node : model.getNodes()) {
			Entity entity = new Entity();
			entity.setBlock(this);
			entity.parseEntity(node);
			this.elts.add(entity);
			this.getRule().insertNodeEntity(node, entity);
			
			for (DeclaredAttribute declaredAttribute : node.getDeclaredAttributesPlusInherited(false)) {
				if (declaredAttribute instanceof RuleDeclaredAttribute) {
					MetaAttribute metaAttribute = new MetaAttribute();
					metaAttribute.setNode(entity);
					metaAttribute.parseDeclaredAttribute(declaredAttribute);
					entity.addMetaAttribute(metaAttribute);
					if (entity.getAttributeId() == null) {
						entity.setAttributeId(Constants.MAUDE_ATTRI + entity.getId().replace(Constants.MAUDE_ID_OID, ""));
					}
				}
			}
			for (InstantiatedAttribute ruleInstantiatedAttribute : node.getInstantiatedAttributes()) {
				if (ruleInstantiatedAttribute instanceof RuleInstantiatedAttribute) {
					ExpressionAttribute expressionAttribute = new ExpressionAttribute();
					expressionAttribute.setNode(entity);
					expressionAttribute.parseExpressionAttribute((RuleInstantiatedAttribute) ruleInstantiatedAttribute);
					entity.addExpressionAttribute(expressionAttribute);
				}
				if (entity.getAttributeId() == null) {
					entity.setAttributeId(Constants.MAUDE_ATTRI + entity.getId().replace(Constants.MAUDE_ID_OID, ""));

				}
			}
		}
		for (IEdge edge : model.getEdges()) {
			Relation relation = new Relation();
			relation.setBlock(this);
			relation.parseRelation(edge);
			//Process variables
			if (edge instanceof EdgeConstant) {
				relation.setMultiplicityVariable(((EdgeConstant) edge).getMultiplicityVariable());
			}
			if (edge instanceof EdgeVariable) {
				relation.setMultiplicityVariable(((EdgeVariable) edge).getMultiplicityVariable());
			}
			this.rels.add(relation);
			this.getRule().insertEdgeRelation(edge, relation);
		}
		
		if (((RuleModel) model).getBoxes() != null && !((RuleModel) model).getBoxes().isEmpty()) {
			parseBoxes((RuleModel) model);
		}
		
		if (((RuleModel) model).getConditions() != null && !((RuleModel) model).getConditions().isEmpty()) {
			parseConditions((RuleModel) model);
		}
	}



	public void parseBlock(IModel from, IModel to) {
		this.level = to.getLevel();
		// TODO add the id or remove it
		this.name = calculateName();
		this.id = calculateBlockId();
		this.eltsId = calculateEltsId();
		this.relsId = calculateRelsId();
		for (INode node : to.getNodes()) {
			// Check only the from, to see if the entity to be generated is new or it was
			// already in the from block
			//We can assume we only have instantiatedAttributes, ie, ExpressionAttributes in the TO at the bottom
			
			//Cases we need to check for the attributes:
			// 1 - The node had the attribute in the from, and is modified (in nameOrValue is different, but the type is the same, it has been modified)
			// 2 - New node with new attribute
			
			if (isNodeInModel(from, node)) {
				Entity auxEntity = getEntity(node);
				Entity entity = new Entity();
				entity.setBlock(this);
				entity.replicateEntityforToBlock(auxEntity);				
				this.elts.add(entity);
				for (InstantiatedAttribute ruleInstantiatedAttribute : node.getInstantiatedAttributes()) {
					if ((ruleInstantiatedAttribute instanceof RuleInstantiatedAttribute)) {
						ExpressionAttribute expressionAttribute = new ExpressionAttribute();
						ExpressionAttribute auxAttribute = entity.findModifiedAttributeInToBlock(ruleInstantiatedAttribute.getType().getNameOrValue());
						if (auxAttribute.getNode() != null) {
							expressionAttribute.setNode(entity);
							expressionAttribute.parseExistingExpressionAttribute((RuleInstantiatedAttribute) ruleInstantiatedAttribute,auxAttribute);
							entity.removeExpressionAttribute(auxAttribute);
							entity.addExpressionAttribute(expressionAttribute);
						}
						else {
							expressionAttribute.setNode(entity);
							expressionAttribute.parseExpressionAttribute((RuleInstantiatedAttribute) ruleInstantiatedAttribute);
							entity.addExpressionAttribute(expressionAttribute);

						}
					}
				}
			} else {
				Entity entity = new Entity();
				entity.setBlock(this);
				entity.parseNewEntity(node);
				this.elts.add(entity);
				this.getRule().insertNodeEntity(node, entity);
				for (InstantiatedAttribute ruleInstantiatedAttribute : node.getInstantiatedAttributes()) {
					if (ruleInstantiatedAttribute instanceof RuleInstantiatedAttribute) {
						ExpressionAttribute expressionAttribute = new ExpressionAttribute();
						expressionAttribute.setNode(entity);
						expressionAttribute.parseExpressionAttribute((RuleInstantiatedAttribute) ruleInstantiatedAttribute);
						entity.addExpressionAttribute(expressionAttribute);
					}
				}
			}
		}
		for (IEdge edge : to.getEdges()) {
			if (isEdgeInModel(from, edge)) {
				Relation relation = getRelation(edge);
				this.rels.add(relation);
			}
			else {
				Relation relation = new Relation();
				relation.setBlock(this);
				relation.parseNewRelation(edge);
				this.rels.add(relation);
				this.getRule().insertEdgeRelation(edge, relation);
			}
		}
		
		if (((RuleModel) to).getBoxes() != null && !((RuleModel) to).getBoxes().isEmpty()) {
			parseBoxes((RuleModel) to);
		}		
	}

	@Override
	public int compareTo(Block block) {
		int comparelevels = ((Block) block).getLevel();
		if (comparelevels != this.level) {
			return this.level - comparelevels;
		} else if (this.name.equals(no.hvl.multecore.mcmt.proliferation.Constants.FROM_BLOCK_ID)) {
			return -1;
		} else {
			return 1;
		}
	}

	@Override
	public String toString() {
		String result = Constants.TABULATION + Constants.MAUDE_OPEN_CONFIGURATION;
		result = result + calculateLevel() + Constants.MAUDE_TYPE_SEPARATOR + "Model"
				+ Constants.MAUDE_VERTICAL_SEPARATOR + Constants.BREAK_LINE;
		result = result + Constants.TABULATION2 + "name" + Constants.MAUDE_TYPE_SEPARATOR + this.name
				+ Constants.MAUDE_SEPARATE_ATTRIBUTE + Constants.BREAK_LINE;
		result = result + Constants.TABULATION2 + "elts" + Constants.MAUDE_TYPE_SEPARATOR;
		if (this.elts.size() > 0) {
			result = result + "(" + Constants.BREAK_LINE;

			for (int i = 0; i < elts.size(); i++) {
				result = result + elts.get(i).toString() + Constants.BREAK_LINE;
			}
			result = result + Constants.TABULATION2 + this.eltsId + ")";
		} else {
			result = result + Constants.TABULATION2 + this.eltsId;
		}
		result = result + Constants.MAUDE_SEPARATE_ATTRIBUTE + Constants.BREAK_LINE;

		result = result + Constants.TABULATION2 + "rels" + Constants.MAUDE_TYPE_SEPARATOR;
		if (this.rels.size() > 0) {
			result = result + "(" + Constants.BREAK_LINE;

			for (int i = 0; i < rels.size(); i++) {
				result = result + rels.get(i).toString() + Constants.BREAK_LINE;
			}
			result = result + Constants.TABULATION2 + this.relsId + ")";
		} else {
			result = result + Constants.TABULATION2 + this.relsId;
		}
		result = result + Constants.MAUDE_SEPARATE_ATTRIBUTE;
		result = result + Constants.TABULATION2 + this.id + Constants.MAUDE_CLOSE_CONFIGURATION + Constants.BREAK_LINE;
		return result;
	}
	

	private String calculateOm() {
		String om = "";
		for (int i = 2; i < this.level; i++) {
			om = om + "'";
		}
		om = "M" + om;
		return om;
	}

	private String calculateEltsId() {
		String elts = "";
		for (int i = 1; i < this.level; i++) {
			elts = elts + "'";
		}
		elts = Constants.MAUDE_ID_ELTS + elts;
		return elts;
	}

	private String calculateRelsId() {
		String rels = "";
		for (int i = 1; i < this.level; i++) {
			rels = rels + "'";
		}
		rels = Constants.MAUDE_ID_RELS + rels;
		return rels;
	}

	private String calculateBlockId() {
		String blockid = "";
		for (int i = 1; i < this.level; i++) {
			blockid = blockid + "'";
		}
		blockid = Constants.MAUDE_ID_ATTRIBUTE_SET + blockid;
		return blockid;
	}

	private String calculateName() {
		String name = "";
		for (int i = 1; i < this.level; i++) {
			name = name + "'";
		}
		name = "M" + name;
		return name;
	}

	private String calculateLevel() {
		String level = "";
		level = "level(L" + this.level + ")";
		return level;
	}

	private boolean isNodeInModel(IModel from, INode node) {
		boolean found = false;
		for (INode n : from.getNodes()) {
			if (n.getName().equals(node.getName()) && n.getType().getName().equals(node.getType().getName())) {
				found = true;
			}
		}
		return found;
	}

	private boolean isEdgeInModel(IModel from, IEdge edge) {
		boolean found = false;
		for (IEdge e : from.getEdges()) {
			if (e.getName().equals(edge.getName()) && e.getType().getName().equals(edge.getType().getName())
					&& e.getSource().getName().equals(edge.getSource().getName())
					&& e.getTarget().getName().equals(edge.getTarget().getName())) {
				found = true;
			}
		}
		return found;
	}

	public Entity getEntity(INode inode) {
		Entity entity = null;
		// Run over the from block (last model of the lhs, and try to find the entity
		// (matching condition, name)
		for (int i = 0; i < this.rule.getLhs().get(this.rule.getLhs().size() - 1).getElts().size(); i++) {
			Entity aux = this.rule.getLhs().get(this.rule.getLhs().size() - 1).getElts().get(i);
			if (aux.getName().equals(inode.getName()))
				entity = aux;
		}
		return entity;
	}
	
	public Boolean findEntityByName(String name) {
		Boolean foundEntity = false;
		// Run over the from block (last model of the lhs, and try to find the entity
		// (matching condition, name)
		for (int i = 0; i < this.rule.getLhs().get(this.rule.getLhs().size() - 1).getElts().size(); i++) {
			Entity aux = this.rule.getLhs().get(this.rule.getLhs().size() - 1).getElts().get(i);
			if (aux.getType().equals(name))
				foundEntity = true;
		}
		return foundEntity;
	}
	

	public Relation getRelation(IEdge iedge) {
		Relation relation = null;
		// Run over the from block (last entity of the lhs, and try to find the
		// relation
		// (matching condition, name, source and target)
		for (int i = 0; i < this.rule.getLhs().get(this.rule.getLhs().size() - 1).getRels().size(); i++) {
			Relation aux = this.rule.getLhs().get(this.rule.getLhs().size() - 1).getRels().get(i);
			if (aux.getName().equals(iedge.getName()) && aux.getSource().getName().equals(iedge.getSource().getName())
					&& aux.getTarget().getName().equals(iedge.getTarget().getName())) {
				relation = aux;
			}
		}
		return relation;
	}

	
	private void parseBoxes(RuleModel model) {
		for (Box box : model.getBoxes()) {
			String boxesRepresentation = "";
			RuleBox ruleBox = new RuleBox(RulesBlock.getBoxesGlobalCounter(), box.getMultiplicityVariable());
			//TODO THIS IS A TEMPORAL SOLUTION. THIS SHOULD BE PROCESSED WHEN WE INCORPORATE TO THE GRAMMAR THE FUNCTIONS
			//NOTE THAT WE SPECIFY IN THE RULE, E.G., tr_1. WHEN THIS IS IMPROVED WE SHOULD WRITE tr AND SEARCH FOR THE ITEM NAME + NAME APPEARANCE
			if (box.getMultiplicityVariable().startsWith("eval")) {
				processVariablesInEval(box.getMultiplicityVariable());
			}
			else {
				if (!this.intVariables.contains(box.getMultiplicityVariable())) {
					this.intVariables.add(box.getMultiplicityVariable());
				}
			}
			boxesRepresentation += "box[" + box.getMultiplicityVariable() + "]{";
			for (Node n : box.getNodes()) {
				ruleBox.addEntity(this.rule.getEntityFromNodeEntity(n));
				boxesRepresentation += this.rule.getEntityFromNodeEntity(n).getId()+",";
			}
			//If no edges we remove the last comma
			if (box.getEdges().isEmpty()) {
				boxesRepresentation = boxesRepresentation.substring(0, boxesRepresentation.length() - 1);
			}
			for (Edge e : box.getEdges()) {
				ruleBox.addRelation(this.rule.getRelationFromEdgeRelation(e));
				boxesRepresentation += this.rule.getRelationFromEdgeRelation(e).getId()+",";
			}
			this.addRuleBox(ruleBox);
			if (box.getEdges().isEmpty()) {
				boxesRepresentation += ",";
			}
			if (box.getInnerBoxes().isEmpty()) {
				boxesRepresentation = boxesRepresentation.substring(0, boxesRepresentation.length() - 1) + "}";
			}
			for (Box inBox : box.getInnerBoxes()) {
				boxesRepresentation += processInnerBoxes(ruleBox, inBox);
			}
			ruleBox.setBoxesRepresentation(boxesRepresentation);
		}
		
	}

	private void processVariablesInEval(String multiplicityVariable) {
		 String nodeVariable = Utils.getBetweenStrings(multiplicityVariable, "eval(", " .");
		if (!this.nameVariables.contains(nodeVariable)) {
			this.nameVariables.add(nodeVariable);
		}
		 String levelVariable = Utils.getBetweenStrings(multiplicityVariable, "id(", ",");
			if (!this.natVariables.contains(levelVariable)) {
				this.natVariables.add(levelVariable);
			}

	}

	private String processInnerBoxes(RuleBox ruleBox, Box inBox) {
		RuleBox inRuleBox = new RuleBox(RulesBlock.getBoxesGlobalCounter(), inBox.getMultiplicityVariable());
		if (!this.intVariables.contains(inBox.getMultiplicityVariable())) {
			this.intVariables.add(inBox.getMultiplicityVariable());
		}
		String boxesRepresentation = "";
		boxesRepresentation += "box[" + inBox.getMultiplicityVariable() + "]{";
		for (Node n : inBox.getNodes()) {
			inRuleBox.addEntity(this.rule.getEntityFromNodeEntity(n));
			boxesRepresentation += this.rule.getEntityFromNodeEntity(n).getId()+",";
		}
		if (inBox.getEdges().isEmpty()) {
			boxesRepresentation = boxesRepresentation.substring(0, boxesRepresentation.length() - 1);
		}		
		for (Edge e : inBox.getEdges()) {
			inRuleBox.addRelation(this.rule.getRelationFromEdgeRelation(e));
			boxesRepresentation += this.rule.getRelationFromEdgeRelation(e).getId()+",";
		}
		ruleBox.addInnerBox(inRuleBox);
		if (inBox.getEdges().isEmpty()) {
			boxesRepresentation += ",";
		}				
		if (inBox.getInnerBoxes().isEmpty()) {
			boxesRepresentation = boxesRepresentation.substring(0, boxesRepresentation.length() - 1) + "}";
		}	
		for (Box innerBox : inBox.getInnerBoxes()) {
			processInnerBoxes(inRuleBox, innerBox);	
		}
		return boxesRepresentation + "}";
	}
	
	private void parseConditions(RuleModel model) {
		for (Condition condition : model.getConditions()) {
			RuleCondition ruleCondition = new RuleCondition(condition.getExpression());
			this.conditions.add(ruleCondition);
		}
	}
	
}
