package no.hvl.multecore.mcmt.maude.rules;

import no.hvl.multecore.common.hierarchy.DeclaredAttribute;
import no.hvl.multecore.mcmt.maude.Constants;

public class MetaAttribute extends Attribute{

	
	public MetaAttribute() {
		super();
	}
	
	public void parseDeclaredAttribute (DeclaredAttribute declaredAttribute) {
		this.id = Constants.MAUDE_ID_OID + String.format("%02d", this.getNode().getBlock().getRule().getVariableCounter());
		this.attributeId = Constants.MAUDE_ID_ATTRIBUTE_SET + String.format("%02d", this.getNode().getBlock().getRule().getVariableCounter());
		this.getNode().getBlock().getRule().increaseVariableCounter();
		this.nameOrValue = Constants.MAUDE_ID_ID_NEW + "(L" + declaredAttribute.getContainingNode().getModel().getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE + "\""+declaredAttribute.getNameOrValue()+"\")";
		this.type = Constants.MAUDE_ID_ID_NEW + "(L" + declaredAttribute.getContainingNode().getModel().getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE + "\""+declaredAttribute.getType().getNameOrValue()+"\")";
	}
	
	public String toString() {
		String attString = "";
		attString = attString + Constants.TABULATION3 + Constants.TABULATION3 + Constants.MAUDE_OPEN_CONFIGURATION
				+ this.id + Constants.MAUDE_TYPE_SEPARATOR + Constants.MAUDE_ATTRI + Constants.MAUDE_VERTICAL_SEPARATOR;
		attString = attString + "nameOrValue" + Constants.MAUDE_TYPE_SEPARATOR + this.nameOrValue
				+ Constants.MAUDE_SEPARATE_ATTRIBUTE;
		attString = attString + "type" + Constants.MAUDE_TYPE_SEPARATOR + this.type
				+ Constants.MAUDE_SEPARATE_ATTRIBUTE;
		attString = attString + this.attributeId + Constants.MAUDE_CLOSE_CONFIGURATION + Constants.BREAK_LINE;
		return attString;
	}
}
