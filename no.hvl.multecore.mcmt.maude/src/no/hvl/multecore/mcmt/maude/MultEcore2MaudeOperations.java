package no.hvl.multecore.mcmt.maude;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Map.Entry;

import no.hvl.multecore.common.Debugger;
import no.hvl.multecore.common.hierarchy.DeclaredAttribute;
import no.hvl.multecore.common.hierarchy.IEdge;
import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.common.hierarchy.InstantiatedAttribute;
import no.hvl.multecore.mcmt.maude.rules.Rule;
import no.hvl.multecore.mcmt.maude.rules.RulesBlock;
import no.hvl.multecore.mcmt.proliferation.rule.RuleHierarchy;

public class MultEcore2MaudeOperations {

	private String absolutePath;
	private RulesBlock rulesBlock;
	private String additionalLanguage;

	public MultEcore2MaudeOperations() {
		absolutePath = "";
		rulesBlock = new RulesBlock();
		additionalLanguage = "";
	}

	public MultEcore2MaudeOperations(String absolutePath) {
		this.absolutePath = absolutePath;
	}

	public File createFile(String projectLocation, String fileName) {
		this.absolutePath = projectLocation + no.hvl.multecore.core.Constants.URI_SEPARATOR_SERIALIZED + fileName;
		this.absolutePath = this.absolutePath.replace("/", "//");
		File newFile = new File(this.absolutePath);
		try {
			if (newFile.createNewFile()) {
				System.out.println(fileName + " File Created");
			} else
				System.out.println("File " + fileName + " already exists in project root directory");
		} catch (IOException e) {
			Debugger.logError("Could not create file " + fileName);
			e.printStackTrace();
		}
		return newFile;
	}
	
	
	public String getAdditionalLanguage() {
		return additionalLanguage;
	}

	public void setAdditionalLanguage(String additionalLanguage) {
		this.additionalLanguage = additionalLanguage;
	}

	public File addTwoTupleBlock(File newFile) {
		String data = "";
		try {
		
		FileOutputStream out = new FileOutputStream(newFile);
		data = data + "fmod 2-TUPLE{X :: TRIV, Y :: TRIV} is" + Constants.BREAK_LINE; 
		data = data + Constants.TABULATION + "sort Tuple{X, Y}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op ((_,_)) : X$Elt Y$Elt -> Tuple{X, Y}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op p1_ : Tuple{X, Y} -> X$Elt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op p2_ : Tuple{X, Y} -> Y$Elt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq p1(A:X$Elt, B:Y$Elt) = A:X$Elt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq p2(A:X$Elt, B:Y$Elt) = B:Y$Elt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endfm";
		out.write(data.getBytes());
		out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return newFile;
	}	
	
	
	public File addThreeTupleBlock(File newFile) {
		String data = "";
		try {
		
		FileOutputStream out = new FileOutputStream(newFile);
		data = data + "fmod 3-TUPLE{X :: TRIV, Y :: TRIV, Z :: TRIV} is" + Constants.BREAK_LINE; 
		data = data + Constants.TABULATION + "sort Tuple{X, Y, Z}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op ((_,_,_)) : X$Elt Y$Elt Z$Elt -> Tuple{X, Y, Z}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op p1_ : Tuple{X, Y, Z} -> X$Elt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op p2_ : Tuple{X, Y, Z} -> Y$Elt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op p3_ : Tuple{X, Y, Z} -> Z$Elt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq p1(A:X$Elt, B:Y$Elt, C:Z$Elt) = A:X$Elt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq p2(A:X$Elt, B:Y$Elt, C:Z$Elt) = B:Y$Elt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq p3(A:X$Elt, B:Y$Elt, C:Z$Elt) = C:Z$Elt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endfm";
		out.write(data.getBytes());
		out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return newFile;
	}
	
	public File addMlmOclBlock(File mlmOclFile) {
		String data = "";
		try {
		FileOutputStream out = new FileOutputStream(mlmOclFile);
		data = data + "sload unboxing.maude" + Constants.BREAK_LINE;
		data = data + "sload ocl.maude" + Constants.BREAK_LINE;
		
		data = data + "mod MLM-UNBOXING is" + Constants.BREAK_LINE; 
		data = data + Constants.TABULATION + "pr UNBOXING{Ocl}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;

		data = data + "mod MLM-OCL is" + Constants.BREAK_LINE; 
		data = data + Constants.TABULATION + "pr OCL{Ocl}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq value2ocl(E:OclExp) = E:OclExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;
	
		data = data + "mod MLM is" + Constants.BREAK_LINE; 
		data = data + Constants.TABULATION + "pr MLM-OCL" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "pr UNBOX-RULE" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;		
		
		out.write(data.getBytes());
		out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return mlmOclFile;
	}
	
	public File addOclBlock(File ocl) {
		String data = "";
		try {
		FileOutputStream out = new FileOutputStream(ocl);
		
		data = data + "sload mlm-core.maude" + Constants.BREAK_LINE + Constants.BREAK_LINE; 
		data = data + "sload ocl-prelude.maude" + Constants.BREAK_LINE; 
		
		data = data + "mod OCL-SIGN is" + Constants.BREAK_LINE; 
		data = data + Constants.TABULATION + "pr OCL-PRELUDE" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "pr MLM-NAME" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "pr INT" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		data = data + Constants.TABULATION + "sorts OclExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "subsort OclBool OclInt OclString OclFloat < OclExp" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		data = data + Constants.TABULATION + "op _._-> size`(`) : Name Name -> OclExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op _._ : Name Name -> OclExp" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;		

		data = data + Constants.TABULATION + "var OclNzN : OclNzNat" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		data = data + Constants.TABULATION + "op ocl2Maude : OclInt -> Int" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq ocl2Maude(0) = 0" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq ocl2Maude(s 0) = 1" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq ocl2Maude(s OclNzN) = s ocl2Maude(OclNzN)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq ocl2Maude(-OclInt OclNzN) = - ocl2Maude(OclNzN)" + Constants.MAUDE_END_LINE;
		data = data + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;		
				
		data = data + "mod OCL{V :: TRIV} is" + Constants.BREAK_LINE; 
		data = data + Constants.TABULATION + "pr OCL-SIGN" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "pr MLM-CORE{V}" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

		data = data + Constants.TABULATION + "op value2ocl : V$Elt -> OclExp" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

		
		data = data + Constants.TABULATION + "op eval : OclExp Configuration -> Int" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq eval(OCLE, Conf) = ocl2Maude(evalOcl(OCLE, Conf))" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

		String[] nameVariables = {"NM", "N", "N'", "Type", "PN", "PN'"};
		data = createVariablesLine(nameVariables, "Name", data);
		String[] stringVariables = {"sourceName", "typeName", "matchedTypeName"};
		data = createVariablesLine(stringVariables, "String", data);		
		String[] configurationVariables = {"Conf", "Conf'", "Rels", "Elts"};
		data = createVariablesLine(configurationVariables, "Configuration", data);
		String[] natVariables = {"sourceLevel", "typeLevel", "matchedTypeLevel"};
		data = createVariablesLine(natVariables, "Nat", data);
		String[] oidVariables = {"Oid'", "O1", "O2"};
		data = createVariablesLine(oidVariables, "Oid", data);
		String[] attributeSetVariables = {"Atts", "Atts'", "Atts''"};
		data = createVariablesLine(attributeSetVariables, "AttributeSet", data);
		String[] OclExpSetVariables = {"OCLE"};
		data = createVariablesLine(OclExpSetVariables, "OclExp", data);
		String[] valueVariables = {"V"};
		data = createVariablesLine(valueVariables, "V$Elt", data);
		
		data = data + Constants.TABULATION + "op evalOcl : OclExp System -> OclExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op evalOcl : OclExp Configuration -> OclExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq evalOcl(OCLE, { Conf }) = evalOcl(OCLE, Conf)" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "eq evalOcl(id(sourceLevel, sourceName) . id(typeLevel, typeName) -> size()," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "< level(sourceLevel) : Model |" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "rels : (" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + Constants.TABULATION3 + "----matched relation (here we could have another kind of arc, which still is an instance of the type given as parameter)" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + Constants.TABULATION3 + "< Oid' : Relation | type : id(matchedTypeLevel, matchedTypeName), source : id(sourceLevel, sourceName), Atts' >" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + Constants.TABULATION3 + "Rels)," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "Atts'' >" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "Conf')" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "----If the matched type is the one we are looking for" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "= if id(matchedTypeLevel, matchedTypeName) == id(typeLevel, typeName)" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "---- or it is transitively we sum 1 and continue searching" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "or-else *(id(matchedTypeLevel, matchedTypeName), level(matchedTypeLevel), id(typeLevel, typeName), Conf')" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "then (1).OclNzNat" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "else (0).OclZero" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "fi" + Constants.BREAK_LINE;	
		data = data + Constants.TABULATION3 + "+ evalOcl(id(sourceLevel, sourceName) . id(typeLevel, typeName) -> size()," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + Constants.TABULATION3 + "< level(sourceLevel) : Model |" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + Constants.TABULATION3 + "rels : Rels," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + Constants.TABULATION3 + "Atts'' >" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + Constants.TABULATION3 + "Conf')" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "eq evalOcl(NM . Type -> size(), < level(sourceLevel) : Model | rels : Rels, Atts > Conf) = 0 [owise]" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;	
		
		data = data + Constants.TABULATION + "eq evalOcl(PN . PN'," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "Conf" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "< level(4) : Model |" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + Constants.TABULATION3 + "elts : < O1 : Node | name : PN," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + Constants.TABULATION3 + "attributes : < O2 : Attri | type : PN', nameOrValue : V >," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + Constants.TABULATION3 + "Atts >" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + Constants.TABULATION3 + "Elts," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "Atts' >)" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "= value2ocl(V)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;
		
		data = data + "view Ocl from TRIV to OCL-SIGN is" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "sort Elt to OclExp" + Constants.MAUDE_END_LINE;
		data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;			
			
		out.write(data.getBytes());
		out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ocl;
	}

	public File addMlmPreludeBlock(File newFile) {
		String data = "";
		try {
			FileOutputStream out = new FileOutputStream(newFile);
			data = data + "sload 2-tuple.maude" + Constants.BREAK_LINE; 
			data = data + "sload 3-tuple.maude" + Constants.BREAK_LINE + Constants.BREAK_LINE;
			
			data = data + "view NatList from TRIV to META-LEVEL is" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION + "sort Elt to NatList" + Constants.MAUDE_END_LINE;
			data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;				

			data = data + "view Term from TRIV to META-TERM is" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION + "sort Elt to Term" + Constants.MAUDE_END_LINE;
			data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;
			
			data = data + "view Substitution from TRIV to META-LEVEL is" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION + "sort Elt to Substitution" + Constants.MAUDE_END_LINE;
			data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;			
			
			data = data + "mod MLM-CONFIGURATION is" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION + "pr CONFIGURATION * (op <_:_|_> : Oid Cid AttributeSet -> Object" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION + "to <_:_|_> [format (ni!r o !r o !r o !r o)])" + Constants.MAUDE_END_LINE;
			data = data + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;
			
			data = data + "view Oid from TRIV to MLM-CONFIGURATION is" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION + "sort Elt to Oid" + Constants.MAUDE_END_LINE;
			data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;

			data = data + "view Object from TRIV to MLM-CONFIGURATION is" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION + "sort Elt to Object" + Constants.MAUDE_END_LINE;
			data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;			
			
			
			data = data + "view Configuration from TRIV to MLM-CONFIGURATION is" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION + "sort Elt to Configuration" + Constants.MAUDE_END_LINE;
			data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;				
			
			data = data + "fmod INT* is" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION + "pr INT" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "sort Int*" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "subsort Int < Int*" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op * : -> Int*" + Constants.MAUDE_END_LINE;
			data = data + "endfm" + Constants.BREAK_LINE + Constants.BREAK_LINE;

			data = data + "view Int* from TRIV to INT* is" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION + "sort Elt to Int*" + Constants.MAUDE_END_LINE;
			data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;				
			
			data = data + "mod MLM-NAME is" + Constants.BREAK_LINE;			
			data = data + Constants.TABULATION + "pr MLM-CONFIGURATION" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "pr META-LEVEL" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "pr STRING" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

			data = data + Constants.TABULATION + "sort Name" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op id : Nat NatList -> Name" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op id : Nat String -> Name" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
			
			data = data + Constants.TABULATION + "op counter : -> Oid" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op level : Nat -> Oid" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op oid : Nat NatList -> Oid" + Constants.MAUDE_END_LINE;
			data = data + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;
		
			
			out.write(data.getBytes());
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return newFile;
	}


	public File addCoreBlock(File newFile) {
		String data = "";
		try {
			FileOutputStream out = new FileOutputStream(newFile);
			
			
			data = data + "sload mlm-prelude.maude" + Constants.BREAK_LINE + Constants.BREAK_LINE;
			
			data = data + "mod MLM-CORE{V :: TRIV} is" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION + "pr SET{Oid}" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "pr MLM-NAME" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "pr 2-TUPLE{NatList, Int*}" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "pr FLOAT" + Constants.MAUDE_END_LINE;
			data = data + Constants.BREAK_LINE;
			
			data = data + Constants.TABULATION + "sort System" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op {_} : Configuration -> System" + Constants.MAUDE_END_LINE;
			data = data + Constants.BREAK_LINE;
			
			data = data + Constants.TABULATION + "sort Model" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "subsort Model < Cid" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op Model : -> Model" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op name`:_ : String -> Attribute [prec 20 gather (&)]"
					+ Constants.MAUDE_END_LINE;			
			data = data + Constants.TABULATION + "op name`:_ : Name -> Attribute [prec 20 gather (&)]"
					+ Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op om`:_ : String -> Attribute [prec 20 gather (&)]"
					+ Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op elts`:_ : Configuration -> Attribute [prec 20 gather (&) format (o o ++i --i)]"
					+ Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op rels`:_ : Configuration -> Attribute [prec 20 gather (&) format (o o ++i --i)]"
					+ Constants.MAUDE_END_LINE;
			data = data + Constants.BREAK_LINE;
			
			data = data + Constants.TABULATION + "sort Node" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "subsort Node < Cid" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op Node : -> Node" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op type`:_ : Name -> Attribute [prec 20 gather (&)]"
					+ Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op attributes`:_ : Configuration -> Attribute [prec 20 gather (&) format (o o ++i --i)]"
					+ Constants.MAUDE_END_LINE;	
			data = data + Constants.BREAK_LINE;
			
			data = data + Constants.TABULATION + "sort Attri" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "subsort Attri < Cid" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op Attri : -> Attri" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op nameOrValue`:_ : Name -> Attribute [prec 20 gather (&)]" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op nameOrValue`:_ : V$Elt -> Attribute [prec 20 gather (&)]" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op type`:_ : Name -> Attribute [prec 20 gather (&)]" + Constants.MAUDE_END_LINE;
			data = data + Constants.BREAK_LINE;		
			
			data = data + Constants.TABULATION + "sort Relation" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "subsort Relation < Cid" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op Relation : -> Relation" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "ops source`:_ target`:_ : Name -> Attribute [prec 20 gather (&)]"
					+ Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op min-mult`:_ : Nat -> Attribute [prec 20 gather (&)]"
					+ Constants.MAUDE_END_LINE;		
			data = data + Constants.TABULATION + "op max-mult`:_ : Int* -> Attribute [prec 20 gather (&)]"
					+ Constants.MAUDE_END_LINE;				
			data = data + Constants.TABULATION + "op type`:_ : Name -> Attribute [prec 20 gather (&)]"
					+ Constants.MAUDE_END_LINE;
			data = data + Constants.BREAK_LINE;
			
			data = data + Constants.TABULATION + "sort Counter" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "subsort Counter < Cid" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op Counter : -> Counter" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op value`:_ : Nat -> Attribute [prec 20 gather (&)]"
					+ Constants.MAUDE_END_LINE;
			data = data + Constants.BREAK_LINE;

			String[] nameVariables = { "NM", "Type", "Type'", "ActualType", "*Part", "sourceName", "typeName", "matchedTypeName"};
			data = createVariablesLine(nameVariables, "Name", data);
			String[] oidVariables = { "O", "Oid", "Oid'", "O01" };
			data = createVariablesLine(oidVariables, "Oid", data);
			String[] configurationVariables = { "Conf", "Elts", "Rels", "Conf'" };
			data = createVariablesLine(configurationVariables, "Configuration", data);
			String[] natVariables = { "L", "N", "sourceLevel", "typeLevel", "matchedTypeLevel"};
			data = createVariablesLine(natVariables, "Nat", data);
			String[] attributeSetVariables = { "Atts", "Atts01", "Atts03", "Atts'", "Atts''"};
			data = createVariablesLine(attributeSetVariables, "AttributeSet", data);
			
			//Star Operation to calculate transitive typing
			data = starOperation(data);

			data = data + Constants.TABULATION + "sort BoxIndex" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "subsort Int* < BoxIndex" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op box[_]{_} : BoxIndex Set{Oid} -> Oid" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op boxes : Set{Oid} -> Object" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op let : V$Elt V$Elt -> Oid" + Constants.MAUDE_END_LINE;
				
			data = data + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;
			
			out.write(data.getBytes());
			out.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return newFile;
	}


	public File addUnboxingBlock(File mlmFile) {
		String data = "";
		try {
			FileOutputStream out = new FileOutputStream(mlmFile);
			data = data + "---- floats cannot be used in Attributes" + Constants.BREAK_LINE + Constants.BREAK_LINE;
			data = data + "sload ocl.maude" + Constants.BREAK_LINE + Constants.BREAK_LINE; 
			//UNBOXING-MODULE
			data = unboxingModule(data);

			//UNBOX-MODULE
			data = unboxModule (data);
		
			out.write(data.getBytes());
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return mlmFile;
	}	


	public File addHierarchyBlock(List<IModel> models, File newFile) {
		String data = "";
		try {
			FileOutputStream out = new FileOutputStream(newFile, false);
			data = data  + "load generic-" + models.get(1).getName().toLowerCase() + ".maude" + Constants.BREAK_LINE + Constants.BREAK_LINE;			
			data = data + "mod " + models.get(1).getName().toUpperCase() + " is" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION + "pr " + ("generic-" + models.get(1).getName()).toUpperCase() + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
			data = data + Constants.TABULATION + "op MLM : -> System" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "eq MLM = {" + Constants.BREAK_LINE;
			data = data + Constants.BREAK_LINE;
			data = data + Constants.TABULATION + Constants.TABULATION + Constants.MAUDE_OPEN_CONFIGURATION
					+ "counter : Counter | value : 100" + Constants.MAUDE_CLOSE_CONFIGURATION + Constants.BREAK_LINE;
			// Running over each model to build the hierarchy
			for (int i = 0; i < models.size(); i++) {
				data = data + Constants.TABULATION + Constants.TABULATION + Constants.MAUDE_OPEN_CONFIGURATION
						+ "level(" + models.get(i).getLevel() + ") : Model |" + Constants.BREAK_LINE;
				data = data + Constants.TABULATION + Constants.TABULATION + Constants.TABULATION + "name : \""
						+ models.get(i).getName() + "\"" + Constants.MAUDE_SEPARATE_ATTRIBUTE + Constants.BREAK_LINE;
				data = data + Constants.TABULATION + Constants.TABULATION + Constants.TABULATION + "om   : \""
						+ models.get(i).getMainMetamodel().getName() + "\"" + Constants.MAUDE_SEPARATE_ATTRIBUTE
						+ Constants.BREAK_LINE;
				data = data + Constants.TABULATION + Constants.TABULATION + Constants.TABULATION + "elts : ";
				int counter = 1;
				if (models.get(i).getNodes().size() > 1) {
					data = data + "(";
				}
				boolean firstIteration = true;
				// Running over each node
				for (INode node : models.get(i).getNodes()) {
					if (!firstIteration) {
						data = data + Constants.TABULATION + Constants.TABULATION + Constants.TABULATION
								+ Constants.TABULATION + Constants.TABULATION;
					}
					data = data + Constants.MAUDE_OPEN_CONFIGURATION + "oid(" + models.get(i).getLevel()
							+ Constants.MAUDE_SEPARATE_ATTRIBUTE + counter + ")" + " : " + Constants.MAUDE_NODE
							+ " | ";
					data = data + "name : " + "id(" + models.get(i).getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE
							+ "\"" + node.getName() + "\")" + Constants.MAUDE_SEPARATE_ATTRIBUTE;
					data = data + "type : " + "id(" + node.getType().getModel().getLevel()
							+ Constants.MAUDE_SEPARATE_ATTRIBUTE + "\"" + node.getType().getName() + "\")";
					counter++;
					//Running over each attribute
					if (!node.getDeclaredAttributesPlusInherited(false).isEmpty() ||
							!node.getInstantiatedAttributes().isEmpty()){
						

						data = data + Constants.MAUDE_SEPARATE_ATTRIBUTE + Constants.BREAK_LINE + Constants.TABULATION3 + Constants.TABULATION3 +
								"attributes : (";
						//Declared Attributes						
						for (DeclaredAttribute attribute : node.getDeclaredAttributesPlusInherited(false)) {
							data = data + Constants.BREAK_LINE + Constants.TABULATION3 + Constants.TABULATION3 + Constants.TABULATION2;
							data = data + Constants.MAUDE_OPEN_CONFIGURATION + "oid(" + models.get(i).getLevel()
									+ Constants.MAUDE_SEPARATE_ATTRIBUTE + counter + ")" + " : " + Constants.MAUDE_ID_NODE_ATTRIBUTE + " | ";
							data = data + "nameOrValue" + " : " + "id(" + models.get(i).getLevel()
									+ Constants.MAUDE_SEPARATE_ATTRIBUTE + "\"" + attribute.getNameOrValue() + "\")"
									+ Constants.MAUDE_SEPARATE_ATTRIBUTE;
							data = data + "type" + " : " + "id(" + models.get(i).getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE  + "\"" + attribute.getType().getNameOrValue() + "\")";
							data = data + Constants.MAUDE_CLOSE_CONFIGURATION + Constants.BREAK_LINE; 
							counter++; 
						}
						//InstantiatedAttributes
						for (InstantiatedAttribute attribute : node.getInstantiatedAttributes()) {
							data = data + Constants.BREAK_LINE + Constants.TABULATION3 + Constants.TABULATION3 + Constants.TABULATION2;
							data = data + Constants.MAUDE_OPEN_CONFIGURATION + "oid(" + models.get(i).getLevel()
									+ Constants.MAUDE_SEPARATE_ATTRIBUTE + counter + ")" + " : " + Constants.MAUDE_ID_NODE_ATTRIBUTE + " | ";
							data = data + "nameOrValue" + " : ";
							
							if (attribute.getType().getType().getNameOrValue().equals("String") && !attribute.getNameOrValue().startsWith("\"") && !attribute.getNameOrValue().endsWith("\"")) {
								data = data + "\"" + attribute.getNameOrValue() + "\""  + Constants.MAUDE_SEPARATE_ATTRIBUTE;
							}
							else {
								data = data + attribute.getNameOrValue() + Constants.MAUDE_SEPARATE_ATTRIBUTE;
							}
							data = data + "type" + " : " + "id(" + attribute.getType().getModel().getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE  + "\"" + attribute.getType().getNameOrValue() + "\")";
							data = data + Constants.MAUDE_CLOSE_CONFIGURATION + Constants.BREAK_LINE; 
							counter++;
						}
						data = data + Constants.TABULATION3 + Constants.TABULATION3 + Constants.TABULATION2 + ")>" + Constants.BREAK_LINE;
					}
					else {
						data = data + Constants.MAUDE_SEPARATE_ATTRIBUTE + "attributes : none >" + Constants.BREAK_LINE;
					}
					firstIteration = false;
				}
				if (models.get(i).getNodes().size() > 1) {
					data = data + Constants.TABULATION + Constants.TABULATION + Constants.TABULATION
							+ Constants.TABULATION + Constants.TABULATION + ")";
				}
				data = data + Constants.MAUDE_SEPARATE_ATTRIBUTE;
				data = data + Constants.BREAK_LINE;
				data = data + Constants.TABULATION + Constants.TABULATION + Constants.TABULATION + "rels : ";
				
				firstIteration = true;
				// Running over each relation
				if (!models.get(i).getEdges().isEmpty()) {
					if (models.get(i).getEdges().size() > 1) {
						data = data + "(";
					}
					for (IEdge edge : models.get(i).getEdges()) {
						if (!firstIteration) {
							data = data + Constants.TABULATION + Constants.TABULATION + Constants.TABULATION
									+ Constants.TABULATION + Constants.TABULATION;
						}
						data = data + Constants.MAUDE_OPEN_CONFIGURATION + "oid(" + models.get(i).getLevel()
								+ Constants.MAUDE_SEPARATE_ATTRIBUTE + counter + ")" + " : " + Constants.MAUDE_RELATION
								+ " | ";
						data = data + "name : " + "id(" + models.get(i).getLevel() + Constants.MAUDE_SEPARATE_ATTRIBUTE
								+ "\"" + edge.getName() + "\")" + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						data = data + "type : " + "id(" + edge.getType().getModel().getLevel()
								+ Constants.MAUDE_SEPARATE_ATTRIBUTE + "\"" + edge.getType().getName() + "\")"
								+ Constants.MAUDE_SEPARATE_ATTRIBUTE;
						data = data + "source : " + "id(" + edge.getSource().getModel().getLevel()
								+ Constants.MAUDE_SEPARATE_ATTRIBUTE + "\"" + edge.getSource().getName() + "\")"
								+ Constants.MAUDE_SEPARATE_ATTRIBUTE;
						data = data + "target : " + "id(" + edge.getTarget().getModel().getLevel()
								+ Constants.MAUDE_SEPARATE_ATTRIBUTE + "\"" + edge.getTarget().getName() + "\")"
								+ Constants.MAUDE_SEPARATE_ATTRIBUTE;
						data = data + "min-mult : " + edge.getLowerBound() + Constants.MAUDE_SEPARATE_ATTRIBUTE;
						String maxMult = "";
						if (edge.getUpperBound() == -1) {
							maxMult = "*";
						}
						else {
							maxMult = String.valueOf(edge.getUpperBound());
						}
						data = data + "max-mult : " + maxMult + Constants.MAUDE_CLOSE_CONFIGURATION;
						data = data + Constants.BREAK_LINE;
						counter++;
						firstIteration = false;
					}
				}
				else {
					data = data + "none";
				}
				if (models.get(i).getEdges().size() > 1) {
					data = data + Constants.TABULATION3 + Constants.TABULATION + ")";
				}
				data = data + Constants.MAUDE_CLOSE_CONFIGURATION;
				data = data + Constants.BREAK_LINE;
			}
			data = data + " }" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
			
			data = data + "endm" + Constants.BREAK_LINE;
			out.write(data.getBytes());
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return newFile;
	}

	public File addRulesBlock(HashMap<String, RuleHierarchy> rules, File newFile, List<IModel> selectedHierarchy) {
		String data = "";
		try {
			FileOutputStream out = new FileOutputStream(newFile, false);
			data += processRules(rules, selectedHierarchy);
			out.write(data.getBytes());
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return newFile;
	}

	private String processRules(HashMap<String, RuleHierarchy> rules, List<IModel> selectedHierarchy) {
		String processedRules = "";
		Iterator<Entry<String, RuleHierarchy>> it = rules.entrySet().iterator();
		while (it.hasNext()) {
			// Pair contains each rule
			Entry<String, RuleHierarchy> pair = it.next();
			Rule rule = new Rule();
			rule.setIsConditional(true);
			rule.setNameModule("GENERIC-"+ selectedHierarchy.get(1).getName().toUpperCase());
			rule.setIsFlexible(false);
			rule.setName(pair.getValue().getName());
			rule.parseRule(pair.getValue());
			this.rulesBlock.addRules(rule);
			this.rulesBlock.calculateVariables();
			it.remove();
		}
		
		//TODO Note this load is dependent of the language! For the moment OCL
		processedRules += "sload mlm-ocl.maude" + Constants.BREAK_LINE + Constants.BREAK_LINE;  
		processedRules += Constants.TABULATION + "mod GENERIC-" + selectedHierarchy.get(1).getName().toUpperCase() + " is"+ Constants.BREAK_LINE;
		processedRules += Constants.TABULATION + "pr MLM" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		
		processedRules += this.rulesBlock.showVariables();
		processedRules += Constants.BREAK_LINE;

		/*
		 * Things to do:
		 * Now the generated elements for which we use s s s N... have the same counter for oid and id in the element
		 * We need to add a duplicate version of the rule. Idea, having two different toString to show the rule in its different forms
		 * Add the boxes in the new form
		 * RHS in the rule without boxes is just a term!
		 * 
		 */ 
		
		processedRules += this.rulesBlock.showRules();
		processedRules += Constants.BREAK_LINE;
//		if (RulesBlock.getBoxesGlobalCounter()> 1) {
//			processedRules += this.rulesBlock.showBoxesSpecification();			
//		}
		processedRules +=  "endm" + Constants.BREAK_LINE;
		RulesBlock.GLOBAL_COUNTER = 1;
		RulesBlock.BOXES_GLOBAL_COUNTER= 1;
		return processedRules;
	}

	private String createVariablesLine(String[] variables, String sort, String data) {
		if (variables.length == 1) {
			data = data + Constants.TABULATION + "var ";
		} else {
			data = data + Constants.TABULATION + "vars ";
		}
		for (int i = 0; i < variables.length; i++) {
			data = data + variables[i] + " ";
		}
		data = data + ": ";
		data = data + sort + Constants.MAUDE_END_LINE;
		return data;
	}

	private String starOperation(String data) {
		data = data + Constants.TABULATION + "op * : Name Oid Name Configuration -> Bool" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op *aux : Name Oid Name Configuration -> Bool" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq *(ActualType, level(L), Type, Conf)"
				+ "= Type == ActualType or-else *aux(ActualType, level(L), Type, Conf)"
				+ Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq *aux(ActualType, level(s L), Type, < level(s L) : Model | elts : ("
				+ "< O01 : Node  | name : ActualType, type : Type', Atts01 >" + " Elts), Atts > Conf)"
				+ "= *(Type', level(L), Type, Conf)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq *aux(ActualType, level(s L), Type, < level(s L) : Model | rels : ("
				+ "< O01 : Relation | name : ActualType, type : Type', Atts03 >" + " Rels), Atts > Conf)"
				+ "= *(Type', level(L), Type, Conf)" + Constants.MAUDE_END_LINE;
		//Note that Paco here changed to Oid in the second parameter from previously : level(0)
		data = data + Constants.TABULATION + "eq *aux(ActualType, Oid, Type, Conf) = false [owise]"
				+ Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		return data;
	}
	
	private String unboxingModule(String data) {
		data = data + "mod UNBOXING{V :: TRIV} is" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "pr OCL{V}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "pr 2-TUPLE{Configuration, Set{Oid}}" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "var  S : System" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars N M L L' : Nat" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  V : V$Elt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  I : Int" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  I* : Int*" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars Conf Elts Elts' Rels Rels' : Configuration" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars OS OS' OS'' : Set{Oid}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars NOS : NeSet{Oid}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars NL NL' : NatList" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars O O' : Oid" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars Atts Atts' Atts'' Atts''' : AttributeSet" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  NM : Name" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  St : String" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  BI : BoxIndex" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		
		data = data + Constants.TABULATION + "op vOid : Oid NatList -> Oid" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op vId : Name NatList -> Name" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		data = data + Constants.TABULATION + "op vVar : AttributeSet NatList -> AttributeSet" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op vVar : Name NatList -> Name" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op vVar : String NatList -> String" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op vVar : Int NatList -> Int" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op vVar : V$Elt NatList -> V$Elt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op vVar : Configuration NatList -> Configuration" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		
		data = data + Constants.TABULATION + "op unbox : Configuration -> Tuple{Configuration,Set{Oid}}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op unbox0 : Configuration Set{Oid} Set{Oid} -> Tuple{Configuration,Set{Oid}}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op unbox1 : Configuration NatList Set{Oid} Set{Oid} -> Tuple{Configuration,Set{Oid}}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op unbox2 : Configuration NatList Set{Oid} Set{Oid} -> Tuple{Configuration,Set{Oid}}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op cleanUpBoxes : Configuration Set{Oid} -> Configuration" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op reconnect : Configuration NatList -> Configuration" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op replicateVars : AttributeSet NatList -> AttributeSet" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op replicateVars : Configuration NatList -> Configuration" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		
		data = data + Constants.TABULATION + "eq unbox(Conf boxes(OS))"+ Constants.BREAK_LINE + Constants.TABULATION2 + " = (cleanUpBoxes(p1 unbox0(Conf, OS, empty), OS), p2 unbox0(Conf, OS, empty))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq unbox(C:[Configuration]) = (C:[Configuration], empty) [owise]" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

		data = data + Constants.TABULATION + "eq unbox0(Conf, (box[N]{OS}, OS'), OS'')"+ Constants.BREAK_LINE + Constants.TABULATION2 + " = unbox0(p1 unbox1(Conf, N, OS, empty), OS', (p2 unbox1(Conf, N, OS, empty), OS''))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq unbox0(Conf, empty, OS) = (Conf, OS)" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

		data = data + Constants.TABULATION + "eq unbox1(Conf, NL s N, OS, OS')" + Constants.BREAK_LINE + Constants.TABULATION2 + "= unbox1(p1 unbox2(Conf, NL s N, OS, empty), NL N, OS, (p2 unbox2(Conf, NL s N, OS, empty), OS'))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq unbox1(Conf, NL 0, OS, OS') = (Conf, OS')" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
				
		data = data + Constants.TABULATION
				+ "eq unbox2(< level(L) : Model |"
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ "elts : (< O : Node | name : NM, Atts > Elts), Atts' > Conf, NL, (O, OS), OS')"
				+ Constants.BREAK_LINE + Constants.TABULATION
				+ "= unbox2(< level(L) : Model |"
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ "elts : (< vOid(O, NL) : Node | name : vId(NM, NL), replicateVars(Atts, NL) >"
				+ Constants.BREAK_LINE + Constants.TABULATION3
				+ "< O : Node | name : NM, Atts > Elts), Atts' > Conf, NL, OS, OS')"
				+ Constants.MAUDE_END_LINE
				; 		
		
		data = data + Constants.TABULATION
				+ "eq unbox2(< level(L) : Model | rels : (< O : Relation | name : NM, Atts > Rels), Atts' > Conf, NL, (O, OS), OS')"
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ "= unbox2(< level(L) : Model | rels : (< vOid(O, NL) : Relation | name : vId(NM, NL), replicateVars(Atts, NL) > < O : Relation | name : NM, Atts > Rels), Atts' > Conf, NL, OS, OS')"
				+ Constants.MAUDE_END_LINE;		
		data = data + Constants.TABULATION + "eq unbox2(Conf, NL N, (box[I]{OS}, OS'), OS'') = unbox2(Conf, NL N, (OS, OS'), (box[vVar(I, N)]{vOid(OS, N)}, OS''))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq unbox2(Conf, NL, empty, OS) = (reconnect(Conf, NL), OS)" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		data = data + Constants.TABULATION + "op vOid : NeSet{Oid} NatList -> NeSet{Oid}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq vOid((O, NOS), NL) = vOid(O, NL), vOid(NOS, NL)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq vOid((box[I]{OS}, NOS), NL) = box[I]{vOid(OS, NL)}, vOid(NOS, NL)" + Constants.MAUDE_END_LINE;
		
		
		data = data + Constants.TABULATION + "eq cleanUpBoxes(Conf, (box[BI]{OS}, OS'))	= cleanUpBoxes(Conf, (OS', OS))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION
				+ "eq cleanUpBoxes(< level(L) : Model | elts : (< O : Node | Atts > Elts), Atts' > Conf, (O, OS))"
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ " = cleanUpBoxes(< level(L) : Model | elts : Elts, Atts' > Conf, OS)"
				+ Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION
				+ "eq cleanUpBoxes(< level(L) : Model | rels : (< O : Relation | Atts > Elts), Atts' > Conf, (O, OS))"
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ " = cleanUpBoxes(< level(L) : Model | rels : Elts, Atts' > Conf, OS)"
				+ Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq cleanUpBoxes(Conf, empty) = Conf" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		
		data = data + Constants.TABULATION + "eq replicateVars((type : NM, Atts), NL) = (type : NM, replicateVars(Atts, NL))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq replicateVars((source : NM, Atts), NL) = (source : NM, replicateVars(Atts, NL))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq replicateVars((target : NM, Atts), NL) = (target : NM, replicateVars(Atts, NL))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq replicateVars((min-mult : N, Atts), NL) = (min-mult : N, replicateVars(Atts, NL))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq replicateVars((max-mult : I*, Atts), NL) = (max-mult : I*, replicateVars(Atts, NL))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq replicateVars((attributes : Conf, Atts), NL) = (attributes : replicateVars(Conf, NL), replicateVars(Atts, NL))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq replicateVars((none).AttributeSet, NL) = none" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq replicateVars(Atts, NL) = vVar(Atts, NL) [owise]" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		data = data + Constants.TABULATION + "eq replicateVars(< O : Attri | nameOrValue : NM, Atts > Conf, NL)"
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ " = < vOid(O, NL) : Attri | nameOrValue : vVar(NM, NL), replicateVars(Atts, NL) > replicateVars(Conf, NL)"
				+ Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq replicateVars(< O : Attri | nameOrValue : V, Atts > Conf, NL)"
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ " = < vOid(O, NL) : Attri | nameOrValue : vVar(V, NL), replicateVars(Atts, NL) > replicateVars(Conf, NL)"
				+ Constants.MAUDE_END_LINE;		
		data = data + Constants.TABULATION + "eq replicateVars((none).Configuration, NL) = none" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq replicateVars(Conf, NL) = vVar(Conf, NL) [owise]" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		

		data = data + Constants.TABULATION
				+ "eq reconnect(< level(L) : Model |" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "rels : (< vOid(O, NL NL') : Relation | source : NM, Atts > Rels)," + Constants.BREAK_LINE + Constants.TABULATION3
				+ "elts : (< vOid(O', NL) : Node | name : vId(NM, NL), Atts' > Elts)," + Constants.BREAK_LINE + Constants.TABULATION3
				+ "Atts'' >" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "Conf," + Constants.BREAK_LINE + Constants.TABULATION2
				+ "NL)" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION
				+ "= reconnect(< level(L) : Model |" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "rels : (< vOid(O, NL NL') : Relation | source : vId(NM, NL), Atts > Rels)," + Constants.BREAK_LINE + Constants.TABULATION3
				+ "elts : (< vOid(O', NL) : Node | name : vId(NM, NL), Atts' > Elts)," + Constants.BREAK_LINE + Constants.TABULATION3
				+ "Atts'' >" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "Conf," + Constants.BREAK_LINE + Constants.TABULATION2
				+ "NL)" + Constants.MAUDE_END_LINE;		
								
		data = data + Constants.TABULATION
				+ "eq reconnect(< level(L) : Model |" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "rels : (< vOid(O, NL NL') : Relation | target : NM, Atts > Rels)," + Constants.BREAK_LINE + Constants.TABULATION3
				+ "elts : (< vOid(O', NL) : Node | name : vId(NM, NL), Atts' > Elts)," + Constants.BREAK_LINE + Constants.TABULATION3
				+ "Atts'' >" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "Conf," + Constants.BREAK_LINE + Constants.TABULATION2
				+ "NL)" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION
				+ "= reconnect(< level(L) : Model |" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "rels : (< vOid(O, NL NL') : Relation | target : vId(NM, NL), Atts > Rels)," + Constants.BREAK_LINE + Constants.TABULATION3
				+ "elts : (< vOid(O', NL) : Node | name : vId(NM, NL), Atts' > Elts)," + Constants.BREAK_LINE + Constants.TABULATION3
				+ "Atts'' >" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "Conf," + Constants.BREAK_LINE + Constants.TABULATION2
				+ "NL)" + Constants.MAUDE_END_LINE;				 						 
		
		data = data + Constants.TABULATION
				+ "eq reconnect(< level(L) : Model |" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "elts : (< vOid(O, NL) : Node | name : vId(NM, NL), Atts > Elts), Atts' >" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "< level(L') : Model | " + Constants.BREAK_LINE + Constants.TABULATION3
				+ "elts : (< vOid(O', NL) : Node | type : NM, Atts'' > Elts')," + Constants.BREAK_LINE + Constants.TABULATION3
				+ "Atts''' >" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "Conf," + Constants.BREAK_LINE + Constants.TABULATION2
				+ "NL)" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION
				+ "= reconnect(< level(L) : Model |" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "elts : (< vOid(O, NL) : Node | name : vId(NM, NL), Atts > Elts),  Atts' >" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "< level(L') : Model |" + Constants.BREAK_LINE + Constants.TABULATION3
				+ "elts : (< vOid(O', NL) : Node | type : vId(NM, NL), Atts'' > Elts')," + Constants.BREAK_LINE + Constants.TABULATION3
				+ "Atts''' >" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "Conf," + Constants.BREAK_LINE + Constants.TABULATION2
				+ "NL)" + Constants.MAUDE_END_LINE;			
		
		data = data + Constants.TABULATION
				+ "eq reconnect(< level(L) : Model |" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "rels : (< vOid(O, NL) : Relation | name : vId(NM, NL), Atts > Rels), Atts' >" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "< level(L') : Model |" + Constants.BREAK_LINE + Constants.TABULATION3
				+ "rels : (< vOid(O', NL) : Relation | type : NM, Atts'' > Rels')," + Constants.BREAK_LINE + Constants.TABULATION3
				+ "Atts''' >" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "Conf," + Constants.BREAK_LINE + Constants.TABULATION2
				+ "NL)" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION
				+ "= reconnect(< level(L) : Model |" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "rels : (< vOid(O, NL) : Relation | name : vId(NM, NL), Atts > Rels), Atts' >" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "< level(L') : Model |" + Constants.BREAK_LINE + Constants.TABULATION3
				+ "rels : (< vOid(O', NL) : Relation | type : vId(NM, NL), Atts'' > Rels')," + Constants.BREAK_LINE + Constants.TABULATION3
				+ "Atts''' >" + Constants.BREAK_LINE + Constants.TABULATION2
				+ "Conf," + Constants.BREAK_LINE + Constants.TABULATION2
				+ "NL)" + Constants.MAUDE_END_LINE;		

		data = data + Constants.TABULATION + "eq reconnect(Conf, NL N) = reconnect(Conf, NL) [owise]" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq reconnect(Conf, nil) = Conf" + Constants.MAUDE_END_LINE;
		
		data = data + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;
		
		return data;
	}
	
	private String unboxModule(String data) {
		data = data + "mod UNBOX-RULE is" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "pr CONVERSION" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "pr 2-TUPLE{Term, Term}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "pr 2-TUPLE{Term, Substitution}" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

		data = data + Constants.TABULATION + "op error : -> [Term]" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		data = data + Constants.TABULATION + "var MN : Header" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var IL : ImportList" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var SS : SortSet" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var SSDS : SubsortDeclSet" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var OPDS : OpDeclSet" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var Mbs : MembAxSet" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var Eqs : EquationSet" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var RL : Qid" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var Rls : RuleSet" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var NL : NatList" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var NNL : NeNatList" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var AtS : AttrSet" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars Cond Cond' : Condition" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var V : Variable" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var C : Constant" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var F : Qid" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars T T' T'' T1 T2 TN TM RHS LHS : Term" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars TL TL' TL'' : TermList" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars Subst Subst' : Substitution" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars N N' : Nat" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var M : Module" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var Ty : Type" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

		data = data + "---- Given a module M, makeModule creates a module copy of M with a copy of" + Constants.BREAK_LINE;
		data = data + "---- its rule RL in which all boxes are unfolded. This operation is used in" + Constants.BREAK_LINE;
		data = data + "---- the condition of a copy of rule RL without boxes, from which the involved" + Constants.BREAK_LINE;
		data = data + "---- subconfiguration is grabbed as term T and the used match is kept as Subst" + Constants.BREAK_LINE;
		data = data + "---- so that the same part of the match is used when the rule is unfolded." + Constants.BREAK_LINE;
		
		data = data + Constants.TABULATION + "op makeModule : Module Term Qid Substitution -> Module" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "ceq makeModule(M, T, RL, Subst)"
				+ Constants.BREAK_LINE + Constants.TABULATION
				+ " = (mod MN is"
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ "IL sorts SS . SSDS OPDS Mbs Eqs"
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ "if fixBoxes(LHS, RHS, M, T, Subst) == ('none.Configuration, 'none.Configuration)"
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ "then none"
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ "else crl '`{_`}[p1 fixBoxes(LHS, RHS, M, T, Subst)]"
				+ Constants.BREAK_LINE + Constants.TABULATION3
				+ "=> '`{_`}[p2 fixBoxes(LHS, RHS, M, T, Subst)]"	
				+ Constants.BREAK_LINE + Constants.TABULATION3
				+ "if Cond"		
				+ Constants.BREAK_LINE + Constants.TABULATION3
				+ "/\\ 'Conf':Configuration := p1 fixBoxes(LHS, RHS, M, T, Subst)"					
				+ Constants.BREAK_LINE + Constants.TABULATION3
				+ "/\\ Cond'"				
				+ Constants.BREAK_LINE + Constants.TABULATION3
				+ "[label(RL) AtS] ."
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ "fi"
				+ Constants.BREAK_LINE + Constants.TABULATION				
				+ "endm)"
				+ Constants.BREAK_LINE + Constants.TABULATION		
				+ "if mod MN is"				
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ "IL sorts SS . SSDS OPDS Mbs Eqs"
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ "(crl '`{_`}[LHS] => '`{_`}[RHS]"				
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ "if Cond /\\ 'Conf':Configuration := T' /\\ Cond' [label(RL) nonexec AtS] ."				
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ "Rls)"				
				+ Constants.BREAK_LINE + Constants.TABULATION				
				+ "endm := M" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;				
				
		data = data + "---- The boxes in the two sides of the rule must be unfolded together, since" + Constants.BREAK_LINE;
		data = data + "---- the instantiation of some variables in the LHS must be used in the unfolding" + Constants.BREAK_LINE;
		data = data + "---- of the boxes in the RHS" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "op fixBoxes : Term Term Module Term Substitution -> Tuple{Term,Term}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixBoxes(LHS, RHS, M, T, Subst)"
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ "= if p2 fixBoxesLHS(LHS, M, T, Subst) == none"
				+ Constants.BREAK_LINE + Constants.TABULATION3
				+ "then ('none.Configuration, 'none.Configuration)"
				+ Constants.BREAK_LINE + Constants.TABULATION3
				+ "else (p1 fixBoxesLHS(LHS, M, T, Subst), fixBoxesRHS(RHS, p2 fixBoxesLHS(LHS, M, T, Subst)))"
				+ Constants.BREAK_LINE + Constants.TABULATION3
				+ "fi" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;	
		
		
		data = data + "---- fixBoxesLHS returns the term with the boxes unfolded and the used substitution." + Constants.BREAK_LINE;
		data = data + "---- It handles nested boxed level after level." + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "op fixBoxesLHS : Term Module Term Substitution -> Tuple{Term,Substitution}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op fixIdsLHS : Term Module Term Substitution -> Tuple{Term,Substitution}" + Constants.MAUDE_END_LINE;	
		
		data = data + Constants.TABULATION + "eq fixBoxesLHS(LHS, M, T, Subst)" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "= if fixBoxCardinalities(LHS, Subst) == error" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "then (LHS, none)" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "else fixIdsLHS(" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + Constants.TABULATION  + "getTerm(" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + Constants.TABULATION2  + "metaReduce(" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + Constants.TABULATION3  + "upModule('MLM-UNBOXING, false)," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + Constants.TABULATION3  + "'unbox[fixBoxCardinalities(LHS, Subst)])), M, T, Subst)" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3  + "fi" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "ceq fixIdsLHS('`(_`,_`)[LHS, 'empty.Set`{Oid`}], M, T, Subst)"
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ "= if RT?:ResultTriple? == failure"
				+ Constants.BREAK_LINE + Constants.TABULATION3
				+ "then (fixIds(LHS), none)"
				+ Constants.BREAK_LINE + Constants.TABULATION3
				+ "else (fixIds(LHS), filter(getSubstitution(RT?:ResultTriple?), Subst))"
				+ Constants.BREAK_LINE + Constants.TABULATION3
				+ "fi"
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ "if RT?:ResultTriple? := metaApply(mkIdRlModule(M, fixIds(LHS)), T, 'Id, Subst, 0)"
				+ Constants.MAUDE_END_LINE + Constants.BREAK_LINE;	
		
		
		
		data = data + Constants.TABULATION + "ceq fixIdsLHS('`(_`,_`)[LHS, T], M, T', Subst)"
				+ Constants.BREAK_LINE + Constants.TABULATION2 + "= if RT?:ResultTriple? == failure"
				+ Constants.BREAK_LINE + Constants.TABULATION3 + "then (fixIds(LHS), none)"
				+ Constants.BREAK_LINE + Constants.TABULATION3 + "else fixBoxesLHS('__[fixIds(LHS), 'boxes[fixIds(T)]], M, T', filter(getSubstitution(RT?:ResultTriple?), Subst))"
				+ Constants.BREAK_LINE + Constants.TABULATION2 + "fi"
				+ Constants.BREAK_LINE + Constants.TABULATION2 + "if RT?:ResultTriple? := metaApply(mkIdRlModule(M, fixIds(LHS)), T', 'Id, Subst, 0)"
				+ Constants.BREAK_LINE + Constants.TABULATION2 + "[owise]"
				+ Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		
		data = data + "---- when multiple elements, the id rule may fail if elements in additional boxes" + Constants.BREAK_LINE;
		data = data + "---- where included in extension variables like Elts, Rels, Conf... By filtering" + Constants.BREAK_LINE + Constants.BREAK_LINE;
		data = data + "---- them we leave only variables with explicit content" + Constants.BREAK_LINE;
		
		data = data + Constants.TABULATION + "op filter : Substitution Substitution -> Substitution" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq filter(Subst, Subst') = filter(Subst) ; getConf(Subst')" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "op getConf : Substitution -> Substitution" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq getConf('Conf':Configuration <- T ; Subst) = 'Conf':Configuration <- T" + Constants.MAUDE_END_LINE;

		data = data + Constants.TABULATION + "---- when multiple elements, the id rule may fail if elements in additional boxes" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "---- where included in extension variables like Elts, Rels, Conf... By filtering" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "---- them we leave only variables with explicit content" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "op filter : Substitution -> Substitution" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION2 + "eq filter(V <- T ; Subst)" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "= if getType(V) == 'Configuration" + Constants.BREAK_LINE;	
		data = data + Constants.TABULATION + Constants.TABULATION3 + "then filter(Subst)" + Constants.BREAK_LINE;		
		data = data + Constants.TABULATION2 + Constants.TABULATION3 + "else V <- T ; filter(Subst)" + Constants.BREAK_LINE;				  
		data = data + Constants.TABULATION3 + Constants.TABULATION3 + "fi" + Constants.MAUDE_END_LINE;			  
			
		data = data + Constants.TABULATION + "eq filter((none).Substitution) = none" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "op fixBoxesRHS : Term Substitution -> Term" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op fixIdsRHS : Term Substitution -> Term" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "eq fixBoxesRHS(RHS, Subst)" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "= if Subst == none" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "then RHS" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "else if fixBoxCardinalities(RHS, Subst) == error" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "then RHS" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "else fixIdsRHS(" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "getTerm(" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "metaReduce(" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + Constants.TABULATION + "upModule('MLM-UNBOXING, false)," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + Constants.TABULATION + "'unbox[fixBoxCardinalities(RHS, Subst)])), Subst)" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "fi" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "fi" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "eq fixIdsRHS('`(_`,_`)[RHS, 'empty.Set`{Oid`}], Subst) = fixIds(RHS)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "ceq fixIdsRHS('`(_`,_`)[RHS, T], Subst)"
				+ Constants.BREAK_LINE + Constants.TABULATION2 + "= fixBoxesRHS('__[fixIds(RHS), 'boxes[fixIds(T)]], Subst)"
				+ Constants.BREAK_LINE + Constants.TABULATION2 + "if T =/= 'empty.Set`{Oid`}"
				+ Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

		
		data = data + Constants.TABULATION + "op mkIdRlModule : Module Term -> Module" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION
				+ "eq mkIdRlModule(mod MN is IL sorts SS . SSDS OPDS Mbs Eqs Rls endm, T) = mod MN is IL sorts SS . SSDS OPDS Mbs Eqs (rl '`{_`}[T] => '`{_`}[T] [label('Id)] .) endm"
				+ Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		
		data = data + Constants.TABULATION + "op stringNL : NatList NzNat ~> String" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq stringNL(N NNL, N') = string(N, N') + \"_\" + stringNL(NNL, N')" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq stringNL(N, N') = string(N, N')" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		
		data = data + Constants.TABULATION + "op fixIds : Term -> Term" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op fixIdsAux : TermList -> TermList" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixIds(T) = fixIdsAux(T)" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "eq fixIdsAux(('vVar[(C, TL), T], TL')) = (C, fixIdsAux('vVar[TL, T]), fixIdsAux(TL'))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixIdsAux(('vVar[(V, TL), T], TL'))"
		+ Constants.BREAK_LINE + Constants.TABULATION2 + "= (qid(string(getName(V)) + \"@\" + stringNL(downNatList(T), 10) + \":\" + string(getType(V))), fixIdsAux('vVar[TL, T]), fixIdsAux(TL'))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixIdsAux(('vVar[(F[TL], TL'), T], TL''))"
		+ Constants.BREAK_LINE + Constants.TABULATION2 + "= (F[fixIdsAux('vVar[TL, T])], fixIdsAux('vVar[TL', T]), fixIdsAux(TL''))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixIdsAux(('vVar[empty, T], TL)) = fixIdsAux(TL)" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "eq fixIdsAux(('vOid[V, T], TL))"+ Constants.BREAK_LINE
				+ Constants.TABULATION2 + "= (qid(string(getName(V)) + \"@\" + stringNL(downNatList(T), 10) + \":Oid\"), fixIdsAux(TL))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixIdsAux(('vOid['oid[V, T], T'], TL)) = ('oid[V, '__[T, T']], fixIdsAux(TL))" + Constants.MAUDE_END_LINE;

		
		data = data + Constants.TABULATION + "eq fixIdsAux(('vId[V, T], TL))"+ Constants.BREAK_LINE 
				+ Constants.TABULATION2 + "= (qid(string(getName(V)) + \"@\" + stringNL(downNatList(T), 10) + \":Name\"), fixIdsAux(TL))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixIdsAux(('vId['id[V, T], T'], TL)) = ('id[V, T], fixIdsAux(TL))" + Constants.MAUDE_END_LINE;		

		data = data + Constants.TABULATION + "eq fixIdsAux((F[TL], TL')) = (F[fixIdsAux(TL)], fixIdsAux(TL')) [owise]" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixIdsAux((V, TL)) = (V, fixIdsAux(TL))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixIdsAux((C, TL)) = (C, fixIdsAux(TL))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixIdsAux(empty) = empty" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixIdsAux(('box`[_`]`{_`}[T, T'], TL)) = ('box`[_`]`{_`}[fixIdsAux(T), fixIdsAux(T')], fixIdsAux(TL))" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "op fixBoxCardinalities : Term Substitution -> [Term]" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op fixBoxCardinalitiesAux : Term Substitution -> [Term]" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op fixBoxCardinalitiesAux2 : TermList Substitution -> [TermList]" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "eq fixBoxCardinalities(('boxes[TL], TL'), Subst)"+ Constants.BREAK_LINE + Constants.TABULATION2 + " = ('boxes[fixBoxCardinalitiesAux(TL, Subst)], TL')" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixBoxCardinalities(('<_:_|_>[TL], TL'), Subst)"+ Constants.BREAK_LINE + Constants.TABULATION2 + " = ('<_:_|_>[TL], fixBoxCardinalities(TL', Subst))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixBoxCardinalities((F[TL], TL'), Subst)"+ Constants.BREAK_LINE + Constants.TABULATION2 + " = (F[fixBoxCardinalities(TL, Subst)], fixBoxCardinalities(TL', Subst)) [owise]" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixBoxCardinalities((V, TL), Subst) = (V, fixBoxCardinalities(TL, Subst))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixBoxCardinalities((C, TL), Subst) = (C, fixBoxCardinalities(TL, Subst))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixBoxCardinalities(empty, Subst) = empty" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		data = data + Constants.TABULATION + "eq fixBoxCardinalitiesAux(('box`[_`]`{_`}[T, TL], TL'), Subst)" + Constants.BREAK_LINE + Constants.TABULATION2 + " = ('box`[_`]`{_`}[fixBoxCardinalitiesAux2(T, Subst), TL], fixBoxCardinalitiesAux(TL', Subst))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixBoxCardinalitiesAux((F[TL], TL'), Subst)" + Constants.BREAK_LINE + Constants.TABULATION2 + " = (F[fixBoxCardinalitiesAux(TL, Subst)], fixBoxCardinalitiesAux(TL', Subst))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixBoxCardinalitiesAux((V, TL), Subst) = (V, fixBoxCardinalitiesAux(TL, Subst))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixBoxCardinalitiesAux((C, TL), Subst) = (C, fixBoxCardinalitiesAux(TL, Subst))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixBoxCardinalitiesAux(empty, Subst) = empty" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
				
		data = data + Constants.TABULATION + "eq fixBoxCardinalitiesAux2((V, TL), (V <- T ; Subst))"+ Constants.BREAK_LINE + Constants.TABULATION2 + "= (T, fixBoxCardinalitiesAux2(TL, (V <- T ; Subst)))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixBoxCardinalitiesAux2((C, TL), Subst)"+ Constants.BREAK_LINE + Constants.TABULATION2 + "= (C, fixBoxCardinalitiesAux2(TL, Subst))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---eq fixBoxCardinalitiesAux2(('_._->`size`(`)`{_`}[TL], TL'), Subst)"+ Constants.BREAK_LINE + Constants.TABULATION2 + "--- = (getTerm(metaReduce(upModule('MLM, false), '_._->`size`(`)`{_`}[fixBoxCardinalitiesAux2(TL, Subst)])), fixBoxCardinalitiesAux2(TL', Subst))" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "eq fixBoxCardinalitiesAux2(('eval[T, T'], TL), Subst)"
				+ Constants.BREAK_LINE + Constants.TABULATION2
				+ "= (getTerm(metaReduce(upModule('MLM-OCL, false), 'eval[fixBoxCardinalitiesAux2(T, Subst), fixBoxCardinalitiesAux2(T', Subst)])), fixBoxCardinalitiesAux2(TL, Subst))"
				+ Constants.MAUDE_END_LINE;		
		
		data = data + Constants.TABULATION + "eq fixBoxCardinalitiesAux2((F[TL], TL'), Subst)"+ Constants.BREAK_LINE + Constants.TABULATION2 + "= (F[fixBoxCardinalitiesAux2(TL, Subst)], fixBoxCardinalitiesAux2(TL', Subst)) [owise]" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq fixBoxCardinalitiesAux2(empty, Subst) = empty" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "op downNatList : Term -> NatList" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq downNatList('__[T, TL]) = downNatList(T) downNatList(TL)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq downNatList('0.Zero) = 0" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq downNatList('s_['0.Zero]) = 1" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "ceq downNatList(F['0.Zero]) = trunc(rat(substr(string(F), 3, 2), 10)) if substr(string(F), 0, 3) = \"s_^\"" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE;
		
		return data;
	}

	public File addXml(File xmlFile) {
		String data = "";
		try {
		
		FileOutputStream out = new FileOutputStream(xmlFile);
		data = data + "sload mlm.maude" + Constants.BREAK_LINE + Constants.BREAK_LINE;
		
		data = data + "mod MLM-XML-PROC{V :: TRIV} is" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "pr OCL-SIGN " + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "pr MLM-CORE{V}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "pr CONVERSION" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "pr CONVERSION" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;		
		
		data = data + Constants.TABULATION + "pr FILE" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "* (op <_:_|_> : Oid Cid AttributeSet -> Object to <_:_|_>" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "[format (ni!r o !r o !r o !r o)])" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		data = data + Constants.TABULATION + "op value2string : V$Elt -> String" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		data = data + "vars L L' : Nat" + Constants.MAUDE_END_LINE;
		data = data + "vars L* L*' : Int*" + Constants.MAUDE_END_LINE;
		data = data + "vars N N' N'' N''' : Name" + Constants.MAUDE_END_LINE;
		data = data + "vars M M' St : String" + Constants.MAUDE_END_LINE;
		data = data + "vars Elts Rels Conf Att : Configuration" + Constants.MAUDE_END_LINE;
		data = data + "vars Oid : Oid" + Constants.MAUDE_END_LINE;
		data = data + "vars NL NNL : NatList" + Constants.MAUDE_END_LINE;
		data = data + "var  Index : Nat" + Constants.MAUDE_END_LINE;
		data = data + "var I : Int" + Constants.MAUDE_END_LINE;
		data = data + "var F : Float" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		data = data + "var RN : Qid" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		data = data + "var V : V$Elt" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		data = data + Constants.TABULATION + "op xml : System -> String" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq xml({ Conf }) = xml@({ Conf }, 0)" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

		
		data = data + Constants.TABULATION + "op xml@ : System Nat -> String" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq xml@({ Conf }, Index) = tabs(Index) + \"<system>\\n\" + xml@(Conf, Index + 1) + tabs(Index) + \"</system>\"" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
			data = data + Constants.TABULATION + "op xml@ : Configuration Nat -> String" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION
					+ "eq xml@(< level(L) : Model | name : M, om : M', elts : Elts, rels : Rels > Conf, Index)\r\n"
					+ "= tabs(Index) + \"<model level=\" + xml@(L) + \" name=\" + xml@(M) + \" om=\" + xml@(M') +\r\n"
					+ "\">\\n\" + tabs(Index + 1) + \"<elts>\\n\" + xml@(Elts, Index + 2) + tabs(Index + 1) + \"</elts>\\n\" + tabs(Index + 1) + \"<rels>\\n\" + xml@(Rels, Index + 2) +\r\n"
					+ "tabs(Index + 1) + \"</rels>\\n\" + tabs(Index) + \"</model>\\n\"\r\n" + "+ xml@(Conf, Index)"
					+ Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

			data = data + Constants.TABULATION
					+ "eq xml@(< Oid : Node | name : N, type : N', attributes : Att > Conf, Index)\r\n"
					+ "= tabs(Index) + \"<node id=\" + xml@(Oid) + \" name=\" + xml@(N) + \" type=\" + xml@(N') + \" >\\n\"\r\n"
					+ "+\r\n" + "if Att == none\r\n" + "then \"\"\r\n"
					+ "else tabs(Index + 1) + \"<attributes>\\n\" + xml@(Att, Index + 2) + tabs(Index + 1) + \"</attributes>\\n\"\r\n"
					+ "fi\r\n" + "+ tabs(Index) + \"</node>\\n\" + xml@(Conf, Index)" + Constants.MAUDE_END_LINE
					+ Constants.BREAK_LINE;	
		
			data = data + Constants.TABULATION + "eq xml@(< Oid : Attri | nameOrValue : N, type : N' > Conf, Index)\r\n" + 
					"= tabs(Index) + \"<attribute id=\" + xml@(Oid) + \" nameOrValue=\" + xml@(N) + \" type=\" + xml@(N') + \" ></attribute>\\n\" + xml@(Conf, Index)"
					+ Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

			data = data + Constants.TABULATION + "eq xml@(< Oid : Attri | nameOrValue : V, type : N' > Conf, Index)\r\n" + 
					"= tabs(Index) + \"<attribute id=\" + xml@(Oid) + \" nameOrValue=\" + value2string(V) + \" type=\" + xml@(N') + \" ></attribute>\\n\" + xml@(Conf, Index)"
					+ Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

			data = data + Constants.TABULATION
					+ "eq xml@(< Oid : Relation | name : N, type : N', source : N'', target : N''', min-mult : L, max-mult : L* > Conf, Index)\r\n" + 
					"= tabs(Index) + \"<relation id=\" + xml@(Oid) + \" name=\" + xml@(N) + \" type=\" + xml@(N')\r\n" + 
					"+ \" source=\" + xml@(N'') + \" target=\" + xml@(N''')\r\n" + 
					"+ \" min-mult=\" + xml@(L)  + \" max-mult=\" + xml@(L*) + \"></relation>\\n\"\r\n" + 
					"+ xml@(Conf, Index)"
					+ Constants.MAUDE_END_LINE + Constants.BREAK_LINE;	

			data = data + Constants.TABULATION + "eq xml@(< counter : Counter | value : L > Conf, Index)\r\n" + 
					"= tabs(Index) + \"<counter id=\" + \"\\\"counter\\\"\" + \" value=\" + xml@(L) + \" ></counter>\\n\"\r\n" + 
					"+ xml@(Conf, Index)" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

			data = data + Constants.TABULATION + "eq xml@((none).Configuration, Index) = \"\"" + Constants.MAUDE_END_LINE
					+ Constants.BREAK_LINE;

			data = data + Constants.TABULATION + "op xml@ : Oid -> String" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "eq xml@(level(L)) = \"level(\" + xml@(L) + \")\""
					+ Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION
					+ "eq xml@(oid(L, L')) = \"\\\"oid(\" + string(L,10) + \",\" + string(L',10) + \")\\\"\""
					+ Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION
					+ "eq xml@(oid(L, NL)) = \"\\\"oid(\" + string(L,10) + \",\" + xml@(NL, 10) + \")\\\"\""
					+ Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

			data = data + Constants.TABULATION + "op xml@ : Name -> String" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION
					+ "eq xml@(id(L, L')) = \"\\\"id(\" + string(L,10) + \",\" + string(L',10) + \")\\\"\""
					+ Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION
					+ "eq xml@(id(L, St)) = \"\\\"id(\" + string(L,10) + \",\" + St + \")\\\"\""
					+ Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION
					+ "eq xml@(id(L, NL)) = \"\\\"id(\" + string(L,10) + \",\" + xml@(NL, 10) + \")\\\"\""
					+ Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

			data = data + Constants.TABULATION + "op xml@ : String -> String" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "eq xml@(St) = \"\\\"\" + St + \"\\\"\"" + Constants.MAUDE_END_LINE
					+ Constants.BREAK_LINE;

			data = data + Constants.TABULATION + "op xml@ : Int -> String" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "eq xml@(I) = \"\\\"\" + string(I,10) + \"\\\"\""
					+ Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

			data = data + Constants.TABULATION + "op xml@ : Bool -> String" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "eq xml@((false).Bool) = \"\\\"\" + \"false\" + \"\\\"\""
					+ Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "eq xml@((true).Bool) = \"\\\"\" + \"true\" + \"\\\"\""
					+ Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

			data = data + Constants.TABULATION + "op xml@ : Float -> String" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "eq xml@(F) = \"\\\"\" + string(F) + \"\\\"\""
					+ Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

			data = data + Constants.TABULATION + "op xml@ : Nat -> String" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "eq xml@(L) = \"\\\"\" + string(L,10) + \"\\\"\""
					+ Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "eq xml@(*) = \"\\\"*\\\"\"" + Constants.MAUDE_END_LINE
					+ Constants.BREAK_LINE;

			data = data + Constants.TABULATION + "op xml@ : NatList Nat -> String" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "eq xml@(L NNL, L') = string(L, L') + \" \" + xml@(NNL, L')"
					+ Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "eq xml@(L, L') = string(L, L')" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "eq xml@(nil, L') = \"\"" + Constants.MAUDE_END_LINE
					+ Constants.BREAK_LINE;			
	

			data = data + Constants.TABULATION + "op xml@ : Qid -> String" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "eq xml@(RN) = \"\\\"\" + string(RN) + \"\\\"\"" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;	

			data = data + Constants.TABULATION + "op tabs : Nat -> String" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "eq tabs(0) = \"\"" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "eq tabs(s L) = \"\\t\" + tabs(L)" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;				

			
			
			data = data + Constants.TABULATION + "op myClass : -> Cid" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op myObj : -> Oid" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "op text`:_ : String -> Attribute" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
			
			data = data + Constants.TABULATION + "op run : String String -> Configuration" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "var  File Output : String" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "var  FH : Oid" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "var  Attrs : AttributeSet" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

			
			data = data + Constants.TABULATION + "eq run(File, Output)" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION2 + "= <>" + Constants.BREAK_LINE;  
			data = data + Constants.TABULATION3 + "< myObj : myClass | text : Output >" + Constants.BREAK_LINE;   
			data = data + Constants.TABULATION3 + "openFile(fileManager, myObj, File, \"w\")" + Constants.MAUDE_END_LINE;
			
			data = data + Constants.TABULATION2 + "rl < myObj : myClass | text : Output, Attrs >" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION3 + "openedFile(myObj, fileManager, FH)" + Constants.BREAK_LINE;  
			data = data + Constants.TABULATION2 + "=> < myObj : myClass | text : Output, Attrs >" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION2 + "write(FH, myObj, Output)" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;

			data = data + Constants.TABULATION + "rl < myObj : myClass | Attrs >" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION2 + "wrote(myObj, FH)" + Constants.BREAK_LINE;  
			data = data + Constants.TABULATION + "=> < myObj : myClass | Attrs >" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION2 + "closeFile(FH, myObj)" + Constants.MAUDE_END_LINE; 
			
			
			data = data + Constants.TABULATION + "rl < myObj : myClass | Attrs >" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION2 + "closedFile(myObj, FH)" + Constants.BREAK_LINE;  
			data = data + Constants.TABULATION2 + "=> none" + Constants.MAUDE_END_LINE;
			
			data = data + "endm" + Constants.BREAK_LINE;
			
		
		out.write(data.getBytes());
		out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return xmlFile;
	}
	

	public File addXmlOcl(File xmlOclFile) {
		String data = "";
		try {
		
		FileOutputStream out = new FileOutputStream(xmlOclFile);
		data = data + "sload xml.maude" + Constants.BREAK_LINE;
		data = data + "sload mlm-ocl.maude" + Constants.BREAK_LINE + Constants.BREAK_LINE;
		
		data = data + "mod MLM-XML is" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "pr MLM-XML-PROC{Ocl}" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		
		data = data + Constants.TABULATION + "var OclSt : OclString" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var OclI : OclInt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var OclB : OclBool" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var OclF : OclFloat" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var OclNzN : OclNzNat" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var OclCh : OclChar" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars V V' V'' : OclExp" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		

		data = data + Constants.TABULATION + "eq value2string(OclSt) = \"\\\"\" + string(OclSt) + \"\\\"\"" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq value2string(OclI) = \"\\\"\" + string(integer(OclI), 10) + \"\\\"\"" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq value2string((false).OclBool) = \"\\\"\" + \"false\" + \"\\\"\"" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq value2string((true).OclBool) = \"\\\"\" + \"true\" + \"\\\"\"" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq value2string(OclF) = \"\\\"\" + string(OclF) + \"\\\"\"" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "op string : OclString -> String" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op stringAux : OclString -> String" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq string(OclSt)" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "= if length(OclSt) == 0" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "then \"\"" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "else stringAux(substr(OclSt, 0, 1)) + string(substr(OclSt, 1, length(OclSt)))" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "fi" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		
		data = data + Constants.TABULATION + "eq stringAux(\"\\\"\") = \"\\\\\\\"\"" + Constants.MAUDE_END_LINE;		
		data = data + Constants.TABULATION + "eq stringAux(OclCh) = char(integer(ascii(OclCh))) [owise]" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;		
		
		data = data + Constants.TABULATION + "op integer : OclInt -> Int" + Constants.MAUDE_END_LINE;		
		data = data + Constants.TABULATION + "eq integer(0) = 0" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "eq integer(s 0) = 1" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "eq integer(s OclNzN) = s integer(OclNzN)" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "eq integer(-OclInt OclNzN) = - integer(OclNzN)" + Constants.MAUDE_END_LINE;	
		
		data = data + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;


		
		
		out.write(data.getBytes());
		out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return xmlOclFile;
	}
	
	public File addXmlSml(File xmlSmlFile) {
		String data = "";
		try {
		
		FileOutputStream out = new FileOutputStream(xmlSmlFile);
		data = data + "sload xml.maude" + Constants.BREAK_LINE;
		data = data + "sload mlm-sml.maude" + Constants.BREAK_LINE + Constants.BREAK_LINE;
		
		data = data + "mod MLM-XML is" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "pr MLM-XML-PROC{Sml}" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		
		data = data + Constants.TABULATION + "var SmlSt : SmlString" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var SmlI : SmlInt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var SmlB : SmlBool" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var SmlF : SmlFloat" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var SmlNzN : SmlNzNat" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var SmlCh : SmlChar" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars V V' V'' : SmlExp" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		

		data = data + Constants.TABULATION + "eq value2string(SmlSt) = \"\\\"\" + string(SmlSt) + \"\\\"\"" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq value2string(SmlI) = \"\\\"\" + string(integer(SmlI), 10) + \"\\\"\"" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq value2string((false).SmlBool) = \"\\\"\" + \"false\" + \"\\\"\"" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq value2string((true).SmlBool) = \"\\\"\" + \"true\" + \"\\\"\"" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq value2string(SmlF) = \"\\\"\" + string(SmlF) + \"\\\"\"" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq value2string((V, V')) = \"(\" + value2string(V) + \", \" + value2string(V') + \")\"" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq value2string((V, V', V'')) = \"(\" + value2string(V) + \", \" + value2string(V') + \", \" + value2string(V'') + \")\"" + Constants.MAUDE_END_LINE;
						
		data = data + Constants.TABULATION + "op string : SmlString -> String" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op stringAux : SmlString -> String" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq string(SmlSt)" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "= if length(SmlSt) == 0" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "then \"\"" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "else stringAux(substr(SmlSt, 0, 1)) + string(substr(SmlSt, 1, length(SmlSt)))" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "fi" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		
		data = data + Constants.TABULATION + "eq stringAux(\"\\\"\") = \"\\\\\\\"\"" + Constants.MAUDE_END_LINE;		
		data = data + Constants.TABULATION + "eq stringAux(SmlCh) = char(integer(ascii(SmlCh))) [owise]" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;		
		
		data = data + Constants.TABULATION + "op integer : SmlInt -> Int" + Constants.MAUDE_END_LINE;		
		data = data + Constants.TABULATION + "eq integer(0) = 0" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "eq integer(s 0) = 1" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "eq integer(s SmlNzN) = s integer(SmlNzN)" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "eq integer(-SmlInt SmlNzN) = - integer(SmlNzN)" + Constants.MAUDE_END_LINE;	
		
		data = data + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;


		
		
		out.write(data.getBytes());
		out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return xmlSmlFile;
	}	

	public File addMlmPredsBlock(File mlmPredsFile) {
		String data = "";
		try {
		
		FileOutputStream out = new FileOutputStream(mlmPredsFile);
		data = data + "sload model-checker.maude" + Constants.BREAK_LINE;
		data = data + "sload mlm-ocl.maude" + Constants.BREAK_LINE + Constants.BREAK_LINE;
		
		data = data + "mod MLM-PREDS is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "protecting MLM-OCL" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "including SATISFACTION" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "including MODEL-CHECKER" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "including LTL-SIMPLIFIER" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		data = data + Constants.TABULATION + "subsort System < State" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		
		data = data + Constants.TABULATION + "var  Conf : Configuration" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  OclExp : OclExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  I : Int" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		data = data + Constants.TABULATION + "---- Basic propositions should be boolean expressions" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "subsort OclExp < Prop" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq { Conf } |= (OclExp >= I) = eval(OclExp, Conf) >= I" + Constants.MAUDE_END_LINE;
		data = data + "endm" + Constants.BREAK_LINE;
		
		
		out.write(data.getBytes());
		out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return mlmPredsFile;
	}
	
	public File addMlmSmlBlock(File mlmSmlFile) {
		String data = "";
		try {
		
		FileOutputStream out = new FileOutputStream(mlmSmlFile);
		data = data + "sload unboxing.maude" + Constants.BREAK_LINE; 
		data = data + "sload sml.maude" + Constants.BREAK_LINE; 	
		data = data + "mod MLM-UNBOXING is" + Constants.BREAK_LINE; 
		data = data + Constants.TABULATION + "pr UNBOXING{Sml}" + Constants.MAUDE_END_LINE;
		data = data + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;

		data = data + "mod MLM-SML is" + Constants.BREAK_LINE; 
		data = data + Constants.TABULATION + "pr OCL{Sml}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- op value2ocl : SmlInt -> OclInt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq value2ocl(s I:SmlInt) = s value2ocl(I:SmlInt)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq value2ocl((0).SmlZero) = (0).OclZero" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;
	
		data = data + "mod MLM is" + Constants.BREAK_LINE; 
		data = data + Constants.TABULATION + "pr MLM-SML" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "pr UNBOX-RULE" + Constants.MAUDE_END_LINE;
		data = data + "endm" + Constants.BREAK_LINE;
		
		out.write(data.getBytes());
		out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		return mlmSmlFile;
	}	

	public File addSmlBlock(File mlmSmlFile) {
		String data = "";
		try {
		
		FileOutputStream out = new FileOutputStream(mlmSmlFile);
		data = data + "sload sml-declarations.maude" + Constants.BREAK_LINE;
		data = data + "sload mlm-core.maude" + Constants.BREAK_LINE + Constants.BREAK_LINE;
		
//		data = data + "mod SML-SIGN is"  + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "pr SML-PRELUDE" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "sorts SmlExp Tuple" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "subsort SmlBool SmlInt SmlString SmlFloat Tuple < SmlExp" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "op `(_`,_`) : SmlExp SmlExp -> Tuple" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "op `(_`,_`,_`) : SmlExp SmlExp SmlExp -> Tuple" + Constants.MAUDE_END_LINE;		
//		data = data + Constants.TABULATION + "op _+_ : SmlExp SmlExp -> SmlExp [ditto]" + Constants.MAUDE_END_LINE;		
//		data = data + Constants.TABULATION + "op _==_ : SmlExp SmlExp -> SmlBool" + Constants.MAUDE_END_LINE;
//		data = data + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;
	
//		data = data + "view SmlExp from TRIV to SML-SIGN is"  + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "sort Elt to SmlExp" + Constants.MAUDE_END_LINE;
//		data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;
		
//		data = data + "view SmlQid from TRIV to SML-SIGN is"  + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "sort Elt to SmlQid" + Constants.MAUDE_END_LINE;
//		data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;		
		
		
		data = data + "mod SML is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "inc SML-DECLARATIONS" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "pr MAP{SmlQid, SmlExp}" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "op v : SmlQid -> SmlExp" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "op match : SmlString SmlString -> [Map{SmlQid, SmlExp}]" + Constants.MAUDE_END_LINE;		
//		data = data + Constants.TABULATION + "op apply : SmlString [Map{SmlQid, SmlExp}] -> SmlString" + Constants.MAUDE_END_LINE;		
//		data = data + Constants.TABULATION + "op evalSml : SmlExp -> SmlExp" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "vars Str Str1 Str2 : SmlString" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars T T' T'' : SmlTerm" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars F QI : SmlQid" + Constants.MAUDE_END_LINE;		
		data = data + Constants.TABULATION + "var  VS : SmlVariableSet" + Constants.MAUDE_END_LINE;		
		data = data + Constants.TABULATION + "var  V : SmlVariable" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars Exp Exp' Exp'' Exp''' Exp1 Exp2 Exp3 Exp1' Exp2' Exp3' : SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  Assign : Map{SmlQid, SmlExp}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars R R' : SmlFloat" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  I : SmlInt" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "vars Subst Subst' : Map{SmlQid, SmlExp}" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
//		data = data + "---- Parsing"  + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "op parse : SmlString SmlVariableSet -> SmlExp" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "op process : SmlTerm -> SmlExp" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "op variables : SmlQidList -> SmlVariableSet" + Constants.MAUDE_END_LINE;
		
//		data = data + Constants.TABULATION + "eq parse(Str, VS) = process(getTerm(metaParse(upModule('SML, false), VS, tokenize(Str), 'SmlExp)))" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
//		data = data + "---- process"  + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "eq process('_==_[T, T']) = process(T) == process(T')" + Constants.MAUDE_END_LINE;
//		data = data + "---- process tuples"  + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "eq process('`(_`,_`)[T, T']) = (process(T), process(T'))" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq process('`(_`,_`,_`)[T, T', T'']) = (process(T), process(T'), process(T''))" + Constants.MAUDE_END_LINE;
//
//		data = data + "---- process int ops"  + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "eq process('_+_[T, T']) = process(T) + process(T')" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq process('_*_[T, T']) = process(T) * process(T')" + Constants.MAUDE_END_LINE;
//
//		data = data + "---- process natural number"  + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "eq process('0.SmlZero) = 0" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq process('s_['0.SmlZero]) = 1" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "ceq process(F['0.SmlZero]) = trunc(rat(substr(smlString(F), 3, 2), 10))" + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "if substr(smlString(F), 0, 3) = \"s_^\"" + Constants.MAUDE_END_LINE;
//		
//		data = data + "---- process string"  + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "ceq process(QI) = substr(smlString(getName(QI)), 1, _-SmlInt_(length(smlString(getName(QI))), 2))" + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "if getType(QI) == 'SmlString" + Constants.MAUDE_END_LINE;		
//		  
//		data = data + "---- process float"  + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "ceq process(QI) = string2float(smlString(getName(QI)))" + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "if getType(QI) == 'SmlFiniteFloat" + Constants.MAUDE_END_LINE;			  
//
//		data = data + "---- process variable"  + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "eq process(V) = v(V)" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
//		  
//		data = data + "---- Matching"  + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "op noMatch : -> [Map{SmlQid, SmlExp}]" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "op match : SmlString SmlString SmlVariableSet -> [Map{SmlQid, SmlExp}]" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "op matchAux : SmlExp SmlExp -> [Map{SmlQid, SmlExp}]" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
//		 
//		data = data + Constants.TABULATION + "rl match(Str1, Str2) => match(Str1, Str2, VBLES)" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq match(Str1, Str2, VS) = matchAux(parse(Str1, VS), parse(Str2, VS))" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "op VBLES : -> SmlVariableSet" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq VBLES = 'x1:SmlExp ; 'x2:SmlExp ; 'x3:SmlExp ; 'x:SmlExp ; 'y:SmlExp ; 'z:SmlExp" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
//
//		data = data + Constants.TABULATION + "eq matchAux(Exp, Exp) = empty" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq matchAux(`(_`,_`)(Exp, Exp'), `(_`,_`)(Exp'', Exp''')) = matchAux(Exp, Exp''), matchAux(Exp', Exp''')" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq matchAux(`(_`,_`,_`)(Exp1, Exp2, Exp3), `(_`,_`,_`)(Exp1', Exp2', Exp3')) = matchAux(Exp1, Exp1'), matchAux(Exp2, Exp2'), matchAux(Exp3, Exp3')" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq matchAux(v(V), Exp) = V |-> Exp" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq matchAux(Exp, Exp') = noMatch [owise]" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
//
//		data = data + Constants.TABULATION + "eq V |-> Exp, V |-> Exp = V |-> Exp" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq V |-> Exp, V |-> Exp' = noMatch [owise]" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq noMatch, V |-> Exp = noMatch" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
//
//		data = data + "---- Application of an assignament to a term"  + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "op applyAux : SmlExp Map{SmlQid, SmlExp} -> SmlString" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "op union : Map{SmlQid, SmlExp} Map{SmlQid, SmlExp} -> Map{SmlQid, SmlExp} [assoc comm]" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq union(Subst, Subst') = Subst # Subst' . ---- TODO this might be a wrong substitution" + Constants.MAUDE_END_LINE;
		
//		data = data + Constants.TABULATION + "op domain : Map{SmlQid, SmlExp} -> SmlVariableSet" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
//		
//		data = data + Constants.TABULATION + "eq domain((V |-> Exp, Assign)) = V ; domain(Assign)" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq domain(empty) = none" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
//		
//		data = data + Constants.TABULATION + "eq V ; V = V" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
//	
//		data = data + Constants.TABULATION + "rl apply(Str, noMatch) => Str" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "rl apply(Str, Assign) => applyAux(parse(Str, domain(Assign)), Assign)" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
//		
//		data = data + Constants.TABULATION + "eq applyAux(`(_`,_`)(Exp, Exp'), Assign) = `(_`,_`)(applyAux(Exp, Assign), applyAux(Exp', Assign))" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq applyAux(`(_`,_`,_`)(Exp1, Exp2, Exp3), Assign) = `(_`,_`,_`)(applyAux(Exp1, Assign), applyAux(Exp2, Assign), applyAux(Exp3, Assign))" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq applyAux(Exp + Exp', Assign) = applyAux(Exp, Assign) + applyAux(Exp', Assign)" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq applyAux(Exp == Exp', Assign) = applyAux(Exp, Assign) == applyAux(Exp', Assign)" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq applyAux(v(V), (V |-> Exp, Assign)) = Exp" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq applyAux(Exp, Assign) = Exp [owise]" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
//		
//		data = data + "---- Evaluation of SML expressions"  + Constants.BREAK_LINE;
//		data = data + Constants.TABULATION + "op evalSmlAux : SmlExp -> SmlExp" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "rl evalSml(Exp) => evalSmlAux(Exp)" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq evalSmlAux(`(_`,_`)(Exp, Exp')) = `(_`,_`)(evalSmlAux(Exp), evalSmlAux(Exp'))" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq evalSmlAux(`(_`,_`,_`)(Exp1, Exp2, Exp3)) = `(_`,_`,_`)(evalSmlAux(Exp1), evalSmlAux(Exp2), evalSmlAux(Exp3))" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq evalSmlAux(Exp + Exp') = evalSmlAux(Exp) +SmlFloat evalSmlAux(Exp')" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq evalSmlAux(Exp == Exp) = true" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq evalSmlAux(Exp == Exp') = false [owise]" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq evalSmlAux(I) = I" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq evalSmlAux(Str) = Str" + Constants.MAUDE_END_LINE;
//		data = data + Constants.TABULATION + "eq evalSmlAux(R) = R" + Constants.MAUDE_END_LINE;
		
		data = data + "endm" + Constants.BREAK_LINE+ Constants.BREAK_LINE;
		
		data = data + "view Sml from TRIV to SML is" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "sort Elt to SmlExp" + Constants.MAUDE_END_LINE;
		data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;		
		
		out.write(data.getBytes());
		out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		return mlmSmlFile;
	}

	public File addMaudePreludeBlock(File maudePreludeFile) {
		String data = "";
		try {
		
		FileOutputStream out = new FileOutputStream(maudePreludeFile);
		data = data + "sload file.maude" + Constants.BREAK_LINE + Constants.BREAK_LINE;
		
		data = data + "mod PRELUDE is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "pr LEXICAL" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "pr META-LEVEL" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "pr CONVERSION" + Constants.MAUDE_END_LINE;
		data = data + "endm"  + Constants.BREAK_LINE + Constants.BREAK_LINE;		
		
		out.write(data.getBytes());
		out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		return maudePreludeFile;
	}

	public File addRunXml(String hierarchyFileName, File runXmlFile, List<IModel> selectedHierarchy) {
		String data = "";
		try {
		
		FileOutputStream out = new FileOutputStream(runXmlFile);
		data = data + "sload " +  hierarchyFileName + Constants.BREAK_LINE + Constants.BREAK_LINE;
		//TODO Note this load is dependent on the language! This will change, for the moment ocl
		data = data + "sload xml-ocl.maude" + Constants.BREAK_LINE;
		
		data = data + "mod RUN is" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "inc MLM-XML" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "inc " + selectedHierarchy.get(1).getName().toUpperCase() + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE;
		
		out.write(data.getBytes());
		out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return runXmlFile;
	}

	public File addOclPreludeBlock(File oclPreludeFile) {
		String data = "";
		try {
		FileOutputStream out = new FileOutputStream(oclPreludeFile);
		data = data + "sload maude-prelude.maude" + Constants.BREAK_LINE + Constants.BREAK_LINE;
		
		data = data + "fmod OCL-TRUTH-VALUE is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "sort OclBool" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op true : -> OclBool [ctor]" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op false : -> OclBool [ctor]" + Constants.MAUDE_END_LINE;
		data = data + "endfm"  + Constants.BREAK_LINE + Constants.BREAK_LINE;	
		
		data = data + "mod OCL-PRELUDE is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "pr OCL-TRUTH-VALUE" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "pr PRELUDE" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "* (---- NAT" + Constants.BREAK_LINE;		
		data = data + Constants.TABULATION3 + "sort NzNat to OclNzNat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Nat to OclNat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Zero to OclZero," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "---- INT" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NzInt to OclNzInt," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Int to OclInt," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op -_ : Int -> Int to -OclInt_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _-_ : Int Int -> Int to _-OclInt_," + Constants.BREAK_LINE;	
		data = data + Constants.TABULATION3 + "---- RAT" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort PosRat to OclPosRat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NzRat to OclNzRat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + " sort Rat to OclRat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "---- FLOAT" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Float to OclFloat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort FiniteFloat to OclFiniteFloat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort DecFloat to OclDecFloat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _+_ : Float Float -> Float to _+OclFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _-_ : Float Float -> Float to _-OclFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _*_ : Float Float -> Float to _*OclFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _/_ : Float Float -> Float to _/OclFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _rem_ : Float Float -> Float to _remOclFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _^_ : Float Float -> Float to _^OclFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op min : Float Float -> Float to minFloat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op max : Float Float -> Float to maxFloat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op -_ : Float -> Float to -OclFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op +_ : Float -> Float to +loat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + " op abs : Float -> Float to absOclFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op floor : Float -> Float to floorOclFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op ceiling : Float -> Float to ceilingOclFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _<_ : Float Float -> Bool to _<OclFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _<=_ : Float Float -> Bool to _<=OclFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _>_ : Float Float -> Bool to _>OclFloat_," + Constants.BREAK_LINE;		
		data = data + Constants.TABULATION3 + "op _>=_ : Float Float -> Bool to _>=OclFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "---- QID" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Qid to OclQid," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "---- STRING" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Char to OclChar," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort String to OclString," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort FindResult to OclFindResult," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op notFound to oclNotFound," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op char : Nat ~> Char to oclChar," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _+_ : String String -> String to _+String_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _<_ : String String -> Bool to _<String_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _<=_ : String String -> Bool to _<=String_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _>_ : String String -> Bool to _>String_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _>=_ : String String -> Bool to _>=String_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "---- CONVERSION" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op float : String ~> Float to string2float," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op float : Rat ~> Float to rat2float," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op string to oclString," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "----" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeNatList to OclNeNatList," + Constants.BREAK_LINE;   
		data = data + Constants.TABULATION3 + "sort NatList to OclNatList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Bound to OclBound," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeQidList to OclNeQidList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort QidList to OclQidList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Sort to OclSort," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Kind to OclKind," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Type to OclType," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Constant to OclConstant," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Variable to OclVariable," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort TermQid to OclTermQid," + Constants.BREAK_LINE;   		
		data = data + Constants.TABULATION3 + "sort GroundTerm to OclGroundTerm," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Term to OclTerm ," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeGroundTermList to OclNeGroundTermList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort GroundTermList to OclGroundTermList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeTermList to OclNeTermList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort TermList to OclTermList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Assignment to OclAssignment," + Constants.BREAK_LINE; 
		data = data + Constants.TABULATION3 + "sort Substitution to OclSubstitution," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Context to OclContext," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeCTermList to OclNeCTermList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort GTermList to OclGTermList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort EqCondition to OclEqCondition," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Condition to OclCondition," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort UsingPair to OclUsingPair," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort UsingPairSet to OclUsingPairSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort RuleApplication to OclRuleApplication," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort CallStrategy to OclCallStrategy," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Strategy to OclStrategy," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort StrategyList to OclStrategyList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeQidSet to OclNeQidSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort QidSet to OclQidSet," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort SubsortDecl to OclSubsortDecl," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort SubsortDeclSet to OclSubsortDeclSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort EmptyQidSet to OclEmptyQidSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeSortSet to OclNeSortSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeKindSet to OclNeKindSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeTypeSet to OclNeTypeSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort SortSet to OclSortSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort KindSet to OclKindSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort TypeSet to OclTypeSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeTypeList to OclNeTypeList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort TypeList to OclTypeList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort TypeListSet to OclTypeListSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Attr to OclAttr," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort AttrSet to OclAttrSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Renaming to OclRenaming," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort RenamingSet to OclRenamingSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Expression to OclExpression," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort ViewExpression to OclViewExpression," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort ModuleExpression to OclModuleExpression," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort EmptyCommaList to OclEmptyCommaList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeParameterList to OclNeParameterList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort ParameterList to OclParameterList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort ParameterDecl to OclParameterDecl," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeParameterDeclList to OclNeParameterDeclList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort ParameterDeclList to OclParameterDeclList," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort Import to OclImport," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort ImportList to OclImportList," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Hook to OclHook," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort NeHookList to OclNeHookList," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort HookList to OclHookList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort OpDecl to OclOpDecl," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort OpDeclSet to OclOpDeclSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort MembAx to OclMembAx," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort MembAxSet to OclMembAxSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Equation to OclEquation," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort EquationSet to OclEquationSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Rule to OclRule," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort RuleSet to OclRuleSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort StratDecl to OclStratDecl," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort StratDeclSet to OclStratDeclSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort StratDefinition to OclStratDefinition," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort StratDefSet to OclStratDefSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort FModule to OclFModule," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort SModule to OclSModule," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort FTheory to OclFTheory," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort STheory to OclSTheory," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Module to OclModule," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Header to OclHeader," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort StratModule to OclStratModule," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort StratTheory to OclStratTheory," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort SortMapping to OclSortMapping," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort SortMappingSet to OclSortMappingSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort OpMapping to OclOpMapping," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort OpMappingSet to OclOpMappingSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort StratMapping to OclStratMapping," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort StratMappingSet to OclStratMappingSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort View to OclView," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort NeVariableSet to OclNeVariableSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort VariableSet to OclVariableSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Parent to OclParent," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Type? to OclType?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort PrintOption to OclPrintOption," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort PrintOptionSet to OclPrintOptionSet," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort VariantOption to OclVariantOption," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort VariantOptionSet to OclVariantOptionSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort SrewriteOption to OclSrewriteOption," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort UnificandPair to OclUnificandPair," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort UnificationProblem to OclUnificationProblem," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort PatternSubjectPair to OclPatternSubjectPair," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort MatchingProblem to OclMatchingProblem," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort ResultPair to OclResultPair," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort ResultTriple to OclResultTriple," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Result4Tuple to OclResult4Tuple," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort MatchPair to OclMatchPair," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort TraceStep to OclTraceStep," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Trace to OclTrace," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort UnificationPair to OclUnificationPair," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort UnificationTriple to OclUnificationTriple," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Variant to OclVariant," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort MatchOrUnificationPair to OclMatchOrUnificationPair," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort NarrowingApplyResult to OclNarrowingApplyResult," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort NarrowingSearchResult to OclNarrowingSearchResult," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort NarrowingSearchPathResult to OclNarrowingSearchPathResult," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort NarrowingStep to OclNarrowingStep," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NarrowingTrace to OclNarrowingTrace," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort ResultPair? to OclResultPair?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort ResultTriple? to OclResultTriple?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Result4Tuple? to OclResult4Tuple?," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort MatchPair? to OclMatchPair?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Substitution? to OclSubstitution?," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Trace? to OclTrace?," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort UnificationPair? to OclUnificationPair?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort UnificationTriple? to OclUnificationTriple?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Variant? to OclVariant?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort NarrowingApplyResult? to OclNarrowingApplyResult?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort NarrowingSearchResult? to OclNarrowingSearchResult?," + Constants.BREAK_LINE;       
		data = data + Constants.TABULATION3 + "sort NarrowingSearchPathResult? to OclNarrowingSearchPathResult?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort SmtResult to OclSmtResult," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort SmtResult? to OclSmtResult?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Strategy? to OclStrategy?," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op strat to oclStrat," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "op prec to oclPrec," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op frozen to oclFrozen," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "op poly to oclPoly," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "op noParse to oclNoParse," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "op noStratParse to oclNoStratParse," + Constants.BREAK_LINE; 
		data = data + Constants.TABULATION3 + "op none : -> Parent to oclParent," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "op upTerm to oclUpTerm," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op downTerm to oclDownTerm)" + Constants.MAUDE_END_LINE;  
		data = data + Constants.TABULATION3 + "endm" + Constants.BREAK_LINE;
		
		out.write(data.getBytes());
		out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		return oclPreludeFile;
	}

	public File addSmlPreludeBlock(File smlPreludeFile) {
		String data = "";
		try {
		FileOutputStream out = new FileOutputStream(smlPreludeFile);
		data = data + "sload maude-prelude.maude" + Constants.BREAK_LINE + Constants.BREAK_LINE;
		
		data = data + "fmod SML-TRUTH-VALUE is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "sort SmlBool" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op true : -> SmlBool [ctor]" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op false : -> SmlBool [ctor]" + Constants.MAUDE_END_LINE;
		data = data + "endfm"  + Constants.BREAK_LINE + Constants.BREAK_LINE;	
		
		data = data + "mod SML-PRELUDE is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "pr SML-TRUTH-VALUE" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "sort SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "subsort SmlBool SmlInt SmlString SmlFloat < SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op _=_ : SmlExp SmlExp -> SmlBool" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "pr PRELUDE" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION2 + "* (---- NAT" + Constants.BREAK_LINE;		
		data = data + Constants.TABULATION3 + "sort NzNat to SmlNzNat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Nat to SmlNat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Zero to SmlZero," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "---- INT" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NzInt to SmlNzInt," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Int to SmlInt," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op -_ : Int -> Int to -SmlInt_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _-_ : Int Int -> Int to _-SmlInt_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "---- RAT" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort PosRat to SmlPosRat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NzRat to SmlNzRat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + " sort Rat to SmlRat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "---- FLOAT" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Float to SmlFloat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort FiniteFloat to SmlFiniteFloat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort DecFloat to SmlDecFloat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _+_ : Float Float -> Float to _+SmlFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _-_ : Float Float -> Float to _-SmlFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _*_ : Float Float -> Float to _*SmlFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _/_ : Float Float -> Float to _/SmlFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _rem_ : Float Float -> Float to _remSmlFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _^_ : Float Float -> Float to _^SmlFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op min : Float Float -> Float to minFloat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op max : Float Float -> Float to maxFloat," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op -_ : Float -> Float to -SmlFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op +_ : Float -> Float to +loat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + " op abs : Float -> Float to absSmlFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op floor : Float -> Float to floorSmlFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op ceiling : Float -> Float to ceilingSmlFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _<_ : Float Float -> Bool to _<SmlFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _<=_ : Float Float -> Bool to _<=SmlFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _>_ : Float Float -> Bool to _>SmlFloat_," + Constants.BREAK_LINE;		
		data = data + Constants.TABULATION3 + "op _>=_ : Float Float -> Bool to _>=SmlFloat_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "---- QID" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Qid to SmlQid," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "---- STRING" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Char to SmlChar," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort String to SmlString," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort FindResult to SmlFindResult," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op notFound to smlNotFound," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op char : Nat ~> Char to smlChar," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _+_ : String String -> String to _+String_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _<_ : String String -> Bool to _<String_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _<=_ : String String -> Bool to _<=String_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _>_ : String String -> Bool to _>String_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op _>=_ : String String -> Bool to _>=String_," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "---- CONVERSION" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op float : String ~> Float to string2float," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op float : Rat ~> Float to rat2float," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op string to smlString," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "----" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeNatList to SmlNeNatList," + Constants.BREAK_LINE;   
		data = data + Constants.TABULATION3 + "sort NatList to SmlNatList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Bound to SmlBound," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeQidList to SmlNeQidList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort QidList to SmlQidList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Sort to SmlSort," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Kind to SmlKind," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Type to SmlType," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Constant to SmlConstant," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Variable to SmlVariable," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort TermQid to SmlTermQid," + Constants.BREAK_LINE;   		
		data = data + Constants.TABULATION3 + "sort GroundTerm to SmlGroundTerm," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Term to SmlTerm ," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeGroundTermList to SmlNeGroundTermList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort GroundTermList to SmlGroundTermList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeTermList to SmlNeTermList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort TermList to SmlTermList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Assignment to SmlAssignment," + Constants.BREAK_LINE; 
		data = data + Constants.TABULATION3 + "sort Substitution to SmlSubstitution," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Context to SmlContext," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeCTermList to SmlNeCTermList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort GTermList to SmlGTermList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort EqCondition to SmlEqCondition," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Condition to SmlCondition," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort UsingPair to SmlUsingPair," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort UsingPairSet to SmlUsingPairSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort RuleApplication to SmlRuleApplication," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort CallStrategy to SmlCallStrategy," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Strategy to SmlStrategy," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort StrategyList to SmlStrategyList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeQidSet to SmlNeQidSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort QidSet to SmlQidSet," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort SubsortDecl to SmlSubsortDecl," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort SubsortDeclSet to SmlSubsortDeclSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort EmptyQidSet to SmlEmptyQidSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeSortSet to SmlNeSortSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeKindSet to SmlNeKindSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeTypeSet to SmlNeTypeSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort SortSet to SmlSortSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort KindSet to SmlKindSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort TypeSet to SmlTypeSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeTypeList to SmlNeTypeList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort TypeList to SmlTypeList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort TypeListSet to SmlTypeListSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Attr to SmlAttr," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort AttrSet to SmlAttrSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Renaming to SmlRenaming," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort RenamingSet to SmlRenamingSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Expression to SmlExpression," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort ViewExpression to SmlViewExpression," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort ModuleExpression to SmlModuleExpression," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort EmptyCommaList to SmlEmptyCommaList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeParameterList to SmlNeParameterList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort ParameterList to SmlParameterList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort ParameterDecl to SmlParameterDecl," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NeParameterDeclList to SmlNeParameterDeclList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort ParameterDeclList to SmlParameterDeclList," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort Import to SmlImport," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort ImportList to SmlImportList," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Hook to SmlHook," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort NeHookList to SmlNeHookList," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort HookList to SmlHookList," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort OpDecl to SmlOpDecl," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort OpDeclSet to SmlOpDeclSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort MembAx to SmlMembAx," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort MembAxSet to SmlMembAxSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Equation to SmlEquation," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort EquationSet to SmlEquationSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Rule to SmlRule," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort RuleSet to SmlRuleSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort StratDecl to SmlStratDecl," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort StratDeclSet to SmlStratDeclSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort StratDefinition to SmlStratDefinition," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort StratDefSet to SmlStratDefSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort FModule to SmlFModule," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort SModule to SmlSModule," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort FTheory to SmlFTheory," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort STheory to SmlSTheory," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Module to SmlModule," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Header to SmlHeader," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort StratModule to SmlStratModule," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort StratTheory to SmlStratTheory," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort SortMapping to SmlSortMapping," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort SortMappingSet to SmlSortMappingSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort OpMapping to SmlOpMapping," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort OpMappingSet to SmlOpMappingSet," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort StratMapping to SmlStratMapping," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort StratMappingSet to SmlStratMappingSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort View to SmlView," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort NeVariableSet to SmlNeVariableSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort VariableSet to SmlVariableSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Parent to SmlParent," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Type? to SmlType?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort PrintOption to SmlPrintOption," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort PrintOptionSet to SmlPrintOptionSet," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort VariantOption to SmlVariantOption," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort VariantOptionSet to SmlVariantOptionSet," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort SrewriteOption to SmlSrewriteOption," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort UnificandPair to SmlUnificandPair," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort UnificationProblem to SmlUnificationProblem," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort PatternSubjectPair to SmlPatternSubjectPair," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort MatchingProblem to SmlMatchingProblem," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort ResultPair to SmlResultPair," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort ResultTriple to SmlResultTriple," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Result4Tuple to SmlResult4Tuple," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort MatchPair to SmlMatchPair," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort TraceStep to SmlTraceStep," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Trace to SmlTrace," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort UnificationPair to SmlUnificationPair," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort UnificationTriple to SmlUnificationTriple," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Variant to SmlVariant," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort MatchOrUnificationPair to SmlMatchOrUnificationPair," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort NarrowingApplyResult to SmlNarrowingApplyResult," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort NarrowingSearchResult to SmlNarrowingSearchResult," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort NarrowingSearchPathResult to SmlNarrowingSearchPathResult," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort NarrowingStep to SmlNarrowingStep," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort NarrowingTrace to SmlNarrowingTrace," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort ResultPair? to SmlResultPair?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort ResultTriple? to SmlResultTriple?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Result4Tuple? to SmlResult4Tuple?," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort MatchPair? to SmlMatchPair?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Substitution? to SmlSubstitution?," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "sort Trace? to SmlTrace?," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "sort UnificationPair? to SmlUnificationPair?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort UnificationTriple? to SmlUnificationTriple?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Variant? to SmlVariant?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort NarrowingApplyResult? to SmlNarrowingApplyResult?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort NarrowingSearchResult? to SmlNarrowingSearchResult?," + Constants.BREAK_LINE;       
		data = data + Constants.TABULATION3 + "sort NarrowingSearchPathResult? to SmlNarrowingSearchPathResult?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort SmtResult to SmlSmtResult," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort SmtResult? to SmlSmtResult?," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "sort Strategy? to SmlStrategy?," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op strat to smlStrat," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "op prec to smlPrec," + Constants.BREAK_LINE;
		data = data + Constants.TABULATION3 + "op frozen to smlFrozen," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "op poly to smlPoly," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "op noParse to smlNoParse," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "op noStratParse to smlNoStratParse," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "op none : -> Parent to smlParent," + Constants.BREAK_LINE;  
		data = data + Constants.TABULATION3 + "op upTerm to smlUpTerm," + Constants.BREAK_LINE;  		
		data = data + Constants.TABULATION3 + "op downTerm to smlDownTerm)" + Constants.MAUDE_END_LINE;  
		data = data + Constants.TABULATION3 + "endm" + Constants.BREAK_LINE;
		

		data = data + "view SmlExp from TRIV to SML-PRELUDE is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "sort Elt to SmlExp" + Constants.MAUDE_END_LINE;
		data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;
		
		data = data + "view SmlQid from TRIV to SML-PRELUDE is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "sort Elt to SmlQid" + Constants.MAUDE_END_LINE;
		data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;
		
		data = data + "mod SML-SIGN is" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "inc MAP{SmlQid, SmlExp} * (op _,_ : Map{SmlQid,SmlExp} Map{SmlQid,SmlExp} -> Map{SmlQid,SmlExp} to _#_)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op _=_ : SmlExp SmlExp -> SmlBool" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "op v : SmlQid -> SmlExp" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		
		
		data = data + Constants.TABULATION + "vars T T' : SmlTerm" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars Exp Exp' : SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  V : SmlVariable" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  VS : SmlVariableSet" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  Assign : Map{SmlQid, SmlExp}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  Str Str1 Str2 : SmlString" + Constants.MAUDE_END_LINE+ Constants.BREAK_LINE;
		

		data = data + Constants.TABULATION + "---- Evaluation of SML expressions .------------------------------------------" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "op evalSml : SmlExp -> SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op evalSmlAux : SmlExp -> SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq evalSml(Exp = Exp') = evalSmlAux(evalSml(Exp) = evalSml(Exp'))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq evalSmlAux(Exp = Exp) = true" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq evalSmlAux(Exp = Exp') = false [owise]" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "---- Parse term processing ---------------------------------------------------" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "---- The parsing of a SML term using mataParse returns a parse tree that must" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "---- then be transformed into a SML expression using the process operation." + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "---- The SML-DECLARATIONS will be defined for each specific case study." + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "---- The parse operation, as metaParse, takes a set of variables as one of its" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "---- arguments. The VBLES constant, defined in the SML-DECLARATIONS module will" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "---- be used." + Constants.BREAK_LINE;
		
		data = data + Constants.TABULATION + "op parse : SmlString SmlVariableSet -> SmlExp . ------------------------- main" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "op process : SmlTerm -> SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq parse(Str, VS) = process(getTerm(metaParse(upModule('SML-DECLARATIONS, false), VS, tokenize(Str), 'SmlExp)))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- the definition of the process function will be completed for each SML type" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "eq process(V) = v(V) .   ---- process variable" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq process('_=_[T, T']) = (process(T) = process(T'))" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		

		data = data + Constants.TABULATION + "---- Matching ----------------------------------------------------------------" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "sort MatchSolution" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "subsort Map{SmlQid, SmlExp} < MatchSolution < SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op noMatch : -> MatchSolution" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq (V |-> Exp) # (V |-> Exp) = V |-> Exp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq (V |-> Exp) # (V |-> Exp') = noMatch [owise]" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq noMatch # V |-> Exp = noMatch" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;
		

		data = data + Constants.TABULATION + "op match : SmlString SmlString -> MatchSolution . ----------------------- main" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "op match : SmlString SmlString SmlVariableSet -> MatchSolution" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op VBLES : -> SmlVariableSet . ---- defined in the SML-DECLARATIONS module" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "eq match(Str1, Str2) = match(Str1, Str2, VBLES)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq match(Str1, Str2, VS) = matchAux(parse(Str1, VS), parse(Str2, VS))" + Constants.MAUDE_END_LINE;
		
		data = data + Constants.TABULATION + "---- the definition of the matchAux function is completed for each SML type" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "op matchAux : SmlExp SmlExp -> MatchSolution" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq matchAux(Exp, Exp) = empty" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq matchAux(v(V), Exp) = V |-> Exp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq matchAux(Exp, Exp') = noMatch [owise]" + Constants.MAUDE_END_LINE + Constants.BREAK_LINE;


		data = data + Constants.TABULATION + "---- Substitution application ------------------------------------------------" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "---- Application of an assignament to a term" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + " op apply : SmlString [Map{SmlQid, SmlExp}] -> SmlString . --------------- main" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "eq apply(Str, noMatch) = Str" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq apply(Str, Assign) = applyAux(parse(Str, VBLES), Assign)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- the definition of the applyAux function is completed for each SML type" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "op applyAux : SmlExp Map{SmlQid, SmlExp} -> SmlString" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq applyAux(Exp = Exp', Assign) = (applyAux(Exp, Assign) = applyAux(Exp', Assign))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq applyAux(v(V), (V |-> Exp) # Assign) = Exp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq applyAux(Exp, Assign) = Exp [owise]" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;	
		

		data = data + "th SML-TRIV is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "pr SML-SIGN" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "sorts Elt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "subsort Elt < SmlExp" + Constants.MAUDE_END_LINE;
		data = data + "endth" + Constants.BREAK_LINE + Constants.BREAK_LINE;		
		
		data = data + "---- colset BOOL;" + Constants.BREAK_LINE;
		data = data + "mod colset-BOOL is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "pr SML-SIGN" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "sort BOOL" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "subsort SmlBool < BOOL < SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars T T' : SmlTerm" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  B : BOOL" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars Exp Exp1 Exp2 : SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  Assign : Map{SmlQid, SmlExp}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  QI : SmlQid" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- boolean variables" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "cmb v(QI) : SmlBool if getType(QI) == 'SmlBool" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "cmb v(QI) : BOOL if getType(QI) == 'BOOL" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- boolean operators" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + " op _and_ : BOOL BOOL -> BOOL [assoc prec 42]" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op not_ : BOOL -> BOOL" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq true and B = B" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq false and B = false" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- process, match and apply definitions" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "---- process" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "eq process('_and_[T, T']) = process(T) and process(T')" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq process('not_[T]) = not process(T)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- applyAux" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "eq applyAux(Exp1 and Exp2, Assign) = applyAux(Exp1, Assign) and applyAux(Exp2, Assign)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq applyAux(not Exp, Assign) = not applyAux(Exp, Assign)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- evalSml" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "eq evalSml(Exp1 and Exp2) = evalSml(Exp1) and evalSml(Exp2)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq evalSml(not Exp1) = not evalSml(Exp1)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq evalSml(true) = true" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq evalSml(false) = false" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;			
		
		data = data + "view BOOL from SML-TRIV to colset-BOOL is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "sort Elt to BOOL" + Constants.MAUDE_END_LINE;
		data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;
		
		data = data + "---- colset INT;" + Constants.BREAK_LINE;
		data = data + "mod colset-INT is"  + Constants.BREAK_LINE;		
		data = data + Constants.TABULATION + "pr colset-BOOL" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "sort INT" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "subsort SmlInt < INT < SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars T T' : SmlTerm" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  F : SmlQid" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars Exp1 Exp2 : SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + " var  Assign : Map{SmlQid, SmlExp}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  QI : SmlQid" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  I : INT" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- integer variables" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "cmb v(QI) : SmlInt if getType(QI) == 'SmlInt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "cmb v(QI) : INT if getType(QI) == 'INT" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- integer operators" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "op _+_ : INT INT -> INT [ditto]" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op _*_ : INT INT -> INT [ditto]" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- process, match and apply definitions" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "---- process int" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "eq process('_+_[T, T']) = process(T) + process(T')" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq process('_*_[T, T']) = process(T) * process(T')" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- process natural number" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "eq process('0.SmlZero) = 0" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq process('s_['0.SmlZero]) = 1" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "ceq process(F['0.SmlZero])  = trunc(rat(substr(smlString(F), 3, 2), 10)) if substr(smlString(F), 0, 3) = \"s_^\"" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- applyAux" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "eq applyAux(Exp1 + Exp2, Assign) = applyAux(Exp1, Assign) + applyAux(Exp2, Assign)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq applyAux(Exp1 * Exp2, Assign) = applyAux(Exp1, Assign) * applyAux(Exp2, Assign)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- eval" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "eq evalSml(I) = I [owise]" + Constants.MAUDE_END_LINE;

		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;			
		
		data = data + "view INT from SML-TRIV to colset-INT is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "sort Elt to INT" + Constants.MAUDE_END_LINE;
		data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;

		data = data + "---- colset REAL;" + Constants.BREAK_LINE;
		data = data + "mod colset-REAL is"  + Constants.BREAK_LINE;		
		data = data + Constants.TABULATION + "pr SML-SIGN" + Constants.MAUDE_END_LINE;		
		data = data + Constants.TABULATION + "sort REAL" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "subsort SmlFloat < REAL < SmlExp" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "var  Ct : SmlConstant" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "var  QI : SmlQid" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "vars T T' : SmlTerm" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "vars Exp1 Exp2 : SmlExp" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "var  Assign : Map{SmlQid, SmlExp}" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "var  R : REAL" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "---- real number variables" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "cmb v(QI) : SmlFloat if getType(QI) == 'SmlFloat" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "cmb v(QI) : REAL if getType(QI) == 'REAL" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "---- real number operators" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "op _+_ : REAL REAL -> SmlFloat [ditto]" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "---- process, match and apply definitions" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "eq process('_+_[T, T']) = process(T) + process(T')" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "ceq process(Ct) = string2float(smlString(getName(Ct))) if getType(Ct) == 'SmlFiniteFloat" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "eq applyAux(Exp1 + Exp2, Assign) = applyAux(Exp1, Assign) + applyAux(Exp2, Assign)" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "---- eval" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "eq evalSml(Exp1 + Exp2) = evalSml(Exp1) +SmlFloat evalSml(Exp2)" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "eq evalSml(R) = R [owise]" + Constants.MAUDE_END_LINE;			
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;			
		
		data = data + "view REAL from SML-TRIV to colset-REAL is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "sort Elt to REAL" + Constants.MAUDE_END_LINE;
		data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;		
		
		data = data + "---- colset STRING = string;" + Constants.BREAK_LINE;
		data = data + "mod colset-STRING is"  + Constants.BREAK_LINE;		
		data = data + Constants.TABULATION + "pr colset-BOOL" + Constants.MAUDE_END_LINE;	
		data = data + Constants.TABULATION + "sort STRING" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "subsort SmlString < STRING < SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var Ct : SmlConstant" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars T T' : SmlTerm" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  QI : SmlQid" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  Str : STRING" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- string variables" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "cmb v(QI) : SmlString if getType(QI) == 'SmlString" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "cmb v(QI) : STRING if getType(QI) == 'STRING" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- string operators" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "---- process, match and apply definitions" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "ceq process(Ct) = substr(smlString(getName(Ct)), 1, _-SmlInt_(length(smlString(getName(Ct))), 2)) if getType(Ct) == 'SmlString" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- eval" + Constants.BREAK_LINE;		
		data = data + Constants.TABULATION + "eq evalSml(Str) = Str [owise]" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;	
		
		data = data + "view STRING from SML-TRIV to colset-STRING is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "sort Elt to STRING" + Constants.MAUDE_END_LINE;
		data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;		
		
		data = data + "---- products" + Constants.BREAK_LINE;
		data = data + "mod SML-PRODUCT-TOP is"  + Constants.BREAK_LINE;		
		data = data + Constants.TABULATION + "pr SML-SIGN" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "sort Product" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "subsort Product < SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  QI : SmlQid" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- product variables" + Constants.BREAK_LINE;	
		data = data + Constants.TABULATION + "cmb v(QI) : Product if getType(QI) == 'Product" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;	

		data = data + "mod SML-2-PRODUCT-TOP is"  + Constants.BREAK_LINE;		
		data = data + Constants.TABULATION + "pr SML-PRODUCT-TOP" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op `(_`,_`) : SmlExp SmlExp -> Product" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- process tuples" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "vars T T' : SmlTerm" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars V1 V1' V2 V2' : SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  Assign : Map{SmlQid, SmlExp}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars Exp Exp' : SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- product operators" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "---- process, match and apply definitions" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "eq process('`(_`,_`)[T, T']) = `(_`,_`)(process(T), process(T'))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq matchAux(`(_`,_`)(V1, V2), `(_`,_`)(V1', V2')) = matchAux(V1, V1') # matchAux(V2, V2')" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq applyAux(`(_`,_`)(V1, V2), Assign) = `(_`,_`)(applyAux(V1, Assign), applyAux(V2, Assign))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq evalSml(`(_`,_`)(Exp, Exp')) = `(_`,_`)(evalSml(Exp), evalSml(Exp'))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;			
		
		data = data + "mod SML-2-PRODUCT{X1 :: SML-TRIV, X2 :: SML-TRIV} is"  + Constants.BREAK_LINE;		
		data = data + Constants.TABULATION + "pr SML-2-PRODUCT-TOP" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "sort Product{X1, X2}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "subsort Product{X1, X2} < Product" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op `(_`,_`) : X1$Elt X2$Elt -> Product{X1, X2}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;	

		data = data + "mod SML-3-PRODUCT-TOP is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "pr SML-PRODUCT-TOP" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op `(_`,_`,_`) : SmlExp SmlExp SmlExp -> Product" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars T T' T'' : SmlTerm" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars V1 V1' V2 V2' V3 V3' : SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  Assign : Map{SmlQid, SmlExp}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- process, match, apply, and eval definitions" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "eq process('`(_`,_`,_`)[T, T', T'']) = `(_`,_`,_`)(process(T), process(T'), process(T''))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq matchAux(`(_`,_`,_`)(V1, V2, V3), `(_`,_`,_`)(V1', V2', V3')) = matchAux(V1, V1') # matchAux(V2, V2') # matchAux(V3, V3')" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq applyAux(`(_`,_`,_`)(V1, V2, V3), Assign) = `(_`,_`,_`)(applyAux(V1, Assign), applyAux(V2, Assign), applyAux(V3, Assign))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq evalSml(`(_`,_`,_`)(V1, V2, V3)) = `(_`,_`,_`)(evalSml(V1), evalSml(V2), evalSml(V3))" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;			
		
		data = data + "mod SML-3-PRODUCT{X1 :: SML-TRIV, X2 :: SML-TRIV, X3 :: SML-TRIV} is"  + Constants.BREAK_LINE;		
		data = data + Constants.TABULATION + "pr SML-3-PRODUCT-TOP" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "sort Product{X1, X2, X3}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "subsort Product{X1, X2, X3} < Product" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op `(_`,_`,_`) : X1$Elt X2$Elt X3$Elt -> Product{X1, X2, X3}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;	

		
		data = data + "---- lists" + Constants.BREAK_LINE;
		data = data + "mod SML-LIST-TOP is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "pr SML-SIGN" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "sorts EmptyList List" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "subsort EmptyList < List < SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op [] : -> EmptyList" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op _::_ : SmlExp SmlExp -> List" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  QI : SmlQid" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars T T' T'' : SmlTerm" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars E E' L L' : SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  Assign : Map{SmlQid, SmlExp}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- list variables" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "cmb v(QI) : List if getType(QI) == 'List" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- process, match, apply, and eval definitions" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "eq process('_::_[T, T']) = process(T) :: process(T')" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq process('`[`].EmptyList) = []" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq matchAux(E :: L, E' :: L') = matchAux(E, E') # matchAux(L, L')" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq applyAux(E :: L, Assign) = applyAux(E, Assign) :: applyAux(L, Assign)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;		
		
		data = data + "mod SML-LIST{X :: SML-TRIV} is"  + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "pr SML-LIST-TOP" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "sort List{X}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "subsort EmptyList < List{X} < List < SmlExp" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op _::_ : X$Elt List{X} -> List{X}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- fun member t ts = List.exists (fn t' => t' = t) ts" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "---- fun remove t ts = List.filter (fn t' => t <> t') ts" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "---- TODO provide support for List.exists and List.filter and to the possibility of defining new functions" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "---- Currently member and remove are predefined operations in the SML-LIST module" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "vars E E' : X$Elt" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars L L' : List{X}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "vars T T' : SmlTerm" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "var  Assign : Map{SmlQid, SmlExp}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- auxiliary functions" + Constants.BREAK_LINE;	
		data = data + Constants.TABULATION + "op remove__ : X$Elt List{X} -> List{X}" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq remove E (E' :: L) = if E == E' then remove E L else E' :: remove E L fi" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq remove E [] = []" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "op member__ : X$Elt List{X} -> SmlBool" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq member E (E' :: L) = if E == E' then true else member E L fi" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq member E [] = false" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "---- process, match, apply, and eval definitions" + Constants.BREAK_LINE;
		data = data + Constants.TABULATION + "eq process('remove__[T, T']) = remove process(T) process(T')" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq process('member__[T, T']) = member process(T) process(T')" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq matchAux(remove E L, remove E' L') = remove matchAux(E, E') matchAux(L, L')" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq matchAux(member E L, remove E' L') = member matchAux(E, E') matchAux(L, L')" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq applyAux(remove E L, Assign) = member applyAux(E, Assign) applyAux(L, Assign)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "eq applyAux(member E L, Assign) = member applyAux(E, Assign) applyAux(L, Assign)" + Constants.MAUDE_END_LINE;
		data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;

	
		out.write(data.getBytes());
		out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	
		return smlPreludeFile;
	}

	public File processDeclarationsFile(File declarationsFile, File smlDeclarations) {
		String data = "";
		data +=  "sload " +  this.getAdditionalLanguage() + "-prelude.maude" + Constants.BREAK_LINE + Constants.BREAK_LINE;
		try {
			FileOutputStream out = new FileOutputStream(smlDeclarations);
			Map<String,Set<String>> variablesDeclared = new HashMap <String,Set<String>> ();
			Set<String> colsetDeclared = new HashSet<String> ();
			Scanner sc;
			sc = new Scanner(declarationsFile);
			//Here we process SML files now. If more languages are to be added, this should be extended to differentiate 
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				if (line != "") {
					//The line is a colour set declaration
					if (line.startsWith("colset")) {
						//Split by equal symbol
						String[] splitDeclararition = line.split("=");
						
						String leftHandSide = splitDeclararition[0];
						String rightHandSide = splitDeclararition[1];
						
						//Get the alias
						String[] splitLeftHandSide = leftHandSide.split(" ");
						String alias = splitLeftHandSide[1];
						//Remove ";"
						if (rightHandSide.contains(";")) {
							rightHandSide = rightHandSide.replace(";", "");
						}
						data += "---- " + line +  Constants.BREAK_LINE;
						data = data + "mod colset-" + alias + " is" + Constants.BREAK_LINE;
						data = data + Constants.TABULATION + "pr SML-SIGN" + Constants.MAUDE_END_LINE;	
						//A product declaration, for now supported products of 2 and 3
						if (rightHandSide.contains("product")) {
							//Remove product
							rightHandSide = rightHandSide.replace("product", "");
							//Remove all white spaces
							rightHandSide = rightHandSide.replaceAll("\\s+","");
							//See whether is a product of 2 or 3
							String[] splitRightHandSide = rightHandSide.split("\\*");


							if (splitRightHandSide.length == 2) {
								data = data + Constants.TABULATION + "pr SML-2-PRODUCT" + "{" + splitRightHandSide[0] + ", " + splitRightHandSide[1] + "}" + Constants.MAUDE_END_LINE;
								data = data + Constants.TABULATION + "sort " + alias + Constants.MAUDE_END_LINE;
								data = data + Constants.TABULATION + "subsort Product{" + splitRightHandSide[0] + ", " + splitRightHandSide[1] + "} < " + alias + " < SmlExp" + Constants.MAUDE_END_LINE;
								data = data + Constants.TABULATION + "var QI : SmlQid" + Constants.MAUDE_END_LINE;
								data = data + Constants.TABULATION + "cmb v(QI) : Product{" + splitRightHandSide[0] + ", " + splitRightHandSide[1] + "} if getType(QI) == '" + alias + Constants.MAUDE_END_LINE;
							}
							else if (splitRightHandSide.length == 3) {
								data = data + Constants.TABULATION + "pr SML-3-PRODUCT" + "{" + splitRightHandSide[0] + ", " + splitRightHandSide[1] + ", " + splitRightHandSide[2] + "}" + Constants.MAUDE_END_LINE;
								data = data + Constants.TABULATION + "sort " + alias + Constants.MAUDE_END_LINE;
								data = data + Constants.TABULATION + "subsort Product{" + splitRightHandSide[0] + ", " + splitRightHandSide[1] + ", " + splitRightHandSide[2] + "} < " + alias + " < SmlExp" + Constants.MAUDE_END_LINE;
								data = data + Constants.TABULATION + "var QI : SmlQid" + Constants.MAUDE_END_LINE;
								data = data + Constants.TABULATION + "cmb v(QI) : Product{" + splitRightHandSide[0] + ", " + splitRightHandSide[1] + ", " + splitRightHandSide[2]+ "} if getType(QI) == '" + alias + Constants.MAUDE_END_LINE;
							}						
							

						}
						//A list declaration
						else if (rightHandSide.contains("list")) {
							//Remove list
							rightHandSide = rightHandSide.replace("list", "");
							//Remove all white spaces
							rightHandSide = rightHandSide.replaceAll("\\s+","");
							String listType = rightHandSide;
							data = data + Constants.TABULATION + "pr SML-LIST{" + listType + "}" + Constants.MAUDE_END_LINE;
							data = data + Constants.TABULATION + "sort " + alias + Constants.MAUDE_END_LINE;
							data = data + Constants.TABULATION + "subsort List{" + listType + "} < " + alias + " < SmlExp" + Constants.MAUDE_END_LINE;
							data = data + Constants.TABULATION + "var QI : SmlQid" + Constants.MAUDE_END_LINE;
							data = data + Constants.TABULATION + "cmb v(QI) : List{" + listType + "} if getType(QI) == '" + alias + Constants.MAUDE_END_LINE;
						}
						//Regular declarations
						else {
							rightHandSide = rightHandSide.replaceAll("\\s+","");
							String listType = rightHandSide;
							data = data + Constants.TABULATION + "pr colset-" + listType + Constants.MAUDE_END_LINE;
							data = data + Constants.TABULATION + "sort " + alias + Constants.MAUDE_END_LINE;
							data = data + Constants.TABULATION + "subsort " + listType + " < " + alias + " < SmlExp" + Constants.MAUDE_END_LINE;
							data = data + Constants.TABULATION + "var QI : SmlQid" + Constants.MAUDE_END_LINE;
							data = data + Constants.TABULATION + "cmb v(QI) : " + listType + "if getType(QI) == '" + alias + Constants.MAUDE_END_LINE;
						}
						data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;
						data = data + "view " + alias + " from SML-TRIV to colset-" + alias + " is"  + Constants.BREAK_LINE;
						data = data + Constants.TABULATION + "sort Elt to " + alias + Constants.MAUDE_END_LINE;
						data = data + "endv" + Constants.BREAK_LINE + Constants.BREAK_LINE;
						colsetDeclared.add(alias);
					}
					//The line is a variable
					else if (line.startsWith("var")) {
						//Split by colon symbol
						String[] splitDeclararition = line.split(":");
						
						String leftHandSide = splitDeclararition[0];
						String rightHandSide = splitDeclararition[1];
						//Remove var
						leftHandSide = leftHandSide.replace("var", "");
						//Remove all white spaces
						leftHandSide = leftHandSide.replaceAll("\\s+","");
						//Remove all white spaces
						rightHandSide = rightHandSide.replaceAll("\\s+","");
						//Remove ";"
						if (rightHandSide.contains(";")) {
							rightHandSide = rightHandSide.replace(";", "");
						}
						String varsType = rightHandSide;
						String[] vars = leftHandSide.split(",");
						variablesDeclared.put(varsType, new HashSet<String> (Arrays.asList(vars)));
					}
				}
			}
			data = data + "mod SML-DECLARATIONS is" + Constants.BREAK_LINE;
			data = data + Constants.TABULATION + "pr SML-SIGN" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "pr colset-BOOL" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "pr colset-INT" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "pr colset-REAL" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "pr colset-STRING" + Constants.MAUDE_END_LINE;
			for (String colset : colsetDeclared) {
				data = data + Constants.TABULATION + "pr colset-" + colset + Constants.MAUDE_END_LINE;
			}
			data = data + Constants.TABULATION + "pr SML-3-PRODUCT-TOP" + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "eq VBLES = ";
			for (Map.Entry<String, Set<String>> entry : variablesDeclared.entrySet()) {
				for (String variable : entry.getValue()) {
					data += "'" + variable + ":" + entry.getKey() + " ; ";
				}
			}
			data = data.substring(0, data.lastIndexOf("; ")) + Constants.MAUDE_END_LINE;
			data = data + Constants.TABULATION + "endm" + Constants.BREAK_LINE + Constants.BREAK_LINE;
			sc.close();
			out.write(data.getBytes());
			out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		return smlDeclarations;
	}
}
