package no.hvl.multecore.mcmt.maude;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Constants {
	
	public static final String MAUDE_PERIOD = " .";
	public static final String BREAK_LINE = "\n";
	public static final String MAUDE_END_LINE = MAUDE_PERIOD + BREAK_LINE;
	public static final String MAUDE_COMMENTARY = "---- ";
	public static final String TABULATION = "\t";
	public static final String TABULATION2 = "\t\t";
	public static final String TABULATION3 = "\t\t\t";
	public static final String MAUDE_OPEN_CONFIGURATION = "< ";
	public static final String MAUDE_CLOSE_CONFIGURATION = " >";
	public static final String MAUDE_OPEN_RULE = "{ ";
	public static final String MAUDE_CLOSE_RULE = " }";
	public static final String MAUDE_SEPARATE_ATTRIBUTE = ", ";
	public static final String MAUDE_NODE = "Node";
	public static final String MAUDE_ATTRI = "Attri";
	public static final String MAUDE_RELATION = "Relation";
	public static final String MAUDE_VERTICAL_SEPARATOR = " | ";
	public static final String MAUDE_TYPE_SEPARATOR = " : ";
	public static final String MAUDE_REWRITE_RULE = " => ";
	public static final String MAUDE_CONDITIONAL_RULE = "crl ";
	public static final String MAUDE_UNCONDITIONAL_RULE = "rl ";
	public static final String MAUDE_BINARY_CONJUNCTION_CONNECTIVE = " /\\ ";
	public static final String MAUDE_SET_COMPLEMENT = " \\ ";
	public static final String MAUDE_BINARY_LESS_THAN = " < ";
	public static final String MAUDE_MATCHING_EQUATION = " := ";
	public static final String MAUDE_START_CONDITION_BLOCK = "if ";
	
	public static final String FIRST_LEVEL_META_RULE = "meta_1";
	
	
	public static final String MAUDE_SORT_NAME = "Name";
	public static final String MAUDE_SORT_ATTRIBUTE_SET ="AttributeSet";
	public static final String MAUDE_SORT_OID ="Oid";
	public static final String MAUDE_SORT_CONFIGURATION ="Configuration";
	public static final String MAUDE_SORT_SET_OID ="Set{Oid}";
	public static final String MAUDE_SORT_LIST_OID ="List{Oid}";
	public static final String MAUDE_SORT_OBJECT ="Object";
	public static final String MAUDE_CONF_VARIABLE ="Conf";
	public static final String MAUDE_CONF_PRIME_VARIABLE ="Conf'";
	public static final String MAUDE_SORT_NAT ="Nat";
	public static final String MAUDE_SORT_INT ="Int";
	public static final String MAUDE_SORT_STRING ="String";
	public static final String MAUDE_SORT_BOOL ="Bool";
	public static final String MAUDE_SORT_FLOAT ="Float";
	public static final String MAUDE_SORT_SUBSTITUTION ="Substitution";
	public static final String MAUDE_NAT_VARIABLE = "N";
	
	public static final String MAUDE_SORT_OCL_INT ="OclInt";
	public static final String MAUDE_SORT_OCL_STRING ="OclString";
	public static final String MAUDE_SORT_OCL_BOOL ="OclBool";
	public static final String MAUDE_SORT_OCL_REAL ="OclFloat";

	public static final String MAUDE_SORT_SML_INT ="SmlInt";
	public static final String MAUDE_SORT_SML_STRING ="SmlString";
	public static final String MAUDE_SORT_SML_BOOL ="SmlBool";
	public static final String MAUDE_SORT_SML_REAL ="SmlFloat";
	
	public static final String MAUDE_ID_ATTRIBUTE_SET ="Atts";
	public static final String MAUDE_ID_ELTS ="Elts";
	public static final String MAUDE_ID_RELS ="Rels";
	public static final String MAUDE_ID_NODE_ATTRIBUTE ="Attri";
	public static final String MAUDE_ID_OID ="O";
	public static final String MAUDE_STAR_SYMBOL = "*";
	public static final String MAUDE_ID_OID_NEW = "oid";
	public static final String MAUDE_ID_ID_NEW = "id";
	public static final String MAUDE_ID_MULTIPLICITY = "Multiplicity";
	public static final String MAUDE_ID_REMOVE_NODES_RHS = "NodeIds2";
	public static final String MAUDE_ID_REMOVE_RELS_RHS = "RelIds2";
	public static final String MAUDE_ID_BOX = "box";
	public static final String MAUDE_ID_BOX_OBJECT = "Ob";
	public static final String MAUDE_ID_BOX_NODE = "NodeIds";
	public static final String MAUDE_ID_BOX_RELS = "RelIds";
	public static final String MAUDE_ID_BOX_MATCH = "MatchIds";
	
	public static final String MAUDE_COUNTER_LHS = "< counter : Counter | value : N > " + MAUDE_CONF_VARIABLE;
	public static final String MAUDE_COUNTER_LHS_CONDITION = "< counter : Counter | value : N > " + MAUDE_CONF_PRIME_VARIABLE;
	public static final String MAUDE_COUNTER_RHS = "< counter : Counter | value : s N > " + MAUDE_CONF_VARIABLE;
	
	public static final String MAUDE_FILE_CORE = "mlm-core.maude";
	public static final String MAUDE_FILE_2_TUPLE = "2-tuple.maude";
	public static final String MAUDE_FILE_3_TUPLE = "3-tuple.maude";
	public static final String MAUDE_FILE_MLM_OCL = "mlm-ocl.maude";
	public static final String MAUDE_FILE_XML = "xml.maude";
	
	public static final String MAUDE_OUTPUTS_DIRECTORY = "/maude-outputs/";
	
	public static final String MAUDE_CURRENT_PROCESS_REW = "REWRITE";
	public static final String MAUDE_CURRENT_PROCESS_SREW = "STRATEGIC_REWRITE";
	public static final String MAUDE_CURRENT_PROCESS_LTL = "LTL_MODEL_CHECKING";
	
	public static final String OCL = "OCL";
	public static final String SML = "SML";
	
}
