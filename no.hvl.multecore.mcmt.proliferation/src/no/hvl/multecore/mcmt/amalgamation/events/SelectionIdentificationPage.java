package no.hvl.multecore.mcmt.amalgamation.events;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;

import no.hvl.multecore.common.hierarchy.IEdge;
import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.mcmt.amalgamation.Combination;
import no.hvl.multecore.mcmt.amalgamation.Identification;
import no.hvl.multecore.mcmt.amalgamation.McmtModuleHierarchyRules;
import no.hvl.multecore.mcmt.proliferation.rule.RuleHierarchy;

public class SelectionIdentificationPage extends WizardPage implements Listener{

    private McmtModuleHierarchyRules mainMcmtModuleHierarchyRules;
    private Composite container;
    private Button selectCombinationButton;
    private Button selectIndividualRuleButton;
    private Button addElementButton;
    private Button saveIdentificationButton;
    private List combinationRulesList;
    private List individualRulesList;
    private List elementsRuleList;
    private List identifiedElementsList;
    private List combinationIdentificationsList;
    
    //This might be applied to previous page
    //Keeping track of what element is selected to not have to search it by string in the Lists
    Combination selectedCombination;
    RuleHierarchy selectedRule;
    Set<INode> nodesUnion;
    Set<IEdge> edgesUnion;
    Set<INode> identifiedNodes;
    Set<IEdge> identifiedEdges;
    
	public SelectionIdentificationPage() {
        super("Third Page");
        setTitle("Select the elements that corresponds in the Identification (I_0)");
        setDescription("");
        nodesUnion = new HashSet<INode>();
        edgesUnion = new HashSet<IEdge>();
        identifiedNodes = new HashSet<INode>();
        identifiedEdges = new HashSet<IEdge>();        
	}

	public SelectionIdentificationPage(McmtModuleHierarchyRules mainHierarchy) {
        super("Third Page");
        setTitle("Select the elements that corresponds in the Identification (I_0)");
    	this.mainMcmtModuleHierarchyRules = mainHierarchy;
        setDescription("");
        nodesUnion = new HashSet<INode>();
        edgesUnion = new HashSet<IEdge>();
        identifiedNodes = new HashSet<INode>();
        identifiedEdges = new HashSet<IEdge>();            
	}	
	
	@Override
	public void setVisible(final boolean visible) {
		if (visible) {
			selectedCombination = null;
			selectedRule = null;
			nodesUnion.clear();
			edgesUnion.clear();
			identifiedNodes.clear();
			identifiedEdges.clear();			
			combinationRulesList.removeAll();
			elementsRuleList.removeAll();
			individualRulesList.removeAll();
			identifiedElementsList.removeAll();
			for (Combination combination : mainMcmtModuleHierarchyRules.getCombinations()) {
				combinationRulesList.add(combination.getCombinationNameToDisplay());
			}
		}
		super.setVisible(visible);
	}	
	
	@Override
	public void createControl(Composite parent) {
        container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
        layout.numColumns = 4;
        container.setLayout(layout);
        
        //First label for list of Rule combinations
        Label label1 = new Label(container, SWT.NONE);
        label1.setText("Combinations:");
        
        // Empty column
        Label emptyLabel = new Label(container, SWT.LEAD);
        
        //Second label for individual Rules
        Label label2 = new Label(container, SWT.NONE);
        label2.setText("MCMT rules:");	          
        
        // Empty column
        Label emptyLabel2 = new Label(container, SWT.LEAD);       

        // Unselected Hierarchies and its grid specification
        combinationRulesList = new List(container, SWT.BORDER | SWT.SINGLE );
        GridData combinationRulesGrid = new GridData();
        combinationRulesGrid.horizontalAlignment = GridData.BEGINNING;
        combinationRulesGrid.heightHint = 100;
        combinationRulesGrid.widthHint = 400;
        combinationRulesGrid.grabExcessVerticalSpace = true;
        combinationRulesGrid.grabExcessHorizontalSpace = true;
        combinationRulesList.setLayoutData(combinationRulesGrid);
        combinationRulesList.addListener(SWT.Selection, this);        
        
        selectCombinationButton = new Button(container, SWT.PUSH);
        selectCombinationButton.setText("Select");
        selectCombinationButton.setAlignment(SWT.CENTER);
        selectCombinationButton.setEnabled(false);
        selectCombinationButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        selectCombinationButton.addListener(SWT.Selection, this);  

        
        // Unselected Hierarchies and its grid specification
        individualRulesList = new List(container, SWT.BORDER | SWT.SINGLE );
        GridData individualRulesGrid = new GridData();
        individualRulesGrid.horizontalAlignment = GridData.BEGINNING;
        individualRulesGrid.heightHint = 100;
        individualRulesGrid.widthHint = 400;
        individualRulesGrid.grabExcessVerticalSpace = true;
        individualRulesGrid.grabExcessHorizontalSpace = true;
        individualRulesList.setLayoutData(individualRulesGrid);
        individualRulesList.addListener(SWT.Selection, this); 
        
        selectIndividualRuleButton = new Button(container, SWT.PUSH);
        selectIndividualRuleButton.setText("Select");
        selectIndividualRuleButton.setAlignment(SWT.CENTER);
        selectIndividualRuleButton.setEnabled(false);
        selectIndividualRuleButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        selectIndividualRuleButton.addListener(SWT.Selection, this);          
        
        //First label for list of Rule combinations
        Label label3 = new Label(container, SWT.NONE);
        label3.setText("Rule elements:");
        
        // Empty column
        Label emptyLabel3 = new Label(container, SWT.LEAD);
        
        //Second label for individual Rules
        Label label4 = new Label(container, SWT.NONE);
        label4.setText("Identified Elements");	          
        
        // Empty column
        Label emptyLabel4 = new Label(container, SWT.LEAD);
        
        // List of elements to of a single rule to be identified
        elementsRuleList = new List(container, SWT.BORDER | SWT.SINGLE );
        GridData elementsRuleGrid = new GridData();
        elementsRuleGrid.horizontalAlignment = GridData.BEGINNING;
        elementsRuleGrid.heightHint = 100;
        elementsRuleGrid.widthHint = 400;
        elementsRuleGrid.grabExcessVerticalSpace = true;
        elementsRuleGrid.grabExcessHorizontalSpace = true;
        elementsRuleList.setLayoutData(elementsRuleGrid);
        elementsRuleList.addListener(SWT.Selection, this);           
        
        addElementButton = new Button(container, SWT.PUSH);
        addElementButton.setText("Add element");
        addElementButton.setAlignment(SWT.CENTER);
        addElementButton.setEnabled(false);
        addElementButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        addElementButton.addListener(SWT.Selection, this); 

        // List of elements to of a single rule to be identified
        identifiedElementsList = new List(container, SWT.BORDER | SWT.SINGLE );
        GridData identifiedElementsGrid = new GridData();
        identifiedElementsGrid.horizontalAlignment = GridData.BEGINNING;
        identifiedElementsGrid.heightHint = 100;
        identifiedElementsGrid.widthHint = 400;
        identifiedElementsGrid.grabExcessVerticalSpace = true;
        identifiedElementsGrid.grabExcessHorizontalSpace = true;
        identifiedElementsList.setLayoutData(identifiedElementsGrid);
        identifiedElementsList.addListener(SWT.Selection, this);           
        
        saveIdentificationButton = new Button(container, SWT.PUSH);
        saveIdentificationButton.setText("Save");
        saveIdentificationButton.setAlignment(SWT.CENTER);
        saveIdentificationButton.setEnabled(false);
        saveIdentificationButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        saveIdentificationButton.addListener(SWT.Selection, this); 

        //label for associated combination with identifications
        Label label5 = new Label(container, SWT.NONE);
        label5.setText("Combination element identifications");
        
        // Empty column
        Label emptyLabel5 = new Label(container, SWT.LEAD);
        
        // Empty column
        Label emptyLabel6 = new Label(container, SWT.LEAD);
        
        // Empty column
        Label emptyLabel7 = new Label(container, SWT.LEAD);
        
        // List of elements to of a single rule to be identified
        combinationIdentificationsList = new List(container, SWT.BORDER | SWT.SINGLE );
        GridData combinationIdentificationsGrid = new GridData(SWT.FILL, SWT.FILL, true, true);
        combinationIdentificationsGrid.horizontalAlignment = GridData.BEGINNING;
        combinationIdentificationsGrid.heightHint = 100;
        combinationIdentificationsGrid.widthHint = 500;
        combinationIdentificationsGrid.grabExcessVerticalSpace = true;
        combinationIdentificationsGrid.grabExcessHorizontalSpace = true;
        combinationIdentificationsList.setLayoutData(combinationIdentificationsGrid);
        combinationIdentificationsList.addListener(SWT.Selection, this);            
        
        setControl(container);
        setPageComplete(false);
        
	}

	@Override
	public void handleEvent(Event event) {
		
		if (event.widget == combinationRulesList) {
			if (combinationRulesList.getSelection().length == 1) {
				individualRulesList.removeAll();
				selectCombinationButton.setEnabled(true);
			}
		}
		if (event.widget == selectCombinationButton) {
			for (Combination combination : this.mainMcmtModuleHierarchyRules.getCombinations()) {
				if (combination.getCombinationNameToDisplay().equals(combinationRulesList.getSelection()[0])) {
					for (RuleHierarchy rule : combination.getRulesCombined()) {
						individualRulesList.add(rule.getName());
					}
					selectedCombination = combination;
				}
			}
			selectCombinationButton.setEnabled(false);
		}
		
		if (event.widget == individualRulesList) {
			if (individualRulesList.getSelection().length == 1) {
				elementsRuleList.removeAll();
				selectIndividualRuleButton.setEnabled(true);
			}
		}
		if (event.widget == selectIndividualRuleButton) {
			nodesUnion.clear();
			edgesUnion.clear();
			for (RuleHierarchy ruleHierarchy : selectedCombination.getRulesCombined()) {
				if (ruleHierarchy.getName().equals(individualRulesList.getSelection()[0])) {
					//We run through the FromModel and the ToModel and display its Union (which is the I)
					IModel fromModel = ruleHierarchy.getFromModel();
					IModel toModel = ruleHierarchy.getToModel();
					
					nodesUnion.addAll(fromModel.getNodes());
					nodesUnion.addAll(toModel.getNodes());

					edgesUnion.addAll(fromModel.getEdges());
					edgesUnion.addAll(toModel.getEdges());
					
					for (INode node : nodesUnion) {
						elementsRuleList.add(node.getName());
					}
					for (IEdge edge : edgesUnion) {
						elementsRuleList.add(edge.getName());
					}
					selectedRule = ruleHierarchy;
				}
			}
			selectCombinationButton.setEnabled(false);
		}
		if (event.widget == elementsRuleList) {
			if (elementsRuleList.getSelection().length == 1) {
				addElementButton.setEnabled(true);
			}
		}
		if (event.widget == addElementButton) {
			//TODO Add some control to avoid adding nodes and relations to be combined
			boolean noNodes = true;
			for (INode selectedNode : nodesUnion) {
				if (selectedNode.getName().equals(elementsRuleList.getSelection()[0])) {
					identifiedElementsList.add(elementsRuleList.getSelection()[0]);
					identifiedNodes.add(selectedNode);
					elementsRuleList.remove(elementsRuleList.getSelection()[0]);
					noNodes = false;
					break;
				}
			}
			if (noNodes) {
				for (IEdge selectedEdge : edgesUnion) {
					if (selectedEdge.getName().equals(elementsRuleList.getSelection()[0])) {
						identifiedEdges.add(selectedEdge);
						identifiedElementsList.add(elementsRuleList.getSelection()[0]);
						elementsRuleList.remove(elementsRuleList.getSelection()[0]);
						break;
					}
				}
			}
			if (identifiedElementsList.getItemCount()>1) {
				saveIdentificationButton.setEnabled(true);
			}
			addElementButton.setEnabled(false);
		}
		
		if (event.widget == saveIdentificationButton) {
			//TODO Test this properly when we have more combinations!
			Identification identification = new Identification(selectedCombination);
			identification.setIdentifiedNodes(identifiedNodes);
			identification.setIdentifiedEdges(identifiedEdges);
			identification.constructIdentificationNodeName();
			identification.constructIdentificationEdgeName();
			identification.constructIdentificationNodeFinalName();
			identification.constructIdentificationEdgeFinalName();
			combinationIdentificationsList.removeAll();
			selectedCombination.addElementIndentification(identification);
			
			for (Combination combination : mainMcmtModuleHierarchyRules.getCombinations()) {
				combination.constructCombinationPlusIdentificationsName();
				combinationIdentificationsList.add(combination.getCombinationPlusIdentificationsName());
			}

			combinationIdentificationsList.redraw();
			identifiedNodes.clear();
			identifiedEdges.clear();
			identifiedElementsList.redraw();
			identifiedElementsList.removeAll();
			mainMcmtModuleHierarchyRules.detectConflict();
			setPageComplete(true);
			
		}

	}

}
