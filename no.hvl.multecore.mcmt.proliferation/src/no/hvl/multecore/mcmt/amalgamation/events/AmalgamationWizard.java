package no.hvl.multecore.mcmt.amalgamation.events;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.wizard.Wizard;

import no.hvl.multecore.common.exceptions.DuplicatedElement;
import no.hvl.multecore.common.exceptions.ElementNotExists;
import no.hvl.multecore.common.exceptions.InvalidSupplementaryElement;
import no.hvl.multecore.common.exceptions.MultEcoreException;
import no.hvl.multecore.mcmt.amalgamation.McmtModuleHierarchyRules;

public class AmalgamationWizard extends Wizard {
	
	protected SelectionHierarchiesPage selectionHierarchiesPage;
    protected SelectionRulesPage selectionRulesPage;
    protected SelectionIdentificationPage selectionIdentificationPage;
    protected ConflictAndAmalgamationComputationPage conflictAndAmalgamationComputationPage;
    
    //This controls: the MCMT project name that was right clicked, its corresponding main hierarchy and its rules.
    protected McmtModuleHierarchyRules mainHierarchy;
    
    //Shared data object to pass from the first page to the second one, the hierarchies selected to be combined
    protected List<String> selectedHierarchiesPageOne;

    public AmalgamationWizard() {
        super();
        setNeedsProgressMonitor(true);
        this.mainHierarchy = new McmtModuleHierarchyRules();
        this.selectedHierarchiesPageOne = new ArrayList<String>();
    }
    
    public AmalgamationWizard(McmtModuleHierarchyRules mainHierarchy) {
        super();
        setNeedsProgressMonitor(true);
        this.mainHierarchy = mainHierarchy;
        this.selectedHierarchiesPageOne = new ArrayList<String>();
    }

	
	@Override
	public void addPages() {
		selectionHierarchiesPage = new SelectionHierarchiesPage(mainHierarchy, selectedHierarchiesPageOne);
		addPage(this.selectionHierarchiesPage);
		selectionRulesPage = new SelectionRulesPage(mainHierarchy, selectedHierarchiesPageOne);
		addPage(this.selectionRulesPage);
		selectionIdentificationPage = new SelectionIdentificationPage(mainHierarchy);
		addPage(this.selectionIdentificationPage);
		conflictAndAmalgamationComputationPage = new ConflictAndAmalgamationComputationPage(mainHierarchy);
		addPage(this.conflictAndAmalgamationComputationPage);		
	}

//	@Override
//	public boolean canFinish() {
//		return false;
//	}
//
//	@Override
//	public void createPageControls(Composite pageContainer) {
//
//	}
//
//	@Override
//	public void dispose() {
//
//	}
//
//	@Override
//	public IWizardContainer getContainer() {
//		return null;
//	}
//
//	@Override
//	public Image getDefaultPageImage() {
//		return null;
//	}
//
//	@Override
//	public IDialogSettings getDialogSettings() {
//		return null;
//	}
//
//	@Override
//	public IWizardPage getNextPage(IWizardPage page) {
//		return null;
//	}
//
//	@Override
//	public IWizardPage getPage(String pageName) {
//		return null;
//	}
//
//	@Override
//	public int getPageCount() {
//		return 0;
//	}
//
//	@Override
//	public IWizardPage[] getPages() {
//		return null;
//	}
//
//	@Override
//	public IWizardPage getPreviousPage(IWizardPage page) {
//		return null;
//	}
//
//	@Override
//	public IWizardPage getStartingPage() {
//		return null;
//	}
//
//	@Override
//	public RGB getTitleBarColor() {
//		return null;
//	}

	@Override
	public String getWindowTitle() {
		
		return "Amalgamation Wizard";
	}

//	@Override
//	public boolean isHelpAvailable() {
//		return false;
//	}
//
//	@Override
//	public boolean needsPreviousAndNextButtons() {
//		return false;
//	}
//
//	@Override
//	public boolean needsProgressMonitor() {
//		return false;
//	}
//
//	@Override
//	public boolean performCancel() {
//		return false;
//	}

	@Override
	public boolean performFinish() {
	//	System.out.println(selectionHierarchiesPage.getText1());
	//	System.out.println(selectionRulesPage.getText1());
		mainHierarchy.computeAmalgamationRules();
		mainHierarchy = null;
		return true;
	}

//	@Override
//	public void setContainer(IWizardContainer wizardContainer) {
//
//	}

}
