package no.hvl.multecore.mcmt.amalgamation.events;

import java.util.NoSuchElementException;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;

import no.hvl.multecore.common.Utils;
import no.hvl.multecore.mcmt.amalgamation.AmalgamationManager;
import no.hvl.multecore.mcmt.amalgamation.McmtModuleHierarchyRules;

public class SelectHierarchiesToAmalgamateHandler implements IObjectActionDelegate {

	Shell shell;
	String PROJECT_EXPLORER_URI = "org.eclipse.ui.navigator.ProjectExplorer";
	
	@Override
	public void run(IAction action) {
		//This Map relates each multilevel hierarchy with its set of rules
		//Note that each mcmt file needs to be "saved" for the association to be made
		Set<McmtModuleHierarchyRules> modulesHierarchiesWithAssociatedRules = AmalgamationManager.instance().getMcmtModuleHierarchyRules();
		
		//There should be a main-supps relation between the hierarchies that the user can select and the right clicked project (mcmt of main).
		//The mcmt file has the "module" attribute which is the name of the mcmt project

		//First we get the project right clicked
		IWorkbenchPage workbenchPage = Utils.getWindow().getActivePage();
		IProject project = ((IResource) ((TreeSelection) workbenchPage
				.getSelection(PROJECT_EXPLORER_URI)).getFirstElement()).getProject();
		//Its name should correspond to one of the "module" attribute from the Set of "McmtModuleHierarchyRules"
		try {
			McmtModuleHierarchyRules mcmtModuleHierarchyRule = modulesHierarchiesWithAssociatedRules.stream()
					.filter((McmtModuleHierarchyRules element) -> element.getModuleName().equals(project.getName()))
					.findFirst().get();
			if (mcmtModuleHierarchyRule != null) {
				WizardDialog dialog = new WizardDialog(shell, new AmalgamationWizard(mcmtModuleHierarchyRule));
				dialog.open();
			}
			
		} catch (NoSuchElementException e) {
			Utils.showPopup("Wrong project selection", "You did not selected a valid project. Make sure the selected project corresponds to the one containing the MCMT rules of the main hierarchy");
		}
		
		//Once we have it, we need to open the wizard, asking to the user if the showed projects are correct prior to amalgamate their rules
		
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		
	}

}
