package no.hvl.multecore.mcmt.amalgamation.events;


import java.util.ArrayList;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;

import no.hvl.multecore.common.hierarchy.MultilevelHierarchy;
import no.hvl.multecore.mcmt.amalgamation.AmalgamationManager;
import no.hvl.multecore.mcmt.amalgamation.McmtModuleHierarchyRules;

public class SelectionHierarchiesPage extends WizardPage implements Listener{
	
//    private Text text1;
    private Composite container;
    private McmtModuleHierarchyRules mainMcmtModuleHierarchyRules;
	private List unselectedHierarchiesList;
	private List selectedHierarchiesList;
	private Button addButton;
	private Button removeButton;
	private ArrayList<String> selectedHierarchiesNames;
	

    public SelectionHierarchiesPage() {
        super("First Page");
        setTitle("Confirm that the following hierarchies contain rules that are about to be amalgamated");
        setDescription("");
        mainMcmtModuleHierarchyRules = new McmtModuleHierarchyRules();
    }

    public SelectionHierarchiesPage(McmtModuleHierarchyRules mainHierarchy, java.util.List<String> selectedHierarchiesPageOne) {
        super("First Page");
        setTitle("Confirm that the following hierarchies contain rules that are about to be amalgamated");
        setDescription("");
    	this.mainMcmtModuleHierarchyRules = mainHierarchy;
    	this.selectedHierarchiesNames = (ArrayList<String>) selectedHierarchiesPageOne;
	}

	@Override
    public void createControl(Composite parent) {
		setErrorMessage("At least two or more hierarchies must be selected");
        container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        layout.numColumns = 3;
        container.setLayout(layout);
        
        //First label for unselected hierarchies
        Label label1 = new Label(container, SWT.NONE);
        label1.setText("Unselected hierarchies:");

        //Add button with listener for the semantics when it is clicked
        addButton = new Button(container, SWT.PUSH);
        addButton.setText("Add");
        addButton.setAlignment(SWT.CENTER);
        addButton.setEnabled(false);
        addButton.addListener(SWT.Selection, this);
        
      //Second label for selected hierarchies
        Label label2 = new Label(container, SWT.NONE);
        label2.setText("Selected hierarchies:");
        
        // Unselected Hierarchies and its grid specification
        unselectedHierarchiesList = new List(container, SWT.BORDER | SWT.MULTI );
        GridData unselectedHierarchyGrid = new GridData();
		unselectedHierarchyGrid.horizontalAlignment = GridData.BEGINNING;
		unselectedHierarchyGrid.heightHint = 100;
		unselectedHierarchyGrid.widthHint = 500;
		unselectedHierarchyGrid.grabExcessVerticalSpace = true;
		unselectedHierarchyGrid.grabExcessHorizontalSpace = true;
        unselectedHierarchiesList.setLayoutData(unselectedHierarchyGrid);
        
        //We add the main multilevel hierarchy to display it at first of our list of hierarchies
        unselectedHierarchiesList.add(mainMcmtModuleHierarchyRules.getMultilevelHierarchy().getName()+ " (main)");
        //Then we iterate over its supplementary hierarchies and verify that it exist in our Mananager, meaning that the rules have been processed.
		for (MultilevelHierarchy suppHierarchy : mainMcmtModuleHierarchyRules.getMultilevelHierarchy()
				.getSupplementaryHierarchies()) {
			for (McmtModuleHierarchyRules suppMcmtModuleHierarchyRule : AmalgamationManager.instance()
					.getMcmtModuleHierarchyRules()) {
				if (suppMcmtModuleHierarchyRule.getMultilevelHierarchy() == suppHierarchy) {
					unselectedHierarchiesList.add(suppHierarchy.getName() + " (Supplementary)");
				}
			}
		}
		unselectedHierarchiesList.addListener(SWT.Selection, this);
		
        removeButton = new Button(container, SWT.PUSH);
        removeButton.setText("Remove");
        removeButton.setAlignment(SWT.CENTER);
        removeButton.setEnabled(false);
        removeButton.addListener(SWT.Selection, this);
		
		// Selected Hierarchies and its grid specification
        selectedHierarchiesList = new List(container, SWT.BORDER | SWT.MULTI );
        GridData selectedHierarchyGrid = new GridData();
        selectedHierarchyGrid.horizontalAlignment = GridData.BEGINNING;
        selectedHierarchyGrid.widthHint = 500;
        selectedHierarchyGrid.heightHint  =100;
        selectedHierarchyGrid.grabExcessVerticalSpace = true;
        selectedHierarchyGrid.grabExcessHorizontalSpace = true;
        selectedHierarchiesList.setLayoutData(selectedHierarchyGrid);
		selectedHierarchiesList.addListener(SWT.Selection, this);
		

//        hierarchiesList.pack(true);
//        hierarchiesList.computeSize(SWT.DEFAULT, 4);
//        hierarchiesList.setVisible(true);
//    	container.setVisible(true);
        setControl(container);
        setPageComplete(false);
    }
	

	//Events handler, each widget is managed separately in each if condition.
	@Override
	public void handleEvent(Event event) {
		if (event.widget == unselectedHierarchiesList) {
				addButton.setEnabled(true);
		}
		if (event.widget == addButton) {
			String[]  selected = unselectedHierarchiesList.getSelection();
			for (int i = 0; i < selected.length; i++) {
				selectedHierarchiesList.add(selected[i]);	
				unselectedHierarchiesList.remove(selected [i]);
			}
			removeButton.setEnabled(true);
		}
		if (event.widget == removeButton) {
			String[]  selected = selectedHierarchiesList.getSelection();
			for (int i = 0; i < selected.length; i++) {
				unselectedHierarchiesList.add(selected[i]);	
				selectedHierarchiesList.remove(selected [i]);
			}
			if (selectedHierarchiesList.getItemCount() == 0) {
				removeButton.setEnabled(false);
			}
		}
		if (event.widget == selectedHierarchiesList) {
			removeButton.setEnabled(true);
			if (selectedHierarchiesList.getItemCount() >= 2){
				for (int i = 0; i < selectedHierarchiesList.getItems().length; i++) {
					if (!this.selectedHierarchiesNames.contains(selectedHierarchiesList.getItem(i))) {
						this.selectedHierarchiesNames.add(selectedHierarchiesList.getItem(i));
					}
				}
				setErrorMessage(null);
				setPageComplete(true);
			}
			else {
				setErrorMessage("At least two or more hierarchies must be selected");
			}
		}
	}
}
