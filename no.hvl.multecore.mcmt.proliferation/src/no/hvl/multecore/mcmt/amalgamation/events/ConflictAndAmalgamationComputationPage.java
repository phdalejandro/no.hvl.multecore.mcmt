package no.hvl.multecore.mcmt.amalgamation.events;

import java.util.HashSet;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;

import no.hvl.multecore.common.hierarchy.IEdge;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.mcmt.amalgamation.Combination;
import no.hvl.multecore.mcmt.amalgamation.McmtModuleHierarchyRules;
import no.hvl.multecore.mcmt.proliferation.rule.RuleHierarchy;

public class ConflictAndAmalgamationComputationPage extends WizardPage implements Listener {
    
	
	private McmtModuleHierarchyRules mainMcmtModuleHierarchyRules;
    private Composite container;
    private List combinationRulesConflictsList;
    private List individualRulesToPrioritiseList;
    private List combinationsToBeAmalgamatedList; 
    private Button selectConflictCombination;
    private Button selectRulePrioritisedCombination;
    Combination selectedCombination;
    RuleHierarchy selectedPrioritisedRule;
    
	public ConflictAndAmalgamationComputationPage() {
        super("Fourth Page");
        setTitle("Solve the conflicts by prioritising one of the rules");
        setDescription("");
        mainMcmtModuleHierarchyRules = new McmtModuleHierarchyRules();
	}
	
	public ConflictAndAmalgamationComputationPage(McmtModuleHierarchyRules mainHierarchy) {
        super("Fourth Page");
        setTitle("Solve the conflicts by prioritising one of the rules");
        setDescription("");
    	this.mainMcmtModuleHierarchyRules = mainHierarchy;
	}	
	
	@Override
	public void setVisible(final boolean visible) {
		if (visible) {
			selectedCombination = null;
			selectedPrioritisedRule = null;
			combinationRulesConflictsList.removeAll();
			individualRulesToPrioritiseList.removeAll();
			combinationsToBeAmalgamatedList.removeAll();
			for (Combination combination : mainMcmtModuleHierarchyRules.getCombinations()) {
				if (combination.getConflict()){
					combinationRulesConflictsList.add(combination.getCombinationNameToDisplay());
				}
			}
			for (Combination combination : mainMcmtModuleHierarchyRules.getCombinations()) {
				if (!combination.getConflict()){
					combinationsToBeAmalgamatedList.add(combination.getCombinationNameToDisplay());
				}
			}			
		}
		if (combinationRulesConflictsList.getItemCount() == 0) {
	        setPageComplete(true);
		}
		super.setVisible(visible);
	}	

	@Override
	public void createControl(Composite parent) {
        container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
        layout.numColumns = 4;
        container.setLayout(layout);
		
        //First label for list of Rule combinations with conflicts
        Label label1 = new Label(container, SWT.NONE);
        label1.setText("Combinations that have conflicts:");
        
        // Empty column
        Label emptyLabel = new Label(container, SWT.LEAD);
        
        //Second label for individual Rules
        Label label2 = new Label(container, SWT.NONE);
        label2.setText("Select the rule that is getting prioritised:");     
        
        // Empty column
        Label emptyLabel2 = new Label(container, SWT.LEAD);
        
        // Combinations with conflicts list
        combinationRulesConflictsList = new List(container, SWT.BORDER | SWT.SINGLE );
        GridData combinationRulesConflictsGrid = new GridData();
        combinationRulesConflictsGrid.horizontalAlignment = GridData.BEGINNING;
        combinationRulesConflictsGrid.heightHint = 100;
        combinationRulesConflictsGrid.widthHint = 400;
        combinationRulesConflictsGrid.grabExcessVerticalSpace = true;
        combinationRulesConflictsGrid.grabExcessHorizontalSpace = true;
        combinationRulesConflictsList.setLayoutData(combinationRulesConflictsGrid);
        combinationRulesConflictsList.addListener(SWT.Selection, this);            
        
        selectConflictCombination = new Button(container, SWT.PUSH);
        selectConflictCombination.setText("Select");
        selectConflictCombination.setAlignment(SWT.CENTER);
        selectConflictCombination.setEnabled(false);
        selectConflictCombination.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        selectConflictCombination.addListener(SWT.Selection, this);  
      
        // Combinations with conflicts list
        individualRulesToPrioritiseList = new List(container, SWT.BORDER | SWT.SINGLE );
        GridData individualRulesToPrioritiseGrid = new GridData();
        individualRulesToPrioritiseGrid.horizontalAlignment = GridData.BEGINNING;
        individualRulesToPrioritiseGrid.heightHint = 100;
        individualRulesToPrioritiseGrid.widthHint = 400;
        individualRulesToPrioritiseGrid.grabExcessVerticalSpace = true;
        individualRulesToPrioritiseGrid.grabExcessHorizontalSpace = true;
        individualRulesToPrioritiseList.setLayoutData(individualRulesToPrioritiseGrid);
        individualRulesToPrioritiseList.addListener(SWT.Selection, this);  
        
        selectRulePrioritisedCombination = new Button(container, SWT.PUSH);
        selectRulePrioritisedCombination.setText("Select");
        selectRulePrioritisedCombination.setAlignment(SWT.CENTER);
        selectRulePrioritisedCombination.setEnabled(false);
        selectRulePrioritisedCombination.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        selectRulePrioritisedCombination.addListener(SWT.Selection, this);
        
        //First label for list of Rule combinations
        Label label3 = new Label(container, SWT.NONE);
        label3.setText("Combinations to be produced:");  

        // Empty column
        Label emptyLabel3 = new Label(container, SWT.LEAD);       
 
        // Empty column
        Label emptyLabel4 = new Label(container, SWT.LEAD);      
        
        // Empty column
        Label emptyLabel5 = new Label(container, SWT.LEAD);
        
        // Combinations with conflicts list
        combinationsToBeAmalgamatedList = new List(container, SWT.BORDER | SWT.SINGLE );
        GridData combinationsToBeAmalgamatedGrid = new GridData();
        combinationsToBeAmalgamatedGrid.horizontalAlignment = GridData.BEGINNING;
        combinationsToBeAmalgamatedGrid.heightHint = 100;
        combinationsToBeAmalgamatedGrid.widthHint = 400;
        combinationsToBeAmalgamatedGrid.grabExcessVerticalSpace = true;
        combinationsToBeAmalgamatedGrid.grabExcessHorizontalSpace = true;
        combinationsToBeAmalgamatedList.setLayoutData(combinationsToBeAmalgamatedGrid);
        combinationsToBeAmalgamatedList.addListener(SWT.Selection, this);         
        
        setControl(container);
        setPageComplete(false);

	}

	@Override
	public void handleEvent(Event event) {
		if (event.widget == combinationRulesConflictsList) {
			if (combinationRulesConflictsList.getSelection().length == 1) {
				individualRulesToPrioritiseList.removeAll();
				selectConflictCombination.setEnabled(true);
			}
		}		
		if (event.widget == selectConflictCombination) {
			individualRulesToPrioritiseList.removeAll();
			for (Combination combination : this.mainMcmtModuleHierarchyRules.getCombinations()) {
				if (combination.getCombinationNameToDisplay().equals(combinationRulesConflictsList.getSelection()[0])) {
					for (RuleHierarchy rule : combination.getRulesCombined()) {
						individualRulesToPrioritiseList.add(rule.getName());
					}
					selectedCombination = combination;
					
				}
			}
			combinationRulesConflictsList.remove(combinationRulesConflictsList.getSelection()[0]);
			selectConflictCombination.setEnabled(false);
		}
		if (event.widget == individualRulesToPrioritiseList) {
			if (individualRulesToPrioritiseList.getSelection().length == 1) {
				selectRulePrioritisedCombination.setEnabled(true);
			}
		}
		if (event.widget == selectRulePrioritisedCombination) {
			for (RuleHierarchy ruleHierarchy : selectedCombination.getRulesCombined()) {
				if (ruleHierarchy.getName().equals(individualRulesToPrioritiseList.getSelection()[0])) {
					selectedPrioritisedRule = ruleHierarchy;
					selectedCombination.setPrioritisedSelectedRule(selectedPrioritisedRule);
					selectedCombination.constructCombinationWithSelectedPrioritisationName();
					combinationsToBeAmalgamatedList.add(selectedCombination.getCombinationWithSelectedPrioritisationName());
					combinationsToBeAmalgamatedList.redraw();
				}
			}
			
			individualRulesToPrioritiseList.redraw();
			individualRulesToPrioritiseList.removeAll();
			selectConflictCombination.setEnabled(false);
			
			if (combinationRulesConflictsList.getItemCount() == 0) {
		        setPageComplete(true);
			}
		}		
	}

}
