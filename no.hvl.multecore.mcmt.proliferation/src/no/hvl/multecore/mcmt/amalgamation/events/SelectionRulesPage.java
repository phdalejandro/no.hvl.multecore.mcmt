package no.hvl.multecore.mcmt.amalgamation.events;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import no.hvl.multecore.common.hierarchy.MultilevelHierarchy;
import no.hvl.multecore.mcmt.amalgamation.AmalgamationManager;
import no.hvl.multecore.mcmt.amalgamation.Combination;
import no.hvl.multecore.mcmt.amalgamation.McmtModuleHierarchyRules;
import no.hvl.multecore.mcmt.amalgamation.Utils;
import no.hvl.multecore.mcmt.proliferation.rule.RuleHierarchy;

public class SelectionRulesPage extends WizardPage implements Listener{
	
    private McmtModuleHierarchyRules mainMcmtModuleHierarchyRules;
    private Composite container;
	private ArrayList<String> selectedHierarchiesNames;
	private List hierarchiesList;
	private List mcmtsList;
	private Button addRuleButton;
	private List candidatesList;
	private Button combineButton;
	private List combinationsList;
	private Button removeButton;

	protected SelectionRulesPage() {
        super("Second Page");
        setTitle("Select the rules to be combined");
        setDescription("");
	}

    public SelectionRulesPage(McmtModuleHierarchyRules mainHierarchy,  java.util.List<String> selectedHierarchiesPageOne) {
        super("Second Page");
        setTitle("Select the rules to be combined");
        setDescription("");
    	this.mainMcmtModuleHierarchyRules = mainHierarchy;
    	this.selectedHierarchiesNames = (ArrayList<String>) selectedHierarchiesPageOne;
	}
    
	@Override
	public void setVisible(final boolean visible) {
		if (visible) {
			this.selectedHierarchiesNames = (ArrayList<String>)
					((AmalgamationWizard) getWizard()).selectedHierarchiesPageOne;
			hierarchiesList.removeAll();
			mcmtsList.removeAll();
			for (String string : selectedHierarchiesNames) {
				hierarchiesList.add(string);
			}
		}
		super.setVisible(visible);
	}


	
	@Override
    public void createControl(Composite parent) {
        container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
        layout.numColumns = 4;
        container.setLayout(layout);
        
        //First label for hierarchies
        Label label1 = new Label(container, SWT.NONE);
        label1.setText("Hierarchies:");

        // Empty column
        Label emptyLabel = new Label(container, SWT.LEAD);        
        
        //Second label for MCMTs
        Label label2 = new Label(container, SWT.NONE);
        label2.setText("MCMT rules:");	      
  
        // Empty column
        Label emptyLabel2 = new Label(container, SWT.LEAD);      
        
        // Unselected Hierarchies and its grid specification
        hierarchiesList = new List(container, SWT.BORDER | SWT.SINGLE );
        GridData hierarchiesGrid = new GridData();
        hierarchiesGrid.horizontalAlignment = GridData.BEGINNING;
        hierarchiesGrid.heightHint = 100;
        hierarchiesGrid.widthHint = 400;
        hierarchiesGrid.grabExcessVerticalSpace = true;
        hierarchiesGrid.grabExcessHorizontalSpace = true;
        hierarchiesList.setLayoutData(hierarchiesGrid);
        hierarchiesList.addListener(SWT.Selection, this);

        // Empty column
        Label emptyLabel3 = new Label(container, SWT.LEAD);      		
		
        // MCMT rules of the selected multilevel hierarchy
        mcmtsList = new List(container, SWT.BORDER | SWT.SINGLE );
        GridData mcmtsGrid = new GridData();
        mcmtsGrid.horizontalAlignment = GridData.BEGINNING;
        mcmtsGrid.heightHint = 100;
        mcmtsGrid.widthHint = 400;
        mcmtsGrid.grabExcessVerticalSpace = true;
        mcmtsGrid.grabExcessHorizontalSpace = true;
        mcmtsList.setLayoutData(mcmtsGrid);		       
        mcmtsList.addListener(SWT.Selection, this);
       
        addRuleButton = new Button(container, SWT.PUSH);
        addRuleButton.setText("Add Rule");
        addRuleButton.setAlignment(SWT.CENTER);
        addRuleButton.setEnabled(false);
        addRuleButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        addRuleButton.addListener(SWT.Selection, this);       
        
        //First label for hierarchies
        Label label3 = new Label(container, SWT.NONE);
        label3.setText("Rule candidates:");
        
        // Empty column
        Label emptyLabel4 = new Label(container, SWT.LEAD);       
        
        //Second label for MCMTs
        Label label4 = new Label(container, SWT.NONE);
        label4.setText("Combinations selected:");	           
 
        // Empty column
        Label emptyLabel5 = new Label(container, SWT.LEAD);      
        
        candidatesList = new List(container, SWT.BORDER | SWT.NONE );
        GridData candidatesGrid = new GridData();
        candidatesGrid.horizontalAlignment = GridData.BEGINNING;
        candidatesGrid.heightHint = 100;
        candidatesGrid.widthHint = 400;
        candidatesGrid.grabExcessVerticalSpace = true;
        candidatesGrid.grabExcessHorizontalSpace = true;
        candidatesList.setLayoutData(candidatesGrid);
        //hierarchiesList.addListener(SWT.Selection, this);        
 
        combineButton = new Button(container, SWT.PUSH);
        combineButton.setText("Combine");
        combineButton.setAlignment(SWT.CENTER);
        combineButton.setEnabled(false);
        combineButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        combineButton.addListener(SWT.Selection, this);             

        combinationsList = new List(container, SWT.BORDER | SWT.SINGLE );
        GridData combinationsGrid = new GridData();
        combinationsGrid.horizontalAlignment = GridData.BEGINNING;
        combinationsGrid.heightHint = 100;
        combinationsGrid.widthHint = 400;
        combinationsGrid.grabExcessVerticalSpace = true;
        combinationsGrid.grabExcessHorizontalSpace = true;
        combinationsList.setLayoutData(combinationsGrid);        

        removeButton = new Button(container, SWT.PUSH);
        removeButton.setText("Remove");
        removeButton.setAlignment(SWT.CENTER);
        removeButton.setEnabled(false);
        removeButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));

        //combineButton.addListener(SWT.Selection, this);              
        
        setControl(container);
        setPageComplete(false);
    }

	@Override
	public void handleEvent(Event event) {
		if (event.widget == hierarchiesList) {
			if (hierarchiesList.getSelection().length == 1) {
				mcmtsList.removeAll();
				Set<RuleHierarchy> mcmts = getRulesFromHierarchy(hierarchiesList.getSelection()[0]);
				if (mcmts != null) {
					for (RuleHierarchy ruleHierarchy : mcmts) {
						mcmtsList.add(ruleHierarchy.getName());
					}
				}
			}
		}
		if (event.widget == mcmtsList) {
			if (mcmtsList.getSelection().length == 1) {
				addRuleButton.setEnabled(true);
			}
		}
		if (event.widget == addRuleButton) {
			addRuleButton.setEnabled(false);
			//TODO Fix in the future, it should not include any other rule from the same hierarchy, not only a rule itself
			boolean exists = false;
			for (int i = 0; i < candidatesList.getItems().length; i++) {
				if (candidatesList.getItem(i).equals(mcmtsList.getSelection()[0])) {
					exists = true;
				}
			}
			if (!exists) {
				candidatesList.add(mcmtsList.getSelection()[0]);
			}
			mcmtsList.remove(mcmtsList.getSelection()[0]);
			mcmtsList.redraw();
			candidatesList.redraw();
			if (candidatesList.getItems().length > 1) {
				combineButton.setEnabled(true);
			}
		}
		if (event.widget == combineButton) {
			//TODO Add control to disallow same combinations
			
			Combination combination = findRuleHierarchiesFromCandidatesList();
			combinationsList.add(combination.getCombinationNameToDisplay());
			candidatesList.removeAll();
			candidatesList.redraw();
	        setPageComplete(true);
		}
	}
	
	private Set<RuleHierarchy> getRulesFromHierarchy (String hierarchyName) {
		Set<RuleHierarchy> mcmts = null;
		for (McmtModuleHierarchyRules mcmtModuleHierarchyRules : AmalgamationManager.instance().getMcmtModuleHierarchyRules()) {
			String cleanHierarchyName = Utils.cleanNameHierarchy(hierarchyName);
			if (mcmtModuleHierarchyRules.getMultilevelHierarchy().getName().equals(cleanHierarchyName)) {
				mcmts = mcmtModuleHierarchyRules.getHierarchyRules();
			}
		}
		return mcmts;
	}
	
	private Combination findRuleHierarchiesFromCandidatesList (){
		Combination combination = new Combination();
		String combinedNameToDisplay = "";
		for (int i = 0; i < candidatesList.getItems().length; i++) {
			//Run over the rules to find the one that corresponds via name (note that this might cause conflict in case of a rule
			//from the main hierarchy has the same name than a rule from a supplementary hierarchy.
			//Run over the rules of the main hierarchy 
			for (RuleHierarchy ruleHierarchy : this.mainMcmtModuleHierarchyRules.getHierarchyRules()) {
				if (ruleHierarchy.getName().equals(candidatesList.getItem(i))) {
					combination.addRulesCombined(ruleHierarchy);
					combinedNameToDisplay += candidatesList.getItem(i) + " + ";
				}
			}
			//Run over each supp hierarchy and its corresponding rules
			for (MultilevelHierarchy supplementaryHierarchy : this.mainMcmtModuleHierarchyRules.getMultilevelHierarchy().getSupplementaryHierarchies()) {
				Set<RuleHierarchy> rulesFromHierarchy = getRulesFromHierarchy(supplementaryHierarchy.getName());
				if (null != rulesFromHierarchy){
					for (RuleHierarchy ruleHierarchy : rulesFromHierarchy) {
						if (ruleHierarchy.getName().equals(candidatesList.getItem(i))) {
							combination.addRulesCombined(ruleHierarchy);
							combinedNameToDisplay += candidatesList.getItem(i) + " + ";						
						}
					}
				}
			}			
		}
		combinedNameToDisplay = combinedNameToDisplay.substring(0, combinedNameToDisplay.length()-3);
		combination.setCombinationNameToDisplay(combinedNameToDisplay);
		if (!this.mainMcmtModuleHierarchyRules.getCombinations().contains(combination)){
			this.mainMcmtModuleHierarchyRules.addCombination(combination);
		}
		return combination;
	}
}
