package no.hvl.multecore.mcmt.amalgamation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import no.hvl.multecore.common.Debugger;
import no.hvl.multecore.common.exceptions.InvalidSupplementaryElement;
import no.hvl.multecore.common.exceptions.MultEcoreException;
import no.hvl.multecore.common.hierarchy.DeclaredAttribute;
import no.hvl.multecore.common.hierarchy.IEdge;
import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.common.hierarchy.InstantiatedAttribute;
import no.hvl.multecore.common.hierarchy.MultilevelHierarchy;
import no.hvl.multecore.core.events.LogView;
import no.hvl.multecore.mcmt.proliferation.Constants;
import no.hvl.multecore.mcmt.proliferation.rule.EdgeConstant;
import no.hvl.multecore.mcmt.proliferation.rule.EdgeVariable;
import no.hvl.multecore.mcmt.proliferation.rule.NodeConstant;
import no.hvl.multecore.mcmt.proliferation.rule.NodeVariable;
import no.hvl.multecore.mcmt.proliferation.rule.RuleDeclaredAttribute;
import no.hvl.multecore.mcmt.proliferation.rule.RuleHierarchy;
import no.hvl.multecore.mcmt.proliferation.rule.RuleModel;

public class McmtModuleHierarchyRules {
	
	private String moduleName;
	
	private MultilevelHierarchy multilevelHierarchy;
	
	private Set<RuleHierarchy> hierarchyRules;
	
	private Set <Combination> combinations;
	
	private String ECLASS_ID = no.hvl.multecore.common.Constants.ECLASS_ID;
	
	private String EREFERENCE_ID = no.hvl.multecore.common.Constants.EREFERENCE_ID;
	
	public McmtModuleHierarchyRules () {
		hierarchyRules = new HashSet<RuleHierarchy>();
		combinations = new HashSet<Combination>();
	}
	
	public void setModuleName (String moduleName) {
		this.moduleName = moduleName;
	}
	
	public String getModuleName () {
		return this.moduleName;
	}
	
	
	public MultilevelHierarchy getMultilevelHierarchy() {
		return multilevelHierarchy;
	}

	public void setMultilevelHierarchy(MultilevelHierarchy multilevelHierarchy) {
		this.multilevelHierarchy = multilevelHierarchy;
	}

	public void setHierarchyRules (HashMap<String,RuleHierarchy> _rules) {
		this.hierarchyRules.addAll(_rules.values());
	}
	
	public  Set<RuleHierarchy> getHierarchyRules (){
		return this.hierarchyRules;
	}
	
	public void addCombination (Combination combination) {
		this.combinations.add(combination);
	}
	
    public Set<Combination> getCombinations() {
		return combinations;
	}

	public void detectConflict () {
		//A conflict might appear when:
		//An identified element (existing in I_0) is removed in one rule but not in the other.
		//Removed means it is in its corresponding I (and L, from) but not in R (to).
		
		//We run over each combination
		//The algorithm for now is in some parts taking into account only combinations of pairs of rules.
		for (Combination combination : combinations) {
			//We run over each identification
			for (Identification identification : combination.getElementIdentifications()) {
				//We first run over the identified nodes
				int counter = 0;
				for (INode node : identification.getIdentifiedNodes()) {
					//We get its rule hierarchy to see if it is in from/to models
					RuleHierarchy ruleHierarchy = (RuleHierarchy) node.getModel().getHierarchy();
					//If it is in LHS but not in RHS it has been removed
					if (ruleHierarchy.getFromModel().getNodes().contains(node) && !ruleHierarchy.getToModel().getNodes().contains(node)) {
						counter ++;
					}
				}
				if (counter % 2 != 0) {
					combination.setConflict(true);
				}
				counter = 0;
				//Then we run over the identified edges
				for (IEdge edge : identification.getIdentifiedEdges()) {
					//We get its rule hierarchy to see if it is in from/to models
					RuleHierarchy ruleHierarchy = (RuleHierarchy) edge.getModel().getHierarchy();
					if (ruleHierarchy.getFromModel().getEdges().contains(edge) && !ruleHierarchy.getToModel().getEdges().contains(edge)) {
						counter ++;
					}
				}				
				if (counter % 2 != 0) {
					combination.setConflict(true);
				}
				counter = 0;
			}
		}
	}
	
	public void computeAmalgamationRules () {
		for (Combination combination : combinations) {
			RuleHierarchy mainRuleHierarchy = null;
			Set<RuleHierarchy> supplementaryHierarchies = new HashSet<RuleHierarchy>();
			for (RuleHierarchy rule : combination.getRulesCombined()) {
				if (this.hierarchyRules.contains(rule)) {
					mainRuleHierarchy = rule;
				}
				else {
					supplementaryHierarchies.add(rule);
				}
			}
			
			//We run over all the elements of the main hierarchy, if it has an identification on I, we add the supp types
			
			//First is the creation of the META(S) BLOCK
			
			//We need to create a new rule with the meta's containing all types from every hierarchy,
			//where they are properly distributed across the internal corresponding metas, ie, main + supp(s)
			//and the from/to as pushouts* where elements are correctly multi-typed when corresponds.
			RuleHierarchy amalgamatedRule = new RuleHierarchy(
					combination.constructAmalgamatedRuleHierarchyName(mainRuleHierarchy));
			
			//We create a new Rule Hierarchy per supplementary hierarchy participating in the combination
			//And add them as supplementary rule hierarchies of the main
			for (RuleHierarchy suppRuleHierarchy : supplementaryHierarchies) {
				RuleHierarchy amalgamatedSuppRule = new RuleHierarchy(suppRuleHierarchy.getName());
				amalgamatedRule.addSupplementaryHierarchy(amalgamatedSuppRule);
			}
			
			//Now we run over the meta-models of the mainRuleHierarchy and create the elements and the models on the amalgamated one
			//We need to sort them though, as a meta_3 might be processed before meta_2, and thus raising exceptions
			try {
				populateMetaModels (mainRuleHierarchy, amalgamatedRule);
			} catch (MultEcoreException e) {
				e.createNotificationDialog();
			}
			
			//Now we run over each supplementary hierarchy, for which each of the meta-models must be also created
			for (RuleHierarchy suppRuleHierarchy : supplementaryHierarchies) {
				RuleHierarchy newSuppRuleHierarchy = (RuleHierarchy) amalgamatedRule.getSupplementaryHierarchy(suppRuleHierarchy.getName());
				try {
					populateMetaModels (suppRuleHierarchy, newSuppRuleHierarchy);
				} catch (MultEcoreException e) {
					e.createNotificationDialog();
				}
			}
			
			
			//We need to calculate L_O. To do so, we run over L_A (main) and check whether the
			//elements are identified in I_0
			//We first run over the nodes for L_0
			for (INode mainRuleNode : mainRuleHierarchy.getFromModel().getNodes()) {
				Identification nodeIdentification = findIdentificationFromNodeName(combination.getElementIdentifications(), mainRuleNode.getName(), mainRuleNode.getType().getName());
				if (null != nodeIdentification) {
					combination.addLeftHandSideIndentification(nodeIdentification);
				}
			}
					
			//Then we run over the edges for I_0
			for (IEdge mainRuleEdge : mainRuleHierarchy.getFromModel().getEdges()) {
				Identification edgeIdentification = findIdentificationFromEdgeName(combination.getElementIdentifications(), mainRuleEdge.getName(), mainRuleEdge.getType().getName());
				if (null != edgeIdentification) {
					combination.addLeftHandSideIndentification(edgeIdentification);
				}
			}
			//The two loops above should produce L_0
			
			
			//Now we compute R_0 following the same logic
			//We first run over the nodes in R_A (main)
			for (INode mainRuleNode : mainRuleHierarchy.getToModel().getNodes()) {
				Identification nodeIdentification = findIdentificationFromNodeName(combination.getElementIdentifications(), mainRuleNode.getName(), mainRuleNode.getType().getName());
				if (nodeIdentification != null) {
					combination.addRightHandSideIndentification(nodeIdentification);
				}
			}
			
			//Then we run over the edges
			for (IEdge mainRuleEdge : mainRuleHierarchy.getToModel().getEdges()) {
				Identification edgeIdentification = findIdentificationFromEdgeName(combination.getElementIdentifications(), mainRuleEdge.getName(), mainRuleEdge.getType().getName());
				if (edgeIdentification != null) {
					combination.addRightHandSideIndentification(edgeIdentification);
				}
			}
			
			IModel bottomMetaModel = amalgamatedRule.getMetaModelInLevel(amalgamatedRule.getMaxLevel());
			//We create the fromModel;
			IModel newFromModel = amalgamatedRule.createModel(Constants.FROM_BLOCK_ID, bottomMetaModel.getName());
			//We create the toModel;
			IModel newToModel = amalgamatedRule.createModel(Constants.TO_BLOCK_ID, bottomMetaModel.getName());
			
			//We add as supplementary models the bottom-most meta models from the involved supplementary hierarchies:
			for (MultilevelHierarchy ruleHierarchy : amalgamatedRule.getSupplementaryHierarchies()) {
				IModel bottomModel = ((RuleHierarchy) ruleHierarchy).getMetaModelInLevel(ruleHierarchy.getMaxLevel());
				try {
					amalgamatedRule.addSupplementaryModel(newFromModel, ruleHierarchy.getName(), bottomModel.getName());
					amalgamatedRule.addSupplementaryModel(newToModel, ruleHierarchy.getName(), bottomModel.getName());
				} catch (MultEcoreException e) {
					e.createNotificationDialog();
				}
			}
			try {
				produceBottomModelAmalgamation (newFromModel, amalgamatedRule, mainRuleHierarchy, combination, false);
			} catch (MultEcoreException e) {
				e.createNotificationDialog();
			}
			
			boolean prioritySelectedRule = false;
			if (null != combination.getPrioritisedSelectedRule()) {
				prioritySelectedRule = combination.getPrioritisedSelectedRule().equals(mainRuleHierarchy);
			}
 			try {
 				produceBottomModelAmalgamation (newToModel, amalgamatedRule, mainRuleHierarchy, combination, prioritySelectedRule);
			} catch (MultEcoreException e) {
				e.createNotificationDialog();
			}
			

			
			LogView logView = (LogView) no.hvl.multecore.common.Utils.getWindow().getActivePage().findView(LogView.ID);
			if (null != logView) {
				no.hvl.multecore.core.Utils.addtoMultEcoreConsole(amalgamatedRule.toString());
			}			
		}
	}


	private void populateMetaModels(RuleHierarchy originalRuleHierarchy, RuleHierarchy newRuleHierarchy)
			throws MultEcoreException {
		List<IModel> sortedModels = originalRuleHierarchy.getFromModel().getMainMetamodel().getBranch(true);
		for (IModel iModel : sortedModels) {
			IModel newModel = newRuleHierarchy.createModel(iModel.getName(), iModel.getMainMetamodel().getName());
			for (INode iNode : iModel.getNodes()) {
				addElementToModel(iNode, newModel, newRuleHierarchy);
				for (DeclaredAttribute declaredAttribute : iNode.getDeclaredAttributes(false)) {
					addElementToModel(declaredAttribute, newModel, newRuleHierarchy);
				}
			}
			for (IEdge iEdge : iModel.getEdges()) {
				addElementToModel(iEdge, newModel, newRuleHierarchy);
			}
		}
	}

	private void addElementToModel (Object element, IModel newModel, RuleHierarchy newRuleHierarchy) throws MultEcoreException {
		if (element instanceof NodeConstant) {
			NodeConstant nodeConstantElement = (NodeConstant) element;
			newRuleHierarchy.addNodeConstant(nodeConstantElement.getName(), new HashSet<String>(), newModel);
		}
		else if (element instanceof NodeVariable) {
			NodeVariable nodeVariableElement = (NodeVariable) element;
			int typeReversePotency = no.hvl.multecore.common.Constants.TYPE_REVERSE_POTENCY_DEFAULT_VALUE;
			typeReversePotency = nodeVariableElement.getModel().getLevel() - nodeVariableElement.getType().getModel().getLevel();
			newRuleHierarchy.addNodeVariable(nodeVariableElement.getName(), nodeVariableElement.getType().getName(), typeReversePotency,
					new HashSet<String>(), null, newModel);
		}
		else if (element instanceof EdgeConstant) {
			EdgeConstant edgeConstantElement = (EdgeConstant) element;
			newRuleHierarchy.addEdgeConstant(edgeConstantElement.getName(), edgeConstantElement.getSource().getName(),
					(edgeConstantElement).getMultiplicityVariable(), newModel);
		}
		else if (element instanceof EdgeVariable) {
			int typeReversePotency = 1;
			EdgeVariable edgeVariableElement = (EdgeVariable) element;
			typeReversePotency = edgeVariableElement.getModel().getLevel() - edgeVariableElement.getType().getModel().getLevel();
			newRuleHierarchy.addEdgeVariable(edgeVariableElement.getName(), edgeVariableElement.getType().getName(),
					edgeVariableElement.getSource().getName(), edgeVariableElement.getSource().getType().getName(),
					edgeVariableElement.getTarget().getName(), edgeVariableElement.getTarget().getType().getName(), typeReversePotency,
					(edgeVariableElement).getMultiplicityVariable(), null, newModel);
		}
		else if (element instanceof RuleDeclaredAttribute) {
			RuleDeclaredAttribute ruleDeclaredAttributeElement = (RuleDeclaredAttribute) element;
			newRuleHierarchy.addMetaDeclaredAttribute(ruleDeclaredAttributeElement.getContainingNode().getName(),
					ruleDeclaredAttributeElement.getNameOrValue(),
					ruleDeclaredAttributeElement.getType().getNameOrValue(), newModel);
		}
	}

	private void produceBottomModelAmalgamation(IModel newModel, RuleHierarchy amalgamatedRule,
			RuleHierarchy mainRuleHierarchy, Combination combination, boolean priorityOnMain) throws MultEcoreException {
		
		boolean isFrom = (newModel.getName().equals(Constants.FROM_BLOCK_ID));
		//First: identified nodes
 		for (Identification identification : combination.getElementIdentifications()) {
			//Taking into account only nodes
			if (identification.getIdentifiedNodes().isEmpty()) continue;
			
			HashSet<INode> suppTypes = new HashSet<INode>();
			boolean addToModel = false;
			INode mainNodeType = amalgamatedRule.getRootModel().getNode(ECLASS_ID);
			for (INode identifiedNode : identification.getIdentifiedNodes()) {
				RuleHierarchy identifiedNodeRuleHierarchy = (RuleHierarchy) identifiedNode.getModel().getHierarchy();
				int typeLevel = identifiedNode.getType().getModel().getLevel();
				String typeName = identifiedNode.getType().getName();
				INode nodeType = null;
				IModel bottomModel = (isFrom) ? identifiedNodeRuleHierarchy.getFromModel() : identifiedNodeRuleHierarchy.getToModel();
				if (identifiedNodeRuleHierarchy == mainRuleHierarchy) {
					if (null != bottomModel.getNode(identifiedNode.getName())) {
						mainNodeType = amalgamatedRule.getMetaModelInLevel(typeLevel).getNode(typeName);
						addToModel = true;
					}
					//We have a conflict: we are in the TO model and the element has been removed in the main					
					else if (!isFrom && priorityOnMain) {
						for (INode conflictingNode : identification.getIdentifiedNodes()) {
							combination.addAmalgamatedNodeTo(conflictingNode, null);
						}
						addToModel = false;
						break;
					}
				}
				else {
					RuleHierarchy supplementaryRuleHierarchy = (RuleHierarchy) amalgamatedRule.getSupplementaryHierarchy(identifiedNodeRuleHierarchy.getName());
					if (null == bottomModel.getNode(identifiedNode.getName())) {
						
						//We have a conflict: we are in the TO model and the element has been removed in the supp					
						if (!isFrom && !priorityOnMain) {
							for (INode conflictingNode : identification.getIdentifiedNodes()) {
								combination.addAmalgamatedNodeTo(conflictingNode, null);
							}
							addToModel = false;
							break;
						}						
						nodeType = supplementaryRuleHierarchy.getRootModel().getNode(ECLASS_ID);
					}
					else {
						nodeType = supplementaryRuleHierarchy.getMetaModelInLevel(typeLevel).getNode(typeName); 
						addToModel = true;
					}
					suppTypes.add(nodeType);
				}
			}
			if (!addToModel) continue;
			
			//At this point we need to add an element in the new model
			String nodeName = identification.getIdentificationNodeFinalName();
			NodeVariable amalgamatedNode = amalgamatedRule.addNodeVariable(nodeName, mainNodeType, new HashSet<String>(), suppTypes, newModel);
			for (INode identifiedNode : identification.getIdentifiedNodes()) {
				IModel bottomModel = (isFrom) ? ((RuleHierarchy) identifiedNode.getModel().getHierarchy()).getFromModel() : ((RuleHierarchy) identifiedNode.getModel().getHierarchy()).getToModel();
				
				INode identifiedNodeInModel = bottomModel.getNode(identifiedNode.getName());
				if (null == identifiedNodeInModel) continue;
				
				if (isFrom)
					combination.addAmalgamatedNodeFrom(identifiedNodeInModel, amalgamatedNode);
				else 
					combination.addAmalgamatedNodeTo(identifiedNodeInModel, amalgamatedNode);
				for (InstantiatedAttribute instantiatedAttribute : identifiedNodeInModel.getInstantiatedAttributes()) {
					amalgamatedRule.addInstantiatedAttribute(instantiatedAttribute.getNameOrValue(), instantiatedAttribute.getType().getNameOrValue(), nodeName, newModel);
				}
			}
		}
		
		//Now we process identified edges
		for (Identification identification : combination.getElementIdentifications()) {
			if (identification.getIdentifiedEdges().isEmpty()) continue;
			
			HashSet<IEdge> suppTypes = new HashSet<IEdge>();
			boolean addToModel = false;	
			INode sourceNode = null, targetNode = null;
			IEdge mainEdgeType = amalgamatedRule.getRootModel().getEdge(EREFERENCE_ID, ECLASS_ID, ECLASS_ID);
			for (IEdge identifiedEdge : identification.getIdentifiedEdges()) {
				RuleHierarchy identifiedEdgeRuleHierarchy = (RuleHierarchy) identifiedEdge.getModel().getHierarchy();
				int typeLevel = identifiedEdge.getType().getModel().getLevel();
				String typeName = identifiedEdge.getType().getName();
				sourceNode = (isFrom) ? combination.getAmalgamatedNodeFrom(identifiedEdge.getSource()) : combination.getAmalgamatedNodeTo(identifiedEdge.getSource());
				targetNode = (isFrom) ? combination.getAmalgamatedNodeFrom(identifiedEdge.getTarget()) : combination.getAmalgamatedNodeTo(identifiedEdge.getTarget());
				//Here we perform the special minus operation, to avoid dangling edges
				if (null == sourceNode || null == targetNode) {
					for (IEdge conflictingEdge : identification.getIdentifiedEdges()) {
						combination.addAmalgamatedEdgeTo(conflictingEdge, null);
					}
					addToModel = false;
					break;
				}
				IEdge edgeType = null;
				
				RuleModel bottomModel = (isFrom) ? (RuleModel) identifiedEdgeRuleHierarchy.getFromModel() : (RuleModel) identifiedEdgeRuleHierarchy.getToModel();
				if (identifiedEdgeRuleHierarchy == mainRuleHierarchy) {
					RuleModel modelEdgeType = (RuleModel) amalgamatedRule.getMetaModelInLevel(typeLevel);
					if (null != bottomModel.getConstantEdge(identifiedEdge.getName(), identifiedEdge.getSource().getName())) {
						mainEdgeType = modelEdgeType.getConstantEdge(typeName, amalgamatedRule.getTypeInModel(sourceNode, modelEdgeType).getName());
						addToModel = true;
					}
					//We have a conflict: We are in the TO model and the element has been removed in the main
					else if (!isFrom && priorityOnMain) {
						for (IEdge conflictingEdge : identification.getIdentifiedEdges()) {
							combination.addAmalgamatedEdgeTo(conflictingEdge, null);
						}
						addToModel = false;
						break;
					}
				}
				else {
					RuleHierarchy supplementaryRuleHierarchy = (RuleHierarchy) amalgamatedRule.getSupplementaryHierarchy(identifiedEdgeRuleHierarchy.getName());
					if (null == bottomModel.getConstantEdge(identifiedEdge.getName(), identifiedEdge.getSource().getName())) {
						//We have a conflict: We are in the TO model and the element has been removed in the supp
						if (!isFrom && !priorityOnMain) {
							for (IEdge conflictingEdge : identification.getIdentifiedEdges()) {
								combination.addAmalgamatedEdgeTo(conflictingEdge, null);
							}
							addToModel = false;
							break;
						}						
						edgeType = supplementaryRuleHierarchy.getRootModel().getEdge(EREFERENCE_ID, ECLASS_ID, ECLASS_ID);
					}
					else {
						RuleModel modelEdgeType = (RuleModel) supplementaryRuleHierarchy.getMetaModelInLevel(typeLevel);
						edgeType = modelEdgeType.getConstantEdge(typeName, identifiedEdge.getType().getSource().getName());
						
						addToModel = true;
					}
					suppTypes.add(edgeType);
				}				
				
			}
			if (!addToModel) continue;			
			String edgeName = identification.getIdentificationEdgeFinalName();
			EdgeVariable amalgamatedEdge = amalgamatedRule.addEdgeVariable(edgeName, mainEdgeType, sourceNode, targetNode, null, suppTypes, newModel);
			for (IEdge identifiedEdge : identification.getIdentifiedEdges()) {
				if (isFrom)
					combination.addAmalgamatedEdgeFrom(identifiedEdge, amalgamatedEdge);
				else
					combination.addAmalgamatedEdgeTo(identifiedEdge, amalgamatedEdge);
			}
		}
		
		Set <RuleHierarchy> suppRules = new HashSet<RuleHierarchy> (combination.getRulesCombined());
		suppRules.remove(mainRuleHierarchy);
		for (RuleHierarchy ruleHierarchy : combination.getRulesCombined()) {
			//Nodes not identified
			IModel bottomModel = (isFrom) ? ruleHierarchy.getFromModel() : ruleHierarchy.getToModel();
			for (INode nonIdentifiedNode : bottomModel.getNodes()) {
				if (isFrom && combination.isNodeIdentifiedFrom(nonIdentifiedNode)) continue;
				
				if (!isFrom && combination.isNodeIdentifiedTo(nonIdentifiedNode)) continue;
				
				INode mainNodeType = amalgamatedRule.getRootModel().getNode(ECLASS_ID);
				HashSet<INode> suppTypes = new HashSet<INode>();

				int typeLevel = nonIdentifiedNode.getType().getModel().getLevel();
				String nodeName = nonIdentifiedNode.getName();
				INode nodeType = null;
				String typeName = nonIdentifiedNode.getType().getName();				
				
				RuleHierarchy nodeHierarchy = (RuleHierarchy) nonIdentifiedNode.getModel().getHierarchy();
				if(nodeHierarchy == mainRuleHierarchy) {
					mainNodeType  = amalgamatedRule.getMetaModelInLevel(typeLevel).getNode(typeName);
				}
				for (RuleHierarchy rule : suppRules) {
					RuleHierarchy suppRuleHierarchy = (RuleHierarchy) amalgamatedRule.getSupplementaryHierarchy(rule.getName());
					if(nonIdentifiedNode.getModel().getHierarchy() == rule) {
						nodeType = suppRuleHierarchy.getMetaModelInLevel(typeLevel).getNode(typeName); 
					}
					else {
						nodeType = suppRuleHierarchy.getRootModel().getNode(ECLASS_ID);
					}
					suppTypes.add(nodeType);
				}
				NodeVariable amalgamatedNode = amalgamatedRule.addNodeVariable(nodeName, mainNodeType, new HashSet<String>(), suppTypes, newModel);
				IModel bottomAmalgamatedModel = (isFrom) ? ((RuleHierarchy) nonIdentifiedNode.getModel().getHierarchy()).getFromModel() : ((RuleHierarchy) nonIdentifiedNode.getModel().getHierarchy()).getToModel();
				INode nonIdentifiedNodeInModel = bottomAmalgamatedModel.getNode(nonIdentifiedNode.getName());
				if (isFrom)
					combination.addAmalgamatedNodeFrom(nonIdentifiedNodeInModel, amalgamatedNode);
				else
					combination.addAmalgamatedNodeTo(nonIdentifiedNodeInModel, amalgamatedNode);
				
				for (InstantiatedAttribute instantiatedAttribute : nonIdentifiedNodeInModel.getInstantiatedAttributes()) {
					amalgamatedRule.addInstantiatedAttribute(instantiatedAttribute.getNameOrValue(), instantiatedAttribute.getType().getNameOrValue(), nodeName, newModel);
				}				
				
			}
			
			//Edges not identified
			for (IEdge nonIdentifiedEdge : bottomModel.getEdges()) {
				//If at least one of them is not, then the reference might not be 
				if (isFrom && combination.isEdgeIdentifiedFrom(nonIdentifiedEdge)) continue;

				if (!isFrom && combination.isEdgeIdentifiedTo(nonIdentifiedEdge)) continue;
				
				INode sourceNode = (isFrom) ? combination.getAmalgamatedNodeFrom(nonIdentifiedEdge.getSource()) : combination.getAmalgamatedNodeTo(nonIdentifiedEdge.getSource());
				INode targetNode = (isFrom) ? combination.getAmalgamatedNodeFrom(nonIdentifiedEdge.getTarget()) : combination.getAmalgamatedNodeTo(nonIdentifiedEdge.getTarget());				
				//If source/target is null is because in the conflict resolution some of them were removed
				if (null == sourceNode || null == targetNode) continue;
				
				IEdge mainEdgeType = amalgamatedRule.getRootModel().getEdge(EREFERENCE_ID, ECLASS_ID, ECLASS_ID);
				HashSet<IEdge> suppTypes = new HashSet<IEdge>();
				
				int typeLevel = nonIdentifiedEdge.getType().getModel().getLevel();
				String typeName = nonIdentifiedEdge.getType().getName();
				String edgeName = nonIdentifiedEdge.getName();
				IEdge edgeType = null;

				RuleHierarchy nodeHierarchy = (RuleHierarchy) nonIdentifiedEdge.getModel().getHierarchy();
				if(nodeHierarchy == mainRuleHierarchy) {
					RuleModel modelEdgeType = (RuleModel) amalgamatedRule.getMetaModelInLevel(typeLevel);
					mainEdgeType = modelEdgeType.getConstantEdge(typeName, amalgamatedRule.getTypeInModel(sourceNode, modelEdgeType).getName());
				}
				
				for (RuleHierarchy rule : suppRules) {
					RuleHierarchy suppRuleHierarchy = (RuleHierarchy) amalgamatedRule.getSupplementaryHierarchy(rule.getName());
					if(nonIdentifiedEdge.getModel().getHierarchy() == rule) {
						RuleModel modelEdgeType = (RuleModel) (suppRuleHierarchy.getMetaModelInLevel(typeLevel));
						edgeType =  modelEdgeType.getConstantEdge(typeName, nonIdentifiedEdge.getType().getSource().getName());						
					}
					else {
						edgeType = suppRuleHierarchy.getRootModel().getEdge(EREFERENCE_ID, ECLASS_ID, ECLASS_ID);
					}
					suppTypes.add(edgeType);
				}				
				EdgeVariable amalgamatedEdge = amalgamatedRule.addEdgeVariable(edgeName, mainEdgeType, sourceNode, targetNode, null, suppTypes, newModel);
				if (isFrom)
					combination.addAmalgamatedEdgeFrom(nonIdentifiedEdge, amalgamatedEdge);
				else
					combination.addAmalgamatedEdgeTo(nonIdentifiedEdge, amalgamatedEdge);
			}
		}
	}
	

	private Identification findIdentificationFromNodeName(Set<Identification> identifications, String nodeName, String nodeTypeName) {
		for (Identification identification : identifications) {
			for (INode node : identification.getIdentifiedNodes()) {
				if (node.getName().equals(nodeName) && node.getType().getName().equals(nodeTypeName)) {
					return identification;
				}
			}
		}
		return null;
	}
	
	private Identification findIdentificationFromEdgeName(Set<Identification> identifications, String edgeName, String edgeTypeName) {
		for (Identification identification : identifications) {
			for (IEdge edge : identification.getIdentifiedEdges()) {
				if (edge.getName().equals(edgeName) && edge.getType().getName().equals(edgeTypeName)) {
					return identification;
				}
			}
		}
		return null;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof McmtModuleHierarchyRules)) {
			return false;
		}
		if (this.moduleName.equals(((McmtModuleHierarchyRules) o).getModuleName()) && this.multilevelHierarchy.getName()
				.equals(((McmtModuleHierarchyRules) o).getMultilevelHierarchy().getName())) {
			return true;
		} else
			return false;
	}
}
