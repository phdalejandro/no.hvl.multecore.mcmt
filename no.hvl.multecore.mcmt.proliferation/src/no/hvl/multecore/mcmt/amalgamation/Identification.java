package no.hvl.multecore.mcmt.amalgamation;

import java.util.HashSet;
import java.util.Set;

import no.hvl.multecore.common.hierarchy.IEdge;
import no.hvl.multecore.common.hierarchy.INode;

public class Identification {
	
	private Set<INode> identifiedNodes;
	private Set<IEdge> identifiedEdges;
	
	private String identificationNodeName;
	private String identificationEdgeName;
	
	private String identificationNodeFinalName;
	private String identificationEdgeFinalName;
	
	private Combination combination;
	
	public Identification(Combination combination) {
		
		identifiedNodes = new HashSet<INode>();
		identifiedEdges = new HashSet<IEdge>();
		this.combination = combination;
		identificationNodeName="";
		identificationEdgeName="";
		identificationNodeFinalName="";
		identificationEdgeFinalName="";
	}
	
	public void addIdentifiedNodes (INode node) {
		identifiedNodes.add(node);
	}
	
	public void addIdentifiedEdge (IEdge edge) {
		identifiedEdges.add(edge);
	}

	public Set<INode> getIdentifiedNodes() {
		return identifiedNodes;
	}

	public Set<IEdge> getIdentifiedEdges() {
		return identifiedEdges;
	}

	public void setIdentifiedNodes(Set<INode> identifiedNodes) {
		this.identifiedNodes.addAll(identifiedNodes);
	}

	public void setIdentifiedEdges(Set<IEdge> identifiedEdges) {
		this.identifiedEdges.addAll(identifiedEdges);
	}

	public String getIdentificationNodeName() {
		return identificationNodeName;
	}

	public void setIdentificationNodeName(String identificationNodeName) {
		this.identificationNodeName = identificationNodeName;
	}

	public String getIdentificationEdgeName() {
		return identificationEdgeName;
	}

	public void setIdentificationEdgeName(String identificationEdgeName) {
		this.identificationEdgeName = identificationEdgeName;
	}

	public Combination getCombination() {
		return combination;
	}

	public void setCombination(Combination combination) {
		this.combination = combination;
	}
	
	public Set<INode> getIdentifiedNodesFromNode (INode node) {
		if (this.identifiedNodes.contains(node)) {
			return identifiedNodes;
		}
		else {
			return null;
		}
	}
	
	public Set<INode> getIdentifiedNodesFromNodeName (String nodeName) {
		for (INode iNode : identifiedNodes) {
			if (iNode.getName().equals(nodeName)) {
				return identifiedNodes;
			}
		}
		return null;
	}	
	
	public Set<IEdge> getIdentifiedEdgesFromEdge(IEdge edge) {
		if (this.identifiedEdges.contains(edge)) {
			return identifiedEdges;
		}
		else {
			return null;
		}
	}
	
	

	public String getIdentificationNodeFinalName() {
		return identificationNodeFinalName;
	}

	public void setIdentificationNodeFinalName(String identificationNodeFinalName) {
		this.identificationNodeFinalName = identificationNodeFinalName;
	}

	public String getIdentificationEdgeFinalName() {
		return identificationEdgeFinalName;
	}

	public void setIdentificationEdgeFinalName(String identificationEdgeFinalName) {
		this.identificationEdgeFinalName = identificationEdgeFinalName;
	}

	public void constructIdentificationNodeName () {
		if (!identifiedNodes.isEmpty()) {
			for (INode iNode : identifiedNodes) {
				this.identificationNodeName += iNode.getName() +"=";
			}
			this.identificationNodeName = this.identificationNodeName.substring(0, this.identificationNodeName.length()-1);
		}
		else {
			this.identificationNodeName = "";
		}
	}	
	
	public void constructIdentificationNodeFinalName () {
		if (!identifiedNodes.isEmpty()) {
			for (INode iNode : identifiedNodes) {
				this.identificationNodeFinalName += iNode.getName();
			}
		}
		else {
			this.identificationNodeFinalName = "";
		}
	}
	
	public void constructIdentificationEdgeName () {
		if (!identifiedEdges.isEmpty()) {
			for (IEdge iEdge : identifiedEdges) {
				this.identificationEdgeName += iEdge.getName() +"=";
			}
			this.identificationEdgeName = this.identificationEdgeName.substring(0, this.identificationEdgeName.length()-1);
		}
		else {
			this.identificationEdgeName = "";
		}
	}
	
	public void constructIdentificationEdgeFinalName () {
		if (!identifiedEdges.isEmpty()) {
			for (IEdge iEdge : identifiedEdges) {
				this.identificationEdgeFinalName += iEdge.getName();
			}
		}
		else {
			this.identificationEdgeFinalName = "";
		}
	}
}
