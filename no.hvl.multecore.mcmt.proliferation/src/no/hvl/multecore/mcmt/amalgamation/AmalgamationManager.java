package no.hvl.multecore.mcmt.amalgamation;

import java.util.HashSet;
import java.util.Set;

public class AmalgamationManager {
	private static AmalgamationManager AMALGAMATION_INSTANCE = null;
	
	private Set<McmtModuleHierarchyRules> mcmtModuleHierarchyRules;
	
	private AmalgamationManager () {
		
		this.mcmtModuleHierarchyRules = new HashSet<McmtModuleHierarchyRules>();
	}
	
	public static AmalgamationManager instance() {
		if (null == AMALGAMATION_INSTANCE)
			AMALGAMATION_INSTANCE = new AmalgamationManager();
		return AMALGAMATION_INSTANCE;
	}
	
	public void setMcmtModuleHierarchyRules (McmtModuleHierarchyRules mcmtModuleHierarchyRules) {
		this.mcmtModuleHierarchyRules.add(mcmtModuleHierarchyRules);
	}
	
	public Set<McmtModuleHierarchyRules> getMcmtModuleHierarchyRules () {
		return this.mcmtModuleHierarchyRules;
	}
	
}
