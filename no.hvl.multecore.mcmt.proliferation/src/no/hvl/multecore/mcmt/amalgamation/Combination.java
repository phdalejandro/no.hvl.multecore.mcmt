package no.hvl.multecore.mcmt.amalgamation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import no.hvl.multecore.common.hierarchy.IEdge;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.mcmt.proliferation.rule.RuleHierarchy;

public class Combination {
	
	private Set <RuleHierarchy> rulesCombined;
	
	//I_0
	private Set <Identification> elementIdentifications;
	
	//L_0
	private Set <Identification> leftHandSideIdentifications;
	
	//R_0 
	private Set <Identification> rightHandSideIdentifications;
	
	private Map <INode, INode> amalgamatedNodesFrom;
	
	private Map <INode, INode> amalgamatedNodesTo;

	private Map <IEdge, IEdge> amalgamatedEdgesFrom;
	
	private Map <IEdge, IEdge> amalgamatedEdgesTo;	
	
	
	private String combinationNameToDisplay;
	
	private String combinationPlusIdentificationsName;
	
	private String amalgamatedRuleHierarchyName;
	
	private Boolean conflict;
	
	private String combinationWithSelectedPrioritisationName;

	private RuleHierarchy prioritisedSelectedRule;
	
	public  Combination() {
		rulesCombined = new HashSet<RuleHierarchy>();
		elementIdentifications = new HashSet<Identification>();
		leftHandSideIdentifications = new HashSet<Identification>();
		rightHandSideIdentifications = new HashSet<Identification>();
		amalgamatedNodesFrom = new HashMap<INode, INode>();
		amalgamatedNodesTo = new HashMap<INode, INode>();
		amalgamatedEdgesFrom = new HashMap<IEdge, IEdge>();
		amalgamatedEdgesTo = new HashMap<IEdge, IEdge>();		
		combinationPlusIdentificationsName="";
		amalgamatedRuleHierarchyName="";
		conflict = false;
		prioritisedSelectedRule = null;
		combinationWithSelectedPrioritisationName="";
	}
	
	
	
	public INode getAmalgamatedNodeFrom(INode originalNode) {
		return amalgamatedNodesFrom.get(originalNode);
	}
	
	public boolean isNodeIdentifiedFrom (INode originalNode) {
		return amalgamatedNodesFrom.containsKey(originalNode);
	}
	
	public INode getAmalgamatedNodeTo(INode originalNode) {
		return amalgamatedNodesTo.get(originalNode);
	}
	
	public boolean isNodeIdentifiedTo (INode originalNode) {
		return amalgamatedNodesTo.containsKey(originalNode);
	}	

	
	public void addAmalgamatedNodeFrom (INode originalNode, INode amalgamatedNode) {
		amalgamatedNodesFrom.put(originalNode, amalgamatedNode);
	}
	

	public void addAmalgamatedNodeTo (INode originalNode, INode amalgamatedNode) {
		amalgamatedNodesTo.put(originalNode, amalgamatedNode);
	}	
	
	public IEdge getAmalgamatedEdgeFrom(IEdge originalEdge) {
		return amalgamatedEdgesFrom.get(originalEdge);
	}
	
	public boolean isEdgeIdentifiedFrom (IEdge originalEdge) {
		return amalgamatedEdgesFrom.containsKey(originalEdge);
	}
		
	public IEdge getAmalgamatedEdgeTo(IEdge originalEdge) {
		return amalgamatedEdgesTo.get(originalEdge);
	}

	public boolean isEdgeIdentifiedTo (IEdge originalEdge) {
		return amalgamatedEdgesTo.containsKey(originalEdge);
	}	
	
	public void addAmalgamatedEdgeFrom (IEdge originalEdge, IEdge amalgamatedEdge) {
		amalgamatedEdgesFrom.put(originalEdge, amalgamatedEdge);
	}

	public void addAmalgamatedEdgeTo (IEdge originalEdge, IEdge amalgamatedEdge) {
		amalgamatedEdgesTo.put(originalEdge, amalgamatedEdge);
	}	
		
	
	
	public void addRulesCombined (RuleHierarchy rulesHierarchy) {
		rulesCombined.add(rulesHierarchy);
		combinationPlusIdentificationsName="";
	}
	
	public void addElementIndentification (Identification identification) {
		elementIdentifications.add(identification);
	}
	
	public Set<Identification> getElementIdentifications() {
		return elementIdentifications;
	}
	
	public void addLeftHandSideIndentification (Identification identification) {
		leftHandSideIdentifications.add(identification);
	}
	
	public Set<Identification> getLeftHandSideIdentifications() {
		return leftHandSideIdentifications;
	}	
	
	public void addRightHandSideIndentification (Identification identification) {
		rightHandSideIdentifications.add(identification);
	}
	
	public Set<Identification> getRightHandSideIdentifications() {
		return rightHandSideIdentifications;
	}		
	

	public String getCombinationNameToDisplay() {
		return combinationNameToDisplay;
	}

	public void setCombinationNameToDisplay(String combinationNameToDisplay) {
		this.combinationNameToDisplay = combinationNameToDisplay;
	}

	public Set<RuleHierarchy> getRulesCombined() {
		return rulesCombined;
	}
	
	public String getCombinationPlusIdentificationsName() {
		return combinationPlusIdentificationsName;
	}

	public void setCombinationPlusIdentificationsName(String combinationPlusIdentificationsName) {
		this.combinationPlusIdentificationsName = combinationPlusIdentificationsName;
	}
	
	
	public Boolean getConflict() {
		return conflict;
	}

	public void setConflict(Boolean conflict) {
		this.conflict = conflict;
	}
	

	public String getCombinationWithSelectedPrioritisationName() {
		return combinationWithSelectedPrioritisationName;
	}

	public void setCombinationWithSelectedPrioritisationName(String combinationWithSelectedPrioritisationName) {
		this.combinationWithSelectedPrioritisationName = combinationWithSelectedPrioritisationName;
	}

	public RuleHierarchy getPrioritisedSelectedRule() {
		return prioritisedSelectedRule;
	}

	public void setPrioritisedSelectedRule(RuleHierarchy prioritisedSelectedRule) {
		this.prioritisedSelectedRule = prioritisedSelectedRule;
	}

	public void constructCombinationPlusIdentificationsName() {
		this.combinationPlusIdentificationsName = this.combinationNameToDisplay + 
				" | identified nodes: ";
		if (!this.elementIdentifications.isEmpty()) {
			for (Identification identification : elementIdentifications) {
				if (!identification.getIdentifiedNodes().isEmpty()) {
					this.combinationPlusIdentificationsName += identification.getIdentificationNodeName() + ", ";
				}
			}
			this.combinationPlusIdentificationsName= combinationPlusIdentificationsName.substring(0, combinationPlusIdentificationsName.length()-2);
		}
		this.combinationPlusIdentificationsName += 	" | identified edges: ";
		if (!this.elementIdentifications.isEmpty()) {
			for (Identification identification : elementIdentifications) {
				if (!identification.getIdentifiedEdges().isEmpty()) {
					this.combinationPlusIdentificationsName += identification.getIdentificationEdgeName() + ", ";
				}
			}
			this.combinationPlusIdentificationsName = combinationPlusIdentificationsName.substring(0, combinationPlusIdentificationsName.length()-2);
		}		
	}

	public String constructAmalgamatedRuleHierarchyName (RuleHierarchy mainHierarchy) {
		this.amalgamatedRuleHierarchyName = mainHierarchy.getName();
		for (RuleHierarchy suppRuleHierarchy: this.rulesCombined) {
			if (!mainHierarchy.getName().equals(suppRuleHierarchy.getName())) {
				this.amalgamatedRuleHierarchyName += suppRuleHierarchy.getName();
			}
		}
		return this.amalgamatedRuleHierarchyName;
	}
	
	public void constructCombinationWithSelectedPrioritisationName () {
		if (null != this.prioritisedSelectedRule) {
			this.combinationWithSelectedPrioritisationName = this.combinationNameToDisplay + " (priority on: " + this.prioritisedSelectedRule.getName() + ")"; 
			
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((rulesCombined == null) ? 0 : rulesCombined.hashCode());
		return result;
	}

	
	@Override
    public boolean equals(Object o) { 
        if (o == this) { 
            return true; 
        }         
        if (this.combinationNameToDisplay.equals(((Combination)o).getCombinationNameToDisplay())) {
        	return true;
        }
       if (this.rulesCombined.equals(((Combination)o).getRulesCombined())) {
    	   return true;
       }
       else return false;
    }


	
}
