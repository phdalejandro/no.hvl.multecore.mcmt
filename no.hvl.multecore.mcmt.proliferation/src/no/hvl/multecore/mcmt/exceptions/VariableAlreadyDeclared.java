package no.hvl.multecore.mcmt.exceptions;

import no.hvl.multecore.common.exceptions.MultEcoreException;

public class VariableAlreadyDeclared extends MultEcoreException {
	
	
	private static final long serialVersionUID = 1L;
	private String variable;
	
	public VariableAlreadyDeclared(String variable) {
		super();
		this.variable = variable;
	}
	
	
	@Override
	public void createNotificationDialog() {
		super.createNotificationDialog("Already used variable",
				"The variable \"" + variable + "\" has already been declared");		
	}
}
