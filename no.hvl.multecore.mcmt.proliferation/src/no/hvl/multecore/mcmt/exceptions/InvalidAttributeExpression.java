package no.hvl.multecore.mcmt.exceptions;

import no.hvl.multecore.common.exceptions.MultEcoreException;

public class InvalidAttributeExpression extends MultEcoreException {
	
	private static final long serialVersionUID = 1L;
	private String expression;
	private String typeName;

	public InvalidAttributeExpression(String expression, String typeName) {
		super();
		this.expression = expression;
		this.typeName = typeName;
	}
	
	@Override
	public void createNotificationDialog() {
		super.createNotificationDialog("Invalid expression",
				"The expression \"" + expression + "\" does not correctly evaluates for the type \"" + typeName + "\"");		
	}

}
