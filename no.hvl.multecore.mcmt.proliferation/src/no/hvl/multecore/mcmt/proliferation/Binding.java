package no.hvl.multecore.mcmt.proliferation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import no.hvl.multecore.common.hierarchy.IEdge;
import no.hvl.multecore.common.hierarchy.INode;

public class Binding {

	private Map<INode, Set<INode>> nodeMatches;
	private Map<IEdge, Set<IEdge>> edgeMatches;

	
	public Binding() {
		nodeMatches = new HashMap<INode, Set<INode>>();
		edgeMatches = new HashMap<IEdge, Set<IEdge>>();
	}
	
	
	public void addBinding(INode ruleNode, INode hierarchyNode) {
		nodeMatches.computeIfAbsent(ruleNode, k -> new HashSet<INode>()).add(hierarchyNode);
	}
	
	
	public void addBinding(IEdge ruleEdge, IEdge hierarchyEdge) {
		edgeMatches.computeIfAbsent(ruleEdge, k -> new HashSet<IEdge>()).add(hierarchyEdge);
	}
	
	
	public void addBindings(INode ruleNode, Set<INode> hierarchyNodes) {
		nodeMatches.computeIfAbsent(ruleNode, k -> new HashSet<INode>()).addAll(hierarchyNodes);
	}
	
	
	public void addBindings(IEdge ruleEdge, Set<IEdge> hierarchyEdge) {
		edgeMatches.computeIfAbsent(ruleEdge, k -> new HashSet<IEdge>()).addAll(hierarchyEdge);
	}
	
	
	public void removeBinding(INode ruleNode, INode hierarchyNode) {
		nodeMatches.get(ruleNode).remove(hierarchyNode);
	}
	
	
	public void removeBinding(IEdge ruleEdge, IEdge hierarchyEdge) {
		edgeMatches.get(ruleEdge).remove(hierarchyEdge);
	}
	
	
	public Set<INode> getBindings(INode ruleNode) {
		return nodeMatches.get(ruleNode);
	}
	
	
	public Set<IEdge> getBindings(IEdge ruleEdge) {
		return edgeMatches.get(ruleEdge);
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((edgeMatches == null) ? 0 : edgeMatches.hashCode());
		result = prime * result + ((nodeMatches == null) ? 0 : nodeMatches.hashCode());
		return result;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Binding other = (Binding) obj;
		if (edgeMatches == null) {
			if (other.edgeMatches != null)
				return false;
		} else if (!edgeMatches.equals(other.edgeMatches))
			return false;
		if (nodeMatches == null) {
			if (other.nodeMatches != null)
				return false;
		} else if (!nodeMatches.equals(other.nodeMatches))
			return false;
		return true;
	}

}
