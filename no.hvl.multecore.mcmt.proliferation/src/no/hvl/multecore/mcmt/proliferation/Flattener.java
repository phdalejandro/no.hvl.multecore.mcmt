package no.hvl.multecore.mcmt.proliferation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;

import no.hvl.multecore.common.hierarchy.DeclaredAttribute;
import no.hvl.multecore.common.hierarchy.DeclaredAttribute.NativeType;
import no.hvl.multecore.common.hierarchy.IEdge;
import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.common.hierarchy.Node;

public class Flattener {
	
	// Creates an XMI from "instance" and turns all its metamodels into packages in a single Ecore
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void flatten(IModel instanceModel, URI locationURI) {
		// Sort the list of metamodels in top-to-bottom order
		List<IModel> metamodels = new ArrayList<IModel>(instanceModel.getAllMetamodels());
		Collections.sort(metamodels);
		
		// Ecore resource, package, root eClass and named, root-contained abstract eClass
		String mainPackageName = instanceModel.getName() + Constants.FLAT_METAMODEL_SUFFIX;
        EPackage mainPackage = EcoreFactory.eINSTANCE.createEPackage();
        mainPackage.setName(mainPackageName);
        mainPackage.setNsPrefix(mainPackageName);
        mainPackage.setNsURI(mainPackageName);
		ResourceSet resourceSet = new ResourceSetImpl();
        Resource ecoreResource = resourceSet.createResource(locationURI.appendSegment(mainPackageName).appendFileExtension("ecore"));
        ecoreResource.getContents().add(mainPackage);
        EClass rootEClass = EcoreFactory.eINSTANCE.createEClass();
        rootEClass.setName(no.hvl.multecore.common.Constants.NODE_NAME_ROOT);
        mainPackage.getEClassifiers().add(rootEClass);
		EClass abstractEClass = EcoreFactory.eINSTANCE.createEClass();
		abstractEClass.setName("Abstract");
		abstractEClass.setAbstract(true);
		EReference containmentEReference = EcoreFactory.eINSTANCE.createEReference();
		containmentEReference.setName("__contains");
		containmentEReference.setEType(abstractEClass);
		containmentEReference.setContainment(true);
		containmentEReference.setLowerBound(0);
		containmentEReference.setUpperBound(-1);
		rootEClass.getEStructuralFeatures().add(containmentEReference);
		EAttribute nameEAttribute = EcoreFactory.eINSTANCE.createEAttribute();
    	nameEAttribute.setName("__name");
    	nameEAttribute.setID(false);
    	nameEAttribute.setEType(EcorePackage.eINSTANCE.getEString());
    	nameEAttribute.setLowerBound(1);
    	nameEAttribute.setUpperBound(1);
    	abstractEClass.getEStructuralFeatures().add(nameEAttribute);
		mainPackage.getEClassifiers().add(abstractEClass);

        // All types available through potency
        Map<INode, Set<IEdge>> availableTypes = instanceModel.getHierarchy().allAvailableTypesInModel(instanceModel, false);
        
        // Keep track of the EObjects generated for each node
        Map<INode,EObject> transformedNodes = new HashMap<INode,EObject>();
        // Keep track of the EReferences generated for each edge and each node where it is available by potency
        Map<IEdge,Map<INode,Set<EReference>>> transformedEdges = new HashMap<IEdge,Map<INode,Set<EReference>>>();
        
        // Transform a model into an EPackage
        for (IModel metamodel : metamodels) {
			// Model package
	        EPackage modelPackage = EcoreFactory.eINSTANCE.createEPackage();
	        modelPackage.setName(metamodel.getName());
	        modelPackage.setNsPrefix(metamodel.getName());
	        modelPackage.setNsURI(metamodel.getName());
	        mainPackage.getESubpackages().add(modelPackage);
	        
	        // Transform nodes into EClasses
			for (INode node : metamodel.getNodes()) {
				// Create class and add to transformed classes
				EClass eClass = EcoreFactory.eINSTANCE.createEClass();
				eClass.setName(node.getName());
				// Make it abstract if it is abstract or it cannot be instantiated through potency
				if (node instanceof Node) {
					eClass.setAbstract((((Node) node).isAbstract()) || (!availableTypes.keySet().contains(node)));
				}
				transformedNodes.put(node, eClass);
				
				// Inherit from abstract EClass, to get name and root containment
				eClass.getESuperTypes().add(abstractEClass);
				
				// Set type with reference
				INode nodeType = node.getType();
				EClass eClassType = (EClass) transformedNodes.get(nodeType);
				EReference typeEReference = EcoreFactory.eINSTANCE.createEReference();
				typeEReference.setName("__type");
				typeEReference.setEType(eClassType);
				typeEReference.setContainment(false);
				typeEReference.setLowerBound(0);
				typeEReference.setUpperBound(1);
				eClass.getEStructuralFeatures().add(typeEReference);
				
				// Add attributes
				for (DeclaredAttribute attribute : node.getDeclaredAttributes(true)) {
			    	EAttribute eAttribute = EcoreFactory.eINSTANCE.createEAttribute();
			    	eAttribute.setName(attribute.getNameOrValue());
			    	eAttribute.setID(attribute.isId());
			    	eAttribute.setEType(((NativeType) attribute.getType()).getEDataType());
			    	eAttribute.setLowerBound(attribute.getLowerBound());
			    	eAttribute.setUpperBound(attribute.getUpperBound());
			    	eClass.getEStructuralFeatures().add(eAttribute);
				}
				
				// Add to package
				modelPackage.getEClassifiers().add(eClass);
			}

			// Transform declared edges into EReferences
			for (IEdge edge : metamodel.getEdges()) {				
				EReference eReference = EcoreFactory.eINSTANCE.createEReference();
				eReference.setName(edge.getName());
				eReference.setEType((EClass) transformedNodes.get(edge.getTarget()));
				eReference.setContainment(edge.isContainment());
				eReference.setLowerBound(edge.getLowerBound());
				eReference.setUpperBound(edge.getUpperBound());
	
				// Add to EClass and to transformed EReferences
				((EClass) transformedNodes.get(edge.getSource())).getEStructuralFeatures().add(eReference);
				Map<INode, Set<EReference>> edgeTransformations = transformedEdges.computeIfAbsent(edge, k -> new HashMap<INode,Set<EReference>>());
				edgeTransformations.computeIfAbsent(edge.getSource(), k -> new HashSet<EReference>()).add(eReference);
			}
			
			// Add inheritance relations
			for (INode node : metamodel.getNodes()) {
				// Inheritance
				for (INode parentNode : node.getParentNodes()) {
					((EClass) transformedNodes.get(node)).getESuperTypes().add((EClass) transformedNodes.get(parentNode));
				}
			}
		}
        
        // Transform instantiable edges into EReferences
		for (INode node : availableTypes.keySet()) {
			Set<IEdge> availableEdges = availableTypes.get(node);
			if (null == availableEdges)
				continue;
			
			for (IEdge edge : availableEdges) {
				// Avoid duplicates
				if (edge.getModel().equals(edge.getSource().getModel()) && edge.getModel().equals(edge.getTarget().getModel()))
					continue;
				
				IModel originalEdgeModel = edge.getModel();
				INode originalEdgeSource = originalEdgeModel.getHierarchy().getTypeInModel(edge.getSource(), originalEdgeModel);
				INode originalEdgeTarget = originalEdgeModel.getHierarchy().getTypeInModel(edge.getTarget(), originalEdgeModel);
				IEdge originalEdge = edge.getModel().getEdgePlusInherited(edge.getName(), originalEdgeSource.getName(), originalEdgeTarget.getName());
				EReference eReference = EcoreFactory.eINSTANCE.createEReference();
				eReference.setName(edge.getName() + "__" + edge.getTarget().getName());
				eReference.setEType((EClass) transformedNodes.get(edge.getTarget()));
				eReference.setContainment(edge.isContainment());
				eReference.setLowerBound(edge.getLowerBound());
				eReference.setUpperBound(edge.getUpperBound());
				
				// Add to EClass and to transformed EReferences
				((EClass) transformedNodes.get(edge.getSource())).getEStructuralFeatures().add(eReference);
				Map<INode, Set<EReference>> edgeTransformations = transformedEdges.computeIfAbsent(originalEdge, k -> new HashMap<INode,Set<EReference>>());
				edgeTransformations.computeIfAbsent(edge.getSource(), k -> new HashSet<EReference>()).add(eReference);
			}
			
		}
        
        // XMI resource and root eObject
        Resource xmiResource = resourceSet.createResource(locationURI.appendSegment(instanceModel.getName()).appendFileExtension("xmi"));
        EObject rootEObject = mainPackage.getEFactoryInstance().create(rootEClass);
        xmiResource.getContents().add(rootEObject);
        
        // Transform nodes into EObjects
		EList<EObject> rootContainedEObjects = (EList<EObject>) rootEObject.eGet(containmentEReference);
        for (INode node : instanceModel.getNodes()) {
			// Create object and add to transformed nodes
        	EObject eObject = ((EClass) transformedNodes.get(node.getType())).getEPackage().getEFactoryInstance().create((EClass) transformedNodes.get(node.getType()));
        	eObject.eSet(nameEAttribute, node.getName());
        	transformedNodes.put(node, eObject);
        	
        	// Add to root
			rootContainedEObjects.add(eObject);
        }
        
        // Transform edges into instances of EReferences
        for (INode node : instanceModel.getNodes()) {
        	for (IEdge edge : instanceModel.getOutEdges(node)) {
        		EObject sourceEObject = transformedNodes.get(node);
        		EObject targetEObject = transformedNodes.get(edge.getTarget());
        		EReference typeEReference = null;
        		for (EReference er : transformedEdges.get(edge.getType()).get(node.getType())) {
        			if (er.getEType().equals(transformedNodes.get(edge.getTarget().getType()))) {
        				typeEReference = er;
        				break;
        			}
					for (INode parent : edge.getTarget().getType().getAllParentNodes()) {
						if (er.getEType().equals(transformedNodes.get(parent))) {
							typeEReference = er;
							break;
						}
					}
        		}
                if (typeEReference.getUpperBound() > 1 || typeEReference.getUpperBound() < 0) {
                	EList<EObject> referenceTargets = (EList<EObject>) sourceEObject.eGet(typeEReference);
                	referenceTargets.add(targetEObject);
                } else {
                	sourceEObject.eSet(typeEReference, targetEObject);
                }
        	}
        }

		// Save Ecore and XMI
		Map options = new HashMap();
        options.put(XMLResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE);
        try {
			ecoreResource.save(options);
	        xmiResource.save(options);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	//TODO A method here may eventually be used to generate the 2-level rules

}
