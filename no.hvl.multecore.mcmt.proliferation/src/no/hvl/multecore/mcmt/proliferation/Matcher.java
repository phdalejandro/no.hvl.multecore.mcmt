package no.hvl.multecore.mcmt.proliferation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.common.hierarchy.MultilevelHierarchy;
import no.hvl.multecore.mcmt.proliferation.rule.NodeVariable;
import no.hvl.multecore.mcmt.proliferation.rule.RuleHierarchy;

public class Matcher {

	public Map<IModel, List<List<Binding>>> findMatches(MultilevelHierarchy hierarchy, RuleHierarchy rule) {
		Map<IModel, List<List<Binding>>> allMatches = new HashMap<IModel, List<List<Binding>>>();
		List<IModel> ruleMeta = rule.getFromModel().getMainMetamodel().getBranch(true); // Ecore excluded from the match, so ruleMeta[0].level == 1
		for (IModel iModel : hierarchy.getAllModels()) {
			// Don't try if the branch of the hierarchy for this model is smaller than the rule itself
			if (iModel.getLevel() < ruleMeta.size())
				continue;
			List<List<Binding>> matches = new ArrayList<List<Binding>>();
			findMatches(iModel.getBranch(true), ruleMeta, 0, 0, matches);
			allMatches.put(iModel, matches);
		}
		return allMatches;
	}


	private boolean findMatches(List<IModel> hierarchyBranch, List<IModel> ruleMeta, int hierarchyLevelIndex, int patternLevelIndex, List<List<Binding>> matches) {
		// Base case, the bottom of the pattern hierarchy is reached with valid matches in every level
		if (patternLevelIndex == ruleMeta.size())
			return true;
		boolean found = false; // Global result of the process; true if at least a full match is found
		while (hierarchyLevelIndex < hierarchyBranch.size()) {
			List<Binding> maps = graphMatch(hierarchyBranch.get(hierarchyLevelIndex), ruleMeta.get(patternLevelIndex));
			for (Binding binding : maps) {
				int size = matches.size();
				if ((size > 0) && (patternLevelIndex > 0)) {
					// Continue exploring all possible ramifications of this candidate pattern
					List<Binding> currentMatch = matches.get(size - 1);
					List<Binding> currentMatchCopy = new ArrayList<>(currentMatch);
					currentMatchCopy.add(binding);
					matches.set(matches.size() - 1, currentMatchCopy);
					found = found || findMatches(hierarchyBranch, ruleMeta, hierarchyLevelIndex + 1, patternLevelIndex + 1, matches);
					matches.add(currentMatch);
				} else {
					// Beginning of new pattern candidate
					List<Binding> newMatchCandidate = new ArrayList<Binding>();
					newMatchCandidate.add(binding);
					matches.add(newMatchCandidate);
					found = found || findMatches(hierarchyBranch, ruleMeta, hierarchyLevelIndex + 1, patternLevelIndex + 1, matches);
				}
			}
			hierarchyLevelIndex++;
		}
		if (patternLevelIndex > 0) {
			matches.remove(matches.size() - 1); // Current candidate match has not been completed successfully, so is deleted
		}
		return found;
	}


	private List<Binding> graphMatch(IModel hierarchyModel, IModel ruleModel) {
		Binding potentialBinding = getFeasibleMatches(hierarchyModel, ruleModel);
		Binding test = dualSimulation(hierarchyModel, ruleModel, potentialBinding);
		List<Binding> foo = new ArrayList<Binding>();
		foo.add(test);
		return foo;
	}
	
	
	
	private Binding getFeasibleMatches(IModel hierarchyModel, IModel ruleModel) {
		Binding binding = new Binding();
		for (INode ruleNode : ruleModel.getNodes()) {
			for (INode hierarchyNode : hierarchyModel.getNodes()) {
				if (hierarchyNode.getType().getName().equals(ruleNode.getType().getName())) { // TODO Change this later to use former binding info
					if (ruleNode instanceof NodeVariable) { // Name matching not required since it is a variable
						binding.addBinding(ruleNode, hierarchyNode);
						continue;
					}
					if (ruleNode.getName().equals(hierarchyNode.getName())) { // Name matching required since it is a constant (normal node)
						binding.addBinding(ruleNode, hierarchyNode);
					}
				}
			}
		}
		return binding;
	}


	private Binding dualSimulation(IModel hierarchyModel, IModel ruleModel, Binding binding) {
		boolean changed = true;
		while (changed) {
			changed = false;
			for (INode ruleNode : ruleModel.getNodes()) {
				for (INode adjacentNode : ruleModel.getOutEdgesTargets(ruleNode)) {
					Binding bindingPrime = new Binding(); // Line 7 pseudocode
					for (INode nodeBinding : binding.getBindings(ruleNode)) {
						Set<INode> setIntersection = hierarchyModel.getOutEdgesTargets(nodeBinding);
						setIntersection.retainAll(binding.getBindings(adjacentNode));
						binding.addBindings(adjacentNode, setIntersection); // Line 9 pseudocode
						if (binding.getBindings(adjacentNode).isEmpty()) {
							binding.removeBinding(ruleNode, nodeBinding); // Line 11 pseudocode
							if (binding.getBindings(ruleNode).isEmpty())
								return new Binding();
							changed = true;
						}
						bindingPrime.addBindings(adjacentNode, binding.getBindings(adjacentNode)); // Line 17 pseudocode
					}
					if (bindingPrime.getBindings(adjacentNode).isEmpty())
						return new Binding();
					if (bindingPrime.getBindings(adjacentNode).size() < binding.getBindings(adjacentNode).size())
						changed = true; 
					binding.addBindings(adjacentNode, bindingPrime.getBindings(adjacentNode)); // Line 25 pseudocode
				}
			}
		}
		return binding;
	}


}
