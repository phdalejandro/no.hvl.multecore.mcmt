package no.hvl.multecore.mcmt.proliferation;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Constants {

	public static final String MODULE_NAME_SEPARATOR = ".";
	public static final String META_BLOCK_ID = "meta";
	public static final String FROM_BLOCK_ID = "from";
	public static final String TO_BLOCK_ID = "to";
	public static final String BLOCK_MODEL_NAME_SEPARATOR = "_";
	public static final String DUPLICATED_REFERENCE_NAME_SEPARATOR = "__";
	public static final String META_MODEL_NAME_PREFIX = META_BLOCK_ID + BLOCK_MODEL_NAME_SEPARATOR;
	
	// Proliferation-related constants
	public static final String FLAT_METAMODEL_SUFFIX = "_flat_mm";
	
	
	//Maude parser
	public static final String variableRegexExpression = "^[a-zA-Z_$][a-zA-Z_$0-9]*$";
	public static final Set<String> IntegerStopWords = new HashSet<String>(Arrays.asList("true", "false"));
	public static final Set<String> IntegerOperators = new HashSet<String>(Arrays.asList("+", "-", "*"));
	public static final Set<String> RealOperators = new HashSet<String>(Arrays.asList("+", "-", "*", "/"));
	public static final Set<String> StringOperators = new HashSet<String>(Arrays.asList("+"));
	public static final Set<String> BooleanOperators = new HashSet<String>(Arrays.asList("<",">","==",">=","<=", "not"));
	
}
