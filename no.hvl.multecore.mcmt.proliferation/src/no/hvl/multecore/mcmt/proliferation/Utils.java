package no.hvl.multecore.mcmt.proliferation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Utils {

	//To later split the expression with the operators belonging to the proper type
	public static  String tokenizeIntegerOperators(Set<String> integeroperators) {
		String tokenized = "";
		boolean first = true;
		for (String operator : integeroperators) {
			if (first) {
				tokenized += "\\" + operator;
				first = false;
			}
			else {
				tokenized += "|\\" + operator;
			}
		}
		return tokenized;
	}
	
	public static  String tokenizeBooleanOperators(Set<String> booleanOperators) {
		String tokenized = "";
		boolean first = true;
		for (String operator : booleanOperators) {
			if (first) {
				tokenized += "" + operator;
				first = false;
			}
			else {
				tokenized += "|" + operator;
				
			}
		}
		return tokenized;
	}
		
	
	//Returns a List with the appearances of the operators, to re-build the expression in Maude syntax.
	public static List<String> positionSymbolCalculator (Set <String> operators, String cleanedExpression){
		List<String> positionSymbol = new ArrayList<String>();
		for (String operator : operators) {
			int lastIndex = 0;
			
			while(lastIndex != -1){
				lastIndex = cleanedExpression.indexOf(operator,lastIndex);
				if(lastIndex != -1){
					positionSymbol.add(operator);
					lastIndex += operator.length();
				}
			}
		}
		return positionSymbol;
	}
	
	public static Boolean containsStopWord (Set <String> stopWords, String variable){
		Boolean containsStopWord = false;
		for (String operator : stopWords) {
			if (operator.equals(variable)){
				containsStopWord = true;
			}
		}
		return containsStopWord;
	}
	
}
