package no.hvl.multecore.mcmt.proliferation.rule;

import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.common.hierarchy.Node;
import no.hvl.multecore.common.hierarchy.Potency;

public class NodeVariable extends Node {

	public NodeVariable(String name, INode type, IModel model) {
		super(name, type, model,
				new Potency(no.hvl.multecore.common.Constants.START_POTENCY_DEFAULT_VALUE,
						no.hvl.multecore.common.Constants.UNBOUNDED,
						no.hvl.multecore.common.Constants.DEPTH_POTENCY_DEFAULT_VALUE));
	}
	
	
	public NodeVariable(String name, INode type, IModel model, Potency potency) {
		super(name, type, model, potency);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Node))
			return false;
		Node other = (Node) obj;
		if (name == null) {
			if (other.getName() != null)
				return false;
		} else if (!name.equals(other.getName()))
			return false;
		if (type == null) {
			if (other.getType() != null)
				return false;
		} else if (!type.equals(other.getType()))
			return false;
		return true;
	}
	
}
