package no.hvl.multecore.mcmt.proliferation.rule;

import no.hvl.multecore.common.Constants;
import no.hvl.multecore.common.hierarchy.AbstractEdge;
import no.hvl.multecore.common.hierarchy.Edge;
import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.common.hierarchy.Potency;

public class EdgeConstant extends Edge {
	
	private String multiplicityVariable;

	public EdgeConstant(String name, INode sourceNode, IModel model) {
		// Using EReference as type and target = source since both are ignored for constants
		super(name,
				model.getMetamodelInLevel(0).getEdge(Constants.EREFERENCE_ID, Constants.ECLASS_ID, Constants.ECLASS_ID),
				sourceNode,
				sourceNode,
				model,
				new Potency(no.hvl.multecore.common.Constants.START_POTENCY_DEFAULT_VALUE,
						no.hvl.multecore.common.Constants.UNBOUNDED,
						no.hvl.multecore.common.Constants.DEPTH_POTENCY_DEFAULT_VALUE));
	}

	
	public EdgeConstant(String name, INode sourceNode, IModel model, Potency potency) {
		// Using EReference as type and target = source since both are ignored for constants
		super(name,
				model.getMetamodelInLevel(0).getEdge(Constants.EREFERENCE_ID, Constants.ECLASS_ID, Constants.ECLASS_ID),
				sourceNode,
				sourceNode,
				model,
				potency);
	}
	
	
	public String getMultiplicityVariable() {
		return multiplicityVariable;
	}

	public void setMultiplicityVariable(String multiplicityVariable) {
		this.multiplicityVariable = multiplicityVariable;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AbstractEdge))
			return false;
		AbstractEdge other = (AbstractEdge) obj;
		if (name == null) {
			if (other.getName() != null)
				return false;
		} else if (!name.equals(other.getName()))
			return false;
		if (sourceNode == null) {
			if (other.getSource() != null)
				return false;
		} else if (!sourceNode.equals(other.getSource()))
			return false;
		if (targetNode == null) {
			if (other.getTarget() != null)
				return false;
		} else if (!targetNode.equals(other.getTarget()))
			return false;
		return true;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result	+ ((sourceNode == null) ? 0 : sourceNode.hashCode());
		result = prime * result	+ ((targetNode == null) ? 0 : targetNode.hashCode());
		return result;
	}
	
}
