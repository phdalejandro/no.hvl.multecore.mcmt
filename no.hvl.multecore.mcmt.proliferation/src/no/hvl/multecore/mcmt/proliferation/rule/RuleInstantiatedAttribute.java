package no.hvl.multecore.mcmt.proliferation.rule;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import no.hvl.multecore.common.hierarchy.DeclaredAttribute;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.common.hierarchy.InstantiatedAttribute;

public class RuleInstantiatedAttribute extends InstantiatedAttribute {

	private String assignedVariable;
	private HashMap <String,String> variables;
	private String parsedExpression;
	
	public RuleInstantiatedAttribute(String value, DeclaredAttribute type, INode containingNode) {
		super(value, type, containingNode);
		assignedVariable = null;
		variables = new HashMap<>();
	}

	public String getAssignedVariable() {
		return assignedVariable;
	}

	public void setAssignedVariable(String assignedVariable) {
		this.assignedVariable = assignedVariable;
	}
	
	public void addVariable (String name, String type) {
		this.variables.put(name, type);
	}

	public String getParsedExpression() {
		return parsedExpression;
	}

	public void setParsedExpression(String parsedExpression) {
		this.parsedExpression = parsedExpression;
	}

	public HashMap<String, String> getVariables() {
		return variables;
	}
	
	
	
}
