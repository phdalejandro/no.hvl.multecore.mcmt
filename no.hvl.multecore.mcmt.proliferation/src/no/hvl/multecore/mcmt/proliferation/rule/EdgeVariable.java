package no.hvl.multecore.mcmt.proliferation.rule;

import no.hvl.multecore.common.hierarchy.Edge;
import no.hvl.multecore.common.hierarchy.IEdge;
import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.common.hierarchy.Potency;

public class EdgeVariable extends Edge {
	
	private String multiplicityVariable;

	public EdgeVariable(String name, IEdge type, INode sourceNode, INode targetNode, IModel model) {
		super(name, type, sourceNode, targetNode, model,
				new Potency(no.hvl.multecore.common.Constants.START_POTENCY_DEFAULT_VALUE,
						no.hvl.multecore.common.Constants.UNBOUNDED,
						no.hvl.multecore.common.Constants.DEPTH_POTENCY_DEFAULT_VALUE));
	}

	
	public EdgeVariable(String name, IEdge type, INode sourceNode, INode targetNode, IModel model, Potency potency) {
		super(name, type, sourceNode, targetNode, model, potency);
	}

	public String getMultiplicityVariable() {
		return multiplicityVariable;
	}

	public void setMultiplicityVariable(String multiplicityVariable) {
		this.multiplicityVariable = multiplicityVariable;
	}
	
	
}
