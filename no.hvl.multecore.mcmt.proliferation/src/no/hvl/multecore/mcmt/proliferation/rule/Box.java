package no.hvl.multecore.mcmt.proliferation.rule;

import java.util.HashSet;
import java.util.Set;

import no.hvl.multecore.common.hierarchy.Edge;
import no.hvl.multecore.common.hierarchy.Node;

public class Box {

	private String multiplicityVariable;

	// TODO We should have an abstract class that represent both types of edges for rules

	private Set<Edge> edges;

	private Set<Node> nodes;
	
	private Set<Box> innerBoxes;
	
	public Box(String multiplicityVariable) {
		this.multiplicityVariable = multiplicityVariable;
		this.edges = new HashSet<Edge>();
		this.nodes = new HashSet<Node>();
		this.innerBoxes = new HashSet<Box>();
	}

	public String getMultiplicityVariable() {
		return multiplicityVariable;
	}

	public Set<Edge> getEdges() {
		return edges;
	}

	public void addEdge(Edge edge) {
		this.edges.add(edge);
	}

	public Set<Node> getNodes() {
		return nodes;
	}

	public void addNode(Node node) {
		this.nodes.add(node);
	}

	public Set<Box> getInnerBoxes() {
		return innerBoxes;
	}

	public void addInnerBox (Box box) {
		this.innerBoxes.add(box);
	}

}
