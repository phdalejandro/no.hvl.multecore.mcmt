package no.hvl.multecore.mcmt.proliferation.rule;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import no.hvl.multecore.common.hierarchy.IEdge;
import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.common.hierarchy.InstantiatedAttribute;
import no.hvl.multecore.common.hierarchy.Model;

public class RuleModel extends Model {

	protected Set<NodeConstant> constantNodes;
	protected Set<NodeVariable> variableNodes;
	protected Set<EdgeConstant> constantEdges;
	protected Set<EdgeVariable> variableEdges;
	protected Set<Box> boxes;
	protected Set<Condition> conditions;

	
	public RuleModel(String name, IModel metamodel) {
		super(name, metamodel);
		constantNodes = new HashSet<NodeConstant>();
		variableNodes = new HashSet<NodeVariable>();
		constantEdges = new HashSet<EdgeConstant>();
		variableEdges = new HashSet<EdgeVariable>();
		boxes = new HashSet<Box>();
		conditions = new HashSet<Condition>();
	}

	
	public RuleModel(String name, IModel metamodel, int level) {
		super(name, metamodel, level);
		constantNodes = new HashSet<NodeConstant>();
		variableNodes = new HashSet<NodeVariable>();
		constantEdges = new HashSet<EdgeConstant>();
		variableEdges = new HashSet<EdgeVariable>();
		boxes = new HashSet<Box>();
		conditions = new HashSet<Condition>();
	}
	
	
	public IEdge getConstantEdge(String edgeName, String sourceName) {
		INode sourceNode = null;
		if (null == (sourceNode = getNode(sourceName)))
			return null;
		for (IEdge edge : outEdges.get(sourceNode)) {
			if (edge.getName().equals(edgeName))
				return edge;
		}
		return null;
	}
	
	public IEdge getConstantEdgePlusInherited(String edgeName, String sourceName) {
		IEdge edge = null;
		INode sourceNode = null;
		if (null == (sourceNode = getNode(sourceName)))
			return edge;
		do {
			edge = getConstantEdge(edgeName, sourceNode.getName());
			Iterator<INode> it = sourceNode.getParentNodes().iterator();
			sourceNode = (it.hasNext()) ? sourceNode.getParentNodes().iterator().next() : null;
		}while(edge == null && sourceNode != null);
		return edge;
	}	
		
	public Set<NodeConstant> getConstantNodes() {
		return constantNodes;
	}


	public Set<NodeVariable> getVariableNodes() {
		return variableNodes;
	}


	public Set<EdgeConstant> getConstantEdges() {
		return constantEdges;
	}


	public Set<EdgeVariable> getVariableEdges() {
		return variableEdges;
	}


	public void addOrReplaceNode(NodeConstant node) {
		super.addOrReplaceNode(node);
		constantNodes.remove(node);
		constantNodes.add(node);
	}
	
	
	public void addOrReplaceNode(NodeVariable node) {
		super.addOrReplaceNode(node);
		variableNodes.remove(node);
		variableNodes.add(node);
	}
	
	
	public void addOrReplaceEdge(EdgeConstant edge) {
		super.addOrReplaceEdge(edge);
		constantEdges.remove(edge);
		constantEdges.add(edge);
	}
	
	
	public void addOrReplaceEdge(EdgeVariable edge) {
		super.addOrReplaceEdge(edge);
		variableEdges.remove(edge);
		variableEdges.add(edge);
	}
	
	public Box getBox (String multiplicityVariable) {
		Box box = null;
		for (Box b : boxes) {
			if (b.getMultiplicityVariable().equals(multiplicityVariable)) {
				box = b;
			}
		}
		return box;
	}
	
	public Set<Box> getBoxes (){
		return boxes;
	}
	
	public void addBox (Box box) {
		boxes.add(box);
	}
	
	public Set<Condition> getConditions (){
		return conditions;
	}
	
	public void addCondition (Condition condition) {
		conditions.add(condition);
	}	
	
	public boolean variableAlreadyUsedInAttribute (String variable) {
		boolean used = false;
		//We try to find if in the set of node variables, some of the attributes already has that variable declared
		for ( NodeVariable node : this.variableNodes) {
			for (InstantiatedAttribute attribute : node.getInstantiatedAttributes()) {
				if (attribute instanceof RuleInstantiatedAttribute) {
					if (((RuleInstantiatedAttribute) attribute).getAssignedVariable() != null) {
						if (((RuleInstantiatedAttribute) attribute).getAssignedVariable().equals(variable)) {
							used = true;
						}
					}
				}
			}
		}
		return used;
	}
}
