package no.hvl.multecore.mcmt.proliferation.rule;

import java.util.HashSet;
import java.util.Set;

import no.hvl.multecore.common.Constants;
import no.hvl.multecore.common.hierarchy.DeclaredAttribute;
import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.Node;
import no.hvl.multecore.common.hierarchy.Potency;

public class NodeConstant extends Node {

	public NodeConstant(String name, IModel model) {
		// Using EClass as type since the type in constants is ignored
		super(name, model.getMetamodelInLevel(0).getNode(Constants.ECLASS_ID), model,
				new Potency(no.hvl.multecore.common.Constants.START_POTENCY_DEFAULT_VALUE,
						no.hvl.multecore.common.Constants.UNBOUNDED,
						no.hvl.multecore.common.Constants.DEPTH_POTENCY_DEFAULT_VALUE));
	}

	public NodeConstant(String name, IModel model, Potency potency) {
		// Using EClass as type since the type in constants is ignored
		super(name, model.getMetamodelInLevel(0).getNode(Constants.ECLASS_ID), model, potency);
	}
	
	
//	public Set<RuleDeclaredAttribute> getRuleDeclaredAttributes() {
//		Set<RuleDeclaredAttribute> ruleDeclaredAttributes = new HashSet<RuleDeclaredAttribute>();
//		for (DeclaredAttribute declaredAttribute : this.getDeclaredAttributes(false)) {
//			ruleDeclaredAttributes.add((RuleDeclaredAttribute) declaredAttribute);
//		}
//		return ruleDeclaredAttributes;
//	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Node))
			return false;
		Node other = (Node) obj;
		if (name == null) {
			if (other.getName() != null)
				return false;
		} else if (!name.equals(other.getName()))
			return false;
		IModel otherModel = other.getModel();
		if (model == null) {
			if (otherModel != null)
				return false;
		} else if (!model.equals(otherModel))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

}
