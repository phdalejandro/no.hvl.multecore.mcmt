package no.hvl.multecore.mcmt.proliferation.rule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import no.hvl.multecore.common.MultEcoreManager;
import no.hvl.multecore.common.exceptions.BadlyFormattedElement;
import no.hvl.multecore.common.exceptions.DanglingEdge;
import no.hvl.multecore.common.exceptions.DuplicatedElement;
import no.hvl.multecore.common.exceptions.ElementNotExists;
import no.hvl.multecore.common.exceptions.InvalidSupplementaryElement;
import no.hvl.multecore.common.exceptions.MultEcoreException;
import no.hvl.multecore.common.hierarchy.DeclaredAttribute.NativeType;
import no.hvl.multecore.common.hierarchy.DeclaredAttribute;
import no.hvl.multecore.common.hierarchy.Edge;
import no.hvl.multecore.common.hierarchy.IAttribute;
import no.hvl.multecore.common.hierarchy.IEdge;
import no.hvl.multecore.common.hierarchy.IModel;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.common.hierarchy.InstantiatedAttribute;
import no.hvl.multecore.common.hierarchy.MultilevelHierarchy;
import no.hvl.multecore.common.hierarchy.Node;
import no.hvl.multecore.common.hierarchy.Potency;
import no.hvl.multecore.mcmt.exceptions.InvalidAttributeExpression;
import no.hvl.multecore.mcmt.exceptions.VariableAlreadyDeclared;
import no.hvl.multecore.mcmt.proliferation.Constants;

public class RuleHierarchy extends MultilevelHierarchy {
	
	public RuleHierarchy(String name) {
		super(name);
	}
	
	
	@Override
	public IModel createModel(String modelName, String metamodelName) {
		IModel metamodel = getModelByName(metamodelName);
		int modelLevel = metamodel.getLevel() + 1;
		IModel model = new RuleModel(modelName, metamodel, modelLevel);
		childrenModels.computeIfAbsent(metamodel, k -> new HashSet<IModel>()).add(model);
		childrenModels.put(model, new HashSet<IModel>());
		nodes.put(model, new HashSet<INode>());
		edges.put(model, new HashSet<IEdge>());
		models.put(modelName, model);
		if (modelLevel > maxLevel)
			maxLevel = modelLevel;
		return model;
	}

	
	public NodeConstant addNodeConstant(String nodeName, Set<String> parentNodeNames, IModel iModel) throws MultEcoreException {
		RuleModel model = (RuleModel) iModel;

		// Create node
		NodeConstant node = new NodeConstant(nodeName, model);
		if (model.getNodes().contains(node))
			throw new DuplicatedElement(nodeName, model.getName());

		// Add parent nodes
		for (String parentName : parentNodeNames) {
        	Node parentNode = (Node) model.getNode(parentName);
        	if (null == parentNode)
        		throw new ElementNotExists(name, model.getName());
        	node.addParentNode(parentNode);
		}
		
		// Register node
		model.addOrReplaceNode(node);
		nodes.computeIfAbsent(model, k -> new HashSet<INode>()).remove(node);
		nodes.get(model).add(node);
		
		return node;
	}

	
	public NodeVariable addNodeVariable(String nodeName, String typeName,int typeReversePotency, Set<String> parentNodeNames, Set<INode> suppTypes, IModel iModel) throws MultEcoreException {
		int modelLevel = iModel.getLevel();

		IModel metamodel = null;
		if (typeName.equals(no.hvl.multecore.common.Constants.ECLASS_ID)) {
			metamodel = models.get(no.hvl.multecore.common.Constants.ECORE_ID);
			typeReversePotency = iModel.getLevel();
		} 
		else {
			metamodel = iModel.getMetamodelInLevel(modelLevel - typeReversePotency);
		}
		
		// Check if metamodel is valid
		if (null == metamodel)
			throw new BadlyFormattedElement(nodeName, typeName, iModel.getName());

		// Check if type is valid
		INode typeNode = metamodel.getNode(typeName);
		if ((null == typeNode) || (!typeNode.getPotency().compatibleWithReversePotency(typeReversePotency)))
			throw new BadlyFormattedElement(nodeName, typeName, iModel.getName());
		
		return addNodeVariable(nodeName, typeNode, parentNodeNames, suppTypes, iModel);
	}


	public NodeVariable addNodeVariable(String nodeName, INode typeNode, Set<String> parentNodeNames, Set<INode> suppTypes, IModel iModel) throws DuplicatedElement, ElementNotExists {
		// Create node
		Potency potency = new Potency(no.hvl.multecore.common.Constants.START_POTENCY_DEFAULT_VALUE, no.hvl.multecore.common.Constants.END_POTENCY_DEFAULT_VALUE, no.hvl.multecore.common.Constants.DEPTH_POTENCY_DEFAULT_VALUE);
		NodeVariable node = new NodeVariable(nodeName, typeNode, iModel, potency);
		if (null != suppTypes && !suppTypes.isEmpty()) {
			for (INode suppType : suppTypes) {
				try {
					((RuleHierarchy) iModel.getHierarchy()).addSupplementaryNode((INode) node, suppType.getModel().getName(), suppType.getName(), suppType.getModel().getHierarchy());
				} catch (MultEcoreException e) {
					e.createNotificationDialog();
				}
			}
		}
		if (iModel.getNodes().contains(node))
			throw new DuplicatedElement(nodeName, iModel.getName());

		// Add parent nodes
		for (String parentName : parentNodeNames) {
        	Node parentNode = (Node) iModel.getNode(parentName);
        	if (null == parentNode)
        		throw new ElementNotExists(name, iModel.getName());
        	node.addParentNode(parentNode);
		}
		
		// Register node
		RuleModel model = (RuleModel) iModel;
		model.addOrReplaceNode(node);
		nodes.computeIfAbsent(iModel, k -> new HashSet<INode>()).remove(node);
		nodes.get(iModel).add(node);
		
		return node;
	}

	public void addMetaDeclaredAttribute (String nodeName, String attributeName, String type, IModel iModel) {
		RuleModel model = (RuleModel) iModel;
		Node node = (Node) model.getNode(nodeName); 
		if (node != null) {
			NativeType nt = null;
			switch(type) {
			case "INTEGER" :
				nt = NativeType.Integer;
				break;	   
			case "STRING" :
				nt = NativeType.String;
				break;
			case "BOOLEAN" :
				nt = NativeType.Boolean;
				break;
			case "REAL" :
				nt = NativeType.Real;
				break;			  
			}
			RuleDeclaredAttribute attribute = new RuleDeclaredAttribute(attributeName, nt, node, new Potency(1, no.hvl.multecore.common.Constants.UNBOUNDED, 1));
			node.addAttribute(attribute);
		}
	}
	
	public void addVariableInstantiatedAttribute (String nodeName, String attributeTypeName, String attributeExpression, IModel iModel) throws MultEcoreException {
		RuleModel model = (RuleModel) iModel;
		Node node = (Node) model.getNode(nodeName);
		DeclaredAttribute attributeType = node.getType().getDeclaredAttributePlusInherited(attributeTypeName, true);
		if (null == attributeType) {
			// Check if the attribute is declared in a supplementary type node
			for (INode supplementaryTypeNode : node.getSupplementaryTypes()) {
				attributeType = supplementaryTypeNode.getDeclaredAttributePlusInherited(attributeTypeName, true);
				if (null != attributeType)
					break;
			}
		}
		if (null == attributeType) {
			throw new BadlyFormattedElement(attributeExpression, attributeTypeName, model.getName());
		}
		RuleInstantiatedAttribute attribute = new RuleInstantiatedAttribute(attributeExpression, attributeType, node);
		//TODO Check whether it makes sense to add the attribute with the hash
		try {
			AttributesParser.checkIsValidExpression(attribute);
		} catch (InvalidAttributeExpression | VariableAlreadyDeclared e ) {
			e.createNotificationDialog(); 
		}
		node.addAttribute(attribute);
	}	
	
	public Edge addEdgeConstant(String edgeName, String sourceName, String multiplicityVariable, IModel iModel) throws MultEcoreException {
		RuleModel model = (RuleModel) iModel;
		INode sourceNode = null;
		
		// Check if source is valid and add it if missing
		if (null == (sourceNode = model.getNode(sourceName)))
			sourceNode = addNodeConstant(sourceName, new HashSet<String>(), iModel);
		
		// Create and register edge
		EdgeConstant edge = new EdgeConstant(edgeName, sourceNode, model);
		edge.setMultiplicityVariable(multiplicityVariable);
		model.addOrReplaceEdge(edge);
		edges.computeIfAbsent(model, k -> new HashSet<IEdge>()).add(edge);
		return edge;
	}
	
	
	public EdgeVariable addEdgeVariable(String edgeName, String typeName, String sourceName, String sourceTypeName, String targetName, String targetTypeName, int typeReversePotency, String multiplicityVariable, Set<IEdge> suppTypes, IModel iModel) throws MultEcoreException {
		INode sourceNode = null, targetNode = null;
		if (null == (sourceNode = iModel.getNode(sourceName)))
			throw new DanglingEdge(edgeName, sourceName, targetName, iModel.getName());
		if (null == (targetNode = iModel.getNode(targetName)))
			throw new DanglingEdge(edgeName, sourceName, targetName, iModel.getName());
		if (null != iModel.getEdgePlusInherited(edgeName, sourceName, targetName))
			throw new DuplicatedElement(edgeName, iModel.getName());
			
		// Find metamodel and type
		IModel metamodel = iModel.getMetamodelInLevel(iModel.getLevel() - typeReversePotency);
		INode sourceType = getTypeInModel(sourceNode, metamodel);
		if (null == sourceType)
			throw new BadlyFormattedElement(edgeName, typeName, iModel.getName());
		
		INode targetType = getTypeInModel(targetNode, metamodel);
		if (null == targetType)
			throw new BadlyFormattedElement(edgeName, typeName, iModel.getName());		
		
		IEdge typeEdge =  metamodel.getEdgePlusInherited(typeName, sourceType.getName(), targetType.getName());
		if (null == typeEdge){
			typeEdge = ((RuleModel) metamodel).getConstantEdgePlusInherited(typeName, sourceType.getName());
		}
		// Check if type is valid
		if (null == typeEdge)
			throw new BadlyFormattedElement(edgeName, typeName, iModel.getName());
			
		return addEdgeVariable(edgeName, typeEdge, sourceNode, targetNode, multiplicityVariable, suppTypes, iModel);
	}


	public EdgeVariable addEdgeVariable(String edgeName, IEdge typeEdge, INode sourceNode, INode targetNode, String multiplicityVariable, Set<IEdge> suppTypes, IModel iModel) {
		// Create and register edge
		Potency potency = new Potency(no.hvl.multecore.common.Constants.START_POTENCY_DEFAULT_VALUE, no.hvl.multecore.common.Constants.END_POTENCY_DEFAULT_VALUE, no.hvl.multecore.common.Constants.DEPTH_POTENCY_DEFAULT_VALUE);
		EdgeVariable edge = new EdgeVariable(edgeName, typeEdge, sourceNode, targetNode, iModel, potency); //TODO Add real depth later
		edge.setMultiplicityVariable(multiplicityVariable);
		RuleModel model = (RuleModel) iModel;
		
		if (null != suppTypes && !suppTypes.isEmpty()) {
			for (IEdge suppType : suppTypes) {
				try {
					((RuleHierarchy) iModel.getHierarchy()).addSupplementaryEdge((IEdge) edge, suppType.getModel().getName(), suppType.getName(), suppType.getModel().getHierarchy());
				} catch (MultEcoreException e) {
					e.printStackTrace();
				}
			}
		}		
		
		model.addOrReplaceEdge(edge);
		edges.computeIfAbsent(iModel, k -> new HashSet<IEdge>()).add(edge);
		edges.get(iModel).add(edge);
		return edge;
	}
	
	
	public IModel getMetaModelInLevel(int level) {
		for (IModel iModel : models.values()) {
			if (iModel.getLevel() == level)
				return iModel;
		}
		return null;
	}


	public IModel getFromModel() {
		return models.get(Constants.FROM_BLOCK_ID);
	}


	public IModel getToModel() {
		return models.get(Constants.TO_BLOCK_ID);
	}
	
	public Box addBox (String multiplicityVariable, IModel fromModel) {
		Box box = new Box(multiplicityVariable);
		((RuleModel) fromModel).addBox(box);
		return box;
	}
	
	public Condition addCondition (String conditionExpression, IModel fromModel) {
			Condition condition = new Condition(conditionExpression);
			((RuleModel) fromModel).addCondition(condition);
			return condition;
	}
	
	public void addParentToNode (INode child, INode parent, IModel model) {
		model.getNode(child.getName()).getAllParentNodes().add(parent);
	}
	

	//TODO Temporal solution as the method should be modified in MultilevelHierarchy.java passing also the supplementary hierarchy name as a parameter
	public void addSupplementaryNode (INode applicationINode, String supplementaryModelName, String supplementaryNodeName, MultilevelHierarchy supplementaryHierarchy) throws MultEcoreException {
		// Check if application node is valid
		if (!(applicationINode instanceof Node))
			throw new InvalidSupplementaryElement("node", supplementaryNodeName, applicationINode.getName());

		// Check if supplementary model is valid
		IModel supplementaryModel = applicationINode.getModel().getSupplementaryModelForHierarchy(supplementaryHierarchy);
		if (null == supplementaryModel)
			throw new InvalidSupplementaryElement("model", supplementaryModelName, applicationINode.getModel().getName());
		
		//Check if the supplementary model is a metamodel of the one assigned as supplementary
		if (!supplementaryModel.getName().equals(supplementaryModelName)) {
			for (IModel metaModel : supplementaryModel.getAllMetamodels()) {
				if (metaModel.getName().equals(supplementaryModelName)) {
					supplementaryModel = metaModel;
					break;
				}
			}
		}
		
		if (!supplementaryModel.getName().equals(supplementaryModelName))
			throw new InvalidSupplementaryElement("model", supplementaryModelName, applicationINode.getModel().getName());		
		
		// Check if supplementary node is valid
		INode supplementaryNode = supplementaryModel.getNode(supplementaryNodeName);
		List<String> primitiveTypes = new ArrayList<String>(Arrays.asList(MultEcoreManager.instance().getPrimitiveTypesNames()));
		if (null == supplementaryNode && !primitiveTypes.contains(supplementaryNodeName))
			throw new InvalidSupplementaryElement("node", supplementaryNodeName, applicationINode.getName());

		Node applicationNode = (Node) applicationINode;
		applicationNode.addSupplementaryNode(supplementaryNode);
	}
	
	//TODO Temporal solution as the method should be modified in MultilevelHierarchy.java passing also the supplementary hierarchy name as a parameter
	public void addSupplementaryEdge (IEdge applicationIEdge, String supplementaryModelName, String supplementaryEdgeName, MultilevelHierarchy supplementaryHierarchy) throws MultEcoreException {
		// Check if application edge is valid
		if (!(applicationIEdge instanceof Edge))
			throw new InvalidSupplementaryElement("edge", supplementaryEdgeName, applicationIEdge.getName());

		// Check if supplementary model is valid
		RuleModel supplementaryModel = (RuleModel) applicationIEdge.getModel().getSupplementaryModelForHierarchy(supplementaryHierarchy);
		if (null == supplementaryModel)
			throw new InvalidSupplementaryElement("model", supplementaryModelName, applicationIEdge.getModel().getName());
		
		//Check if the supplementary model is a metamodel of the one assigned as supplementary
		if (!supplementaryModel.getName().equals(supplementaryModelName)) {
			for (IModel metaModel : supplementaryModel.getAllMetamodels()) {
				if (metaModel.getName().equals(supplementaryModelName)) {
					supplementaryModel = (RuleModel) metaModel;
					break;
				}
			}
		}
		
		if (!supplementaryModel.getName().equals(supplementaryModelName))
			throw new InvalidSupplementaryElement("model", supplementaryModelName, applicationIEdge.getModel().getName());
		
		// Check if supplementary edge is valid
		String supplementaryEdgeSourceName = applicationIEdge.getSource().getSupplementaryTypeInModel(supplementaryModel, true).getName();
		String supplementaryEdgeTargetName = applicationIEdge.getTarget().getSupplementaryTypeInModel(supplementaryModel, true).getName();
		IEdge supplementaryEdge = supplementaryModel.getConstantEdge(supplementaryEdgeName, supplementaryEdgeSourceName);
		
		List<String> primitiveTypes = new ArrayList<String>(Arrays.asList(MultEcoreManager.instance().getPrimitiveTypesNames()));
		if (null == supplementaryEdge && !primitiveTypes.contains(supplementaryEdgeName))
			throw new InvalidSupplementaryElement("edge", supplementaryEdgeName, applicationIEdge.getName());

		Edge applicationEdge = (Edge) applicationIEdge;
		applicationEdge.addSupplementaryEdge(supplementaryEdge);
	}
	
	
	
	
	
}
