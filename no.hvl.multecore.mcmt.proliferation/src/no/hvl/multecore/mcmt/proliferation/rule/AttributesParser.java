package no.hvl.multecore.mcmt.proliferation.rule;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.regex.Pattern;

import no.hvl.multecore.mcmt.exceptions.InvalidAttributeExpression;
import no.hvl.multecore.mcmt.exceptions.VariableAlreadyDeclared;
import no.hvl.multecore.mcmt.proliferation.Constants;
import no.hvl.multecore.mcmt.proliferation.Utils;

public class AttributesParser {

	
	public static final boolean checkIsValidExpression (RuleInstantiatedAttribute ruleInstantiatedAttribute) throws InvalidAttributeExpression, VariableAlreadyDeclared {
		
		//TODO For the moment we consider # as the delimiter of the expression
		//remove the delimiters
		String cleanedExpression = ruleInstantiatedAttribute.getNameOrValue().substring(1, ruleInstantiatedAttribute.getNameOrValue().length()-1);
		String[] splittedExpression;
		Boolean isValidExpression = true;
		
		
		//We need to check if the expression is just the variable, if we are using the same variable for different scopes
		//Indirectly we could say that if the "if" is false, we are in the LHS declaring a new variable.
    	RuleModel ruleModel = (RuleModel) ruleInstantiatedAttribute.getContainingNode().getModel();
		if (ruleModel.variableAlreadyUsedInAttribute(cleanedExpression.trim())) {
			throw new VariableAlreadyDeclared(cleanedExpression.trim());
		}
		
		switch (ruleInstantiatedAttribute.getType().getType().getNameOrValue()) {
		case ("Integer"):
			//TODO Alex: Temporal workaround to give Maude prefix when "-" operator
			if (!cleanedExpression.startsWith("_-_")) {
			splittedExpression = cleanedExpression.split(Utils.tokenizeIntegerOperators(Constants.IntegerOperators));
			//See if every splitted element is either an integer or a variable that could be an integer 
				for (int i = 0; i < splittedExpression.length; i++) {
					//removing white spaces at beginning and end
					splittedExpression[i] = splittedExpression[i].trim();
					if (Pattern.matches(Constants.variableRegexExpression, splittedExpression[i]) == false){
					    try {
					    	Integer.parseInt(splittedExpression[i]);
					    } catch (NumberFormatException | NullPointerException nfe) {
					        isValidExpression = false;
					    }
					}
					else {
					    if (Utils.containsStopWord(Constants.IntegerStopWords, splittedExpression[i])) {
					    	isValidExpression= false;
					    }
					}
				}
			}
			if (!isValidExpression) {
				throw new InvalidAttributeExpression(cleanedExpression,"Integer");
			}
			break;
		case ("String"):
			splittedExpression = cleanedExpression.split(Utils.tokenizeIntegerOperators(Constants.StringOperators));
			for (int i = 0; i < splittedExpression.length; i++) {
				//removing white spaces at beginning and end
				splittedExpression[i] = splittedExpression[i].trim();
				if (Pattern.matches(Constants.variableRegexExpression, splittedExpression[i]) == false){
					if (splittedExpression[i].startsWith("\"") && splittedExpression[i].endsWith("\"")
							&& splittedExpression[i].length() - splittedExpression[i].replace("\"", "").length() == 2) {
					}
					else {
						isValidExpression = false;
					}
				}
			}
			if (!isValidExpression) {
				throw new InvalidAttributeExpression(cleanedExpression,"String");
			}	
			break;
		
		case ("Boolean"):
			splittedExpression = cleanedExpression.split(Utils.tokenizeBooleanOperators(Constants.BooleanOperators));
			for (int i = 0; i < splittedExpression.length; i++) {
				// removing white spaces at beginning and end
				splittedExpression[i] = splittedExpression[i].trim();
				if (Pattern.matches(Constants.variableRegexExpression, splittedExpression[i]) == false) {
					if (!splittedExpression[i].equals("true") && !splittedExpression[i].equals("false")) {
						isValidExpression = false;
					}
				} else {
					if (!splittedExpression[i].equals("true") && !splittedExpression[i].equals("false")) {
						ruleInstantiatedAttribute.setAssignedVariable(splittedExpression[i]);
					}
				}
			}
			if (!isValidExpression) {
				throw new InvalidAttributeExpression(cleanedExpression, "Boolean");
			}
			break;

		case ("Real"):
			splittedExpression = cleanedExpression.split(Utils.tokenizeIntegerOperators(Constants.RealOperators));
			//See if every splitted element is either an integer or a variable that could be an integer 
			for (int i = 0; i < splittedExpression.length; i++) {
				//removing white spaces at beginning and end
				splittedExpression[i] = splittedExpression[i].trim();
				if (Pattern.matches(Constants.variableRegexExpression, splittedExpression[i]) == false){
				    try {
				    	Float.parseFloat(splittedExpression[i]);
				    } catch (NumberFormatException | NullPointerException nfe) {
				        isValidExpression = false;
				    }
				}
				else {
				    if (Utils.containsStopWord(Constants.IntegerStopWords, splittedExpression[i])) {
				    	isValidExpression= false;
				    }
				}
			}
			if (!isValidExpression) {
				throw new InvalidAttributeExpression(cleanedExpression,"Real");
			}
			break;
		
		}
		return isValidExpression;
	}
	
	public static final RuleInstantiatedAttribute parseVariableExpression (RuleInstantiatedAttribute ruleInstantiatedAttribute){
		
		//TODO For the moment we consider # as the delimiter of the expression
		//remove the delimiters. Eg: #n +4 - 3# = n +4 - 3
		String cleanedExpression = ruleInstantiatedAttribute.getNameOrValue().substring(1, ruleInstantiatedAttribute.getNameOrValue().length()-1);
		
		//Remove all white spaces. = n+4-3
		//TODO Alex: Temporal workaround to give Maude prefix when "-" operator
		String parsedVariableExpression ="";
		if (!cleanedExpression.startsWith("_-_")) {
			cleanedExpression = cleanedExpression.replaceAll("\\s+","");
			
			
			List<String> positionOperator;
			String[] splittedExpression;
			switch (ruleInstantiatedAttribute.getType().getType().getNameOrValue()) {
			case ("Integer"):
				//Find and store operators and order. The key stores the position, the String the operator.
				positionOperator = Utils.positionSymbolCalculator(Constants.IntegerOperators, cleanedExpression);
	
				splittedExpression = cleanedExpression.split(Utils.tokenizeIntegerOperators(Constants.IntegerOperators));
				//See if every splitted element is either an integer or a variable that could be an integer 
				
				for (int i = 0; i < splittedExpression.length; i++) {
					//removing white spaces at beginning and end
					splittedExpression[i] = splittedExpression[i].trim();
					if (Pattern.matches(Constants.variableRegexExpression, splittedExpression[i]) == true){
						//TODO Check this, creating a variable name which is the variable itself + the name of the type, to make it less generic
						parsedVariableExpression += " " +splittedExpression[i];
						ruleInstantiatedAttribute.addVariable(splittedExpression[i], ruleInstantiatedAttribute.getType().getType().getNameOrValue());
					}
					else { 
						parsedVariableExpression += " " +splittedExpression[i];
					}
					if (!positionOperator.isEmpty()) {
						parsedVariableExpression += " " +positionOperator.get(0);
						positionOperator.remove(0);
					}
				}
				break;
			case ("String"):
				positionOperator = Utils.positionSymbolCalculator(Constants.StringOperators, cleanedExpression);
				splittedExpression = cleanedExpression.split(Utils.tokenizeIntegerOperators(Constants.StringOperators));
				for (int i = 0; i < splittedExpression.length; i++) {
					//removing white spaces at beginning and end
					splittedExpression[i] = splittedExpression[i].trim();
					if (Pattern.matches(Constants.variableRegexExpression, splittedExpression[i]) == true){
						//TODO Check this, creating a variable name which is the variable itself + the name of the type, to make it less generic
						parsedVariableExpression += " " +splittedExpression[i];
						ruleInstantiatedAttribute.addVariable(splittedExpression[i], ruleInstantiatedAttribute.getType().getType().getNameOrValue());				}
					else { 
						parsedVariableExpression += " " +splittedExpression[i];
					}
					if (!positionOperator.isEmpty()) {
						parsedVariableExpression += " " +positionOperator.get(0);
						positionOperator.remove(0);
					}
				}
				
				break;
			
			case ("Boolean"):
				//First step, just considering true and false
				if (Pattern.matches(Constants.variableRegexExpression, cleanedExpression) == true && !Utils.containsStopWord(Constants.IntegerStopWords, cleanedExpression)){
					//TODO Check this, creating a variable name which is the variable itself + the name of the type, to make it less generic
					parsedVariableExpression += " " + cleanedExpression;
					ruleInstantiatedAttribute.addVariable(cleanedExpression, ruleInstantiatedAttribute.getType().getType().getNameOrValue());			}
				else { 
					parsedVariableExpression += cleanedExpression;
				}
				break;
			
			case ("Real"):
				//This is going to be Float in Maude
					//Find and store operators and order. The key stores the position, the String the operator.
					positionOperator = Utils.positionSymbolCalculator(Constants.RealOperators, cleanedExpression);
			
					splittedExpression = cleanedExpression.split(Utils.tokenizeIntegerOperators(Constants.RealOperators));
					//See if every splitted element is either an integer or a variable that could be an integer 
					
					for (int i = 0; i < splittedExpression.length; i++) {
						//removing white spaces at beginning and end
						splittedExpression[i] = splittedExpression[i].trim();
						if (Pattern.matches(Constants.variableRegexExpression, splittedExpression[i]) == true){
							//TODO Check this, creating a variable name which is the variable itself + the name of the type, to make it less generic
							parsedVariableExpression += " " +splittedExpression[i];
							ruleInstantiatedAttribute.addVariable(splittedExpression[i], ruleInstantiatedAttribute.getType().getType().getNameOrValue());					}
						else {
							//We need to give a valid float format
							if (splittedExpression[i].contains(".")) {
								Float number = Float.parseFloat(splittedExpression[i]);
								String value = String.format("%02f", number);
								parsedVariableExpression += " " + value.replace(",", ".");
							}
							else {
								DecimalFormat df = new DecimalFormat("#.00");
								String value = df.format(Integer.parseInt(splittedExpression[i]));
								parsedVariableExpression += " " + value.replace(",", ".");
							}
						}
						if (!positionOperator.isEmpty()) {
							parsedVariableExpression += " " +positionOperator.get(0);
							positionOperator.remove(0);
						}
					}		
				break;
			}
		}
		else {
			parsedVariableExpression = cleanedExpression;
		}
		ruleInstantiatedAttribute.setParsedExpression(parsedVariableExpression);
		return ruleInstantiatedAttribute;
	}	

	
}
