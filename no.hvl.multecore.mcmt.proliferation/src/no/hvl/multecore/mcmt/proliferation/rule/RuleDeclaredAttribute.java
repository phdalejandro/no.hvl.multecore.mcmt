package no.hvl.multecore.mcmt.proliferation.rule;

import no.hvl.multecore.common.hierarchy.DeclaredAttribute;
import no.hvl.multecore.common.hierarchy.IAttribute;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.common.hierarchy.Potency;

public class RuleDeclaredAttribute extends DeclaredAttribute{

	public RuleDeclaredAttribute(String name, IAttribute type, INode containingNode) {
		super(name, type, containingNode);
	}
	
	public RuleDeclaredAttribute(String name, IAttribute type, INode containingNode, Potency potency) {
		super(name, type, containingNode, potency);
	}	
}
